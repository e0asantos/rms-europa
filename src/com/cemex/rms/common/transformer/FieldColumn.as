package com.cemex.rms.common.transformer
{
	import mx.core.ClassFactory;

	public class FieldColumn
	{
		public function FieldColumn(name:String,orderField:Function,text:String,width:int,renderer:*=null,headerRenderer:*=null) {
			
			
			if (renderer != null){
				if (renderer is Class){
					this.renderer = new ClassFactory(renderer as Class);
				}
				else if (renderer is ClassFactory){
					this.renderer =  renderer as ClassFactory;
				}
				else {
					throw new Error("The Renderer is not a valid DataType");
				}
			}
			if (headerRenderer != null){
				if (headerRenderer is Class){
					this.headerRenderer = new ClassFactory(headerRenderer as Class);
				}
				else if (headerRenderer is ClassFactory){
					this.headerRenderer =  renderer as ClassFactory;
				}
				else {
					throw new Error("The Renderer is not a valid DataType");
				}
			}
			
			this.orderField = orderField;
			
			this.name = name;
			this.text =  text;
			this.width = width;
			//this.renderer =  renderer;
		}
		
		public var name:String;
		public var orderField:Function;
		public var text:String;
		public var width:int;
		public var renderer:ClassFactory;
		public var headerRenderer:ClassFactory;
		
		
	}
}