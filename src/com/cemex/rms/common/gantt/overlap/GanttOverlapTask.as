package com.cemex.rms.common.gantt.overlap
{
	import com.cemex.rms.common.date.FlexDateHelper;
	import com.cemex.rms.common.gantt.GanttTask;
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.core.UIComponent;
	import mx.utils.ArrayUtil;

	public class GanttOverlapTask extends GanttTask
	{
		public function GanttOverlapTask()
		{
			super();
		}
		
		[Bindable]
		public var colorsConfiguration:Array=[];
		[Bindable]
		public var fillColors:Array=[];
		[Bindable]
		public var fillAlphas:Array=[];
		[Bindable]
		public var gradientRatio:Array=[];
		
		
		public override function initExtraValues():void {
			super.initExtraValues();
			
			
			if (data == null || getContainer() == null){
				return ;
			}
			
			
			/*var overlap:OverLapDescription = getOverLaps();
			
			if (overlap != null && overlap.overlapType != OverLapDescription.OVERLAP_TYPE_OFF){
				gradientRatio = getFieldArray(overlap.points, "gradientRatio");
				fillAlphas = getFieldArray(overlap.points, "fillAlphas");
				fillColors = getFieldArray(overlap.points, "fillColors");
				colorsConfiguration = [overlap.points.length];
			}
			else {
				gradientRatio = [0, 255];
				fillAlphas =  [0.00,0.00];
				fillColors = [0xFF0000,0xFF0000];
				colorsConfiguration = [2];
			}*/
		}
		
		
		
		public function getFieldArray(points:Array,fieldName:String):Array{
			var result:Array =  new Array();
			for (var i:int = 0  ; i < points.length ; i ++){
				var temp:OverlapPoint = points[i] as OverlapPoint;
				result.push(temp[fieldName]);
			}
			return result;
		}
	
		
		public function getView():UIComponent{
			
			return null;
		}
		
		private var logger:ILogger = LoggerFactory.getLogger("GanttOverlapTask");
		public function getOverLaps():OverLapDescription {
			
			var result:OverLapDescription = new OverLapDescription();
			result.overlapType = OverLapDescription.OVERLAP_TYPE_MULTI;
			
			var container:ArrayCollection = getContainer();
			
			var points:Array =  new Array();
			var temp:GanttTask;
			var i:int ;
			//logger.setLoggerOff();
			logger.debug("GanttOverlapTask:getOverLaps");
			for (i  = 0 ; i < container.length ; i ++ ) {
				
				temp = container.getItemAt(i) as GanttTask;
				logger.debug("container:"+temp.data.itemNumber+"->"
					+ FlexDateHelper.getTimestampString(temp.startTime)+FlexDateHelper.getTimestampString(temp.endTime)  );
			}
			for (i  = 0 ; i < container.length ; i ++ ) {
				
				temp = container.getItemAt(i) as GanttTask;
				logger.debug("start:" + this.data.itemNumber + "overlap" + temp.data.itemNumber );
				var overlap:OverLapDescription = getOverlap(this,temp);
				logger.debug("validation:" + this.data.itemNumber + "overlap" + temp.data.itemNumber + "=>"+ overlap.overlapType);
				if (overlap.overlapType != OverLapDescription.OVERLAP_TYPE_OFF) {
					//
					
					for (var j:int = 0 ; j <  overlap.points.length ; j ++ ){
						
						var point:OverlapPoint = overlap.points[j] as OverlapPoint;
						logger.debug("points.push:" + point.overlapType  + "::"  );
						points.push(point);
					}
				}
			}
			
			if (points.length > 0){
				try {
					points = points.sortOn(["gradientRatio","index"],Array.NUMERIC );
				}
				catch (e:*){
					Alert.show(""+e);
				}
				
				
				result.overlapType = OverLapDescription.OVERLAP_TYPE_MULTI;
				rawPoints = points;
				result.points = removeNoisePoints(points);
				return result;
			}
			else {
				return null
			}
			
		}
		
		public var rawPoints:Array;
		
		private var overlapColor:Number = 0xFF0000;
		private var overlapCenterColor:Number = 0x990000;
	
	
		public function removeNoisePoints(points:Array):Array{
			var result:Array =  new Array();
			var isRight:Boolean = false;
			
			var isOverLap:Boolean = false;
			for (var i:int  = 0 ; i < points.length/2; i ++ ) {
				// Se obtienen los dos puntos porque desde un inicio estan en orden
				var p1:OverlapPoint = points[( i * 2 ) + 0] as OverlapPoint;
				var p2:OverlapPoint = points[( i * 2 ) + 1] as OverlapPoint;
				
				if (p1.overlapType == OverLapDescription.OVERLAP_TYPE_ALL){
					result =  new Array();
					result.push(getOverLapPoint(0, overlapColor,0.8,OverLapDescription.OVERLAP_TYPE_ALL));
					result.push(getOverLapPoint(255, overlapColor,0.8,OverLapDescription.OVERLAP_TYPE_ALL));
					//break;
				}
				else if (p1.overlapType == OverLapDescription.OVERLAP_TYPE_RIGHT){
					if (!isRight){
						result.push(p1);
						//result.push(points[points.length - 1]);
						p2 = getOverLapPoint(255, overlapColor,0.8,OverLapDescription.OVERLAP_TYPE_RIGHT);
						result.push(p2);
					}
					 isRight = true;
				}
				else if (p1.overlapType == OverLapDescription.OVERLAP_TYPE_LEFT){
					
					if (isRight){
							
							result =  new Array();
							
							p1 = getOverLapPoint(0, overlapColor,0.8,OverLapDescription.OVERLAP_TYPE_ALL);
							p2 = getOverLapPoint(255, overlapColor,0.8,OverLapDescription.OVERLAP_TYPE_ALL);
							result.push(p1);
							result.push(p2);
							
							break;
					}
					else {
						result =  new Array();
						//result.push(points[0]);
						p1 = getOverLapPoint(0, 	overlapColor,0.8,OverLapDescription.OVERLAP_TYPE_LEFT);
						
						result.push(p1);
						result.push(p2);
					}
				}
				else if (p1.overlapType == OverLapDescription.OVERLAP_TYPE_CENTER){
					if(!isRight){
						result.push(p1);
						result.push(p2);
					}
				}
			}
			logger.debug(ReflectionHelper.object2XML(result));
				
			return result;
		}
		public function sortPoints(points:Array):void{
			
			for (var pass:int  = 1 ; pass < points.length/2 ; pass ++ ) {
				for (var j:int  = 0 ; j < points.length/2 - pass ; j ++ ) {
					
					var p1:OverlapPoint = points[(2*j)] as OverlapPoint;
					var p2:OverlapPoint = points[(2*j)+1] as OverlapPoint;

					var q1:OverlapPoint = points[(j + 1) * 2] as OverlapPoint;
					var q2:OverlapPoint = points[(j + 1) * 2 + 1] as OverlapPoint;
					
					if (p1.gradientRatio > q1.gradientRatio){
						var temp1:OverlapPoint = p1;
						var temp2:OverlapPoint = p2;
						points[ j*2 ] = q1;
						points[ (j*2)+1 ] = q2;
						points[ (j + 1)*2 ] = temp1;
						points[ (j + 1)*2 +1] = temp2;
						
					}
				}
			}
		}
		
		public function getCoordinate(date:Date):Number{
			return 0;
		}
		public function getFirstOverLap():OverLapDescription {
			
			var result:Array = new Array();
			var container:ArrayCollection = getContainer();
			for (var i:int  = 0 ; i < container.length ; i ++ ) {
				
				var temp:GanttTask = container.getItemAt(i) as GanttTask;
				var overlap:OverLapDescription = getOverlap(this,temp);
				if (overlap.overlapType != OverLapDescription.OVERLAP_TYPE_OFF) {
					return overlap;
				}
			}
			return null;
		}
		
		private function getOverlap(task:GanttTask, other:GanttTask):OverLapDescription{
			var overlap:OverLapDescription = new OverLapDescription();
			var p:OverlapPoint;
			var view:UIComponent = getView();
			
			if (task != other){
				if(task.startTime!=null && task.endTime!=null){
				var validation:Boolean = (task.startTime.time <= other.endTime.time)
            						  && (task.endTime.time   >= other.startTime.time);
				if (validation){
					// Ahora necesitamos saber que tipo de overlap existe
					var taskTime:Number = task.endTime.time - task.startTime.time;
					var temp:int;
					if (task.startTime.time  >= other.startTime.time) { 
						
						if (task.endTime.time  > other.endTime.time){
							// LEFT
							
							overlap.overlapType = OverLapDescription.OVERLAP_TYPE_LEFT;
							//logger.info("LEFT(o.s:"+other.startTime.time+", o.e:"+other.endTime.time+" )"+
							//			",t.s:"+task.startTime.time+", t.e:"+task.endTime.time+" )");
							
								var overTimeL:Number = other.endTime.time - task.startTime.time;	
								temp = (overTimeL / taskTime) * 255;
							
							p = getOverLapPoint(temp - 1, overlapColor,0.8,OverLapDescription.OVERLAP_TYPE_LEFT);
							p.index = 0;
							overlap.points.push(p);
							
							p = getOverLapPoint(temp, 0,0.00,OverLapDescription.OVERLAP_TYPE_LEFT);
							p.index = 1;
							overlap.points.push(p);	
							
						}
						else {
							//ALL
							overlap.overlapType = OverLapDescription.OVERLAP_TYPE_ALL;
							
							
							p = getOverLapPoint(0, overlapColor,0.8,OverLapDescription.OVERLAP_TYPE_ALL);
							p.index = 0;
							overlap.points.push(p);
							p = getOverLapPoint(255, overlapColor,0.8,OverLapDescription.OVERLAP_TYPE_ALL);
							p.index = 1;
							overlap.points.push(p);
						}
					}
					else {
						if (task.endTime.time  > other.endTime.time){
							// CENTER
							var overStart:Number = other.startTime.time - task.startTime.time;
							var overEnd:Number   = other.endTime.time - task.startTime.time;
							
							overlap.overlapType = OverLapDescription.OVERLAP_TYPE_CENTER;
							
							temp = (overStart / taskTime) * 255;
							
							
							p = getOverLapPoint(temp - 1, 0,0.00,OverLapDescription.OVERLAP_TYPE_CENTER);
							p.index = 0;
							overlap.points.push(p);
							
							
							p = getOverLapPoint(temp, overlapColor,0.8,OverLapDescription.OVERLAP_TYPE_CENTER);
							p.index = 1;
							overlap.points.push(p);	
							
							temp = (overEnd / taskTime) * 255;
							
							p = getOverLapPoint(temp - 1, overlapColor,0.8,OverLapDescription.OVERLAP_TYPE_CENTER);
							p.index = 0;
							overlap.points.push(p);
							
							p = getOverLapPoint(temp, 0,0.00,OverLapDescription.OVERLAP_TYPE_CENTER);
							p.index = 1;
							overlap.points.push(p);	
							
							
						}
						else {
							// RIGHT
							var overTimeR:Number = task.endTime.time - other.startTime.time;
							overlap.overlapType = OverLapDescription.OVERLAP_TYPE_RIGHT;
							temp = 255 - (overTimeR / taskTime) * 254;
							
							p = getOverLapPoint(temp - 1, 0, 0.00,OverLapDescription.OVERLAP_TYPE_RIGHT);
							p.index = 0;
							overlap.points.push(p);
							
							
							p = getOverLapPoint(temp , overlapColor,0.8,OverLapDescription.OVERLAP_TYPE_RIGHT);
							p.index = 1;
							overlap.points.push(p);
						}
					}
				}			
				else {
					logger.debug("task("+task.data.itemNumber+") != other("+other.data.itemNumber+")");
					overlap.overlapType = OverLapDescription.OVERLAP_TYPE_OFF;
				}
			}
			else {
				logger.debug("task("+task.data.itemNumber+") != other("+other.data.itemNumber+")");
				overlap.overlapType = OverLapDescription.OVERLAP_TYPE_OFF;
			}
			}
			
			//logger.info(overlap.overlapType + "(p:"+overlap.p +", q:" + overlap.q+")");
			return overlap;
		}
		
		public function getOverLapPoint(ratio:int, colors:Number,alpha:Number,desc:String ):OverlapPoint{
			var p:OverlapPoint =  new OverlapPoint();
			p.gradientRatio = ratio;
			p.fillColors = colors; 
			p.fillAlphas = alpha;
			p.overlapType = desc;
			return p;
		}
		
		
		
		
		
		
		
		
		
	
	}
}