package com.cemex.rms.common.gantt
{
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.tree.NodeTask;
	import com.cemex.rms.dispatcher.helpers.TooltipHelper;
	
	import ilog.gantt.TaskItem;
	
	import mx.collections.ArrayCollection;

	/**
	 * Esta es la clase es la implementacion del GanttTree, pero ya mas especializada
	 * Esta implementacion ya tiene validaciones de los Fantasmas y unas cuantas etiquetas, pero esta clase debe sobre escribirse
	 * algunos metodos para el render especifico, porque el starttime y endTime depende de la vista es el valor que se necesita desplegar, 
	 * Esta clase esta asociada a muchas validaciones de Movimientos Permitidos, y de que forma de lanzan los eventos   
	 * 
	 */ 
	public class GanttTask extends NodeTask
	{
		public function GanttTask()
		{
			super();
			
		}
		
		private var taskItem:TaskItem;
		public function setTaskItem(taskItem:TaskItem):void{
			this.taskItem = taskItem;
		}
		public function getTaskItem():TaskItem{
			return this.taskItem;
		}
		//public static const STEP_MOVING:String = "MOVING";
		
		public static const STEP_INITIAL:String = "INITIAL";
		public static const STEP_GHOSTX:String = "GHOST";
		public static const STEP_MOVED:String = "MOVED";

		
		public var delayTimeLabel:String = "";
		
		[Bindable]
		public var color:Number  = -1;
		[Bindable]
		public var border:Number = -1;
		
		[Bindable]
		public var thickness:Number = -1;
		[Bindable]
		public var alpha:Number = 1;
		
		
		
		private var logger:ILogger = LoggerFactory.getLogger("GanttTask");
		
		public var renegotiationLabel:String;
		
		
		public override function get resourcePosition():int{
			if (getContainer() == null){
				return -1;
			}
			return getPositionDate(this,getContainer());
		}
	

		private function getPositionDate(task:GanttTask, from:ArrayCollection):int{
			var result:int = 0;
			if (task.startTime != null){
				var time:Number =  task.startTime.getTime();
				for (var i:int  = 0 ; i < from.length ; i ++ ){
					var node:NodeTask = from.getItemAt(i) as NodeTask;
					var time2:Number = node.startTime.getTime();
					if (time2 > time){
						result ++;
						
					}	
				}
			}
			else {
				logger.debug("getPositionDate:null:task.resourceId:"+task.resourceId + "");
				result = -1;
			}
			return result + 1;
		}
		

		public override function initExtraValues():void {
			super.initExtraValues();
			if (isGhost()){
				alpha = 1;
			}
			else {
				alpha = 1;
			}
		
		}
		
		
		
		public function isGhost():Boolean{
			return step == STEP_GHOSTX; 
		}
		
		
		
	}
}
