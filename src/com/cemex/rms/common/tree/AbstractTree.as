package com.cemex.rms.common.tree
{
	import com.cemex.rms.common.filters.FilterHelper;
	import com.cemex.rms.common.gantt.GanttTask;
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.common.reflection.builder.SameClassBuilder;
	import com.cemex.rms.common.utils.DictionaryMap;
	import com.cemex.rms.dispatcher.constants.ExtrasConstants;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.helpers.TimeZoneHelper;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandImpl;
	import com.cemex.rms.dispatcher.services.flashislands.vo.ProductionData;
	import com.cemex.rms.dispatcher.views.assignedLoads.tree.AssignedLoadsTree;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.tree.LoadsPerOrderTree;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.tree.LoadsPerVehicleResource;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.tree.LoadsPerVehicleTask;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.tree.LoadsPerVehicleTree;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	import com.cemex.rms.dispatcher.views.planningScreen.tree.PlanningScreenTree;
	import com.cemex.rms.dispatcher.views.reports.ReportObject;
	import com.cemex.rms.dispatcher.views.serviceAgentScreen.tree.ServiceAgentScreenTree;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import flash.utils.getTimer;
	
	import mx.collections.ArrayCollection;
	import mx.collections.CursorBookmark;
	import mx.collections.HierarchicalCollectionView;
	import mx.collections.HierarchicalCollectionViewCursor;
	import mx.collections.HierarchicalData;
	import mx.collections.IHierarchicalCollectionViewCursor;
	import mx.controls.AdvancedDataGrid;
	import mx.controls.Alert;
	
	import ilog.gantt.GanttDataGrid;
	
	
	public class AbstractTree {
		public function AbstractTree(fields:ArrayCollection) {
			
			this.fields = fields;
			
		}
		
		private var logger:ILogger = LoggerFactory.getLogger("AbstractTree");
		public var currentDataGrid:GanttDataGrid;
		protected function newNode():NodeResource{
			throw new Error("This method should be overriden because is an abstract Class");
		}
		protected function newTask():NodeTask {
			throw new Error("This method should be overriden because is an abstract Class");
		}
		
		/**
		 * Esta variable contiene todos los reportes que se necesitan 
		 */
		private var reportMap:DictionaryMap;
		
		
		public function setFilters(filters:ArrayCollection):void{
			this.filters = filters;
		}
		public function timeChanged(time:Date):void {
		
			if (time == null){
				time = TimeZoneHelper.getServer();
			}
			if(allTasks!=null){
				for(var i:int = allTasks.length - 1 ; i >= 0   ; i--) {
					var task:NodeTask = allTasks.getItemAt(i) as NodeTask;
					if (task != null) {
						if (!timeChangedTask(time,task)){
							
							removeIndex(task,allTasks,i);
							removeFromAll(task,task.getContainer());
							// Cuando se esta borrando un elemento se le debe avisar a los padres para uqe 
							//actualizen sus contadores
							acknowledgeRemove2Parents(task);
						}
					}
				}
			}
		}
		
		public function timeChangedTask(time:Date,nodeTask:NodeTask):Boolean{
			return true;
		}
		
		private var root:NodeResource;
		private var fields:ArrayCollection;
		//private var dataFields:ArrayCollection;
		public var ctx:DictionaryMap;
		private var rawDatas:ArrayCollection;
		private var allTasks:ArrayCollection;
		private var filters:ArrayCollection;
		
		public function getTasksTree():ArrayCollection {
			return allTasks;
		}
		
		public function getResourcesTree():ArrayCollection {
			return root.getChildren();
		}
		
		public function getRoot():NodeResource{
			return root;
		}
		public function reset(reports:DictionaryMap):void {
			
			this.reportMap = reports;
			this.ctx = new DictionaryMap();
			if (this.rawDatas == null)
				this.rawDatas = new ArrayCollection();
				
			
			if (this.allTasks == null)
				this.allTasks = new ArrayCollection();
			
			this.allTasks.removeAll();
			this.rawDatas.removeAll();
			
			//this.allTasks.enableAutoUpdate();
			//this.rawDatas.enableAutoUpdate();
			
			root = newNode();
			root.id = "";
			ctx.put("",root);
			
			initReports("",root);
		}
		
		private var objectBuilder:SameClassBuilder =  new SameClassBuilder();
		private function initReports(field:String,node:NodeResource):void{
			if (reportMap != null) {
				var reportsObject:ReportObject = reportMap.get(field) as ReportObject;
				if (reportsObject != null){
					
					if (!reportsObject.singleton){
						// SE crea un objeto acumulador en cada Nodo 
						var report:ReportObject = objectBuilder.builderFromClass(reportsObject) as ReportObject;
						// Pero configuraciones se mantienen
						report.setReportsConfig(reportsObject.getReportsConfig());
						node.setReportsObject(report);
					}
					else {
						node.setReportsObject(reportsObject);
					}
				}
			}
		}
		/*
		public function regenerate():void {
			
			// chale
			/*
			var currentTasks:ArrayCollection = allTasks;
			//reset();
			for (var  i:int  = 0 ; i < currentTasks.length ; i ++){
				var temp:NodeTask =  currentTasks.getItemAt(i) as NodeTask;
				currentTasks.itemUpdated(temp);
				//addData(temp.data, temp.step);
			}
			
		}
		*/
		public function refresh():void {
			
		}

		public function addData(data:Object,step:String,date:Date,isHeader:Boolean):void {
			if (data != null){
				
				rawDatas.addItem(data);
				
				var baseId:String = "";
				// Se iteran los campos que se tienen configurados 
				for (var i:int = 0 ; i < fields.length ; i ++){
					var field:String = fields.getItemAt(i) as String; 
					var value:String="";
					var nodeId:String ="";
					var task:NodeTask = null;
					if (field.indexOf("|") != -1){
						var fieldsx:Array = field.split("|");
						if (!isHeader) { 	
							for (var j:int = 0 ; j < fieldsx.length ; j++){
								field = ""+fieldsx[j];
								if (data.hasOwnProperty(field) && data[field] != null && ((""+data[field]) != "")){
									j = fieldsx.length;
								}
							}
						}
						else {
							field = ""+fieldsx[0];
						}
					}
					if (data.hasOwnProperty(field) && data[field] != null && ((""+data[field])  != "")){
						value =  data[field];
						nodeId = baseId + ( i != 0 ? ":" : "" )+ value;
						logger.info("addNode:"+"("+field+":i="+i+")"+nodeId);
						/*if(field=="plant" && data.hasOwnProperty("name")){
							value=value+" "+data["name"];
						}
*/						
						task = addNode( baseId, nodeId, i, data, field, value,step,date,isHeader);	
						baseId = nodeId;
					}
					else {
						logger.info("breakaddNode"+"("+field+":i="+i+")"+nodeId);
						break;
					}
					if (task != null && task.canRenderField(field)){
						logger.info("breakAfterAddNode"+"("+field+":i="+i+")"+nodeId);
						break;
					}
					
				}
			}
		}
		
		public function shouldRenderTask(target:Object):Boolean {
			var info:ArrayCollection = new ArrayCollection();
			//var shouldRender:Boolean = FilterHelper.matchFilters(target,filters,info);
			var shouldRender:Boolean=true;
			//fix fuera de la tvarv
			if(target.loadingDate==null){
				return false;
			}
			if(target.hasOwnProperty("loadStatus") && this is LoadsPerVehicleTree){
				
				//if(target.loadStatus=="CMPL" || target.loadStatus=="CNCL" || target.itemCategory=="ZTX0"){
				if(target.itemCategory=="ZTX0"){
					//todas menos completadas en vista de cargas por vehiculos
					shouldRender=false;
				} else {
					shouldRender=true;
				}
				if(Number((target as OrderLoad).shipConditions)==2 && !GanttServiceReference.getCollectFilterStatus()){
					shouldRender=false;
				}
			} else if(target.hasOwnProperty("loadingDate") && this is LoadsPerOrderTree){
				/*if((target.loadingDate as Date).date<DispatcherIslandImpl.fechaBase.date && shouldRender){
					shouldRender=false;
				}*/
				if(target.hasOwnProperty("loadStatus")){
					if(target.loadStatus=="ASSG" || (target as OrderLoad).loadStatus=="TJST"){
						//todas menos completadas en vista de cargas por vehiculos
						shouldRender=false;
					} else {
						shouldRender=true;
					}
				}
				if(Number((target as OrderLoad).shipConditions)==2 && !GanttServiceReference.getCollectFilterStatus()){
					shouldRender=false;
				}
			}
			if (!shouldRender){
				/*logger.warning("The object " +
					"\r\n " +
					ReflectionHelper.object2XML(target,"target")+
					"didn't match the filter "+
					"\r\n " +
					ReflectionHelper.object2XML(info,"FilterInfo")
				);*/
			}
			if(this is PlanningScreenTree){
				shouldRender=true;
			}
			if(this is ServiceAgentScreenTree){
				if(Number((target as OrderLoad).shipConditions)==2 && !GanttServiceReference.getCollectFilterStatus()){
					shouldRender=false;
				}
			}
			return shouldRender;
		}
		
		/**
		 * Este metodo se encarga de asignar al todo lo que este relacionado al campo (Por ejemplo los reportes)
		 */
		public function setLabelToNode(field:String, node:NodeResource):void{
			node.setLabelField(field);
			
			if (reportMap != null ){
				initReports(field,node);
			}
			
		}
		public function processHeaders(oldNode:NodeResource,node:NodeResource,field:String):void{
			
		}
		public function deleteOldNode(oldNode:NodeResource,node:NodeResource,field:String):Boolean{
			return false;
			//return oldNode.id != node.id;
		}
		
		
		private var quickReferences:DictionaryMap = new DictionaryMap();
		
		/**
		 * Este metodo nos ayuda agregar un nodo, ya sea GanttResource o un ArrayCollection
		 * 
		 * 
		 */
		internal var one:Boolean=true;
		public function addNode( baseId:String, nodeId:String, index:int,data:Object, field:String,value:String, step:String,date:Date,isHeader:Boolean):NodeTask {
			
			var parent:NodeResource;
			var shouldRender:Boolean = false;//isHeader || shouldRenderTask(data);
			
			var isTempHeader:Boolean = false;
			if (!isHeader){
				
				shouldRender = shouldRenderTask(data);
				logger.info(nodeId + ":shouldRender:" + shouldRender);
				if(data is OrderLoad && shouldRender){
					var crear:String="";
					if(data.orderLoad.indexOf("38337")!=-1){
						one=false;
					}
					//crear=ReflectionHelper.object2ObjectString(data);
					//Alert.show(crear);
				}
			}
			
			if (value != null){
				
				// Se obtiene el padre para agregarle los nodos hijos
				// este nunca puede ser null
				parent = ctx.get(baseId) as NodeResource;
				
				var node:NodeResource = ctx.get(nodeId) as NodeResource;
				
				// si el nodo no existe e crea y se pone en el mapa
				if (node == null) {
					logger.info("entra a Node Null");
					node =  newNode();
					node.isHeader = isHeader;
					node.id = nodeId;
					node.setParent(parent);
					
					ReflectionHelper.copyParameters(data,node.data);
					
					var tempFields:ArrayCollection = ReflectionHelper.cloneObject(fields) as ArrayCollection;
					tempFields.setItemAt(field,index);
					ReflectionHelper.copyParameterFromList(data,node , tempFields, index + 1);
					//node.label = value;
					setLabelToNode(field,node);
					if(shouldRender || isHeader){
						isTempHeader = shouldRender && !isHeader;
						ctx.put(nodeId,node);
						insert   (node,parent.getChildren());
						logger.info("insert:node:("+node.id + ")-parent:(" + parent.id +")");
						/* Due to the nodes must be rearanged by date,
						identify the screen,
						identify the plant,
						rearange the item*/
						
					}
				} else {
					logger.info("entra a Node not Null("+shouldRender+"::"+ isHeader+")");
					
					if(shouldRender || isHeader){
						ReflectionHelper.copyParameters(data,node.data);
						//chale
						parent.getChildren().itemUpdated(node);
					}
				}
				
			
				// si no es el ultimo se va a crear el nodo
				var task:NodeTask = newTask();
				// Se borra de todos
				
				
				if (isHeader) {

					/*var valor:String = node.data[field];
					var oldNode:NodeResource = quickReferences.get(valor) as NodeResource;
					logger.info("removing:oldNode:field("+oldNode+":"+node.id+","+field+")");
					if(oldNode != null && deleteOldNode(oldNode,node,field)){
						
						//node.getParent()
						var oldParent:NodeResource = oldNode.getParent() as NodeResource;
						logger.info("children.length.before="+oldParent.getChildren().length);
						removeFromAll(oldNode,oldParent.getChildren(),field);
						
						//oldParent.getChildren().refresh();
						logger.info("children.length.after="+oldParent.getChildren().length);
						acknowledgeRemove2Parents(oldNode);
						
						ctx.put(oldNode.id,null);
						
						logger.info("removing:done:oldNode:("+oldNode.id+")");
						//processHeaders(oldNode,node,field);
					}
					logger.info("putting:newNode:("+valor+""+node+")");
					quickReferences.put(valor,node);*/
				}
				// else { Quite el else porque la variable isTempHeader puede cambiar el comportamiento
				
				//if (!isHeader) {
				else {
					task.data = data;
					var taskRemoved:NodeTask = removeFromAll(task,allTasks) as NodeTask;
					
					logger.info("notHeader:"+taskRemoved);
					if (taskRemoved != null){
						// ahora se borra en cascada en base al padre de la tarea borrada		
							
						logger.info("removing:Task("+taskRemoved.resourceId+")");
						//se borra la tarea del padre anterios
						//removeFromAll(taskRemoved,taskRemoved.getContainer());
						//chale
						//taskRemoved.getContainer().itemUpdated(taskRemoved);
						//taskRemoved.getContainer().refresh();
						// se le avisa a todos los padres anteriores para que actualizen sus valores
						//acknowledgeRemove2Parents(taskRemoved);
						allTasks.refresh();
					}
				}
				if ((index == fields.length - 1 
					|| (!isHeader && task.canRenderField(field)))){
					// si es el ultimo quiere decir que aqui se van a agregar como ArrayCollection
					if(data is OrderLoad){
						DispatcherViewMediator.reportPlant(data);
						if(data.equipLabel!=""){
							DispatcherViewMediator.reportTruck(data);
						}
					}
					node.hasVisibleTasks = true;
					task.setIdFields(fields);
					//ReflectionHelper.copyParameters(data,task);
					task.resourceId = nodeId;
					logger.info("Task.resourceId:("+task.resourceId+")");
					task.setContainer(node.getChildren());
					task.setAllTasks(allTasks);
					task.step = step;
					
					if (shouldRender && (date == null || (date != null && timeChangedTask(date,task)))) {
						logger.info("Task.resourceId:(shouldRender)");
						// SEle asigna el padre
						task.setParent(node);
						//Se agrega  a los hijos del padre
						insert(task,node.getChildren());
						// Si se obtuvo una fecha se calcula
						// se pone como parametro
						//timeChangedTask(date,task))
						// Se inserta en todas las tareas
						insert   (task,allTasks);
						// se avisa a los padres que se esta agregando la tarea
						acknowledgeAdding2Parents(task);
						
					}
					else {
						logger.info("Task.resourceId:("+date+")");
					}
					allTasks.refresh();
					//logger.info("insert allTasks("+ReflectionHelper.object2XML(task)+")");
					return task;
				} else {
					node.hasVisibleTasks = false;
				}
			}
			return null;
		}
		
		protected function acknowledgeRemove2Parents(node:GenericNode):void{
			
			var resource:NodeResource = node.getParent() as NodeResource;
			while(resource != null){
				resource.removing(node);
				if(resource.totalChildrenCount==0){
				//if (shouldRemoveResourceIfEmpty(resource)){
					removeFromParent(resource);
				}
				resource = acknowledgeParent(resource);
			}
		}
		
		public function shouldRemoveResourceIfEmpty(resource:NodeResource):Boolean{
			return false;
		}
		
		public function acknowledgeParent(resource:NodeResource):NodeResource{
			var parent:NodeResource = resource.getParent() as NodeResource;
			if (parent != null) {
				parent.getChildren().itemUpdated(resource);
				
			}
			return parent;
		}
		private var lista:Array=[];
		public function removeFromParent(resource:NodeResource):NodeResource{
			var parent:NodeResource = resource.getParent() as NodeResource;
			if (parent != null) {
				var index:int = parent.getChildren().getItemIndex(resource);
				if(index>-1){
					var algo2:*=DispatcherViewMediator.gH;
					logger.debug("removing("+index+")");
					//parent.getChildren().removeItemAt(index);
					var res:*=parent.getChildren().getItemAt(index);
					parent.getChildren().refresh();
					lista.push(res);
					/*
					
					para borrarlo del dataprovider
					debe ser un objeto -LoadsPeroOrderResource- o similar
					
					*/
					//var dp:*=currentDataGrid.dataProvider;
					//lista.push(dp);
					//currentDataGrid.selectedItem=res;
					this.forceErase();
					/*currentDataGrid.dataProvider.removeChild( currentDataGrid.dataProvider.getParentItem(res), res );
					currentDataGrid.dataProvider.refresh();*/
				}
			}
			return parent;
		}
		private var ctimer:Timer;
		private var lastt:Number=0;
		private function forceErase():void{
			//if((flash.utils.getTimer()-lastt)>4000){
			if(lista.length>0){
				if(ctimer!=null){
					ctimer.reset();
					ctimer.stop();
				}
				ctimer=new Timer(300,lista.length);
				ctimer.addEventListener(TimerEvent.TIMER,forceEraseTimer);
				ctimer.reset();
				ctimer.start();
				lastt=flash.utils.getTimer();
			}
			/*} else {
				ctimer.stop();
			}*/
			
		}
		private function forceEraseTimer(evt:TimerEvent):void{
			if(lista.length>0){
				try{
					currentDataGrid.dataProvider.removeChild( currentDataGrid.dataProvider.getParentItem(lista[0]), lista[0] );
					lista.shift();
				} catch(e:Error){
					trace("ERROR en FORCEERASETIMER");
				}
				ctimer.reset();
				ctimer.stop();
			}
		}
		
		protected function acknowledgeAdding2Parents(node:NodeTask):void{
			var resource:NodeResource = node.getParent() as NodeResource;
			while(resource != null){
				resource.adding(node);
				resource = acknowledgeParent(resource);
			}
		}
		/*
		protected function refreshAllTaskFrom(tasks:ArrayCollection,allTasks:ArrayCollection):void{
			for(var i:int = 0 ; i < tasks.length ; i ++){
				var task:* = tasks.getItemAt(i);
				allTasks.itemUpdated(task);
			}
		}
		*/
		
		protected function compareFunctions(target:Object,temp:Object):Boolean{
			return ReflectionHelper.compareObject(target,temp);
		}
		
		protected function compareFunctionJustField(target:Object,temp:Object, field:String):Boolean{
			if (target == null || temp == null){
				return false;
			}
			return (""+target[field])==(""+temp[field]);
		}
		
		/*
		private function removeFromAllResources(target:Object, node:NodeResource):void{
			var arr:ArrayCollection = node.getChildren();
			
			var childIsTask:Boolean =  false;
			for ( var i:int = arr.length - 1 ; i >= 0   ; i--) {
				var temp:Object = arr.getItemAt(i);
				if (temp is NodeResource){
					var nodeTemp:NodeResource = temp  as NodeResource;
					removeFromAllResources(target, nodeTemp);
				}
				else {
					childIsTask = true;
					break;
				}
			}
			if (childIsTask){
				removeAll(target, arr);
			}
		}
		
	
		*/
		
		private function insert(target:Object, values:ArrayCollection):void{
			var previousNum:Number=values.length;
			for(var c:int=0;c<values.length;c++){
				if(values.getItemAt(c) is LoadsPerVehicleTask || values.getItemAt(c) is LoadsPerVehicleResource){
					var maxNumber:Number=parseInt(String(values.getItemAt(c).payload["loadNumber"]).substr(1));
					var minNumber:Number=parseInt(String((target).payload["loadNumber"]).substr(1));
					if(maxNumber>minNumber){
						values.addItemAt(target,c);
						break;
					} /*else {
						values.addItem(target);
						break;
					}*/
				} else {
					values.addItem(target);
					break;
				}
			}
			if(previousNum==0 || (previousNum==values.length)){
				values.addItem(target);
			}
			//values.addItem(target);
		}
		/*
		public function removeAll(target:Object, values:ArrayCollection, compareField:String=null):void{
			
			if (values != null ){
				for ( var i:int = values.length - 1 ; i >= 0   ; i--) {
					if (values.length > 0){
						var temp:Object = values.getItemAt(i);
						try {
							if (compareField == null){
								if (compareFunctions(target,temp)){
									
									values.removeItemAt(i);
									//break;
								}
							}
							else {
								if (compareFunctionJustField(target,temp,compareField)){
									values.removeItemAt(i);
								}					
							}
						}
						catch (e:*){
							logger.error(e);
						}
					}
				}
				
			}
		}
		*/
		
		/*
		public function removeFromAll(target:Object, values:ArrayCollection, compareField:String=null):Object{
			var temp:Object = null;
			if (values != null ){
				
				
				var arr:Array = values.toArray();
				values.removeAll();
				for ( var i:int = arr.length - 1 ; i >= 0   ; i--) {
					temp = arr[i] ;//removeIndex(target,values,i,compareField);
					
					if (compareField == null){
						if (!compareFunctions(target,temp)){							
							values.addItem(temp);		
						}
					}
					else {
						if (!compareFunctionJustField(target,temp,compareField)){
							values.addItem(temp);
						}					
					}
					
				}
				
			}
			return temp;
		}
		*/
			
		// Estoy tratano de regenerar el array Collection para que se actualize el que se borro, pero nada que lo hace
		public function regenerateArray(values:ArrayCollection):Object{
			var temp:Object = null;
			if (values != null ){
				var arr:Array = values.toArray();
				values.removeAll();
				for ( var i:int = arr.length - 1 ; i >= 0   ; i--) {
					temp = arr[i] ;
					values.addItem(temp);					
				}
				
			}
			return temp;
		}
		
		public function removeFromAll(target:Object, values:ArrayCollection, compareField:String=null):Object{
			
			if (values != null ){
				for ( var i:int = values.length - 1 ; i >= 0   ; i--) {
					var temp:Object = removeIndex(target,values,i,compareField);
					if (temp != null){
							// Comentamos esta linea porque no esta sirviendo realmente
						// el problema es de render
						//regenerateArray(values);
						return temp;
					}
				}
			}
			return null;
		}
		
		public function removeIndex(target:Object, values:ArrayCollection,i:int, compareField:String=null):Object{
			
			
			if (values != null ){
				var temp:Object = values.getItemAt(i);
				
				if (compareField == null){
					if (compareFunctions(target,temp)){							
						return values.removeItemAt(i);
					}
				}
				else {
					if (compareFunctionJustField(target,temp,compareField)){
						if(i<=values.length-1){
							try{
								return values.removeItemAt(i);
							} catch(e:Error){
								trace("ERROR en REMOVEINDEX");
							}
						}
					}					
				}
			}
			return null;
		}
		
		public function getTask(target:Object):GanttTask {
			var values:ArrayCollection = allTasks;
			if (values != null ){
				for ( var i:int = values.length - 1 ; i >= 0   ; i--) {
					var temp:Object = values.getItemAt(i);
					if (compareFunctions(target,temp)){							
						return temp as GanttTask;
					}
					if(target is ProductionData && temp.payload.prodOrder==target.aufnr){
						return temp as GanttTask;
					}
				}
			}
			return null;
		}
		public function getTasks(target:Object):Array {
			var values:ArrayCollection = allTasks;
			var retTo:Array=[]
			if (values != null ){
				for ( var i:int = values.length - 1 ; i >= 0   ; i--) {
					var temp:Object = values.getItemAt(i);
					if (compareFunctions(target,temp)){
						retTo.push(temp);
					}else if(target is ProductionData && temp.payload.prodOrder==target.aufnr){
						retTo.push(temp);
					}
				}
			}
			return retTo;
		}
		
	}
}