package com.cemex.rms.dispatcher.helpers
{
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandImpl;

	public class TimeZoneHelper
	{
		public function TimeZoneHelper()
		{
		}
		public static var hourOffSetTime:Number=0;
		public static var initialHour:Date=null;
		public static var lockHour:Boolean=false;
		public static var lastKnownHour:Date=null;
		public static var lastMinute:Number=0;
		public static var lastServerMinute:Number=-1;
		
		public static function getServer():Date{
			var hora:Date=DispatcherIslandImpl.fechaBase;
			if(TimeZoneHelper.lastKnownHour!=null && hora!=null && !isNaN(hora.date)){
				if(TimeZoneHelper.lastServerMinute==-1){
					TimeZoneHelper.lastServerMinute=hora.minutes;
				} 
				TimeZoneHelper.lastKnownHour=hora;
			}
			if(TimeZoneHelper.initialHour==null){
				TimeZoneHelper.initialHour=new Date();
				
			}
			if(hora==null || isNaN(hora.date)){
				if(TimeZoneHelper.lastKnownHour==null){
					hora=new Date();
				} else {
					hora=TimeZoneHelper.lastKnownHour;
				}
			} 
			/*if(hora.hours!=(new Date()).hours){
				TimeZoneHelper.hourOffSetTime=hora.hours-(new Date()).hours;
			}*/
			var dummy:Date=new Date();
			if(dummy.minutes!=TimeZoneHelper.lastMinute){
				TimeZoneHelper.lastMinute=dummy.minutes;
				if(TimeZoneHelper.lastServerMinute!=-1){
					TimeZoneHelper.lastServerMinute=TimeZoneHelper.lastServerMinute+1;
					if(TimeZoneHelper.lastServerMinute==60){
						TimeZoneHelper.lastServerMinute=0;
						hora.hours=hora.hours+1;
					}
				}
			}
			/*if(dummy.hours!=TimeZoneHelper.initialHour.hours){
				TimeZoneHelper.initialHour=new Date();
				hora.hours=hora.hours+1;
			}*/
			hora.minutes=TimeZoneHelper.lastServerMinute;
			hora.milliseconds=dummy.milliseconds;
			hora.seconds=dummy.seconds;
			TimeZoneHelper.lastKnownHour=new Date();
			TimeZoneHelper.lastKnownHour.hours=hora.hours;
			TimeZoneHelper.lastKnownHour.minutes=hora.minutes;
			//hora.date=(new Date()).date;
			return hora;
		}
	}
}