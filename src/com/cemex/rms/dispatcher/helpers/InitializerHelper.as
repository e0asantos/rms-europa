package com.cemex.rms.dispatcher.helpers
{
	public class InitializerHelper
	{
		public function InitializerHelper()
		{
		}
		
		public static function initHelpers():void{
			//ColorHelper.init();
			StatusHelper.initFilters();
			ILogDateFormatterHelper.init();
			TooltipHelper.init();

		}
	}
}