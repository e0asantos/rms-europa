package com.cemex.rms.dispatcher.helpers
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.common.flashislands.vo.TVARVC;
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.services.config.IServiceModel;
	import com.cemex.rms.common.utils.DictionaryMap;
	import com.cemex.rms.dispatcher.services.IGanttServiceFactory;
	import com.cemex.rms.dispatcher.services.flashislands.vo.DateText;
	import com.cemex.rms.dispatcher.services.flashislands.vo.Equipment;
	import com.cemex.rms.dispatcher.services.flashislands.vo.Plant;
	import com.cemex.rms.dispatcher.services.flashislands.vo.ProductionData;
	import com.cemex.rms.dispatcher.views.mediators.GenericTaskRendererMediator;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;

	public class GanttServiceReference
	{
		public function GanttServiceReference()
		{
		}
		
		private static var collectFilterStatus:Boolean = false;
		public static function setCollectFilterStatus(status:Boolean):void{
			collectFilterStatus = status;
		}
		public static function getCollectFilterStatus():Boolean{
			return collectFilterStatus;
		}
		
		public static function getColorMap():DictionaryMap {
			return services.getFlashIslandService().getColorMap();
		}
		
		public static var services:IGanttServiceFactory;
		
		public static function getLoadMinutes():Number{
			return 3;
		}
		
		public static function getPlantsPlain():ArrayCollection{
			return services.getFlashIslandService().getPlantsPlain();
		}
		public static function getPlants():ArrayCollection{
			return services.getFlashIslandService().getPlants();
		}
		public static function getEquipmentsPlain():ArrayCollection{
			return services.getFlashIslandService().getEquipmentsPlain();
		}
		
		public static function updateEquipment(vehicle:Equipment):Object{
			return services.getFlashIslandService().updateEquipment(vehicle);
		}
		public static function getPlantInfo(plantId:String):Plant{
			return services.getFlashIslandService().getPlantInfo(plantId);
		}
		
		public static function getColorPalette():Object{
			return services.getFlashIslandService().getColorPalette();
		}
		
		public static function getTVARVCParam(ref:String):TVARVC{
			return services.getFlashIslandService().getTVARVCParam(ref);
		}
		public static function getTVARVCSelection(ref:String):ArrayCollection{
			return services.getFlashIslandService().getTVARVCSelection(ref);
		}
		public static function getLabel(label:String):String{
			return services.getFlashIslandService().getOTRValue(label);
		}
		
		public static function getParamString(param:String,plant:String=null):String {
			return services.getFlashIslandService().getParamString(param,plant);
		}
		
		public static function getParamNumber(param:String):Number {
			return services.getFlashIslandService().getParamNumber(param);
		}
		
		
		public static function getPData(payload:OrderLoad):ProductionData {
			
			return services.getFlashIslandService().getProductionData(payload);
		}
		
		public static function getPlanningLoads():ArrayCollection{
			return services.getFlashIslandService().getPlanningLoads();
		}
		
		public static function getModel():IServiceModel{
			return services.getModel();
		}
		
		/*
		public static function isBatcher():Boolean{
			
			return services.getFlashIslandService().isBatcher();
		}
		
		public static function isDispatcher():Boolean{
			return services.getFlashIslandService().isDispatcher();
		}
		*/
		
		private static var logger:ILogger = LoggerFactory.getLogger("GanttServiceRefernece");
		
		public static function isVisibleField(view:String,event:String,operativeMode:String):Boolean{
			
			logger.info("services.getFlashIslandService().isVisibleField("+view+","+event+","+operativeMode+")=" + services.getFlashIslandService().isVisibleField(view,event,operativeMode));
			return services.getFlashIslandService().isVisibleField(view,event,operativeMode);
		}
		public static function isEnabledField(view:String,event:String,operativeMode:String):Boolean{
			logger.info("services.getFlashIslandService().isEnabledField("+view+","+event+","+operativeMode+")=" + services.getFlashIslandService().isVisibleField(view,event,operativeMode));
			return services.getFlashIslandService().isEnabledField(view,event,operativeMode);
		}
			
		public static function dispatchIslandEvent(fio:IFlashIslandEventObject):void{
			services.getFlashIslandService().dispatchWDEvent(fio);
		}
		public static function getProductionData(payload:*):ProductionData{
			return services.getFlashIslandService().getProductionData(payload);
		}
		
		public static function setProductionData(payload:ProductionData):void{
			services.getFlashIslandService().setProductionData(payload);
		}
		public static function getPlantType(plantId:String):String{
			return services.getFlashIslandService().getPlantType(plantId);
		}
		public static function setPlantType(plantId:String,plantType:String,statusCode:String):void{
			if (services != null && services.getFlashIslandService() != null){
				services.getFlashIslandService().setPlantType(plantId,plantType,statusCode);
			}
		}
		public static function refreshData():void{
			services.getFlashIslandService().refreshData();
			//var serv:Object=services.getFlashIslandService();
		}
		public static function getDateText(date:String):DateText {
			return services.getFlashIslandService().getDateText(date);			
		}
		
	}
	
}