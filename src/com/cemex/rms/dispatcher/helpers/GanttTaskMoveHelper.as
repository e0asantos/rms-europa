package com.cemex.rms.dispatcher.helpers
{
	import com.cemex.rms.common.date.FlexDateHelper;
	import com.cemex.rms.common.gantt.GanttResource;
	import com.cemex.rms.common.gantt.GanttTask;
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.events.GanttTaskMovementEvent;
	import com.cemex.rms.dispatcher.services.IGanttServiceFactory;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.tree.LoadsPerVehicleResource;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import flash.events.IEventDispatcher;
	
	import mx.controls.Alert;
	
	import ilog.gantt.TaskItem;

	public class GanttTaskMoveHelper extends GanttServiceReference
	{
		public function GanttTaskMoveHelper()
		{
		}
		
		
		private static var logger:ILogger = LoggerFactory.getLogger("GanttTaskMoveHelper");
		public static  function commitItem(task:TaskItem):GanttTaskMovementEvent{
			
		if(DispatcherViewMediator.currentView!=TVARVCPreffixes.PLANNING_SCREEN_VIEW_ID){
			var fromTask:GanttTask = task.data as GanttTask;
			
			fromTask.step = GanttTask.STEP_INITIAL;
			var fromTaskPayload:OrderLoad = fromTask.data as OrderLoad ;
			
			var toResource:GanttResource = task.resource as GanttResource;
			
			//logger.info("payload("+ReflectionHelper.object2XML(fromTaskPayload)+")");
			// Se crea un objeto con los valores que cambiaron
			var destiny:OrderLoad = fromTaskPayload.clone();
			
			// SE setean los nuevo valores deseados
			//destiny.endTime = FlexDateHelper.copyDate(task.endTime);
			// SE deben copiar solamente los valores que puedan ser permitidos en los movimientos de dragg and drop
			
			//destiny["equipLabel"] = toResource.payload["equipLabel"];
			
			if (toResource.getLabelField() == "orderNumber"
				/*&&  toResource is LoadsPerVehicleResource*/){
				
				destiny["equipNumber"] = "";//toResource.payload["equipNumber"];
				//destiny["equipLabel"] = "";//toResource.payload["equipLabel"];
				
			}
			else if (toResource.getLabelField() == "equipNumber"){
				destiny["equipNumber"] = toResource.payload["equipNumber"];
				//destiny["equipLabel"] = toResource.payload["equipLabel"];
				//destiny["equipLabel"] = toResource.payload["equipLabel"];
			}
			else if (toResource.getLabelField() == "equipStatus"){
				
			}
			destiny["plant"] = toResource.payload["plant"];
			//destiny["equipNumber"] = toResource.payload["equipNumber"];
			destiny.startTime = FlexDateHelper.copyDate(task.startTime);
			// SE copian todos los datos de detino para que alguien ams tome una decision
			// del movimiento
			
			// Aqui esta la bronca
			
			
			//
			//destiny["startTime"] = toResource.payload["startTime"];
			
			// SE lanza un evento del movimiento que se genero
			
			destiny.resId=task.resourceId;
			var event:GanttTaskMovementEvent = new GanttTaskMovementEvent(fromTaskPayload,destiny);
			
			
				
			// Se regresa la tarea a su posicion inicial 
				task.endTime =  FlexDateHelper.copyDate(fromTask.endTime);
				task.startTime =  FlexDateHelper.copyDate(fromTask.startTime);
				task.resourceId = fromTask.resourceId;
				
				return event;
			}
			return null;
			
		}
		
		/**
		 * Movimiento Horizontal
		 * 
		 * Esta funcion nos dice si el movimiento puede ser realizado 
		 * 
		 */
		public static  function getMove(task:TaskItem, dispatcher:IEventDispatcher):Boolean{
			
			var result:Boolean= false;
			var ganttTask:GanttTask = task.data as GanttTask;
			
			// Si es una copia no se puede mover
			if (ganttTask.step != GanttTask.STEP_GHOSTX) {
				// talvez se tenga que hacer una validacion de la hora de trabajo
				result = true;
				
				
				
			}
			return result;
		}
		
		/**
		 * El movimeiento Vertical
		 * esta funcion nos dice si una tarea peude ser movida de planta o
		 * 
		 */
		public static  function getReassign(task:TaskItem, dispatcher:IEventDispatcher):Boolean{
			
			var ganttTask:GanttTask = task.data as GanttTask;
			var result:Boolean = false;
			// En caos que no sea una copia
			if (ganttTask.step!= GanttTask.STEP_GHOSTX) {	
				
				result = true;
			}
			return result;
		}
		
		
	
		
	}
}