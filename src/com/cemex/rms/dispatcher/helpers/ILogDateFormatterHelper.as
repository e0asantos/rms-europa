package com.cemex.rms.dispatcher.helpers
{
	import com.cemex.rms.common.date.FlexDateHelper;
	import com.cemex.rms.dispatcher.services.IGanttServiceFactory;
	import com.cemex.rms.dispatcher.services.flashislands.vo.DateText;
	
	import ilog.gantt.DateFormatter;
	
	import mx.controls.Alert;
	

	public class ILogDateFormatterHelper extends GanttServiceReference
	{
		public function ILogDateFormatterHelper()
		{
		}
		
		public static function init():void{
		
		}
		
		
		public static   function formatDateComplete(date:Date, dateFormat:String):String {
			
			var i:int = 0 ;
			var j:int = 0 ;
			var result:String = "";
			/*
			if (dateFormat == "EEEE MMMM dd yyyy, h a"){
			}
			else if (dateFormat == "EEE MMM dd ''yy, h a"){
			}
			else if (dateFormat == "EEEE MMMM dd, yyyy"){
			}
			*/
			
			var index:int = dateFormat.indexOf(","); 
			if (index != -1 ){
				var temp:String = dateFormat.substring(0,index);
				var refDate:String = FlexDateHelper.getDateString(date,"YYYYMMDD");
				var dateText:DateText = getDateText(refDate);
				
				if (dateText != null){
					var nextTemp:String = dateFormat.substring(index);
					if (temp == "EEEE MMMM dd yyyy"){
						result = dateText.largeDate;
						dateFormat = nextTemp;
					}
					else if (temp == "EEE MMM dd ''yy"){
						result = dateText.shortDate;
						dateFormat = nextTemp;
					}
					else if (dateFormat == "EEEE MMMM dd, yyyy" ){
						result = dateText.largeDate;
						dateFormat = "";						
					}
				}
			}
			var espacios:Array = dateFormat.split(" ");
			for (i = 0 ; i  < espacios.length ; i ++) {
				if (i != 0){
					result += " ";
				}
				var comas:Array = espacios[i].split(",");
				for (j = 0 ; j  < comas.length ; j ++) {
					if (j != 0){
						result += ",";
					}
					result += formatDate(date,comas[j]);
				}
			}
			return result.replace("''","'");
		}
		public static   function fillString(string:String, max:int,filler:String, orientation:String="left"):String {
			var i:int = 0 ; 
			for (i = string.length ; i < max ; i ++){
				if (orientation == "left"){
					string = filler + string;
				}
				else {
					string += filler;
				}
				
			}
			return string;
		}
		public static   function getWeek(date:Date):Number {  
			
			var days:Array = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
			var year:Number = date.fullYearUTC;
			var isLeap:Boolean = (year % 4 == 0) && (year % 100 != 0) || (year % 100 == 0) && (year % 400 == 0);
			
			if(isLeap)
				days[1]++;
			
			var d:int = 0;
			
			//month is conveniently 0 indexed.   
			for(var i:int = 0; i < date.month; i++)
				d += days[i];
			d += date.dateUTC;
			var temp:Date = new Date(year, 0, 1);
			var jan1:Number = temp.dayUTC;
			/**    
			 * If Jan 1st is a Friday (as in 2010), does Mon, 4th Jan     
			 * fall in the first week or the second one?     
			 *    
			 * Comment the next line to make it in first week    
			 * This will effectively make the week start on Friday     
			 * or whatever day Jan 1st of that year is.    
			 **/   
			d += jan1;
			return int(d / 7);
		}  
		
		public static   function formatDate(date:Date, dateFormat:String):String {
			var result:String = "X";
			var formater:DateFormatter = new DateFormatter();
			
			var tienecomilla:Boolean = false;
			
			
			if (dateFormat == "yy" || dateFormat == "YY" ){
				result = String(date.getFullYear()).substring(2,4);
			}
			else if (dateFormat == "''yy" || dateFormat == "''YY" ){
				result = "'"+String(date.getFullYear()).substring(2,4);
			}
			else if (dateFormat == "yyyy" || dateFormat == "YYYY" ){
				result = String(date.getFullYear());
			}
			else if (dateFormat == "RRR" ){
				if (date.getMonth() < 6){
					result = "HY1";
				}
				else {
					result = "HY2";
				}					
			}
			else if (dateFormat == "QQQ" ){
				if (date.getMonth() < 3){
					result = "Q1";
				}
				else if (date.getMonth() < 6){
					result = "Q2";
				}
				else if (date.getMonth() < 9){
					result = "Q3";
				}
				else {
					result = "Q4";
				}
			}
			else if (dateFormat == "MM" ){
				result = "M"+String(date.getMonth());
			}
			else if (dateFormat == "MMM" ){
				// Este es el mes en 3 letras
				formater.formatString = dateFormat;
				result = formater.format(date);
			}
			else if (dateFormat == "MMMM" ){
				// Este es el mes en completo 
				formater.formatString = dateFormat;
				result = formater.format(date);
			}
			else if (dateFormat == "'W'w" ){
				//formater.formatString = "w";
				result = "W"+getWeek(date);
			}
			else if (dateFormat == "mm" ){
				result = fillString(String(date.getHours()),2,"0") +":"+fillString(String(date.getMinutes()),2,"0");
			}
			else if (dateFormat == "a" ){
				
				//formater.formatString = "U";
				//result = String(formater.format(date)).toLowerCase();
				result = fillString(String(date.getMinutes()),2,"0");
			} 
			else if (dateFormat == "h" ){
				
				result = fillString(String(date.getHours()),2,"0");
			}
			else if (dateFormat == "dd"){
				
				result = FlexDateHelper.getDayOfMonth(date);
				//result = "DD";	
			}
			else if (dateFormat == "DD") {
				
				result = FlexDateHelper.getDayOfMonth(date);
				//result = "DD";	
			}
			else if (dateFormat == "EEE"){
				//Este es el dia en tres letras
				formater.formatString = dateFormat;
				result = formater.format(date);
				
			} 
			else if (dateFormat == "EEEE"){
				// ESte es el dia en palabras largars
				formater.formatString = dateFormat;
				result = formater.format(date);
				
				//result = "XXXX";
			} 
			else if (dateFormat == "U"){
				// am, pm
				formater.formatString = dateFormat;
				result = formater.format(date);
			}
			else if (dateFormat == ""){
				result = "";
			}
			else {
				// por default
				formater.formatString = dateFormat;
				result = formater.format(date);
			}
			return result;
		}
	}
	
}