package com.cemex.rms.dispatcher.helpers
{
	import com.cemex.rms.common.gantt.GanttTask;
	import com.cemex.rms.dispatcher.constants.OTRConstants;
	import com.cemex.rms.dispatcher.services.flashislands.vo.AddProd;
	import com.cemex.rms.dispatcher.services.flashislands.vo.ProductionData;
	import com.cemex.rms.dispatcher.views.DispatcherView;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	import com.cemex.rms.dispatcher.views.reports.PlantReport;
	import com.cemex.rms.dispatcher.views.reports.VehicleReport;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import mx.collections.ArrayCollection;
	import mx.controls.ToolTip;
	import mx.formatters.Formatter;
	import mx.formatters.NumberFormatter;
	import mx.managers.ToolTipManager;

	public class TooltipHelper extends GanttServiceReference
	{
		public function TooltipHelper()
		{
			
		}
		
		public static function init():void{
			// DE cajon este valor si va
			ToolTipManager.enabled = true;// Optional. Default value is true.
			
			
			ToolTip.maxWidth = 500;
			//ToolTipManager.showDelay = 2000;// Display immediately.
			ToolTipManager.hideDelay = 100000000; // Hide after 3 seconds of being viewed.
			numbFormat.precision = 1;
			numbFormat.rounding = "up";
			numbFormat.decimalSeparatorTo=".";
			numbFormat.thousandsSeparatorTo=",";
			numbFormat.useThousandsSeparator=true;
			numbFormat.useNegativeSign=true;
				

		}
		
		public static var numbFormat:NumberFormatter = new NumberFormatter();
		
		public static function getPlantsTooltip(plant:Object,report:PlantReport):String{
				
			if (plant == null || report == null){
				return "";
			}
			var tooltip:String ="";
			
			tooltip += getTooltipField(plant["data"],"plantId","");
			tooltip += " : ";
			tooltip += getTooltipField(plant["data"],"name","")+ "\n";
			
			/*tooltip += getTooltipField(report,"assignedLoads",getLabel(OTRConstants.PLANT_ASSIGNED_TOOLTIP) + "\t: ",numbFormat);
			tooltip += " ";
			tooltip += getTooltipField(report,"UOM","") + "\n";*/
			//tooltip += getTooltipField(report,"ScheduledVolume",getLabel(OTRConstants.PLANT_SCHEDULED_TOOLTIP) + "\t: ",numbFormat);
			tooltip += getLabel(OTRConstants.PLANT_SCHEDULED_TOOLTIP) + "\t: "+DispatcherViewMediator.reportesPlantas[plant.plant].ScheduledVolume;
			tooltip += " ";
			tooltip += getTooltipField(report,"UOM","") + "\n";
			//tooltip += getTooltipField(report,"DeliveredVolume",getLabel(OTRConstants.PLANT_DELIVERED_TOOLTIP) + "\t: ",numbFormat);
			tooltip += getLabel(OTRConstants.PLANT_DELIVERED_TOOLTIP) + "\t: "+DispatcherViewMediator.reportesPlantas[plant.plant].DeliveredVolume;
			tooltip += " ";
			tooltip += getTooltipField(report,"UOM","") + "\n";
			//tooltip += getTooltipField(report,"ConfirmedVolume",getLabel(OTRConstants.PLANT_CONFIRMED_TOOLTIP) + "\t: ",numbFormat);
			tooltip += getLabel(OTRConstants.PLANT_CONFIRMED_TOOLTIP) + "\t: "+DispatcherViewMediator.reportesPlantas[plant.plant].ConfirmedVolume;
			tooltip += " ";
			tooltip += getTooltipField(report,"UOM","") + "\n";
			tooltip += getLabel(OTRConstants.PLANT_TO_BE_CONFIRMED_TOOLTIP) + "\t: "+DispatcherViewMediator.reportesPlantas[plant.plant].ToBeConfirmedVolume;
			tooltip += " ";
			tooltip += getTooltipField(report,"UOM","") + "\n";
			var plantas:ArrayCollection=GanttServiceReference.getPlants();
			var pstatus:String="?";
			for(var q:int=plantas.length-1;q>-1;q--){
				if(plantas.getItemAt(q).plantId==plant.plant){
					if(plantas.getItemAt(q).status=="E0001"){
						pstatus=GanttServiceReference.getLabel(OTRConstants.PLANT_READY);
					}
					if(plantas.getItemAt(q).status=="E0002"){
						pstatus=GanttServiceReference.getLabel(OTRConstants.PLANT_CLOSE);
					}
					if(plantas.getItemAt(q).status=="E0003"){
						pstatus=GanttServiceReference.getLabel(OTRConstants.PLANT_STOPPAGE);
					}
					
					
					//pstatus=plantas.getItemAt(q).status;
				}
			}
			switch(plant.data.zoperativeMode){
				case "01":
					//rojo centralizada
					tooltip += getLabel(OTRConstants.PLANT_CENTRALIZED) + ": "+pstatus;
					break;
				case "02":
					//distribuida gris
					tooltip += getLabel(OTRConstants.PLANT_DISTRIBUTED) + ": "+pstatus;
					break;
				case "03":
					//autonoma verde
					tooltip += getLabel(OTRConstants.PLANT_AUTONOMOUS) + ": "+pstatus;
					break;
			} 
			
			//tooltip += getTooltipField(report,"CanceledVolume",getLabel(OTRConstants.PLANT_CANCELED_TOOLTIP) + "\t: ",numbFormat);
			//tooltip += " ";
			//tooltip += getTooltipField(report,"UOM","") + "\n";
			return tooltip;
		}
		
		
		public static function getVehicleTooltip(vehicle:Object,report:VehicleReport):String{
			if (vehicle == null || report == null){
				return "";
			}
			var tooltip:String ="";
			
			if (vehicle["data"]["equipNumber"] != null){
				tooltip += getTooltipField(vehicle["data"],"equipNumber","");
				tooltip += " : ";
				if (vehicle["data"]["equipLabel"] != null){
					tooltip += getTooltipField(vehicle["data"],"equipLabel","")+ "\n";
				}
				tooltip += getLabel(OTRConstants.DRIVER_NAME_TOOLTIP)+ ": " ;
				for each (var vehicleitr:Object in DispatcherViewMediator.vehiculosCorrectos){
					if(vehicleitr.equipNumber==vehicle["data"].equipNumber){
						tooltip += vehicleitr.driver+" "+vehicleitr.driverName+"\n"; //getTooltipField(vehicle["data"],"driverName","");
					}
					
				}
			}
			else {
				tooltip += "U\n";
			}
			//tooltip += getTooltipField(report,"DeliveredVolume"			,getLabel(OTRConstants.VEHICLE_DELIVERED_TOOLTIP) + " : ",numbFormat);
			tooltip +=getLabel(OTRConstants.VEHICLE_DELIVERED_TOOLTIP) + " : "+DispatcherViewMediator.reportesTrucks[vehicle.data.equipNumber].DeliveredVolume;
			
			tooltip += " ";
			tooltip += getTooltipField(report,"UOM","") +" - " ;
			
			tooltip += getTooltipField(report,"DeliveredLoadsCount"		,"")+ "\n";
			
			
			//tooltip += getTooltipField(report,"PendingConfirmedVolume"		,getLabel(OTRConstants.VEHICLE_CONFIRMED_TOOLTIP) + " : ",numbFormat);
			tooltip += getLabel(OTRConstants.VEHICLE_CONFIRMED_TOOLTIP) + " : "+DispatcherViewMediator.reportesTrucks[vehicle.data.equipNumber].PendingConfirmedVolume;
			tooltip += " ";
			tooltip += getTooltipField(report,"UOM","") +" - " ;
			tooltip += getTooltipField(report,"PendingConfirmedLoadsCount"	,"")+ "\n";
			
			//tooltip += getTooltipField(report,"PendingToBeConfirmedVolume"	,getLabel(OTRConstants.VEHICLE_TO_BE_CONFIRMED_TOOLTIP) + " : ",numbFormat);
			tooltip += getLabel(OTRConstants.VEHICLE_TO_BE_CONFIRMED_TOOLTIP) + " : "+DispatcherViewMediator.reportesTrucks[vehicle.data.equipNumber].PendingToBeConfirmedVolume;
			tooltip += " ";
			tooltip += getTooltipField(report,"UOM","") +" - " ;
			tooltip += getTooltipField(report,"PendingToBeConfirmedLoadsCount"	,"")+ "\n";
			
			return tooltip;
		}
		
		
		public static  function getOrderTooltip(resource:Object):String {
			
			var tooltip:String= "";
			
			if (resource != null && resource.payload){
			
				tooltip =
					getOrderNumber(resource.payload) 
					+"  "
					+getNumberOfLoads(resource)
				+ "\n"
					+getOrderVolume(resource.payload)
					+ "  "
					+getOrderTime(resource.payload) + " "+resource.payload.mdLabel
				+ "  \n"
					+getMaterialDes(resource.payload)  
				+ "\n" 
					+getSoldToPartyName(resource.payload)
				+"\n"
					+getJobSiteName(resource.payload)
				+"\n"
				+getContact(resource.payload) 
				+"\n"
				/*+"Slump:"+resource.payload.slump
				+"\n"
				+"Task number:"+resource.payload.taskNumber
				+"\n"*/
				+getExtraText(resource.payload)
				+"Status:"+resource.payload.orderStatus+"\n";
				if((resource.payload as Object).hasOwnProperty("addProd")){
					for(var zz:int=0;zz<resource.payload.addProd.length;zz++){
						if(resource.payload.addProd.getItemAt(zz).zzshortId!=null){
							if(resource.payload.addProd.getItemAt(zz).zzshortId!=""){
								tooltip+="Short id : "+resource.payload.addProd.getItemAt(zz).zzshortId+"\n";
							}
						}
					}
					
				}
				
			}
			return tooltip;
			
		}

		
		
		
		public static  function getLoadTooltip(task:GanttTask):String {
			
			var tooltip:String= "";
			if (task != null) {
				var payload:OrderLoad = task.data as OrderLoad;
				 if( payload != null){
					 
					 if (StatusHelper.isConstructionProduct(payload)){
						 return getConstructionProductTooltip(payload);
					 }
					 else if (StatusHelper.isPumpingService(payload)){
						 return getPumpServiceTooltip(payload)
					 }
					 else {
						 //return getSimpleLoadTooltip(payload);
						 return getProductionLoadTooltip(payload);
						 
					 }
				 }
			}
			return tooltip;
			
		}
		
		public static  function getProdLoadTooltip(task:GanttTask):String {
			
			var tooltip:String= "";
			if (task != null) {
				var payload:OrderLoad = task.data as OrderLoad;
				if( payload != null){
					
					if (StatusHelper.isConstructionProduct(payload)){
						return getConstructionProductTooltip(payload);
					}
					else if (StatusHelper.isPumpingService(payload)){
						return getPumpServiceTooltip(payload)
					}
					else {
						return getProductionLoadTooltip(payload);
						
					}
				}
			}
			return tooltip;
			
		}
		
		
		
		public static  function getPumpServiceTooltip(payload:OrderLoad):String {
			return "";
		}
		
		public static  function getConstructionProductTooltip(payload:OrderLoad):String {
			var tooltip:String = "";
			if( payload != null){
				tooltip = 
					getCPProduct(payload)
					+"\n" 
					+getCPAmount(payload)
					+"\n" 
					+getDeliveryTime(payload)
					+"\n" 
					+getContactName(payload)
					+"\n"
					+getJobSiteName(payload)
					+"\n"
					+getAddictionalProducts(payload)
					+"\n"
					+getAdditives(payload);
			}
			
			
			return tooltip;
		}
		public static  function getSimpleLoadTooltip(payload:OrderLoad):String {
			var equipos:ArrayCollection=DispatcherViewMediator.vehiculosCorrectos;
			var vn:String="-";
			for(var q:int=0;q<equipos.length;q++){
				if(payload.equipNumber==equipos.getItemAt(q).equipNumber){
					if(equipos.getItemAt(q).equipLabel!=null){
						vn=equipos.getItemAt(q).equipLabel;
					}
				}
			}
			var tooltip:String = "";
			if( payload != null){
				tooltip = 
					getOrderNumber(payload)
					+" - "
					+ getLoadNumber(payload)
					+"\n"
					+ getLoadVolume(payload)+" "
					+getLoadTime(payload)
					+"\n"
					+getExtraText(payload)
					+"\n"
					+ getMaterialDes(payload)
					+"\n"
					+getJobSiteName(payload)
					+"\n"
					+getAddictionalProducts(payload)
					+vn+"\n"
					+"Status:"+payload.loadStatus+"\n";
				for(var zz:int=0;zz<payload.addProd.length;zz++){
					if(payload.addProd.getItemAt(zz).zzshortId!=null){
						if(payload.addProd.getItemAt(zz).zzshortId!=""){
							tooltip+="Short id : "+payload.addProd.getItemAt(zz).zzshortId+"\n";
						}
					}
				}
				
				
				
			}
			
			
			return tooltip;
			
		}
		
		public static  function getProductionLoadTooltip(payload:OrderLoad):String {
			var tooltip:String = "";
			var equipos:ArrayCollection=DispatcherViewMediator.vehiculosCorrectos;
			var vn:String="-";
			for(var q:int=0;q<equipos.length;q++){
				if(payload.equipNumber==equipos.getItemAt(q).equipNumber){
					if(equipos.getItemAt(q).equipLabel!=null){
						vn=equipos.getItemAt(q).equipLabel;
					}
				}
			}
			if( payload != null){
				tooltip =
					getOrderNumber(payload)
					+" - "
					+ getLoadNumber(payload)
					+"\n"
					+ getLoadVolume(payload)+" "
					+getLoadTime(payload)+ "  "+payload.mdLabel+"-"+payload.mdGroup+"-"+payload.mdGroupQty+" "+payload.loadUom
					+"\n"
					+ getMaterialDes(payload)
					+"\n"
					+getBetweenStreets(payload)
					+ "\n"
					+getCity2(payload)
					+ "\n"
					+getJobSiteName(payload)
					+"\n";
					if(payload.otcFlag=="X"){
						tooltip+=payload.houseNoStreet+"\n";
					}
					tooltip+=getAddictionalProducts(payload)
					+vn+"\n"
					+getExtraText(payload)
					+"\n"
					+getLabel(OTRConstants.LOAD)+" status: "+payload.loadStatus+"\n"
					+"\n"
					+ getComentDisp(payload)+"\n";
					/*+"Slump:"+payload.slump
					+"\n"
					+"Task number:"+payload.taskNumber
					+"\n";*/
					
					if(getLabel(OTRConstants.LOAD)!="Carga"){
						tooltip+="Production order: "+payload.prodOrder+"\n";
						tooltip+="Production status : "+payload.txtStatusProdOrder+"\n";
					} else {
						tooltip+="Orden producción: "+payload.prodOrder+"\n";
						tooltip+="Status producción : "+payload.txtStatusProdOrder+"\n";
					}
					var cargadas:Array=[];
					for(var zz:int=0;zz<payload.addProd.length;zz++){
						if(payload.addProd.getItemAt(zz).zzshortId!=null){
							if(payload.addProd.getItemAt(zz).zzshortId!="" && cargadas.indexOf(payload.addProd.getItemAt(zz).zzshortId)==-1 && payload.addProd.getItemAt(zz).itemNumber==payload.itemNumber){
								tooltip+="Short id : "+payload.addProd.getItemAt(zz).zzshortId+"\n";
								cargadas.push(payload.addProd.getItemAt(zz).zzshortId);
							}
						}
					}
					
					/*getOrderNumber(payload)
					+" - "
					+ getLoadNumber(payload)
					+"\n"
					+ getLoadVolume(payload)+" "
					+getLoadTime(payload)
					+"\n"
					+ getMaterialDes(payload)
					+"\n"
					
					+getJobSiteName(payload)
					+"\n"
					+getAddictionalProducts(payload)
					
					+"\n"
					+getAdditives(payload)
					+vn+"\n"
					+"Status:"+payload.loadStatus;*/
				
				
					/*var productionData:ProductionData = getPData(payload);
					if (productionData != null){
						tooltip +="\n"+ 
							getProductionOrder(productionData)
							+ "  "
							+ getProductionStatus(productionData);
					}		*/
			}
			
			
			return tooltip;
			
		}
		
		public static function getBetweenStreets(payload:Object):String{
			var ret:String="";
			if(payload["strSuppl3"]!=null){
				ret=payload["strSuppl3"]
			}
			if(payload["location"]!=null){
				ret+="  "+payload["location"]
			}
			return ret;
		}
		/**
		 * Regresa Colonia del payload
		 */
		public static function getCity2(payload:Object):String{
			var col:String="";
			if(payload["city2"]!=null){
				col=payload["city2"]
			}
			return col;
		}
		
		
		/**
		 * Regresa comentarios
		 */
		public static function getComentDisp(payload:Object):String{
			var col:String="";
			if(payload["comentDisp"]!=null){
				col=payload["comentDisp"]
				//if col.le
			}
			return col;
		}
				 
		
		protected static function getCPProduct(payload:Object):String{
			return "";
		}
		protected static function getCPAmount(payload:Object):String{
			return "";
		}
		
		protected static function getAdditives(payload:Object):String{
		return "";
		}
		
		
		/*****************************/
		
		protected static function getExtraText(payload:Object):String{
			if(!payload.hasOwnProperty("texts")){
				return "";
			}
			if(payload.texts==null){
				return "";
			}
			var str:String=""
			for(var q:int=0;q<payload.texts.length;q++){
				//try{
				if(payload.texts.getItemAt(q).hasOwnProperty("tdline")){
					str+=payload.texts.getItemAt(q).tdline+"\n";					
				} else if(payload.texts.getItemAt(q).hasOwnProperty("tdLine")){
					str+=payload.texts.getItemAt(q).tdLine+"\n";
				}
				//} catch(error){
					
				//}
			}
			return str;
		}
		
		
		protected static function getNumberOfLoads(payload:Object):String{
			var totales:String=getTooltipField(payload,"totalChildrenCount"," ");
			if(Number(totales)<10){
				totales="00"+totales.substr(-1);
			} else if(Number(totales)<100){
				totales="0"+totales.substr(-1);
			}
			return " Total: "+totales;
		}
		
		
		/**
		 * Production Order number                 (AUFK-AUFNR)
		 */
		protected static function getProductionOrder(payload:Object):String {
			//services.getFlashIslandService().
			return getTooltipField(payload,"aufnr",getLabel(OTRConstants.PRODUCTION_ORDER_TOOLTIP) + ": ");
			
		}
		
		/**
		 * Status (TJ30T-TXT04)
		 */
		protected static function getProductionStatus(payload:Object):String {
			
			return getTooltipField(payload,"txt04","" );
		}
		
		
		
		
		
		
		
		
		/**
		 * 
		 */
		protected static function getTooltipField(obj:Object, field:String, display:String,formatter:Formatter=null):String {
			//var tooltip:String = display + ":";
			var tooltip:String = display + " ";
			if (!obj.hasOwnProperty(field)) {
				
				tooltip += "?";
			}
			else if (obj[field] != null && obj[field] != ""){
				if (formatter != null){
					tooltip += formatter.format(obj[field]) + "";
				}
				else {
					tooltip += obj[field] + "";
				}
				
			}
			else {
				if (formatter != null){
					tooltip += "0.00";
				}
				else {
					tooltip += "";
				}
			}
			return tooltip;
		}
		
		
		
		
		
		
		
		/**
		 * ORDER
		 * 6.1 Order number: VBAP-VBELN
		 * ORDER_NUMBER	VBELN_VA	CHAR	10	0	Sales Document
		 * 
		 */
		public static function getOrderNumber(payload:Object):String{
			var tooltip:String= "";
			
			if (!payload)
				return null;
			
			tooltip += getTooltipField(payload,"orderNumber",getLabel(OTRConstants.ORDER_NUMBER));
			
			return tooltip;
		}
		
		
		/**
		 * LOAD
		 * 6.2 Load number: VBKD-POSEX_E where VBKD-VBELN = VBAP-VBELN = VBAK-VBELN
		 * LOAD_NUMBER	POSEX_E	CHAR	6	0	Item Number of the Underlying Purchase Order
		 * 
		 */
		public static function getLoadNumber(payload:Object):String{
			return getTooltipField(payload,"loadNumber",getLabel(OTRConstants.LOAD)+":" );
		}
		
		
		
		/**
		 * 
		 * 5.2 Loading time VBEP- LDUHR
		 * LOADING_TIME	LDUHR	TIMS	6	0	Loading Time (Local Time Relating to a Shipping Point)
		 * 
		 */
		protected static function getDeliveryTime(payload:Object):String {
			
			return getTooltipField(payload,"deliveryTime","" );
		}
		
		
		/**
		 * 
		 * 5.2 Loading time VBEP- LDUHR
		 * LOADING_TIME	LDUHR	TIMS	6	0	Loading Time (Local Time Relating to a Shipping Point)
		 * 
		 */
		protected static function getLoadTime(payload:Object):String {
			
			return getTooltipField(payload,"loadingTime","" );
		}

		/** 
		 * Order
		 * 6.2 . Se considerará el primer Concreto de la orden (ZTC1)
		 * 6.4.  La hora, se considerará el campo VBEP-LDUHR loading time del primer concreto de la order (ZTC1)
		 */ 
		public static function getOrderTime(payload:Object):String{
			return getTooltipField(payload,"firstDeliveryTime","" );
		}

		
		
		
		
		
		
		
		/**
		 * 
		 *6.3 Volume of the service: VBAP-KWMENG + VBAP-VRKME 
		 * LOAD_VOLUME	KWMENG	QUAN	15	3	Cumulative Order Quantity in Sales Units
		 * 6.3 Volume of the service: VBAP-KWMENG + VBAP-VRKME 
		 * ESte se esta calculando en flex sumando todas las cargas
		 */
		public static function getLoadVolume(payload:Object):String{
			return getVolume(payload,"loadVolume","loadUom");
			
		}
		
		
		/**
		 * 
		 * 6.3 . Para el volumen,  se considera el valor de VBAP-ZZ_HPUMPSS  del ítem  (ZTAT)
		 * Para la unidad de medida, se considerará el campo VBAP-VRKME del ítem (ZTAT)
		 */
		public static function getOrderVolume(payload:Object):String{
			return getVolume(payload,"orderVolume","orderUom");	
		}
		
		
		private static function getVolume(payload:Object,volumeField:String,volumeUOMField:String):String{
			var tooltip:String = "";
			if (payload != null){
				tooltip = 
					getTooltipField(payload,volumeField,"")
					+ " "
					+ getTooltipField(payload,volumeUOMField,"")
					+ " - ";
			}
			return tooltip;
		}
		
	
		
		
		
		/**
		 * 
		 * 
		 * 
		 * ORDER
		 * 6.7 Job Site Name: KNA1/ADRC-NAME1
		 * (see Data Base Relationship)
		 * MOVE lw_kna1-name1 TO lw_order-sold_to_name.
		 * 
		 * LOAD
		 * 	6.7 Job Site Name: KNA1/ADRC-NAME1 (see Data Base Relationship) 
		 * JOB_SITE_NAME	NAME1_GP	CHAR	35	0	Name 1
		 * 
		 */
		public static function getJobSiteName(payload:Object):String{
			return getTooltipField(payload,"jobSiteName","");
		}
	
		
		/**
		 * 
		 * Order 
		 * 6.5 Item description: VBAP- ARKTX
		 * MATERIAL_DES	ARKTX	CHAR	40	0	Short text for sales order item
		 * 
		 * Item 
		 * 6.5 Item description: VBAP- ARKTX
		 * MATERIAL_DES	ARKTX	CHAR	40	0	Short text for sales order item
		 */
		public static function getMaterialDes(payload:Object):String {
			
			
			return getTooltipField(payload,"materialDes","");
		} 
		
		/**
		 * 
		 * 6.8 Additional Products: Select item category (VA) corresponding at the current concrete load 
		 * Select VBAP where VBAP- PSTYV = “ZADD” and VBAP-(pos sup) = VBAP-POSNR
		 * VBAP(VA)- ARKTX 
		 * 
		 */
		public static function getAddictionalProducts(payload:OrderLoad):String{
			var tooltip:String= "";
			
			if (!payload)
				return null;
			
			
			var arr:ArrayCollection = payload.addProd;
			if (arr != null){
				for(var i:int = 0 ; i < arr.length ; i ++ ){
					var temp:AddProd = arr.getItemAt(i) as AddProd;
					
					if (temp != null && temp.itemNumber == payload.itemNumber){
						temp.additionalDes;
						tooltip +=getTooltipField(temp,"additionalDes","" ) +"\n";
					}
					
				}
			}
			return tooltip;
		}
		
		
		/**
		 * 6.9 Contact name: KNVK-NAMEV + KNVK-NAME1, 
		 * Select KNVK where KNVK-KUNNR = VBAK-KUNNR
		 * CONTACT_NAME1	NAMEV_VP	CHAR	35	0	First name
		 * CONTACT_NAME2	NAME1_GP	CHAR	35	0	Name 1
		 * 
		 * MOVE: lw_knvk-namev TO lw_order-contact_name1,
		 *	lw_knvk-name1 TO lw_order-contact_name2,
		 *	kunnr TYPE knvk-kunnr,
		 *	namev TYPE knvk-namev,
		 *	name1 TYPE knvk-name1,
		 *	
		 *	6.10 Contact telephone: KNVK-TELF1
		 *	lw_knvk-telf1 TO lw_order-contact_tel.
		 *	telf1 TYPE knvk-telf1, 
	     * 
		 */
		public static function getContact(payload:Object):String{
			var tooltip:String= "";
			
			if (!payload)
				return null;
			
			tooltip = getContactName(payload);
			//tooltip +="\n";
			tooltip +=" ";
			
			//tooltip +=getTooltipField(payload,"contactTel",getLabel(OTRConstants.TELEPHONE_SHORT) );
			tooltip +=getTooltipField(payload,"contactTel"," ");
			return tooltip;
		}
		
		public static function getContactName(payload:Object):String{
			var tooltip:String= "";
			
			if (!payload)
				return null;
			
			//tooltip +=getTooltipField(payload,"contactName1",getLabel(OTRConstants.CONTACT) );
			tooltip +=getTooltipField(payload,"contactName1","" );
			tooltip +=" ";
			tooltip +=getTooltipField(payload,"contactName2","" );
		
			return tooltip;
		}
		
		
		
		/**
		 * 
		 * Order 
		 * 6.6 Sold-to Party name: KNA1/ADRC-NAME1.  
		 * Select KNA1 where KNA1-KUNNR = VBAK-KUNNR
		 * SOLD_TO_NAME	NAME1_GP	CHAR	35	0	Name 1
		 * MOVE lw_kna1-name1 TO lw_order-sold_to_name.
		 */
		public static function getSoldToPartyName(payload:Object):String{
			return getTooltipField(payload,"soldToName","" );;
		}
		
		
		
		
		
		
		
		
		
	}
}