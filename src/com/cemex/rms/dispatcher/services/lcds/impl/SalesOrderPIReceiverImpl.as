package com.cemex.rms.dispatcher.services.lcds.impl
{
	import com.cemex.rms.common.lcds.AbstractReceiverService;
	import com.cemex.rms.common.push.PushConfigHelper;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.dispatcher.services.lcds.IDispatcherReceiver;
	import com.cemex.rms.dispatcher.services.lcds.events.SalesOrderLCDSEvent;
	import com.cemex.rms.dispatcher.services.lcds.helpers.DispatchLCDSDynamicObjectHelper;
	
	import mx.collections.ArrayCollection;
	import mx.messaging.ChannelSet;
	
	public class SalesOrderPIReceiverImpl extends AbstractReceiverService implements IDispatcherReceiver
	{
		public function SalesOrderPIReceiverImpl()
		{
			super();
		}
		
		public override function getDestinationName():String{
			return PushConfigHelper.getDestinyName("SALES_ORDER");
		}
		
		public override function getChannelSet():ChannelSet{
			return servicesContext.getValue("ChannelSet") as ChannelSet;
		}
		protected override function receiveMessage(message:Object):void {
			logger.info(ReflectionHelper.object2XML(message,getDestinationName()));
			if (message != null && message.hasOwnProperty("orderNumber")){
				var datas:ArrayCollection =  DispatchLCDSDynamicObjectHelper.transformASOrders2Plain(message);
				
				logger.info(ReflectionHelper.object2XML(datas,"SalesOrderLCDSEvent"));
				var event:SalesOrderLCDSEvent =  new SalesOrderLCDSEvent(datas);
				dispatcher.dispatchEvent(event);
			}
		}
		
		
		
		
	}
}