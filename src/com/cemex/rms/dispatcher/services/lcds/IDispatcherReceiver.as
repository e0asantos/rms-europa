package com.cemex.rms.dispatcher.services.lcds
{
	public interface IDispatcherReceiver
	{
		function reconnect():void;
		function saveRemove():void;
	}
}