package com.cemex.rms.dispatcher.services.lcds.events
{
	import com.cemex.rms.dispatcher.services.flashislands.vo.FlashIslandsData;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	
	public class SalesOrderLCDSEvent extends Event
	{
		public static const LCDS_DATA_RECEIVED:String = "salesOrderLCDSDataReceived";
		
		public var datas:ArrayCollection;
		
		public function SalesOrderLCDSEvent(datas:ArrayCollection, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(LCDS_DATA_RECEIVED, bubbles, cancelable);
			this.datas = datas;
		}
	}
}