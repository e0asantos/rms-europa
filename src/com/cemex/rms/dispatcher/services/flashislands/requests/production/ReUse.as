 	package com.cemex.rms.dispatcher.services.flashislands.requests.production
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class ReUse implements IFlashIslandEventObject
	{
		public function ReUse()
		{
		}
		public var AUFNR:String;
		public var POSNR:String;
		public var VBELN:String;
		public var EQUNR:String;
		
		public function getEventName():String{
			return "reUse";
		}
	}
}