 	package com.cemex.rms.dispatcher.services.flashislands.requests.production
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class MultiDelivery implements IFlashIslandEventObject
	{
		public function MultiDelivery()
		{
		}
		
		public var vbeln:String;
		public var posnr:String;
			
		public function getEventName():String{
			return "requestMultidelivery";
		}
	}
}