 	package com.cemex.rms.dispatcher.services.flashislands.requests.vehicle
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class VehicleInPlant implements IFlashIslandEventObject
	{
		public function VehicleInPlant()
		{
		}
		
		public var EQUNR:String;
		public var WERKS:String;
		public function getEventName():String{
			return "vehicleInPlant";
		}
	}
}