package com.cemex.rms.dispatcher.services.flashislands.requests.dispatch
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	public class AnticipatedLoads implements IFlashIslandEventObject
	{
		public function AnticipatedLoads()
		{
		}
		
		public var VBELN:String;
		public var WERKS:String;
		public var POSNR:String;
		
		public function getEventName():String{
			return "AnticipatedLoads";
		}
	}
}
