 	package com.cemex.rms.dispatcher.services.flashislands.requests.production
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class StartBatching implements IFlashIslandEventObject
	{
		public function StartBatching()
		{
		}
		public var VBELN:String;
		public var AUFNR:String;
		public var POSNR:String;
		public var IS_RESALE:String;
		
		public function getEventName():String{
			return "startBatching";
		}
	}
}