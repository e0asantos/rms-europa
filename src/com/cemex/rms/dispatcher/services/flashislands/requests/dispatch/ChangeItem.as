 	package com.cemex.rms.dispatcher.services.flashislands.requests.dispatch
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class ChangeItem implements IFlashIslandEventObject
	{
		public function ChangeItem()
		{
		}
		public var orderNumber:String;
		public var itemNumber:String;
		
		public function getEventName():String{
			return "eventCancelOrder";
		}
	}
}