package com.cemex.rms.dispatcher.services.flashislands.requests.dispatch
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class OrderOnHoldRequest implements IFlashIslandEventObject {
		public function OrderOnHoldRequest(){}
		public var orderNumber:String;
		public var itemNumber:String;
		public var deliveryBlock:String;
		public var plant:String;
		
		public function getEventName():String{
			return "orderOnHold";
		}
		
	}
}