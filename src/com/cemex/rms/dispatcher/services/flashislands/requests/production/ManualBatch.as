 	package com.cemex.rms.dispatcher.services.flashislands.requests.production
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class ManualBatch implements IFlashIslandEventObject
	{
		public function ManualBatch()
		{
		}
		public var AUFNR:String;
		public var STONR:String;
		public var LICENSE_NUM:String;
		
		public var ARBPL:String;
		
		public var WERKS:String;
		public var VBELN:String;
		public var POSNR:String;
		public var EQUNR:String;
		public var IS_RESALE:String
		public function getEventName():String{
			return "manualBatch";
		}
	}
}