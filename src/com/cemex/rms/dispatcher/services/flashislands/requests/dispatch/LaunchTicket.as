package com.cemex.rms.dispatcher.services.flashislands.requests.dispatch
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;

	public class LaunchTicket implements IFlashIslandEventObject
	{
		
		public var orderNumber:String;
		
		public var itemNumber:String;
		
		
		public function getEventName():String{
			return "launchTicket";
		}
		
		public function LaunchTicket()
		{
		}
	}
}