 	package com.cemex.rms.dispatcher.services.flashislands.requests.production
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class RestartLoad implements IFlashIslandEventObject
	{
		public function RestartLoad()
		{
		}
		public var AUFNR:String;
		
		public var POSNR:String;
		
		public var WERKS:String;
		
		public var ARBPL:String;
		public var VBELN:String;
		
		
		public function getEventName():String{
			return "restartLoad";
		}
	}
}