 	package com.cemex.rms.dispatcher.services.flashislands.requests.dispatch
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class AssignPlant implements IFlashIslandEventObject
	{
		public function AssignPlant()
		{
		}
		public var orderNumber:String;
		public var itemNumber:String;
		public var toPlant:String;
		public var toPlantType:String;
		public var loadStatus:String;
		public var itemCategory:String;
		public var pincheparametro:String;
		
		
		public function getEventName():String{
			return "assignPlant";
		}
	}com.cemex.rms.common.flashislands.IFlashIslandEventObject;
}