package com.cemex.rms.dispatcher.services.flashislands.requests.dispatch
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	
	public class ChangeDriver implements IFlashIslandEventObject
	{
		public function ChangeDriver()
		{
		}
		
		public var plant:String;
		public var driver:String;
		public var vehicle:String;
		public function getEventName():String{	
			return "changeDriver";
		}
	}
}


