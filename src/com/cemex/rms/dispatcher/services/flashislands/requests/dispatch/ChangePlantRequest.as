package com.cemex.rms.dispatcher.services.flashislands.requests.dispatch
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class ChangePlantRequest implements IFlashIslandEventObject
	{
		public function ChangePlantRequest()
		{
		}
		public var orderNumber:String;
		public var itemNumber:String;
		public var toPlant:String;
		public var toPlantType:String;
		public var scope:String;
		public var material:String;
		public var eventType:String;
		public var fromView:String;
		
		public var isDistributedInsideArea:String
		
		public function getEventName():String{
			return "changePlant";
		}
	}
}