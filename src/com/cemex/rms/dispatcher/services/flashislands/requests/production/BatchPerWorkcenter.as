 	package com.cemex.rms.dispatcher.services.flashislands.requests.production
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class BatchPerWorkcenter implements IFlashIslandEventObject
	{
		public function BatchPerWorkcenter()
		{
		}
		public var ARBPL:String;
		public var WERKS:String;
		public var AUFNR:String;
		public var VBELN:String;
		public var POSNR:String;
		public var EQUNR:String;
		public var LICENSE_NUM:String;
		public var IS_RESALE:String
	
		public function getEventName():String{
			return "batchPerWorkCenter";
		}
	}
}