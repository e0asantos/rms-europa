package com.cemex.rms.dispatcher.services.flashislands.requests
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;

	public class RefreshRequest implements IFlashIslandEventObject
	{
		public function RefreshRequest()
		{
			var init:String="init";
		}
		
		public var vista:String="none";
		public var plant:String="none";
		public function getEventName():String
		{
			return "populateDS";
		}
	}
}