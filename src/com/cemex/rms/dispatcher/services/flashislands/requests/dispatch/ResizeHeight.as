package com.cemex.rms.dispatcher.services.flashislands.requests.dispatch
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	
	public class ResizeHeight implements IFlashIslandEventObject
	{
		public function ResizeHeight()
		{
		}
		
		public var heightSize:String;
		
		public function getEventName():String
		{
			return "resizeHeight";
		}
	}
}