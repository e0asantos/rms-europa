 	package com.cemex.rms.dispatcher.services.flashislands.requests.production
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class DisplayPO implements IFlashIslandEventObject
	{
		public function DisplayPO()
		{
		}
		public var AUFNR:String;
		public function getEventName():String{
			return "displayPO";
		}
	}
}