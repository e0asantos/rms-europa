 	package com.cemex.rms.dispatcher.services.flashislands.requests.dispatch
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class VisualConfigurationEventClose implements IFlashIslandEventObject
	{
		public function VisualConfigurationEventClose()
		{
		}
		public var showOnePlantAtATime:Boolean=false;
		
		public function getEventName():String{
			return "closeUserPreferences";
		}
	}com.cemex.rms.common.flashislands.IFlashIslandEventObject;
}