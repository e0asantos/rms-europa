package com.cemex.rms.dispatcher.services.flashislands.requests.dispatch
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;

	public class AssignVehicle implements IFlashIslandEventObject
	{
		public function AssignVehicle()
		{
		}	
		public var orderNumber:String;
		public var itemNumber:String;
		public var equipNumber:String;
		public var toPlant:String;
		public var toPlantType:String;
		public var loadStatus:String;
		public var isCallDirect:String;
		public var dosifi:String;
		public var isprod:String;
		public function getEventName():String{
			return "assignVehicle";
		}
	}
}