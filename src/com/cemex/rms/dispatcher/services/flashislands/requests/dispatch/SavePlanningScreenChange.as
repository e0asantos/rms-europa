 	package com.cemex.rms.dispatcher.services.flashislands.requests.dispatch
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class SavePlanningScreenChange implements IFlashIslandEventObject
	{
		public var plant:String="";
		public var dateCreate:String="";
		public var vehicle:String="";
		public var vbeln:String="";
		public var posnr:String="";
		public var loadVol:String="";
		public var deliveryIhour:String="";
		public var deliveryFhour:String="";
		public var modificationDat:String="";
		public var userCreate:String="";
		
		
		public function BringAllVehicles():void
		{
		}
		
		
		public function getEventName():String{
			return "savePlanningScreenChange";
		}
	}
}