 	package com.cemex.rms.dispatcher.services.flashislands.requests.dispatch
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class BringAllVehicles implements IFlashIslandEventObject
	{
		public var allOrNative:String="";
		public function BringAllVehicles()
		{
		}
		
		
		public function getEventName():String{
			return "bringAllVehicles";
		}
	}
}