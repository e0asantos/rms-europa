 	package com.cemex.rms.dispatcher.services.flashislands.requests.production
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class RePrint implements IFlashIslandEventObject
	{
		public function RePrint()
		{
		}
		public var AUFNR:String;
		public var STONR:String;
		public var VBELN:String;
		public var POSNR:Number;
		
		public function getEventName():String{
			return "rePrint";
		}
	}
}