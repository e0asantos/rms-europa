package com.cemex.rms.dispatcher.services.flashislands.requests.dispatch
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;

	public class AssignAdvance implements IFlashIslandEventObject
	{
		public function AssignAdvance()
		{
		}	
		public var orderNumber:String;
		public var itemNumber:String;
		public var equipNumber:String;
		public var toPlant:String;
		public var plantType:String;
		public var loadStatus:String;
		public var dosifi:String;
		public var batchType:String;
		public function getEventName():String{
			return "advanceBatch";
		}
	}
}