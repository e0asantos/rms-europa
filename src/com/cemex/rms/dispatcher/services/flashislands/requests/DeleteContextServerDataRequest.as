package com.cemex.rms.dispatcher.services.flashislands.requests
{
	import com.cemex.rms.common.flashislands.SimpleFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	
	public class DeleteContextServerDataRequest extends SimpleFlashIslandEventObject
	{
		public function DeleteContextServerDataRequest()
		{
			super();
		}
		public override function getEventName():String {
			return "invalidateDS";
		}
	}
}