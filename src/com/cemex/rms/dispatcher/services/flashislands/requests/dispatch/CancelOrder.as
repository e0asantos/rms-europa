package com.cemex.rms.dispatcher.services.flashislands.requests.dispatch
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class CancelOrder implements IFlashIslandEventObject
	{
		public function CancelOrder()
		{
		}
		public var orderNumber:String;
		public var itemNumber:String;
		public var toTime:String;
		public var plant:String;
		public var toDate:Date;
		
		
		public function getEventName():String{
			return "cancelOrder";
		}
	}
}