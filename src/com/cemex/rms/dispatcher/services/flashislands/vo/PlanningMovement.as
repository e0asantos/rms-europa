package com.cemex.rms.dispatcher.services.flashislands.vo
{
	public class PlanningMovement
	{
		public function PlanningMovement()
		{
		}
		
		public var plant:String;
		public var initialDate:Date;
		public var initialHour:String;
		public var finalDate:Date;
		public var finalHour:String;
		public var durationMov:String;
	}
}