package com.cemex.rms.dispatcher.services.flashislands.vo
{
	public class SecurityFieldPerProcess
	{
		public function SecurityFieldPerProcess()
		{
		}
		
		public var land:String;
		public var modoen:String;
		public var process:String;
		public var attributeName:String;
		
		public var required:Boolean;
		public var readOnly:Boolean;
		public var visible:Boolean;
		public var enabled:Boolean;
		
	}
}