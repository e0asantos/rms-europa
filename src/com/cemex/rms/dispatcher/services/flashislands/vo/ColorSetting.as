package com.cemex.rms.dispatcher.services.flashislands.vo
{
	public class ColorSetting
	{
		public function ColorSetting()
		{
		}
		
		
		public var status:String;
		public var fillColor:Number;
		public var borderColor:Number;
		public var borderThickness:Number;
		
		public function get rgbStencil():String{
			if (borderColor == 0 && borderThickness == 1){
				return "0x"+fillColor.toString(16);	
			}
			else if (borderColor != 0 && borderThickness == 1){
				return "0x"+fillColor.toString(16)+","+"0x"+borderColor.toString(16);
			}
				
			else if (borderColor != 0 && borderThickness != 1){
				return "0x"+fillColor.toString(16)+","+"0x"+borderColor.toString(16)+","+borderThickness;
			}
			else {
				return "0x000000";
			}
		}
		public function set rgbStencil(_rgbStencil:String):void{
			var split:Array = _rgbStencil.split(",");
			if (split.length == 1){
				fillColor = Number(_rgbStencil);
				borderColor = 0;
				borderThickness = 1;
			}
			else if (split.length == 2){
				fillColor = Number(split[0]);
				borderColor = Number(split[1]);
				borderThickness = 1;
			}
			else if (split.length == 3){
				fillColor = Number(split[0]);
				borderColor = Number(split[1]);
				borderThickness = Number(split[2]);
			}
		}
	}
}