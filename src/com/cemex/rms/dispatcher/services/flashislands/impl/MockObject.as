package com.cemex.rms.dispatcher.services.flashislands.impl
{
	import mx.collections.ArrayCollection;

	public class MockObject
	{
		public function MockObject()
		{
		}
		
		public var data: Object =
			/*Object*/{
				"DATA":/*sap.core.wd.context::WDContextNode*/{
					"PRODUCTION_DATA":new ArrayCollection([])
					,"RETURN_LOG":new ArrayCollection([])
					,"leadselection":/*String*/ "0"
					,"name":/*String*/ "DATA"
				}
				,"DS_SORDER":new ArrayCollection([
					/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_DES":/*String*/ "Fibra Polipropileno"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"ORDER_NUMBER":/*String*/ "0012712842"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_DES":/*String*/ "Fibra Polipropileno"
								,"ITEM_NUMBER":/*String*/ "3010"
								,"ORDER_NUMBER":/*String*/ "0012712842"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_DES":/*String*/ "Fibra Polipropileno"
								,"ITEM_NUMBER":/*String*/ "1003"
								,"ORDER_NUMBER":/*String*/ "0012712842"
							}
						])
						,"CONTACT_NAME1":/*String*/ "JUAN ESTRATA"
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ " 8113670434"
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "16:45:00"
							,"DESCRIPTION":/*String*/ "M-200-0-C-28-14-1-1-22H"
							,"ORDER_NUMBER":/*String*/ "0012712842"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "MONTERREY"
							,"HOUSE_NO_STREET":/*String*/ "AV REVOLUCION S/N"
							,"JOB_SITE_ID":/*String*/ "0065085181"
							,"JOB_SITE_NAME":/*String*/ "PUENTE CONCHELLO (REG)"
							,"POSTAL_CODE":/*String*/ "64019"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0X000000"
								,"COD_STATUS_PROD_ORDER":/*String*/ "10"
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:55:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "1"
								,"DELIVERY_TIME":/*String*/ "16:45:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"FILL_COLOR":/*String*/ "0XFFFFFF"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "16:35:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L001"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 14
								,"MATERIAL_DES":/*String*/ "M-200-0-C-28-14-1-1-22H"
								,"MATERIAL_ID":/*String*/ "000000000010002941"
								,"MEP_TOTCOST":/*Number*/ 4999.47
								,"PLANT":/*String*/ "D159"
								,"PROD_ORDER":/*String*/ "700000001242"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*Number*/ 7731.36
								,"TRUCK_VOLUME":/*int*/ 7
								,"TXT_STATUS_PROD_ORDER":/*String*/ "CRTD"
								,"UNLOADING_TIME":/*String*/ "00:30:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0X000000"
								,"COD_STATUS_PROD_ORDER":/*String*/ "10"
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:55:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "2"
								,"DELIVERY_TIME":/*String*/ "17:05:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"FILL_COLOR":/*String*/ "0XFFFFFF"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1003"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "16:55:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L002"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "M-200-0-C-28-14-1-1-22H"
								,"MATERIAL_ID":/*String*/ "000000000010002941"
								,"MEP_TOTCOST":/*Number*/ 4999.47
								,"PLANT":/*String*/ "D159"
								,"PROD_ORDER":/*String*/ "700000001164"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*Number*/ 7731.36
								,"TRUCK_VOLUME":/*int*/ 7
								,"TXT_STATUS_PROD_ORDER":/*String*/ "CRTD"
								,"UNLOADING_TIME":/*String*/ "00:30:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0XD0E0EB"
								,"COD_STATUS_PROD_ORDER":/*String*/ "0"
								,"CONFIRM_QTY":/*int*/ 14
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:00:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "0"
								,"DELIVERY_TIME":/*String*/ "16:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ "000000000011001545"
								,"FILL_COLOR":/*String*/ "0XD0E0EB"
								,"HLEVEL_ITEM":/*String*/ "0"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTX0"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "3000"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "16:45:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ ""
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 14
								,"MATERIAL_DES":/*String*/ "SERVICIO BOMBEO BP 24M"
								,"MATERIAL_ID":/*String*/ "000000000060000101"
								,"MEP_TOTCOST":/*int*/ 0
								,"PLANT":/*String*/ "D159"
								,"PROD_ORDER":/*String*/ ""
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "17:35:00"
								,"TOT_COST":/*int*/ 0
								,"TRUCK_VOLUME":/*int*/ 0
								,"TXT_STATUS_PROD_ORDER":/*String*/ ""
								,"UNLOADING_TIME":/*String*/ "00:00:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0012712842"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "CONF"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*int*/ 14
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "CONSTRUCTORA TECHA, S.A. DE C.V."
						,"SOLD_TO_NUMBER":/*String*/ "0050009623"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 12
						,"WARNING":/*String*/ ""
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_DES":/*String*/ "Fibra Polipropileno"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"ORDER_NUMBER":/*String*/ "0012712907"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_DES":/*String*/ "Fibra Polipropileno"
								,"ITEM_NUMBER":/*String*/ "1003"
								,"ORDER_NUMBER":/*String*/ "0012712907"
							}
						])
						,"CONTACT_NAME1":/*String*/ ". ."
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ ""
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "18:00:00"
							,"DESCRIPTION":/*String*/ "D-250-2-C-28-14-1-1-348"
							,"ORDER_NUMBER":/*String*/ "0012712907"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "COATEPEC"
							,"HOUSE_NO_STREET":/*String*/ "CARRETERA COATEPEC - XICO KM 7+650"
							,"JOB_SITE_ID":/*String*/ "E872"
							,"JOB_SITE_NAME":/*String*/ "PAVIMENTOS CARR. COATEPEC - XICO"
							,"POSTAL_CODE":/*String*/ "91500"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0X000000"
								,"COD_STATUS_PROD_ORDER":/*String*/ "10"
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:55:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "1"
								,"DELIVERY_TIME":/*String*/ "18:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"FILL_COLOR":/*String*/ "0XFFFFFF"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "17:35:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L001"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "D-250-2-C-28-14-1-1-348"
								,"MATERIAL_ID":/*String*/ "000000000010002966"
								,"MEP_TOTCOST":/*Number*/ 6999.3
								,"PLANT":/*String*/ "D162"
								,"PROD_ORDER":/*String*/ "700000001188"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*Number*/ 9623.74
								,"TRUCK_VOLUME":/*int*/ 7
								,"TXT_STATUS_PROD_ORDER":/*String*/ "CRTD"
								,"UNLOADING_TIME":/*String*/ "00:15:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0X000000"
								,"COD_STATUS_PROD_ORDER":/*String*/ "10"
								,"CONFIRM_QTY":/*int*/ 3
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:55:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "2"
								,"DELIVERY_TIME":/*String*/ "18:15:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"FILL_COLOR":/*String*/ "0XFFFFFF"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1003"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "17:50:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L002"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 3
								,"MATERIAL_DES":/*String*/ "D-250-2-C-28-14-1-1-348"
								,"MATERIAL_ID":/*String*/ "000000000010002966"
								,"MEP_TOTCOST":/*int*/ 0
								,"PLANT":/*String*/ "D162"
								,"PROD_ORDER":/*String*/ "700000001207"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*Number*/ 4124.46
								,"TRUCK_VOLUME":/*int*/ 7
								,"TXT_STATUS_PROD_ORDER":/*String*/ "CRTD"
								,"UNLOADING_TIME":/*String*/ "00:15:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0XD0E0EB"
								,"COD_STATUS_PROD_ORDER":/*String*/ "0"
								,"CONFIRM_QTY":/*int*/ 10
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:00:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "0"
								,"DELIVERY_TIME":/*String*/ "18:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ "000000000011001548"
								,"FILL_COLOR":/*String*/ "0XD0E0EB"
								,"HLEVEL_ITEM":/*String*/ "0"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTX0"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "3000"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "18:00:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ ""
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 10
								,"MATERIAL_DES":/*String*/ "SERVICIO BOMBEO BP 24M"
								,"MATERIAL_ID":/*String*/ "000000000060000101"
								,"MEP_TOTCOST":/*int*/ 0
								,"PLANT":/*String*/ "D162"
								,"PROD_ORDER":/*String*/ ""
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "20:30:00"
								,"TOT_COST":/*int*/ 0
								,"TRUCK_VOLUME":/*int*/ 0
								,"TXT_STATUS_PROD_ORDER":/*String*/ ""
								,"UNLOADING_TIME":/*String*/ "00:00:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0012712907"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "CONF"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*int*/ 10
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "CEMEX CONCRETOS, S.A. DE C.V."
						,"SOLD_TO_NUMBER":/*String*/ "0000007100"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 12
						,"WARNING":/*String*/ ""
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_DES":/*String*/ "Fibra Polipropileno"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"ORDER_NUMBER":/*String*/ "0012712914"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_DES":/*String*/ "Fibra Polipropileno"
								,"ITEM_NUMBER":/*String*/ "1003"
								,"ORDER_NUMBER":/*String*/ "0012712914"
							}
						])
						,"CONTACT_NAME1":/*String*/ "BETTY ESCAMILLA"
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ " 8186768193"
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "18:00:00"
							,"DESCRIPTION":/*String*/ "M-100-0-C-28-14-0-1-22H"
							,"ORDER_NUMBER":/*String*/ "0012712914"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "ALLENDE"
							,"HOUSE_NO_STREET":/*String*/ "CENTRO DE EDUCACION Y DEPORTE 403"
							,"JOB_SITE_ID":/*String*/ "0065258240"
							,"JOB_SITE_NAME":/*String*/ "(FA) POLIDEPORTIVO ALLENDE (RDQS)"
							,"POSTAL_CODE":/*String*/ "67350"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0X000000"
								,"COD_STATUS_PROD_ORDER":/*String*/ "10"
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:55:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "1"
								,"DELIVERY_TIME":/*String*/ "18:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"FILL_COLOR":/*String*/ "0XFFFFFF"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "17:35:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L001"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "M-100-0-C-28-14-0-1-22H"
								,"MATERIAL_ID":/*String*/ "000000000010002947"
								,"MEP_TOTCOST":/*int*/ 9999
								,"PLANT":/*String*/ "D162"
								,"PROD_ORDER":/*String*/ "700000001194"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*Number*/ 9005.5
								,"TRUCK_VOLUME":/*int*/ 7
								,"TXT_STATUS_PROD_ORDER":/*String*/ "CRTD"
								,"UNLOADING_TIME":/*String*/ "00:15:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0X000000"
								,"COD_STATUS_PROD_ORDER":/*String*/ "10"
								,"CONFIRM_QTY":/*int*/ 3
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:55:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "2"
								,"DELIVERY_TIME":/*String*/ "18:10:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"FILL_COLOR":/*String*/ "0XFFFFFF"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1003"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "17:45:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L002"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 3
								,"MATERIAL_DES":/*String*/ "M-100-0-C-28-14-0-1-22H"
								,"MATERIAL_ID":/*String*/ "000000000010002947"
								,"MEP_TOTCOST":/*int*/ 0
								,"PLANT":/*String*/ "D162"
								,"PROD_ORDER":/*String*/ "700000001205"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*Number*/ 3859.5
								,"TRUCK_VOLUME":/*int*/ 7
								,"TXT_STATUS_PROD_ORDER":/*String*/ "CRTD"
								,"UNLOADING_TIME":/*String*/ "00:15:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0XD0E0EB"
								,"COD_STATUS_PROD_ORDER":/*String*/ "0"
								,"CONFIRM_QTY":/*int*/ 10
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:00:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "0"
								,"DELIVERY_TIME":/*String*/ "18:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ "000000000011001545"
								,"FILL_COLOR":/*String*/ "0XD0E0EB"
								,"HLEVEL_ITEM":/*String*/ "0"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTX0"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "3000"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "18:00:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ ""
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 10
								,"MATERIAL_DES":/*String*/ "SERVICIO BOMBEO BP 24M"
								,"MATERIAL_ID":/*String*/ "000000000060000101"
								,"MEP_TOTCOST":/*int*/ 0
								,"PLANT":/*String*/ "D162"
								,"PROD_ORDER":/*String*/ ""
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:40:00"
								,"TOT_COST":/*int*/ 0
								,"TRUCK_VOLUME":/*int*/ 0
								,"TXT_STATUS_PROD_ORDER":/*String*/ ""
								,"UNLOADING_TIME":/*String*/ "00:00:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0012712914"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "CONF"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*int*/ 10
						,"PAYTERM_LABEL":/*String*/ "COD"
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "GUAJARDO Y ASOCIADOS CONSTRUCTORA,"
						,"SOLD_TO_NUMBER":/*String*/ "0050006634"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 12
						,"WARNING":/*String*/ ""
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([])
						,"CONTACT_NAME1":/*String*/ "FERNANDO LEON TELL0"
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ ""
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "00:00:00"
							,"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "SAN PEDRO GARZA GARCIA"
							,"HOUSE_NO_STREET":/*String*/ "MONTES APALACHES S/N"
							,"JOB_SITE_ID":/*String*/ "0065051376"
							,"JOB_SITE_NAME":/*String*/ "CASA DEL TEC 184 (CARG)"
							,"POSTAL_CODE":/*String*/ "66260"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0XC4F9C4"
								,"COD_STATUS_PROD_ORDER":/*String*/ "0"
								,"CONFIRM_QTY":/*int*/ 17
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:00:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "0"
								,"DELIVERY_TIME":/*String*/ "18:30:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ "000000000011001545"
								,"FILL_COLOR":/*String*/ "0XFEF745"
								,"HLEVEL_ITEM":/*String*/ "0"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTX0"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "3000"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "11:00:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ ""
								,"LOAD_STATUS":/*String*/ "TJST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 17
								,"MATERIAL_DES":/*String*/ "SERVICIO BOMBEO BP 24M"
								,"MATERIAL_ID":/*String*/ "000000000060000101"
								,"MEP_TOTCOST":/*int*/ 0
								,"PLANT":/*String*/ "D159"
								,"PROD_ORDER":/*String*/ ""
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "19:15:00"
								,"TOT_COST":/*int*/ 0
								,"TRUCK_VOLUME":/*int*/ 0
								,"TXT_STATUS_PROD_ORDER":/*String*/ ""
								,"UNLOADING_TIME":/*String*/ "00:00:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0012712971"
						,"ORDER_REASON":/*String*/ "954"
						,"ORDER_STATUS":/*String*/ "INPC"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*Number*/ 22.7
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "GONZALEZ GUERRERO CONSTRUCCIONES Y"
						,"SOLD_TO_NUMBER":/*String*/ "0050006835"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 12
						,"WARNING":/*String*/ ""
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([])
						,"CONTACT_NAME1":/*String*/ "FERNANDO LEON TELL0"
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ ""
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "15:06:00"
							,"DESCRIPTION":/*String*/ "M-100-0-C-14-18-1-1-22H"
							,"ORDER_NUMBER":/*String*/ "0012712972"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "SAN PEDRO GARZA GARCIA"
							,"HOUSE_NO_STREET":/*String*/ "MONTES APALACHES S/N"
							,"JOB_SITE_ID":/*String*/ "0065051376"
							,"JOB_SITE_NAME":/*String*/ "CASA DEL TEC 184 (CARG)"
							,"POSTAL_CODE":/*String*/ "66260"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0X000000"
								,"COD_STATUS_PROD_ORDER":/*String*/ "10"
								,"CONFIRM_QTY":/*int*/ 12
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:40:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "2"
								,"DELIVERY_TIME":/*String*/ "15:06:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"FILL_COLOR":/*String*/ "0XFFFFFF"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1002"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "14:56:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L002"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 12
								,"MATERIAL_DES":/*String*/ "M-100-0-C-14-18-1-1-22H"
								,"MATERIAL_ID":/*String*/ "000000000010003022"
								,"MEP_TOTCOST":/*Number*/ 5999.4
								,"PLANT":/*String*/ "D159"
								,"PROD_ORDER":/*String*/ "700000001202"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*Number*/ 9188.88
								,"TRUCK_VOLUME":/*int*/ 12
								,"TXT_STATUS_PROD_ORDER":/*String*/ "CRTD"
								,"UNLOADING_TIME":/*String*/ "00:15:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0X000000"
								,"COD_STATUS_PROD_ORDER":/*String*/ "10"
								,"CONFIRM_QTY":/*int*/ 1
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:40:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "3"
								,"DELIVERY_TIME":/*String*/ "15:51:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"FILL_COLOR":/*String*/ "0XFFFFFF"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1003"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "15:41:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L003"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 1
								,"MATERIAL_DES":/*String*/ "M-100-0-C-14-18-1-1-22H"
								,"MATERIAL_ID":/*String*/ "000000000010003022"
								,"MEP_TOTCOST":/*Number*/ 499.95
								,"PLANT":/*String*/ "D159"
								,"PROD_ORDER":/*String*/ "700000001221"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*Number*/ 765.74
								,"TRUCK_VOLUME":/*int*/ 12
								,"TXT_STATUS_PROD_ORDER":/*String*/ "CRTD"
								,"UNLOADING_TIME":/*String*/ "00:15:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0XD0E0EB"
								,"COD_STATUS_PROD_ORDER":/*String*/ "0"
								,"CONFIRM_QTY":/*int*/ 20
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:00:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "0"
								,"DELIVERY_TIME":/*String*/ "18:30:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ "000000000011001545"
								,"FILL_COLOR":/*String*/ "0XD0E0EB"
								,"HLEVEL_ITEM":/*String*/ "0"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTX0"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "3000"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "12:10:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ ""
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 20
								,"MATERIAL_DES":/*String*/ "SERVICIO BOMBEO BP 24M"
								,"MATERIAL_ID":/*String*/ "000000000060000101"
								,"MEP_TOTCOST":/*int*/ 0
								,"PLANT":/*String*/ "D159"
								,"PROD_ORDER":/*String*/ ""
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "20:25:00"
								,"TOT_COST":/*int*/ 0
								,"TRUCK_VOLUME":/*int*/ 0
								,"TXT_STATUS_PROD_ORDER":/*String*/ ""
								,"UNLOADING_TIME":/*String*/ "00:00:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0012712972"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "INPC"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*int*/ 22
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "GONZALEZ GUERRERO CONSTRUCCIONES Y"
						,"SOLD_TO_NUMBER":/*String*/ "0050006835"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 12
						,"WARNING":/*String*/ ""
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([])
						,"CONTACT_NAME1":/*String*/ "Edgar Rayas Tel: 83051765"
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ ""
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "00:00:00"
							,"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "MONTERREY"
							,"HOUSE_NO_STREET":/*String*/ "AV. IGNACIO MORONES PRIETO O E  400"
							,"JOB_SITE_ID":/*String*/ "D159"
							,"JOB_SITE_NAME":/*String*/ "PD011 MORONES PRIETO"
							,"POSTAL_CODE":/*String*/ "64800"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0XC4F9C4"
								,"COD_STATUS_PROD_ORDER":/*String*/ "0"
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:00:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "0"
								,"DELIVERY_TIME":/*String*/ "12:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ "000000000011001545"
								,"FILL_COLOR":/*String*/ "0XFEF745"
								,"HLEVEL_ITEM":/*String*/ "0"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZRWN"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "3000"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "12:00:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ ""
								,"LOAD_STATUS":/*String*/ "TJST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "SERVICIO BOMBEO BP 24M"
								,"MATERIAL_ID":/*String*/ "000000000060000101"
								,"MEP_TOTCOST":/*int*/ 0
								,"PLANT":/*String*/ "D159"
								,"PROD_ORDER":/*String*/ ""
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "01:15:00"
								,"TOT_COST":/*int*/ 0
								,"TRUCK_VOLUME":/*int*/ 0
								,"TXT_STATUS_PROD_ORDER":/*String*/ ""
								,"UNLOADING_TIME":/*String*/ "00:00:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0012712976"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "INPC"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*int*/ 7
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "CEMEX CONCRETOS, S.A. DE C.V."
						,"SOLD_TO_NUMBER":/*String*/ "0000007100"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 12
						,"WARNING":/*String*/ ""
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([])
						,"CONTACT_NAME1":/*String*/ "ASESOR COMERCIAL"
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ ""
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "12:25:00"
							,"DESCRIPTION":/*String*/ "M-100-0-C-28-14-1-1-22H"
							,"ORDER_NUMBER":/*String*/ "0012712978"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "SAN NICOLAS DE LOS GARZA"
							,"HOUSE_NO_STREET":/*String*/ "PALOMA 211"
							,"JOB_SITE_ID":/*String*/ "0065172022"
							,"JOB_SITE_NAME":/*String*/ "I-CALLE PALOMA (JAN)"
							,"POSTAL_CODE":/*String*/ "66414"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0XF5F67E"
								,"COD_STATUS_PROD_ORDER":/*String*/ "80"
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "01:10:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "2"
								,"DELIVERY_TIME":/*String*/ "12:25:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ "CR3764"
								,"EQUIP_NUMBER":/*String*/ "000000000011003850"
								,"FILL_COLOR":/*String*/ "0XAAEEA3"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1002"
								,"LICENSE":/*String*/ "CR3764"
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "12:00:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L002"
								,"LOAD_STATUS":/*String*/ "TJST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "M-100-0-C-28-14-1-1-22H"
								,"MATERIAL_ID":/*String*/ "000000000010002962"
								,"MEP_TOTCOST":/*Number*/ 4999.47
								,"PLANT":/*String*/ "D162"
								,"PROD_ORDER":/*String*/ "700000001210"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ "TJST"
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*Number*/ 6747.93
								,"TRUCK_VOLUME":/*int*/ 7
								,"TXT_STATUS_PROD_ORDER":/*String*/ "CNF"
								,"UNLOADING_TIME":/*String*/ "00:30:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ "CR3764"
								,"VEHICLE_TYPE":/*String*/ "CW060"
								,"VISIBLE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0012712978"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "INPC"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*int*/ 14
						,"PAYTERM_LABEL":/*String*/ "COD"
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "MANUEL GUADALUPE PRADO GUERRERO"
						,"SOLD_TO_NUMBER":/*String*/ "0050019747"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 12
						,"WARNING":/*String*/ ""
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_DES":/*String*/ "Fibra Polipropileno"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"ORDER_NUMBER":/*String*/ "0012712989"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_DES":/*String*/ "Fibra Polipropileno"
								,"ITEM_NUMBER":/*String*/ "3010"
								,"ORDER_NUMBER":/*String*/ "0012712989"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_DES":/*String*/ "Fibra Polipropileno"
								,"ITEM_NUMBER":/*String*/ "1003"
								,"ORDER_NUMBER":/*String*/ "0012712989"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_DES":/*String*/ "Fibra Polipropileno"
								,"ITEM_NUMBER":/*String*/ "3021"
								,"ORDER_NUMBER":/*String*/ "0012712989"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_DES":/*String*/ "Fibra Polipropileno"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"ORDER_NUMBER":/*String*/ "0012712989"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_DES":/*String*/ "Fibra Polipropileno"
								,"ITEM_NUMBER":/*String*/ "3010"
								,"ORDER_NUMBER":/*String*/ "0012712989"
							}
						])
						,"CONTACT_NAME1":/*String*/ ""
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ ""
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "11:34:00"
							,"DESCRIPTION":/*String*/ "M-100-0-C-14-18-1-1-22H"
							,"ORDER_NUMBER":/*String*/ "0012712989"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "MONTERREY"
							,"HOUSE_NO_STREET":/*String*/ "BADEM ESQUINA CON MONTRAVEL S/N"
							,"JOB_SITE_ID":/*String*/ "E570"
							,"JOB_SITE_NAME":/*String*/ "HABITA MISION DE LOS VIÑEDOS"
							,"POSTAL_CODE":/*String*/ "64800"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0XF5F67E"
								,"COD_STATUS_PROD_ORDER":/*String*/ "80"
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:35:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "1"
								,"DELIVERY_TIME":/*String*/ "11:34:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ "CR3717"
								,"EQUIP_NUMBER":/*String*/ "000000000016001292"
								,"FILL_COLOR":/*String*/ "0XAAEEA3"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"LICENSE":/*String*/ "CR3717"
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "11:29:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L001"
								,"LOAD_STATUS":/*String*/ "TJST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "M-100-0-C-14-18-1-1-22H"
								,"MATERIAL_ID":/*String*/ "000000000010003022"
								,"MEP_TOTCOST":/*Number*/ 4999.47
								,"PLANT":/*String*/ "D163"
								,"PROD_ORDER":/*String*/ "700000001212"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ "ARRV"
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*Number*/ 4999.47
								,"TRUCK_VOLUME":/*int*/ 7
								,"TXT_STATUS_PROD_ORDER":/*String*/ "CNF"
								,"UNLOADING_TIME":/*String*/ "00:15:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ "CR3717"
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0X000000"
								,"COD_STATUS_PROD_ORDER":/*String*/ "10"
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:35:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "2"
								,"DELIVERY_TIME":/*String*/ "12:09:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"FILL_COLOR":/*String*/ "0XFFFFFF"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1003"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "12:04:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L002"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 14
								,"MATERIAL_DES":/*String*/ "M-100-0-C-14-18-1-1-22H"
								,"MATERIAL_ID":/*String*/ "000000000010003022"
								,"MEP_TOTCOST":/*Number*/ 4999.47
								,"PLANT":/*String*/ "D163"
								,"PROD_ORDER":/*String*/ "700000001216"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*Number*/ 4999.47
								,"TRUCK_VOLUME":/*int*/ 12
								,"TXT_STATUS_PROD_ORDER":/*String*/ "CRTD"
								,"UNLOADING_TIME":/*String*/ "00:15:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0XC4F9C4"
								,"COD_STATUS_PROD_ORDER":/*String*/ "20"
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:35:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "1"
								,"DELIVERY_TIME":/*String*/ "11:34:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ "CR3717"
								,"EQUIP_NUMBER":/*String*/ "000000000016001292"
								,"FILL_COLOR":/*String*/ "0XC4F9C4"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "3010"
								,"LICENSE":/*String*/ "CR3717"
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "11:29:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L001"
								,"LOAD_STATUS":/*String*/ "ASSG"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "M-100-0-C-14-18-1-1-22H"
								,"MATERIAL_ID":/*String*/ "000000000010003022"
								,"MEP_TOTCOST":/*Number*/ 4999.47
								,"PLANT":/*String*/ "D163"
								,"PROD_ORDER":/*String*/ "700000001215"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ "ASSN"
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*Number*/ 4999.47
								,"TRUCK_VOLUME":/*int*/ 7
								,"TXT_STATUS_PROD_ORDER":/*String*/ "REL"
								,"UNLOADING_TIME":/*String*/ "00:15:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ "CR3717"
								,"VEHICLE_TYPE":/*String*/ "CW060"
								,"VISIBLE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0012712989"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "INPC"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*Number*/ 15.4
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "CEMEX VIVIENDA, S.A. DE C.V."
						,"SOLD_TO_NUMBER":/*String*/ "0000007955"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 12
						,"WARNING":/*String*/ "X"
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([])
						,"CONTACT_NAME1":/*String*/ "ALAN PANICO"
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ ""
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "15:00:00"
							,"DESCRIPTION":/*String*/ "M-200-0-C-28-14-1-1-22H"
							,"ORDER_NUMBER":/*String*/ "0012713005"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "MONTERREY"
							,"HOUSE_NO_STREET":/*String*/ "AV. CONSTITUCIÓN 444"
							,"JOB_SITE_ID":/*String*/ "E830"
							,"JOB_SITE_NAME":/*String*/ "RMS PTA FLUJO 10A1"
							,"POSTAL_CODE":/*String*/ "64000"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0X000000"
								,"COD_STATUS_PROD_ORDER":/*String*/ "10"
								,"CONFIRM_QTY":/*int*/ 12
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "01:55:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "1"
								,"DELIVERY_TIME":/*String*/ "15:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"FILL_COLOR":/*String*/ "0XFFFFFF"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "14:50:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L001"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 12
								,"MATERIAL_DES":/*String*/ "M-200-0-C-28-14-1-1-22H"
								,"MATERIAL_ID":/*String*/ "000000000010002941"
								,"MEP_TOTCOST":/*Number*/ 8570.52
								,"PLANT":/*String*/ "D159"
								,"PROD_ORDER":/*String*/ "700000001224"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 0
								,"TRUCK_VOLUME":/*int*/ 12
								,"TXT_STATUS_PROD_ORDER":/*String*/ "CRTD"
								,"UNLOADING_TIME":/*String*/ "01:30:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0X000000"
								,"COD_STATUS_PROD_ORDER":/*String*/ "10"
								,"CONFIRM_QTY":/*int*/ 2
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "01:55:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "2"
								,"DELIVERY_TIME":/*String*/ "15:20:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"FILL_COLOR":/*String*/ "0XFFFFFF"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1002"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "15:10:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L002"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 2
								,"MATERIAL_DES":/*String*/ "M-200-0-C-28-14-1-1-22H"
								,"MATERIAL_ID":/*String*/ "000000000010002941"
								,"MEP_TOTCOST":/*Number*/ 1428.42
								,"PLANT":/*String*/ "D159"
								,"PROD_ORDER":/*String*/ "700000001225"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*Number*/ 10705.94
								,"TRUCK_VOLUME":/*int*/ 12
								,"TXT_STATUS_PROD_ORDER":/*String*/ "CRTD"
								,"UNLOADING_TIME":/*String*/ "01:30:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0XD0E0EB"
								,"COD_STATUS_PROD_ORDER":/*String*/ "0"
								,"CONFIRM_QTY":/*int*/ 14
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:00:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "0"
								,"DELIVERY_TIME":/*String*/ "22:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ "000000000011001545"
								,"FILL_COLOR":/*String*/ "0XD0E0EB"
								,"HLEVEL_ITEM":/*String*/ "0"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTX0"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "3000"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "15:00:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ ""
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 14
								,"MATERIAL_DES":/*String*/ "SERVICIO BOMBEO BP 24M"
								,"MATERIAL_ID":/*String*/ "000000000060000101"
								,"MEP_TOTCOST":/*int*/ 0
								,"PLANT":/*String*/ "D159"
								,"PROD_ORDER":/*String*/ ""
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "08:50:00"
								,"TOT_COST":/*int*/ 0
								,"TRUCK_VOLUME":/*int*/ 0
								,"TXT_STATUS_PROD_ORDER":/*String*/ ""
								,"UNLOADING_TIME":/*String*/ "00:00:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0012713005"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "CONF"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*int*/ 14
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "CEMEX CONCRETOS, S.A. DE C.V."
						,"SOLD_TO_NUMBER":/*String*/ "0000007100"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 12
						,"WARNING":/*String*/ ""
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_DES":/*String*/ "Fibra Polipropileno"
								,"ITEM_NUMBER":/*String*/ "2011"
								,"ORDER_NUMBER":/*String*/ "0012713009"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_DES":/*String*/ "Fibra Polipropileno"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"ORDER_NUMBER":/*String*/ "0012713009"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_DES":/*String*/ "Fibra Polipropileno"
								,"ITEM_NUMBER":/*String*/ "1003"
								,"ORDER_NUMBER":/*String*/ "0012713009"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_DES":/*String*/ "Fibra Polipropileno"
								,"ITEM_NUMBER":/*String*/ "1005"
								,"ORDER_NUMBER":/*String*/ "0012713009"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_DES":/*String*/ "Fibra Polipropileno"
								,"ITEM_NUMBER":/*String*/ "2011"
								,"ORDER_NUMBER":/*String*/ "0012713009"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_DES":/*String*/ "Fibra Polipropileno"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"ORDER_NUMBER":/*String*/ "0012713009"
							}
						])
						,"CONTACT_NAME1":/*String*/ ""
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ ""
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "16:00:00"
							,"DESCRIPTION":/*String*/ "M-100-0-C-14-18-1-1-22H"
							,"ORDER_NUMBER":/*String*/ "0012713009"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "MONTERREY"
							,"HOUSE_NO_STREET":/*String*/ "BADEM ESQUINA CON MONTRAVEL S/N"
							,"JOB_SITE_ID":/*String*/ "E570"
							,"JOB_SITE_NAME":/*String*/ "HABITA MISION DE LOS VIÑEDOS"
							,"POSTAL_CODE":/*String*/ "64800"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0XF5F67E"
								,"COD_STATUS_PROD_ORDER":/*String*/ "80"
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:40:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "1"
								,"DELIVERY_TIME":/*String*/ "16:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"FILL_COLOR":/*String*/ "0XAAEEA3"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "15:50:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L001"
								,"LOAD_STATUS":/*String*/ "TJST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "M-100-0-C-14-18-1-1-22H"
								,"MATERIAL_ID":/*String*/ "000000000010003022"
								,"MEP_TOTCOST":/*Number*/ 3332.98
								,"PLANT":/*String*/ "D159"
								,"PROD_ORDER":/*String*/ "700000001226"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ "ARRV"
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*Number*/ 5451.18
								,"TRUCK_VOLUME":/*int*/ 7
								,"TXT_STATUS_PROD_ORDER":/*String*/ "CNF"
								,"UNLOADING_TIME":/*String*/ "00:15:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ "CW060"
								,"VISIBLE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0X000000"
								,"COD_STATUS_PROD_ORDER":/*String*/ "10"
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:40:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "2"
								,"DELIVERY_TIME":/*String*/ "16:35:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"FILL_COLOR":/*String*/ "0XFFFFFF"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1003"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "16:25:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L002"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "M-100-0-C-14-18-1-1-22H"
								,"MATERIAL_ID":/*String*/ "000000000010003022"
								,"MEP_TOTCOST":/*Number*/ 3332.98
								,"PLANT":/*String*/ "D159"
								,"PROD_ORDER":/*String*/ "700000001227"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*Number*/ 5451.18
								,"TRUCK_VOLUME":/*int*/ 7
								,"TXT_STATUS_PROD_ORDER":/*String*/ "CRTD"
								,"UNLOADING_TIME":/*String*/ "00:15:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0X000000"
								,"COD_STATUS_PROD_ORDER":/*String*/ "10"
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:40:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "3"
								,"DELIVERY_TIME":/*String*/ "17:10:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"FILL_COLOR":/*String*/ "0XFFFFFF"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1005"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "17:00:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L003"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "M-100-0-C-14-18-1-1-22H"
								,"MATERIAL_ID":/*String*/ "000000000010003022"
								,"MEP_TOTCOST":/*Number*/ 3332.98
								,"PLANT":/*String*/ "D159"
								,"PROD_ORDER":/*String*/ "700000001228"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*Number*/ 5451.18
								,"TRUCK_VOLUME":/*int*/ 7
								,"TXT_STATUS_PROD_ORDER":/*String*/ "CRTD"
								,"UNLOADING_TIME":/*String*/ "00:15:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0X000000"
								,"COD_STATUS_PROD_ORDER":/*String*/ "10"
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:40:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "1"
								,"DELIVERY_TIME":/*String*/ "16:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"FILL_COLOR":/*String*/ "0XFFFFFF"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "2011"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "15:50:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L001"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "M-100-0-C-14-18-1-1-22H"
								,"MATERIAL_ID":/*String*/ "000000000010003022"
								,"MEP_TOTCOST":/*Number*/ 3332.98
								,"PLANT":/*String*/ "D159"
								,"PROD_ORDER":/*String*/ "700000001241"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*Number*/ 5451.18
								,"TRUCK_VOLUME":/*int*/ 7
								,"TXT_STATUS_PROD_ORDER":/*String*/ "CRTD"
								,"UNLOADING_TIME":/*String*/ "00:15:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0012713009"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "INPC"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*Number*/ 23.1
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "CEMEX VIVIENDA, S.A. DE C.V."
						,"SOLD_TO_NUMBER":/*String*/ "0000007955"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 12
						,"WARNING":/*String*/ ""
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_DES":/*String*/ "Fibra Polipropileno"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"ORDER_NUMBER":/*String*/ "0012713010"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_DES":/*String*/ "Fibra Polipropileno"
								,"ITEM_NUMBER":/*String*/ "1003"
								,"ORDER_NUMBER":/*String*/ "0012713010"
							}
						])
						,"CONTACT_NAME1":/*String*/ "PRUEBA PLAN A RECIBE EN OBRA"
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ " 8123232392343.."
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "16:00:00"
							,"DESCRIPTION":/*String*/ "M-100-0-C-14-18-1-1-22H"
							,"ORDER_NUMBER":/*String*/ "0012713010"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "MONTERREY"
							,"HOUSE_NO_STREET":/*String*/ "ALDAMA 1345"
							,"JOB_SITE_ID":/*String*/ "0065311044"
							,"JOB_SITE_NAME":/*String*/ "LOFT CHINCHULINES"
							,"POSTAL_CODE":/*String*/ "64000"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0XFD675C"
								,"COD_STATUS_PROD_ORDER":/*String*/ "10"
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "01:55:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "1"
								,"DELIVERY_TIME":/*String*/ "16:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"FILL_COLOR":/*String*/ "0XA1E5FF"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "15:50:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L001"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "M-100-0-C-14-18-1-1-22H"
								,"MATERIAL_ID":/*String*/ "000000000010003022"
								,"MEP_TOTCOST":/*Number*/ 90.02
								,"PLANT":/*String*/ "D159"
								,"PROD_ORDER":/*String*/ "700000001230"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*Number*/ 7117.67
								,"TRUCK_VOLUME":/*int*/ 7
								,"TXT_STATUS_PROD_ORDER":/*String*/ "CRTD"
								,"UNLOADING_TIME":/*String*/ "01:30:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0XFD675C"
								,"COD_STATUS_PROD_ORDER":/*String*/ "10"
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "01:55:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "2"
								,"DELIVERY_TIME":/*String*/ "16:05:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"FILL_COLOR":/*String*/ "0XA1E5FF"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1003"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "15:55:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L002"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "M-100-0-C-14-18-1-1-22H"
								,"MATERIAL_ID":/*String*/ "000000000010003022"
								,"MEP_TOTCOST":/*Number*/ 90.02
								,"PLANT":/*String*/ "D159"
								,"PROD_ORDER":/*String*/ "700000001231"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*Number*/ 7117.67
								,"TRUCK_VOLUME":/*int*/ 7
								,"TXT_STATUS_PROD_ORDER":/*String*/ "CRTD"
								,"UNLOADING_TIME":/*String*/ "01:30:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0012713010"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "ADBO"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*Number*/ 15.4
						,"PAYTERM_LABEL":/*String*/ "COD"
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "Cliente - CRM - MR2"
						,"SOLD_TO_NUMBER":/*String*/ "0050999999"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 12
						,"WARNING":/*String*/ ""
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([])
						,"CONTACT_NAME1":/*String*/ "ALAN PANICO"
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ ""
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "22:20:00"
							,"DESCRIPTION":/*String*/ "M-200-0-C-28-14-1-1-22H"
							,"ORDER_NUMBER":/*String*/ "0012713020"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "MONTERREY"
							,"HOUSE_NO_STREET":/*String*/ "AV. CONSTITUCIÓN 444"
							,"JOB_SITE_ID":/*String*/ "E830"
							,"JOB_SITE_NAME":/*String*/ "RMS PTA FLUJO 10A1"
							,"POSTAL_CODE":/*String*/ "64000"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0X000000"
								,"COD_STATUS_PROD_ORDER":/*String*/ "10"
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:40:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "2"
								,"DELIVERY_TIME":/*String*/ "22:20:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"FILL_COLOR":/*String*/ "0XFFFFFF"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1002"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "22:10:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L002"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "M-200-0-C-28-14-1-1-22H"
								,"MATERIAL_ID":/*String*/ "000000000010002941"
								,"MEP_TOTCOST":/*int*/ 0
								,"PLANT":/*String*/ "D159"
								,"PROD_ORDER":/*String*/ "700000001246"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*Number*/ 7473.76
								,"TRUCK_VOLUME":/*int*/ 7
								,"TXT_STATUS_PROD_ORDER":/*String*/ "CRTD"
								,"UNLOADING_TIME":/*String*/ "01:30:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0XD0E0EB"
								,"COD_STATUS_PROD_ORDER":/*String*/ "0"
								,"CONFIRM_QTY":/*int*/ 14
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:00:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "0"
								,"DELIVERY_TIME":/*String*/ "22:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ "000000000011001545"
								,"FILL_COLOR":/*String*/ "0XD0E0EB"
								,"HLEVEL_ITEM":/*String*/ "0"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTX0"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "3000"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "22:00:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ ""
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 14
								,"MATERIAL_DES":/*String*/ "SERVICIO BOMBEO BP 24M"
								,"MATERIAL_ID":/*String*/ "000000000060000101"
								,"MEP_TOTCOST":/*int*/ 0
								,"PLANT":/*String*/ "D159"
								,"PROD_ORDER":/*String*/ ""
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "15:50:00"
								,"TOT_COST":/*int*/ 0
								,"TRUCK_VOLUME":/*int*/ 0
								,"TXT_STATUS_PROD_ORDER":/*String*/ ""
								,"UNLOADING_TIME":/*String*/ "00:00:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0012713020"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "INPC"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*int*/ 14
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "CEMEX CONCRETOS, S.A. DE C.V."
						,"SOLD_TO_NUMBER":/*String*/ "0000007100"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 12
						,"WARNING":/*String*/ ""
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([])
						,"CONTACT_NAME1":/*String*/ "ALAN PANICO"
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ ""
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "22:00:00"
							,"DESCRIPTION":/*String*/ "M-200-0-C-28-14-1-1-22H"
							,"ORDER_NUMBER":/*String*/ "0012713040"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "MONTERREY"
							,"HOUSE_NO_STREET":/*String*/ "AV. CONSTITUCIÓN 444"
							,"JOB_SITE_ID":/*String*/ "E830"
							,"JOB_SITE_NAME":/*String*/ "RMS PTA FLUJO 10A1"
							,"POSTAL_CODE":/*String*/ "64000"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0XF5F67E"
								,"COD_STATUS_PROD_ORDER":/*String*/ "80"
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:40:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "1"
								,"DELIVERY_TIME":/*String*/ "22:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ "CR3400"
								,"EQUIP_NUMBER":/*String*/ "000000000016001453"
								,"FILL_COLOR":/*String*/ "0XAAEEA3"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"LICENSE":/*String*/ "CR3400"
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "21:50:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L001"
								,"LOAD_STATUS":/*String*/ "TJST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "M-200-0-C-28-14-1-1-22H"
								,"MATERIAL_ID":/*String*/ "000000000010002941"
								,"MEP_TOTCOST":/*int*/ 0
								,"PLANT":/*String*/ "D159"
								,"PROD_ORDER":/*String*/ "700000001259"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ "TJST"
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*Number*/ 7473.76
								,"TRUCK_VOLUME":/*int*/ 12
								,"TXT_STATUS_PROD_ORDER":/*String*/ "CNF"
								,"UNLOADING_TIME":/*String*/ "01:30:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ "CR3400"
								,"VEHICLE_TYPE":/*String*/ "CW060"
								,"VISIBLE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0X000000"
								,"COD_STATUS_PROD_ORDER":/*String*/ "10"
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:40:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "2"
								,"DELIVERY_TIME":/*String*/ "22:20:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"FILL_COLOR":/*String*/ "0XFFFFFF"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1002"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "22:10:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L002"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "M-200-0-C-28-14-1-1-22H"
								,"MATERIAL_ID":/*String*/ "000000000010002941"
								,"MEP_TOTCOST":/*int*/ 0
								,"PLANT":/*String*/ "D159"
								,"PROD_ORDER":/*String*/ "700000001260"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*Number*/ 7473.76
								,"TRUCK_VOLUME":/*int*/ 7
								,"TXT_STATUS_PROD_ORDER":/*String*/ "CRTD"
								,"UNLOADING_TIME":/*String*/ "01:30:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0XD0E0EB"
								,"COD_STATUS_PROD_ORDER":/*String*/ "0"
								,"CONFIRM_QTY":/*int*/ 14
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:00:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "0"
								,"DELIVERY_TIME":/*String*/ "22:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ "000000000011001545"
								,"FILL_COLOR":/*String*/ "0XD0E0EB"
								,"HLEVEL_ITEM":/*String*/ "0"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTX0"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "3000"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "22:00:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ ""
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 14
								,"MATERIAL_DES":/*String*/ "SERVICIO BOMBEO BP 24M"
								,"MATERIAL_ID":/*String*/ "000000000060000101"
								,"MEP_TOTCOST":/*int*/ 0
								,"PLANT":/*String*/ "D159"
								,"PROD_ORDER":/*String*/ ""
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "15:50:00"
								,"TOT_COST":/*int*/ 0
								,"TRUCK_VOLUME":/*int*/ 0
								,"TXT_STATUS_PROD_ORDER":/*String*/ ""
								,"UNLOADING_TIME":/*String*/ "00:00:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0012713040"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "INPC"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*int*/ 14
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "CEMEX CONCRETOS, S.A. DE C.V."
						,"SOLD_TO_NUMBER":/*String*/ "0000007100"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 12
						,"WARNING":/*String*/ ""
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([])
						,"CONTACT_NAME1":/*String*/ "ALEJANDRO DEL ROSAL"
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ " 83283000-8328.."
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "00:00:00"
							,"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "SANTA CATARINA"
							,"HOUSE_NO_STREET":/*String*/ "LAS PALMAS 1234"
							,"JOB_SITE_ID":/*String*/ "0065311051"
							,"JOB_SITE_NAME":/*String*/ "CASINO CULIACAN"
							,"POSTAL_CODE":/*String*/ "66350"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0XC4F9C4"
								,"COD_STATUS_PROD_ORDER":/*String*/ "0"
								,"CONFIRM_QTY":/*int*/ 21
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:00:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "0"
								,"DELIVERY_TIME":/*String*/ "19:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"FILL_COLOR":/*String*/ "0XFEF745"
								,"HLEVEL_ITEM":/*String*/ "0"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTX0"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "3000"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "19:00:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ ""
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 21
								,"MATERIAL_DES":/*String*/ "SERVICIO BOMBEO BP 24M"
								,"MATERIAL_ID":/*String*/ "000000000060000101"
								,"MEP_TOTCOST":/*int*/ 0
								,"PLANT":/*String*/ "D163"
								,"PROD_ORDER":/*String*/ ""
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "01:05:00"
								,"TOT_COST":/*int*/ 0
								,"TRUCK_VOLUME":/*int*/ 0
								,"TXT_STATUS_PROD_ORDER":/*String*/ ""
								,"UNLOADING_TIME":/*String*/ "00:00:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0012713059"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "ADBO"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*int*/ 21
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "Cliente - CRM - MR2"
						,"SOLD_TO_NUMBER":/*String*/ "0050999999"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 7
						,"WARNING":/*String*/ ""
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([])
						,"CONTACT_NAME1":/*String*/ "ALEJANDRA REYES"
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ " 11112222-1111.."
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "18:30:00"
							,"DESCRIPTION":/*String*/ "D-250-2-C-28-14-1-1-348"
							,"ORDER_NUMBER":/*String*/ "0012713063"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "SANTA CATARINA"
							,"HOUSE_NO_STREET":/*String*/ "LAS PALMAS 1234"
							,"JOB_SITE_ID":/*String*/ "0065311051"
							,"JOB_SITE_NAME":/*String*/ "CASINO CULIACAN"
							,"POSTAL_CODE":/*String*/ "66350"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0XC4F9C4"
								,"COD_STATUS_PROD_ORDER":/*String*/ "20"
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "01:10:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "1"
								,"DELIVERY_TIME":/*String*/ "18:30:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"FILL_COLOR":/*String*/ "0XC4F9C4"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "18:05:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L001"
								,"LOAD_STATUS":/*String*/ "ASSG"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "D-250-2-C-28-14-1-1-348"
								,"MATERIAL_ID":/*String*/ "000000000010002966"
								,"MEP_TOTCOST":/*Number*/ 4999.47
								,"PLANT":/*String*/ "D162"
								,"PROD_ORDER":/*String*/ "700000001264"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*Number*/ 7366.17
								,"TRUCK_VOLUME":/*int*/ 7
								,"TXT_STATUS_PROD_ORDER":/*String*/ "REL"
								,"UNLOADING_TIME":/*String*/ "00:30:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0012713063"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "INPC"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*int*/ 14
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "Cliente - CRM - MR2"
						,"SOLD_TO_NUMBER":/*String*/ "0050999999"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 7
						,"WARNING":/*String*/ "X"
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_DES":/*String*/ "Fibra Polipropileno"
								,"ITEM_NUMBER":/*String*/ "1003"
								,"ORDER_NUMBER":/*String*/ "0012713066"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_DES":/*String*/ "Fibra Polipropileno"
								,"ITEM_NUMBER":/*String*/ "3010"
								,"ORDER_NUMBER":/*String*/ "0012713066"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_DES":/*String*/ "Fibra Polipropileno"
								,"ITEM_NUMBER":/*String*/ "1003"
								,"ORDER_NUMBER":/*String*/ "0012713066"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_DES":/*String*/ "Fibra Polipropileno"
								,"ITEM_NUMBER":/*String*/ "3010"
								,"ORDER_NUMBER":/*String*/ "0012713066"
							}
						])
						,"CONTACT_NAME1":/*String*/ ""
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ ""
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "19:45:00"
							,"DESCRIPTION":/*String*/ "M-100-0-C-14-18-1-1-22H"
							,"ORDER_NUMBER":/*String*/ "0012713066"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "MONTERREY"
							,"HOUSE_NO_STREET":/*String*/ "BADEM ESQUINA CON MONTRAVEL S/N"
							,"JOB_SITE_ID":/*String*/ "E570"
							,"JOB_SITE_NAME":/*String*/ "HABITA MISION DE LOS VIÑEDOS"
							,"POSTAL_CODE":/*String*/ "64800"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0XF5F67E"
								,"COD_STATUS_PROD_ORDER":/*String*/ "80"
								,"CONFIRM_QTY":/*int*/ 2
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:35:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "2"
								,"DELIVERY_TIME":/*String*/ "19:45:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ "CR3760"
								,"EQUIP_NUMBER":/*String*/ "000000000016001658"
								,"FILL_COLOR":/*String*/ "0XAAEEA3"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1003"
								,"LICENSE":/*String*/ "CR3760"
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "19:40:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L002"
								,"LOAD_STATUS":/*String*/ "TJST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 2
								,"MATERIAL_DES":/*String*/ "M-100-0-C-14-18-1-1-22H"
								,"MATERIAL_ID":/*String*/ "000000000010003022"
								,"MEP_TOTCOST":/*Number*/ 1428.42
								,"PLANT":/*String*/ "D163"
								,"PROD_ORDER":/*String*/ "700000001267"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ "ARRV"
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*Number*/ 1428.42
								,"TRUCK_VOLUME":/*int*/ 7
								,"TXT_STATUS_PROD_ORDER":/*String*/ "CNF"
								,"UNLOADING_TIME":/*String*/ "00:15:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ "CR3760"
								,"VEHICLE_TYPE":/*String*/ "CW060"
								,"VISIBLE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0X000000"
								,"COD_STATUS_PROD_ORDER":/*String*/ "10"
								,"CONFIRM_QTY":/*int*/ 2
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:35:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "2"
								,"DELIVERY_TIME":/*String*/ "19:45:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"FILL_COLOR":/*String*/ "0XFFFFFF"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "3010"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "19:40:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L002"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 2
								,"MATERIAL_DES":/*String*/ "M-100-0-C-14-18-1-1-22H"
								,"MATERIAL_ID":/*String*/ "000000000010003022"
								,"MEP_TOTCOST":/*Number*/ 1428.42
								,"PLANT":/*String*/ "D163"
								,"PROD_ORDER":/*String*/ "700000001274"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*Number*/ 1428.42
								,"TRUCK_VOLUME":/*int*/ 7
								,"TXT_STATUS_PROD_ORDER":/*String*/ "CRTD"
								,"UNLOADING_TIME":/*String*/ "00:15:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0012713066"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "INPC"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*Number*/ 15.4
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "CEMEX VIVIENDA, S.A. DE C.V."
						,"SOLD_TO_NUMBER":/*String*/ "0000007955"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 12
						,"WARNING":/*String*/ ""
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([])
						,"CONTACT_NAME1":/*String*/ "ALEJANDRO DEL ROSAL"
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ " 83283000-8328.."
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "00:00:00"
							,"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "SANTA CATARINA"
							,"HOUSE_NO_STREET":/*String*/ "LAS PALMAS 1234"
							,"JOB_SITE_ID":/*String*/ "0065311051"
							,"JOB_SITE_NAME":/*String*/ "CASINO CULIACAN"
							,"POSTAL_CODE":/*String*/ "66350"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0XC4F9C4"
								,"COD_STATUS_PROD_ORDER":/*String*/ "0"
								,"CONFIRM_QTY":/*int*/ 21
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:00:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "0"
								,"DELIVERY_TIME":/*String*/ "19:30:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ "000000000011001545"
								,"FILL_COLOR":/*String*/ "0XFEF745"
								,"HLEVEL_ITEM":/*String*/ "0"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTX0"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "3000"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "20:00:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ ""
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 21
								,"MATERIAL_DES":/*String*/ "SERVICIO BOMBEO BP 24M"
								,"MATERIAL_ID":/*String*/ "000000000060000101"
								,"MEP_TOTCOST":/*int*/ 0
								,"PLANT":/*String*/ "D163"
								,"PROD_ORDER":/*String*/ ""
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "21:25:00"
								,"TOT_COST":/*int*/ 0
								,"TRUCK_VOLUME":/*int*/ 0
								,"TXT_STATUS_PROD_ORDER":/*String*/ ""
								,"UNLOADING_TIME":/*String*/ "00:00:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0012713069"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "ADBO"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*int*/ 21
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "Cliente - CRM - MR2"
						,"SOLD_TO_NUMBER":/*String*/ "0050999999"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 7
						,"WARNING":/*String*/ ""
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_DES":/*String*/ "Fibra Polipropileno"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"ORDER_NUMBER":/*String*/ "0012713074"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_DES":/*String*/ "Fibra Polipropileno"
								,"ITEM_NUMBER":/*String*/ "3010"
								,"ORDER_NUMBER":/*String*/ "0012713074"
							}
						])
						,"CONTACT_NAME1":/*String*/ ""
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ ""
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "19:25:00"
							,"DESCRIPTION":/*String*/ "M-100-0-C-14-18-1-1-22H"
							,"ORDER_NUMBER":/*String*/ "0012713074"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "MONTERREY"
							,"HOUSE_NO_STREET":/*String*/ "BADEM ESQUINA CON MONTRAVEL S/N"
							,"JOB_SITE_ID":/*String*/ "E570"
							,"JOB_SITE_NAME":/*String*/ "HABITA MISION DE LOS VIÑEDOS"
							,"POSTAL_CODE":/*String*/ "64800"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0X000000"
								,"COD_STATUS_PROD_ORDER":/*String*/ "10"
								,"CONFIRM_QTY":/*int*/ 5
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:35:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "1"
								,"DELIVERY_TIME":/*String*/ "19:25:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"FILL_COLOR":/*String*/ "0XFFFFFF"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "19:20:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L001"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "M-100-0-C-14-18-1-1-22H"
								,"MATERIAL_ID":/*String*/ "000000000010003022"
								,"MEP_TOTCOST":/*Number*/ 24997.5
								,"PLANT":/*String*/ "D163"
								,"PROD_ORDER":/*String*/ "700000001275"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*Number*/ 24997.5
								,"TRUCK_VOLUME":/*int*/ 12
								,"TXT_STATUS_PROD_ORDER":/*String*/ "CRTD"
								,"UNLOADING_TIME":/*String*/ "00:15:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0XD0E0EB"
								,"COD_STATUS_PROD_ORDER":/*String*/ "0"
								,"CONFIRM_QTY":/*int*/ 5
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:00:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "0"
								,"DELIVERY_TIME":/*String*/ "17:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ "000000000011002133"
								,"FILL_COLOR":/*String*/ "0XD0E0EB"
								,"HLEVEL_ITEM":/*String*/ "0"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTX0"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "3000"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "19:25:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ ""
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 2
								,"MATERIAL_DES":/*String*/ "SERVICIO BOMBEO BP 24M"
								,"MATERIAL_ID":/*String*/ "000000000060000101"
								,"MEP_TOTCOST":/*int*/ 0
								,"PLANT":/*String*/ "D163"
								,"PROD_ORDER":/*String*/ ""
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "18:40:00"
								,"TOT_COST":/*int*/ 0
								,"TRUCK_VOLUME":/*int*/ 0
								,"TXT_STATUS_PROD_ORDER":/*String*/ ""
								,"UNLOADING_TIME":/*String*/ "00:00:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0012713074"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "CONF"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*int*/ 5
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "CEMEX VIVIENDA, S.A. DE C.V."
						,"SOLD_TO_NUMBER":/*String*/ "0000007955"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 12
						,"WARNING":/*String*/ ""
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([])
						,"CONTACT_NAME1":/*String*/ "ING ALFONSO VELARDE"
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ " 8100000000"
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "21:00:00"
							,"DESCRIPTION":/*String*/ "D-250-2-C-28-14-1-1-348"
							,"ORDER_NUMBER":/*String*/ "0012713092"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "MONTERREY"
							,"HOUSE_NO_STREET":/*String*/ "AV REVOLUCION S/N"
							,"JOB_SITE_ID":/*String*/ "0065085181"
							,"JOB_SITE_NAME":/*String*/ "PUENTE CONCHELLO (REG)"
							,"POSTAL_CODE":/*String*/ "64019"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0XF5F67E"
								,"COD_STATUS_PROD_ORDER":/*String*/ "80"
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:50:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "1"
								,"DELIVERY_TIME":/*String*/ "21:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ "CR3824"
								,"EQUIP_NUMBER":/*String*/ "000000000016001676"
								,"FILL_COLOR":/*String*/ "0XAAEEA3"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"LICENSE":/*String*/ "CR3824"
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "20:55:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L001"
								,"LOAD_STATUS":/*String*/ "TJST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "D-250-2-C-28-14-1-1-348"
								,"MATERIAL_ID":/*String*/ "000000000010002966"
								,"MEP_TOTCOST":/*Number*/ 9999.01
								,"PLANT":/*String*/ "D163"
								,"PROD_ORDER":/*String*/ "700000001276"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ "TJST"
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*Number*/ 9999.01
								,"TRUCK_VOLUME":/*int*/ 7
								,"TXT_STATUS_PROD_ORDER":/*String*/ "CNF"
								,"UNLOADING_TIME":/*String*/ "00:30:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ "CR3824"
								,"VEHICLE_TYPE":/*String*/ "CW060"
								,"VISIBLE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0012713092"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "INPC"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*int*/ 7
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "CONSTRUCTORA TECHA, S.A. DE C.V."
						,"SOLD_TO_NUMBER":/*String*/ "0050009623"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 12
						,"WARNING":/*String*/ ""
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([])
						,"CONTACT_NAME1":/*String*/ "ALEJANDRO DEL ROSAL"
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ " 83283000-8328.."
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "20:00:00"
							,"DESCRIPTION":/*String*/ "D-250-2-C-28-14-1-1-348"
							,"ORDER_NUMBER":/*String*/ "0012713093"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "SANTA CATARINA"
							,"HOUSE_NO_STREET":/*String*/ "LAS PALMAS 1234"
							,"JOB_SITE_ID":/*String*/ "0065311051"
							,"JOB_SITE_NAME":/*String*/ "CASINO CULIACAN"
							,"POSTAL_CODE":/*String*/ "66350"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0XFD675C"
								,"COD_STATUS_PROD_ORDER":/*String*/ "0"
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:00:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "1"
								,"DELIVERY_TIME":/*String*/ "20:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"FILL_COLOR":/*String*/ "0XA1E5FF"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "20:00:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L001"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "D-250-2-C-28-14-1-1-348"
								,"MATERIAL_ID":/*String*/ "000000000010002966"
								,"MEP_TOTCOST":/*int*/ 0
								,"PLANT":/*String*/ "D159"
								,"PROD_ORDER":/*String*/ ""
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 0
								,"TRUCK_VOLUME":/*int*/ 7
								,"TXT_STATUS_PROD_ORDER":/*String*/ ""
								,"UNLOADING_TIME":/*String*/ "00:30:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"BORDER_COLOR":/*String*/ "0XFD675C"
								,"COD_STATUS_PROD_ORDER":/*String*/ "0"
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "00:00:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "2"
								,"DELIVERY_TIME":/*String*/ "20:20:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"FILL_COLOR":/*String*/ "0XA1E5FF"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1002"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
								,"LOADING_TIME":/*String*/ "20:20:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L002"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "D-250-2-C-28-14-1-1-348"
								,"MATERIAL_ID":/*String*/ "000000000010002966"
								,"MEP_TOTCOST":/*int*/ 0
								,"PLANT":/*String*/ "D159"
								,"PROD_ORDER":/*String*/ ""
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 0
								,"TRUCK_VOLUME":/*int*/ 7
								,"TXT_STATUS_PROD_ORDER":/*String*/ ""
								,"UNLOADING_TIME":/*String*/ "00:30:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
								,"VISIBLE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0012713093"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "ADBO"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*int*/ 14
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "Cliente - CRM - MR2"
						,"SOLD_TO_NUMBER":/*String*/ "0050999999"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 7
						,"WARNING":/*String*/ ""
					}
				])
				,"IMPORT_DISSO":/*sap.core.wd.context::WDContextNode*/{
					"COST_TYPE_DISPLAY":/*String*/ "1"
					,"DATE":/*Date*/ new Date(2012,4,14,0,0,0)
					,"DATES":new ArrayCollection([
						/*sap.core.wd.context::WDContextNode*/{
							"LARGE_DATE":/*String*/ "Tuesday May 15 2012"
							,"REF_DATE":/*String*/ "20120515"
							,"SHORT_DATE":/*String*/ "MAY 15 2012"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"LARGE_DATE":/*String*/ "Monday May 14 2012"
							,"REF_DATE":/*String*/ "20120514"
							,"SHORT_DATE":/*String*/ "MAY 14 2012"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"LARGE_DATE":/*String*/ "Wednesday May 16 2012"
							,"REF_DATE":/*String*/ "20120516"
							,"SHORT_DATE":/*String*/ "MAY 16 2012"
						}
					])
					,"DEACTIVATE_MENU":/*Boolean*/ false
					,"EQUIPMENT_SELECTED":/*String*/ ""
					,"FABRICATION_PLANT":/*String*/ ""
					,"FLASH_ISLAND_STATUS":/*String*/ "READ_DATA"
					,"IS_DATE_BETWEEN_FUTURE_DAYS":/*Boolean*/ false
					,"ORDER_SELECTED":/*String*/ ""
					,"PLANT":new ArrayCollection([
						/*sap.core.wd.context::WDContextNode*/{
							"EQUIPMENTS":new ArrayCollection([
								/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ "0000296285"
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001263"
									,"EQUIP_LABEL":/*String*/ "CR3608"
									,"FLEET_NUM":/*String*/ "CR3608"
									,"LICENSE_NUM":/*String*/ "CR3608"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000016001263"
									,"STATUS":/*String*/ "AVLB"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
									,"TURN_TIME":/*String*/ "17:50:04"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001273"
									,"EQUIP_LABEL":/*String*/ "CR3648"
									,"FLEET_NUM":/*String*/ "CR3648"
									,"LICENSE_NUM":/*String*/ "CR3648"
									,"LOAD_VOL":/*int*/ 12
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000016001273"
									,"STATUS":/*String*/ "ASSN"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001874"
									,"EQUIP_LABEL":/*String*/ "CR3507"
									,"FLEET_NUM":/*String*/ "CR3507"
									,"LICENSE_NUM":/*String*/ "CR3507"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000016001874"
									,"STATUS":/*String*/ "ATPL"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001291"
									,"EQUIP_LABEL":/*String*/ "CR3716"
									,"FLEET_NUM":/*String*/ "CR3716"
									,"LICENSE_NUM":/*String*/ "CR3716"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000016001291"
									,"STATUS":/*String*/ "ASSN"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016002230"
									,"EQUIP_LABEL":/*String*/ "CR3707"
									,"FLEET_NUM":/*String*/ "CR3707"
									,"LICENSE_NUM":/*String*/ "CR3707"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000016002230"
									,"STATUS":/*String*/ "TJST"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
									,"TURN_TIME":/*String*/ "17:50:09"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001872"
									,"EQUIP_LABEL":/*String*/ "CR3504"
									,"FLEET_NUM":/*String*/ "CR3504"
									,"LICENSE_NUM":/*String*/ "CR3504"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000016001872"
									,"STATUS":/*String*/ "ATPL"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001453"
									,"EQUIP_LABEL":/*String*/ "CR3400"
									,"FLEET_NUM":/*String*/ "CR3400"
									,"LICENSE_NUM":/*String*/ "CR3400"
									,"LOAD_VOL":/*int*/ 12
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000016001453"
									,"STATUS":/*String*/ "TJST"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001871"
									,"EQUIP_LABEL":/*String*/ "CR3503"
									,"FLEET_NUM":/*String*/ "CR3503"
									,"LICENSE_NUM":/*String*/ "CR3503"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000016001871"
									,"STATUS":/*String*/ "ATPL"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001870"
									,"EQUIP_LABEL":/*String*/ "CR3502"
									,"FLEET_NUM":/*String*/ "CR3502"
									,"LICENSE_NUM":/*String*/ "CR3502"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000016001870"
									,"STATUS":/*String*/ "ATPL"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001869"
									,"EQUIP_LABEL":/*String*/ "CR3501"
									,"FLEET_NUM":/*String*/ "CR3501"
									,"LICENSE_NUM":/*String*/ "CR3501"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000016001869"
									,"STATUS":/*String*/ "ATPL"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001855"
									,"EQUIP_LABEL":/*String*/ "CR3401"
									,"FLEET_NUM":/*String*/ "CR3401"
									,"LICENSE_NUM":/*String*/ "CR3401"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000016001855"
									,"STATUS":/*String*/ "ATPL"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001615"
									,"EQUIP_LABEL":/*String*/ "CR3409"
									,"FLEET_NUM":/*String*/ "CR3409"
									,"LICENSE_NUM":/*String*/ "CR3409"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000016001615"
									,"STATUS":/*String*/ "AVLB"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
									,"TURN_TIME":/*String*/ "17:50:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016002159"
									,"EQUIP_LABEL":/*String*/ "CR3406"
									,"FLEET_NUM":/*String*/ "CR3406"
									,"LICENSE_NUM":/*String*/ "CR3406"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000016002159"
									,"STATUS":/*String*/ "TJST"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016002230"
									,"EQUIP_LABEL":/*String*/ "CR3707"
									,"FLEET_NUM":/*String*/ "CR3707"
									,"LICENSE_NUM":/*String*/ "CR3707"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000016002230"
									,"STATUS":/*String*/ "AVLB"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
									,"TURN_TIME":/*String*/ "17:50:09"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016002203"
									,"EQUIP_LABEL":/*String*/ "CR3620"
									,"FLEET_NUM":/*String*/ "CR3620"
									,"LICENSE_NUM":/*String*/ "CR3620"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000016002203"
									,"STATUS":/*String*/ "TJST"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ "0000110098"
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000011002784"
									,"EQUIP_LABEL":/*String*/ "CR0756"
									,"FLEET_NUM":/*String*/ "CR0756"
									,"LICENSE_NUM":/*String*/ "CR0756"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000011002784"
									,"STATUS":/*String*/ "ATPL"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000011003440"
									,"EQUIP_LABEL":/*String*/ ""
									,"FLEET_NUM":/*String*/ ""
									,"LICENSE_NUM":/*String*/ "CR2374"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000011003440"
									,"STATUS":/*String*/ "ASSN"
									,"STATUS_LIGHT":/*String*/ ""
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001912"
									,"EQUIP_LABEL":/*String*/ "CR3745"
									,"FLEET_NUM":/*String*/ "CR3745"
									,"LICENSE_NUM":/*String*/ "CR3745"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000016001912"
									,"STATUS":/*String*/ "ASSN"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ "0000296285"
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016002234"
									,"EQUIP_LABEL":/*String*/ "CR3761"
									,"FLEET_NUM":/*String*/ "CR3761"
									,"LICENSE_NUM":/*String*/ "CR3761"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000016002234"
									,"STATUS":/*String*/ "TJST"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016002231"
									,"EQUIP_LABEL":/*String*/ "CR3747"
									,"FLEET_NUM":/*String*/ "CR3747"
									,"LICENSE_NUM":/*String*/ "CR3747"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000016002231"
									,"STATUS":/*String*/ "ASSN"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001906"
									,"EQUIP_LABEL":/*String*/ "CR3605"
									,"FLEET_NUM":/*String*/ "CR3605"
									,"LICENSE_NUM":/*String*/ "CR3605"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000016001906"
									,"STATUS":/*String*/ "TJST"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
									,"TURN_TIME":/*String*/ "17:49:09"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001906"
									,"EQUIP_LABEL":/*String*/ "CR3605"
									,"FLEET_NUM":/*String*/ "CR3605"
									,"LICENSE_NUM":/*String*/ "CR3605"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000016001906"
									,"STATUS":/*String*/ "AVLB"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
									,"TURN_TIME":/*String*/ "17:49:09"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001905"
									,"EQUIP_LABEL":/*String*/ "CR3604"
									,"FLEET_NUM":/*String*/ "CR3604"
									,"LICENSE_NUM":/*String*/ "CR3604"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000016001905"
									,"STATUS":/*String*/ "ATPL"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016000985"
									,"EQUIP_LABEL":/*String*/ "CR3763"
									,"FLEET_NUM":/*String*/ "CR3763"
									,"LICENSE_NUM":/*String*/ "CR3763"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000016000985"
									,"STATUS":/*String*/ "TJST"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016000986"
									,"EQUIP_LABEL":/*String*/ "CR3766"
									,"FLEET_NUM":/*String*/ "CR3766"
									,"LICENSE_NUM":/*String*/ "CR3766"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000016000986"
									,"STATUS":/*String*/ "ATPL"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001219"
									,"EQUIP_LABEL":/*String*/ "CR3405"
									,"FLEET_NUM":/*String*/ "CR3405"
									,"LICENSE_NUM":/*String*/ "CR3405"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000016001219"
									,"STATUS":/*String*/ "ATPL"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001904"
									,"EQUIP_LABEL":/*String*/ "CR3603"
									,"FLEET_NUM":/*String*/ "CR3603"
									,"LICENSE_NUM":/*String*/ "CR3603"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000016001904"
									,"STATUS":/*String*/ "TJST"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001225"
									,"EQUIP_LABEL":/*String*/ "CR3483"
									,"FLEET_NUM":/*String*/ "CR3483"
									,"LICENSE_NUM":/*String*/ "CR3483"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000016001225"
									,"STATUS":/*String*/ "ATPL"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001235"
									,"EQUIP_LABEL":/*String*/ "CR3500"
									,"FLEET_NUM":/*String*/ "CR3500"
									,"LICENSE_NUM":/*String*/ "CR3500"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D159"
									,"OBJNR":/*String*/ "IE000000000016001235"
									,"STATUS":/*String*/ "ATPL"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
							])
							,"LAND":/*String*/ "MX"
							,"NAME":/*String*/ "MX-PD-011 MORONES PRIETO"
							,"PLANT_ID":/*String*/ "D159"
							,"WORKCENTER":new ArrayCollection([])
							,"ZCLOSE":/*Boolean*/ false
							,"ZOPERATIVE_MODE":/*String*/ "03"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"EQUIPMENTS":new ArrayCollection([
								/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ "0000110098"
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016002158"
									,"EQUIP_LABEL":/*String*/ "CR3404"
									,"FLEET_NUM":/*String*/ "CR3404"
									,"LICENSE_NUM":/*String*/ "CR3404"
									,"LOAD_VOL":/*int*/ 5
									,"MAINTPLANT":/*String*/ "D162"
									,"OBJNR":/*String*/ "IE000000000016002158"
									,"STATUS":/*String*/ "AVLB"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
									,"TURN_TIME":/*String*/ "17:50:21"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001902"
									,"EQUIP_LABEL":/*String*/ "CR3601"
									,"FLEET_NUM":/*String*/ "CR3601"
									,"LICENSE_NUM":/*String*/ "CR3601"
									,"LOAD_VOL":/*int*/ 6
									,"MAINTPLANT":/*String*/ "D162"
									,"OBJNR":/*String*/ "IE000000000016001902"
									,"STATUS":/*String*/ "AVLB"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
									,"TURN_TIME":/*String*/ "17:50:17"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001910"
									,"EQUIP_LABEL":/*String*/ "CR3706"
									,"FLEET_NUM":/*String*/ "CR3706"
									,"LICENSE_NUM":/*String*/ "CR3706"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D162"
									,"OBJNR":/*String*/ "IE000000000016001910"
									,"STATUS":/*String*/ "TJST"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001659"
									,"EQUIP_LABEL":/*String*/ "CR3762"
									,"FLEET_NUM":/*String*/ "CR3762"
									,"LICENSE_NUM":/*String*/ "CR3762"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D162"
									,"OBJNR":/*String*/ "IE000000000016001659"
									,"STATUS":/*String*/ "TJST"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001903"
									,"EQUIP_LABEL":/*String*/ "CR3602"
									,"FLEET_NUM":/*String*/ "CR3602"
									,"LICENSE_NUM":/*String*/ "CR3602"
									,"LOAD_VOL":/*int*/ 12
									,"MAINTPLANT":/*String*/ "D162"
									,"OBJNR":/*String*/ "IE000000000016001903"
									,"STATUS":/*String*/ "ASSN"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016002228"
									,"EQUIP_LABEL":/*String*/ "CR3702"
									,"FLEET_NUM":/*String*/ "CR3702"
									,"LICENSE_NUM":/*String*/ "CR3702"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D162"
									,"OBJNR":/*String*/ "IE000000000016002228"
									,"STATUS":/*String*/ "TJST"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000011003850"
									,"EQUIP_LABEL":/*String*/ "CR3764"
									,"FLEET_NUM":/*String*/ "CR3764"
									,"LICENSE_NUM":/*String*/ "CR3764"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D162"
									,"OBJNR":/*String*/ "IE000000000011003850"
									,"STATUS":/*String*/ "TJST"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ "BPG"
									,"EQUICATGRY":/*String*/ "W"
									,"EQUIPMENT":/*String*/ "000000000013039111"
									,"EQUIP_LABEL":/*String*/ "RMS-DUMMY-D162"
									,"FLEET_NUM":/*String*/ "RMS-DUMMY-D162"
									,"LICENSE_NUM":/*String*/ "RMS-DUMMY-D162"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D162"
									,"OBJNR":/*String*/ "IE000000000013039111"
									,"STATUS":/*String*/ "AVLB"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,3,23,0,0,0)
									,"TURN_TIME":/*String*/ "09:16:25"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016000910"
									,"EQUIP_LABEL":/*String*/ "CR3408"
									,"FLEET_NUM":/*String*/ "CR3408"
									,"LICENSE_NUM":/*String*/ "CR3408"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D162"
									,"OBJNR":/*String*/ "IE000000000016000910"
									,"STATUS":/*String*/ "TJST"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016000950"
									,"EQUIP_LABEL":/*String*/ "CR3607"
									,"FLEET_NUM":/*String*/ "CR3607"
									,"LICENSE_NUM":/*String*/ "CR3607"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D162"
									,"OBJNR":/*String*/ "IE000000000016000950"
									,"STATUS":/*String*/ "TJST"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016000968"
									,"EQUIP_LABEL":/*String*/ "CR3672"
									,"FLEET_NUM":/*String*/ "CR3672"
									,"LICENSE_NUM":/*String*/ "CR3672"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D162"
									,"OBJNR":/*String*/ "IE000000000016000968"
									,"STATUS":/*String*/ "TJST"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001220"
									,"EQUIP_LABEL":/*String*/ "CR3407"
									,"FLEET_NUM":/*String*/ "CR3407"
									,"LICENSE_NUM":/*String*/ "CR3407"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D162"
									,"OBJNR":/*String*/ "IE000000000016001220"
									,"STATUS":/*String*/ "TJST"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001260"
									,"EQUIP_LABEL":/*String*/ "CR3578"
									,"FLEET_NUM":/*String*/ "CR3578"
									,"LICENSE_NUM":/*String*/ "CR3578"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D162"
									,"OBJNR":/*String*/ "IE000000000016001260"
									,"STATUS":/*String*/ "AVLB"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
									,"TURN_TIME":/*String*/ "19:29:48"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001286"
									,"EQUIP_LABEL":/*String*/ "CR3703"
									,"FLEET_NUM":/*String*/ "CR3703"
									,"LICENSE_NUM":/*String*/ "CR3703"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D162"
									,"OBJNR":/*String*/ "IE000000000016001286"
									,"STATUS":/*String*/ "AVLB"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
									,"TURN_TIME":/*String*/ "19:11:34"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001290"
									,"EQUIP_LABEL":/*String*/ "CR3715"
									,"FLEET_NUM":/*String*/ "CR3715"
									,"LICENSE_NUM":/*String*/ "CR3715"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D162"
									,"OBJNR":/*String*/ "IE000000000016001290"
									,"STATUS":/*String*/ "ASSN"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001290"
									,"EQUIP_LABEL":/*String*/ "CR3715"
									,"FLEET_NUM":/*String*/ "CR3715"
									,"LICENSE_NUM":/*String*/ "CR3715"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D162"
									,"OBJNR":/*String*/ "IE000000000016001290"
									,"STATUS":/*String*/ "TJST"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001308"
									,"EQUIP_LABEL":/*String*/ "CR3765"
									,"FLEET_NUM":/*String*/ "CR3765"
									,"LICENSE_NUM":/*String*/ "CR3765"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D162"
									,"OBJNR":/*String*/ "IE000000000016001308"
									,"STATUS":/*String*/ "AVLB"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,15,0,0,0)
									,"TURN_TIME":/*String*/ "11:46:55"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001454"
									,"EQUIP_LABEL":/*String*/ "CR3402"
									,"FLEET_NUM":/*String*/ "CR3402"
									,"LICENSE_NUM":/*String*/ "CR3402"
									,"LOAD_VOL":/*int*/ 9
									,"MAINTPLANT":/*String*/ "D162"
									,"OBJNR":/*String*/ "IE000000000016001454"
									,"STATUS":/*String*/ "ASSN"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001456"
									,"EQUIP_LABEL":/*String*/ "CR3609"
									,"FLEET_NUM":/*String*/ "CR3609"
									,"LICENSE_NUM":/*String*/ "CR3609"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D162"
									,"OBJNR":/*String*/ "IE000000000016001456"
									,"STATUS":/*String*/ "ASSN"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001462"
									,"EQUIP_LABEL":/*String*/ "CR3708"
									,"FLEET_NUM":/*String*/ "CR3708"
									,"LICENSE_NUM":/*String*/ "CR3708"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D162"
									,"OBJNR":/*String*/ "IE000000000016001462"
									,"STATUS":/*String*/ "AVLB"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
									,"TURN_TIME":/*String*/ "17:49:55"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001463"
									,"EQUIP_LABEL":/*String*/ "CR3709"
									,"FLEET_NUM":/*String*/ "CR3709"
									,"LICENSE_NUM":/*String*/ "CR3709"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D162"
									,"OBJNR":/*String*/ "IE000000000016001463"
									,"STATUS":/*String*/ "ASSN"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001616"
									,"EQUIP_LABEL":/*String*/ "CR3410"
									,"FLEET_NUM":/*String*/ "CR3410"
									,"LICENSE_NUM":/*String*/ "CR3410"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D162"
									,"OBJNR":/*String*/ "IE000000000016001616"
									,"STATUS":/*String*/ "ASSN"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001651"
									,"EQUIP_LABEL":/*String*/ "CR3705"
									,"FLEET_NUM":/*String*/ "CR3705"
									,"LICENSE_NUM":/*String*/ "CR3705"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D162"
									,"OBJNR":/*String*/ "IE000000000016001651"
									,"STATUS":/*String*/ "ASSN"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016002229"
									,"EQUIP_LABEL":/*String*/ "CR3704"
									,"FLEET_NUM":/*String*/ "CR3704"
									,"LICENSE_NUM":/*String*/ "CR3704"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D162"
									,"OBJNR":/*String*/ "IE000000000016002229"
									,"STATUS":/*String*/ "ASSN"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
							])
							,"LAND":/*String*/ "MX"
							,"NAME":/*String*/ "MX-PD-019 SAN NICOLAS"
							,"PLANT_ID":/*String*/ "D162"
							,"WORKCENTER":new ArrayCollection([])
							,"ZCLOSE":/*Boolean*/ false
							,"ZOPERATIVE_MODE":/*String*/ "01"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"EQUIPMENTS":new ArrayCollection([
								/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016002157"
									,"EQUIP_LABEL":/*String*/ "CR3403"
									,"FLEET_NUM":/*String*/ "CR3403"
									,"LICENSE_NUM":/*String*/ "CR3403"
									,"LOAD_VOL":/*int*/ 13
									,"MAINTPLANT":/*String*/ "D163"
									,"OBJNR":/*String*/ "IE000000000016002157"
									,"STATUS":/*String*/ "TJST"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001901"
									,"EQUIP_LABEL":/*String*/ "CR3600"
									,"FLEET_NUM":/*String*/ "CR3600"
									,"LICENSE_NUM":/*String*/ "CR3600"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D163"
									,"OBJNR":/*String*/ "IE000000000016001901"
									,"STATUS":/*String*/ "TJST"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001676"
									,"EQUIP_LABEL":/*String*/ "CR3824"
									,"FLEET_NUM":/*String*/ "CR3824"
									,"LICENSE_NUM":/*String*/ "CR3824"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D163"
									,"OBJNR":/*String*/ "IE000000000016001676"
									,"STATUS":/*String*/ "TJST"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001658"
									,"EQUIP_LABEL":/*String*/ "CR3760"
									,"FLEET_NUM":/*String*/ "CR3760"
									,"LICENSE_NUM":/*String*/ "CR3760"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D163"
									,"OBJNR":/*String*/ "IE000000000016001658"
									,"STATUS":/*String*/ "TJST"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
									,"TURN_TIME":/*String*/ "19:52:09"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001658"
									,"EQUIP_LABEL":/*String*/ "CR3760"
									,"FLEET_NUM":/*String*/ "CR3760"
									,"LICENSE_NUM":/*String*/ "CR3760"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D163"
									,"OBJNR":/*String*/ "IE000000000016001658"
									,"STATUS":/*String*/ "AVLB"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
									,"TURN_TIME":/*String*/ "19:52:09"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001652"
									,"EQUIP_LABEL":/*String*/ "CR3746"
									,"FLEET_NUM":/*String*/ "CR3746"
									,"LICENSE_NUM":/*String*/ "CR3746"
									,"LOAD_VOL":/*int*/ 12
									,"MAINTPLANT":/*String*/ "D163"
									,"OBJNR":/*String*/ "IE000000000016001652"
									,"STATUS":/*String*/ "AVLB"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,14,0,0,0)
									,"TURN_TIME":/*String*/ "19:22:07"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ "0000110098"
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001617"
									,"EQUIP_LABEL":/*String*/ "CR3411"
									,"FLEET_NUM":/*String*/ "CR3411"
									,"LICENSE_NUM":/*String*/ "CR3411"
									,"LOAD_VOL":/*int*/ 12
									,"MAINTPLANT":/*String*/ "D163"
									,"OBJNR":/*String*/ "IE000000000016001617"
									,"STATUS":/*String*/ "TJST"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000016001292"
									,"EQUIP_LABEL":/*String*/ "CR3717"
									,"FLEET_NUM":/*String*/ "CR3717"
									,"LICENSE_NUM":/*String*/ "CR3717"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D163"
									,"OBJNR":/*String*/ "IE000000000016001292"
									,"STATUS":/*String*/ "ASSN"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,4,20,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ "BPG"
									,"EQUICATGRY":/*String*/ "W"
									,"EQUIPMENT":/*String*/ "000000000013039114"
									,"EQUIP_LABEL":/*String*/ "RMS-DUMMY-D163"
									,"FLEET_NUM":/*String*/ "RMS-DUMMY-D163"
									,"LICENSE_NUM":/*String*/ "RMS-DUMMY-D163"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D163"
									,"OBJNR":/*String*/ "IE000000000013039114"
									,"STATUS":/*String*/ "AVLB"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,3,24,0,0,0)
									,"TURN_TIME":/*String*/ "18:00:05"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ "BPG"
									,"EQUICATGRY":/*String*/ "W"
									,"EQUIPMENT":/*String*/ "000000000013039114"
									,"EQUIP_LABEL":/*String*/ "RMS-DUMMY-D163"
									,"FLEET_NUM":/*String*/ "RMS-DUMMY-D163"
									,"LICENSE_NUM":/*String*/ "RMS-DUMMY-D163"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D163"
									,"OBJNR":/*String*/ "IE000000000013039114"
									,"STATUS":/*String*/ "ARRV"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2012,3,24,0,0,0)
									,"TURN_TIME":/*String*/ "18:00:05"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
							])
							,"LAND":/*String*/ "MX"
							,"NAME":/*String*/ "MX-PD-020 SAN NICOLAS A"
							,"PLANT_ID":/*String*/ "D163"
							,"WORKCENTER":new ArrayCollection([])
							,"ZCLOSE":/*Boolean*/ false
							,"ZOPERATIVE_MODE":/*String*/ "01"
						}
					])
					,"TIME":/*String*/ "00:00:00"
					,"USER":/*String*/ "E0ASANTOS"
					,"leadselection":/*String*/ "0"
					,"name":/*String*/ "IMPORT_DISSO"
				}
				,"INITIALCONFIG":/*sap.core.wd.context::WDContextNode*/{
					"COLORMAP":new ArrayCollection([
						/*sap.core.wd.context::WDContextNode*/{
							"RGB_STENCIL":/*String*/ "0xffffff"
							,"STATUS":/*String*/ "O_NORMAL"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"RGB_STENCIL":/*String*/ "0xffff00"
							,"STATUS":/*String*/ "O_RNGT"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"RGB_STENCIL":/*String*/ "0xffc000"
							,"STATUS":/*String*/ "O_DELA"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"RGB_STENCIL":/*String*/ "0xffffff"
							,"STATUS":/*String*/ "L_NORMAL"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"RGB_STENCIL":/*String*/ "0xffc000"
							,"STATUS":/*String*/ "L_DELA"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"RGB_STENCIL":/*String*/ "0xffffff,0x0070c0,3"
							,"STATUS":/*String*/ "L_PUMP"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"RGB_STENCIL":/*String*/ "0xffff00"
							,"STATUS":/*String*/ "L_PROV"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"RGB_STENCIL":/*String*/ "0x548dd4"
							,"STATUS":/*String*/ "L_MAKEUP"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"RGB_STENCIL":/*String*/ "0xd8d8d8"
							,"STATUS":/*String*/ "L_DIST"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"RGB_STENCIL":/*String*/ "0x7030a0"
							,"STATUS":/*String*/ "L_COLLECT"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"RGB_STENCIL":/*String*/ "0x92d050"
							,"STATUS":/*String*/ "L_BACHED"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"RGB_STENCIL":/*String*/ "0xb6dde8"
							,"STATUS":/*String*/ "L_ONHOLD"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"RGB_STENCIL":/*String*/ "0x974706"
							,"STATUS":/*String*/ "L_DELIVER"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"RGB_STENCIL":/*String*/ "0xb6dde8,0xff0000,3"
							,"STATUS":/*String*/ "L_RESERVE"
						}
					])
					,"CONVENTION_COLOR":new ArrayCollection([
						/*sap.core.wd.context::WDContextNode*/{
							"BORDER_COLOR":/*String*/ "0XC4F9C4"
							,"CONCEPT":/*String*/ "UNDEFINED"
							,"DESCRIPTION":/*String*/ "Undefined"
							,"FILL_COLOR":/*String*/ "0XFEF745"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"BORDER_COLOR":/*String*/ "0XA1E5FF"
							,"CONCEPT":/*String*/ "TO_BE_CONFIRMED"
							,"DESCRIPTION":/*String*/ "To be confirmed"
							,"FILL_COLOR":/*String*/ "0XA1E5FF"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"BORDER_COLOR":/*String*/ "0XFD675C"
							,"CONCEPT":/*String*/ "TO_BE_CONF_BK"
							,"DESCRIPTION":/*String*/ "To be confirmed Bloc"
							,"FILL_COLOR":/*String*/ "0XA1E5FF"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"BORDER_COLOR":/*String*/ "0XE5E0C2"
							,"CONCEPT":/*String*/ "PICK_UP"
							,"DESCRIPTION":/*String*/ "Pick up"
							,"FILL_COLOR":/*String*/ "0XE5E0C2"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"BORDER_COLOR":/*String*/ "0XFD675C"
							,"CONCEPT":/*String*/ "PICK_UP_BK"
							,"DESCRIPTION":/*String*/ "Pick up Blocked"
							,"FILL_COLOR":/*String*/ "0XE5E0C2"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"BORDER_COLOR":/*String*/ "0X000000"
							,"CONCEPT":/*String*/ "IN_PROCESS"
							,"DESCRIPTION":/*String*/ "In process"
							,"FILL_COLOR":/*String*/ "0XFFFFFF"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"BORDER_COLOR":/*String*/ "0XFD675C"
							,"CONCEPT":/*String*/ "IN_PROCESS_BK"
							,"DESCRIPTION":/*String*/ "In process blocked"
							,"FILL_COLOR":/*String*/ "0XC4F9C4"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"BORDER_COLOR":/*String*/ "0XF5F67E"
							,"CONCEPT":/*String*/ "IN_PROC_BATCH"
							,"DESCRIPTION":/*String*/ "In Process Batcher"
							,"FILL_COLOR":/*String*/ "0XAAEEA3"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"BORDER_COLOR":/*String*/ "0XC4F9C4"
							,"CONCEPT":/*String*/ "IN_PROC_ASSG"
							,"DESCRIPTION":/*String*/ "In Process assigned"
							,"FILL_COLOR":/*String*/ "0XC4F9C4"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"BORDER_COLOR":/*String*/ "0XD0E0EB"
							,"CONCEPT":/*String*/ "PUMP"
							,"DESCRIPTION":/*String*/ "Pump"
							,"FILL_COLOR":/*String*/ "0XD0E0EB"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"BORDER_COLOR":/*String*/ "0XFD675C"
							,"CONCEPT":/*String*/ "PUMP_BK"
							,"DESCRIPTION":/*String*/ "Pump Blocked"
							,"FILL_COLOR":/*String*/ "0XD0E0EB"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"BORDER_COLOR":/*String*/ "0XFD675C"
							,"CONCEPT":/*String*/ "NOT_START_BK"
							,"DESCRIPTION":/*String*/ "Not Started Blocked"
							,"FILL_COLOR":/*String*/ "0XFFFFFF"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"BORDER_COLOR":/*String*/ "0XFD675C"
							,"CONCEPT":/*String*/ "RENEGOTIATED_BK"
							,"DESCRIPTION":/*String*/ "Renegotiate blocked"
							,"FILL_COLOR":/*String*/ "0XFEF745"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"BORDER_COLOR":/*String*/ "0XFEF745"
							,"CONCEPT":/*String*/ "RENEGOTIATED"
							,"DESCRIPTION":/*String*/ "Renegotiated"
							,"FILL_COLOR":/*String*/ "0XFEF745"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"BORDER_COLOR":/*String*/ "0XDBDBDB"
							,"CONCEPT":/*String*/ "VISIBLE_AT_PLANT"
							,"DESCRIPTION":/*String*/ "Visible at plant"
							,"FILL_COLOR":/*String*/ "0XDBDBDB"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"BORDER_COLOR":/*String*/ "0XFCCE8B"
							,"CONCEPT":/*String*/ "DELAYED"
							,"DESCRIPTION":/*String*/ "Delayed"
							,"FILL_COLOR":/*String*/ "0XFCCE8B"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"BORDER_COLOR":/*String*/ "0XFD675C"
							,"CONCEPT":/*String*/ "DELAYED_BK"
							,"DESCRIPTION":/*String*/ "Delayed blocked"
							,"FILL_COLOR":/*String*/ "0XFCCE8B"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"BORDER_COLOR":/*String*/ "0XFD675C"
							,"CONCEPT":/*String*/ "CONFIRM_BK"
							,"DESCRIPTION":/*String*/ "Confirm blocked"
							,"FILL_COLOR":/*String*/ "0XFFFFFF"
						}
					])
					,"FX_OTR_LABELS":new ArrayCollection([
						/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ACTIVE_VEHICLES"
							,"VALUE":/*String*/ "Available Vehicles"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ADDITIONAL_COMMENTS"
							,"VALUE":/*String*/ "Additional Comments"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ADVANCED_BOOKING"
							,"VALUE":/*String*/ "Advanced booking"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ALL_LINK"
							,"VALUE":/*String*/ "[ All ]"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ALL_LINK_TOOLTIP"
							,"VALUE":/*String*/ "Show All Orders"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ALTERNATIVE_TEXT"
							,"VALUE":/*String*/ "Alternative Text"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_AMOUNT_TO_DISPATCH"
							,"VALUE":/*String*/ "Amount to Dispatch"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ANTICIPATED_LOADS"
							,"VALUE":/*String*/ "Anticipated loads"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ASSIGNED"
							,"VALUE":/*String*/ "Assigned"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ASSIGNED_LOADS_HEADER"
							,"VALUE":/*String*/ "Assigned Loads"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ASSIGNED_LOADS_HEADER_TOOLTIP"
							,"VALUE":/*String*/ "Assigned Loads"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ASSIGNED_VEHICLE"
							,"VALUE":/*String*/ "Assigned Vehicle"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ASSIGNED_VOLUME"
							,"VALUE":/*String*/ "Assigned volume"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ASSIGN_ANTICIPATED_LOAD"
							,"VALUE":/*String*/ "Assign Anticipated Load"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ASSIGN_CHANGE_VEHICLE"
							,"VALUE":/*String*/ "Assign Change Vehicle"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ASSIGN_PLANT_NO_VEHICLE"
							,"VALUE":/*String*/ "The load can not be dispatched without a vehicle. Please assign a vehicle and retry again."
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ASSIGN_PLANT_WARNING"
							,"VALUE":/*String*/ "Assign to Plant. Warning!"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ASSIGN_TO_PLANT"
							,"VALUE":/*String*/ "Assign to Plant"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ASSIGN_VEHICLE"
							,"VALUE":/*String*/ "Assign Vehicle"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ASSIGN_VEHICLE_HEADER"
							,"VALUE":/*String*/ "Assign Vehicle"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_BATCH"
							,"VALUE":/*String*/ "Batch"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_BTN_CONFIRM"
							,"VALUE":/*String*/ "Confirm"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_BTN_IN_PLANT"
							,"VALUE":/*String*/ "In Plant"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CALL_BACK"
							,"VALUE":/*String*/ "Call back"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CALL_BACK_BLOCKED"
							,"VALUE":/*String*/ "Call back blocked"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CANCEL"
							,"VALUE":/*String*/ "Cancel"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CANCELED_TOOLTIP"
							,"VALUE":/*String*/ "Canceled"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CANCELLED"
							,"VALUE":/*String*/ "Cancelled"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CHANGE_DELIVERY_TIME"
							,"VALUE":/*String*/ "Change Delivery Time"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CHANGE_DELIVERY_TIME_HEADER"
							,"VALUE":/*String*/ "Change Delivery Time"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CHANGE_FREQUENCY"
							,"VALUE":/*String*/ "Change Frequency"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CHANGE_PLANT"
							,"VALUE":/*String*/ "Change Plant"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CHANGE_VOLUME"
							,"VALUE":/*String*/ "Change Volume"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CHANGE_VOLUME_AFFECTED_HEADER"
							,"VALUE":/*String*/ "Order Volume was affected"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CHANGE_VOLUME_HEADER"
							,"VALUE":/*String*/ "Change Volume"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CHECK_ALL_VEHICLES"
							,"VALUE":/*String*/ "All Vehicles"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CHECK_CLOSE_PLANT"
							,"VALUE":/*String*/ "Close Plant"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_COLD_JOINT_RISK_TITLE"
							,"VALUE":/*String*/ "Cold Joint Risk!"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_COLOR_ADVANCED_BOOKING"
							,"VALUE":/*String*/ "Advanced booking"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_COLOR_BLOCKED"
							,"VALUE":/*String*/ "Blocked"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_COLOR_CALLBACK"
							,"VALUE":/*String*/ "Call back"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_COLOR_CANCELLED"
							,"VALUE":/*String*/ "Cancelled"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_COLOR_COMPLETED"
							,"VALUE":/*String*/ "Color completed"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_COLOR_COMPLETED_DELAYED"
							,"VALUE":/*String*/ "Completed with delayed"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_COLOR_CONFIRMED"
							,"VALUE":/*String*/ "Confirmed"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_COLOR_DELAYED"
							,"VALUE":/*String*/ "Delayed"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_COLOR_IN_PROCESS"
							,"VALUE":/*String*/ "In process"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_COLOR_ORDER_ON_HOLD"
							,"VALUE":/*String*/ "Order on hold"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_COLOR_PUMP_SERVICE"
							,"VALUE":/*String*/ "Pump service"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_COLOR_RENEGOTIATED"
							,"VALUE":/*String*/ "Renegotiated"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_COLOR_SELF_COLLECT"
							,"VALUE":/*String*/ "Self collect"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_COLOR_TO_BE_CONFIRMED"
							,"VALUE":/*String*/ "Color to be confirmed"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_COLOR_VISIBLE_AT_PLANT"
							,"VALUE":/*String*/ "Visible at plant"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CONFIRMED"
							,"VALUE":/*String*/ "Confirmed"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CONFIRM_BK"
							,"VALUE":/*String*/ "Confirm blocked"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CONFIRM_SHIPPING_DETAILS"
							,"VALUE":/*String*/ "Please confirm shipping details"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CONTACT"
							,"VALUE":/*String*/ "Contact"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CONTINUE_PRODUCTION"
							,"VALUE":/*String*/ "Continue Production"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CURRENT_COST_CURRENCY"
							,"VALUE":/*String*/ "$"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CURRENT_COST_LABEL"
							,"VALUE":/*String*/ "Current Cost"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CURRENT_FREQUENCY"
							,"VALUE":/*String*/ "Current Frequency"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CURRENT_PLANT"
							,"VALUE":/*String*/ "Current Plant"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CURRENT_TIME_TOOLTIP"
							,"VALUE":/*String*/ "Current Time"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CUSTOMER"
							,"VALUE":/*String*/ "Customer"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_DELAYED"
							,"VALUE":/*String*/ "Delayed"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_DELAYED_BK"
							,"VALUE":/*String*/ "Delayed blocked"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_DELIVERED_AMOUNT"
							,"VALUE":/*String*/ "Delivered Amount"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_DELIVERED_VOLUME"
							,"VALUE":/*String*/ "Delivered Volume"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_DELIVERY_TIME"
							,"VALUE":/*String*/ "Delivery Time"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_DISPATCH"
							,"VALUE":/*String*/ "Dispatch"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_DISPLAY_PO"
							,"VALUE":/*String*/ "Display PO"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_DISPOSE_LOAD"
							,"VALUE":/*String*/ "Dispose Load"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_DRIVER"
							,"VALUE":/*String*/ "Driver"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_EARLIER_BATCHING"
							,"VALUE":/*String*/ "Earlier Batching"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_EARLIEST_BATCHING"
							,"VALUE":/*String*/ "Earliest Batching"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_EOD_DELIVERY_CONFIRM_PUMPING"
							,"VALUE":/*String*/ "Delivery confirmation pumping services"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_EOD_MAIN_MENU"
							,"VALUE":/*String*/ "Start/End of Day"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_EOD_OPERATIVE_MODE"
							,"VALUE":/*String*/ "Plant Operative Mode"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_EOD_RVP_MSG_ASSIGNED_USER"
							,"VALUE":/*String*/ "worked by the user"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_EOD_RVP_MSG_WORKFLOW"
							,"VALUE":/*String*/ "the workflow id"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_EOD_VEHICLE"
							,"VALUE":/*String*/ "Release Vehicle"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_EOD_VEHICLE_PLANT"
							,"VALUE":/*String*/ "Release Vehicle at Plant"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ESTIMATED_NEW_BATCHING_TIME"
							,"VALUE":/*String*/ "Estimated New Batching Time"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ESTIMATED_TIME_ON_HOLD"
							,"VALUE":/*String*/ "Estimated Time On Hold"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_FABRICATION_PLANT"
							,"VALUE":/*String*/ ""
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_FAMILY"
							,"VALUE":/*String*/ "Family"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_FAM_DESCRIPTION"
							,"VALUE":/*String*/ "Family Descriptioin"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_FORMULA"
							,"VALUE":/*String*/ "Formula"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_FXD_TEST"
							,"VALUE":/*String*/ "Test"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_IN_PROCESS"
							,"VALUE":/*String*/ "In process"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_IN_PROCESS_BK"
							,"VALUE":/*String*/ "In process blocked"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_IN_PROC_ASSG"
							,"VALUE":/*String*/ "In Process assigned"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_IN_PROC_BATCH"
							,"VALUE":/*String*/ "In Process Batcher"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_IN_PROGRESS_VOLUME"
							,"VALUE":/*String*/ "In Progress Volume"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_JOB_SITE"
							,"VALUE":/*String*/ "Job site"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_LICENSE_PLATE"
							,"VALUE":/*String*/ "License Plate"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_LOAD"
							,"VALUE":/*String*/ "Load"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_LOAD2"
							,"VALUE":/*String*/ "Load"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_LOADS_PER_ORDER_HEADER"
							,"VALUE":/*String*/ "Loads per Order"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_LOADS_PER_ORDER_HEADER_TOOLTIP"
							,"VALUE":/*String*/ "Loads per Order"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_LOADS_PER_VEHICLE_HEADER"
							,"VALUE":/*String*/ "Loads Per Vehicle"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_LOADS_PER_VEHICLE_HEADER_TOOLTIP"
							,"VALUE":/*String*/ "Ctrl+Click to Show All"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_LOAD_CONTAINS_CONTRUCTION_PRODUCT"
							,"VALUE":/*String*/ "The selected load contains Construction Product"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_LOAD_LABEL"
							,"VALUE":/*String*/ "Load"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_LOAD_LEVEL"
							,"VALUE":/*String*/ "Load Level"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_MANUAL_BATCH"
							,"VALUE":/*String*/ "Manual Batch"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_MATERIAL_IN_PLANT"
							,"VALUE":/*String*/ "Material in Plant"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_MONITORING_GRAPH_MAIN_MENU"
							,"VALUE":/*String*/ "Monitoring Graph"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_MONITORING_LOADS_PH_GRAPH_OPTION"
							,"VALUE":/*String*/ "Loads per Hour"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_MONITORING_VEHICLES_PH_GRAPH_OPTION"
							,"VALUE":/*String*/ "Vehicles per Hour"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_NEW_DELIVERY_TIME"
							,"VALUE":/*String*/ "New Delivery time"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_NEW_FREQUENCY"
							,"VALUE":/*String*/ "New Frequency"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_NEW_LOADING_TIME"
							,"VALUE":/*String*/ "New Loading time"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_NEW_LOAD_BATCHING_TIME"
							,"VALUE":/*String*/ "New Load Batching Time"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_NEW_VOLUME"
							,"VALUE":/*String*/ "New Volume"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_NEXT_LOAD_BATCHING_TIME"
							,"VALUE":/*String*/ "Next Load Batching Time"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_NOT_START_BK"
							,"VALUE":/*String*/ "Not Started Blocked"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_NUMBER_VEHICLES_RELEASE"
							,"VALUE":/*String*/ "Number of Vehicles suggested for release"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_NUMBER_VEHICLES_RELEASE2"
							,"VALUE":/*String*/ "Number of Vehicles suggested for release"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_OPTIMAL_COST_CURRENCY"
							,"VALUE":/*String*/ "$"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_OPTIMAL_COST_LABEL"
							,"VALUE":/*String*/ "Optimal Cost"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ORDER_AMOUNT"
							,"VALUE":/*String*/ "Order Amount"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ORDER_DETAIL"
							,"VALUE":/*String*/ "Order detail"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ORDER_LABEL"
							,"VALUE":/*String*/ "Order"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ORDER_LEVEL"
							,"VALUE":/*String*/ "Order Level"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ORDER_NUMBER"
							,"VALUE":/*String*/ "Order"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ORDER_NUMBER2"
							,"VALUE":/*String*/ "Order Number"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ORDER_ON_HOLD"
							,"VALUE":/*String*/ "Order on Hold"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ORDER_ON_HOLD_HEADER"
							,"VALUE":/*String*/ "Order On Hold"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ORDER_RENEGOTIATE_HEADER"
							,"VALUE":/*String*/ "Renegotiate Order"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ORDER_TAKING"
							,"VALUE":/*String*/ "Order Taking"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ORDER_TO_CONFIRM_ASK"
							,"VALUE":/*String*/ "Please Contact customer to confirm details."
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ORDER_UNHOLD_CONFIRM"
							,"VALUE":/*String*/ "Has the order been confirmed?"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ORDER_UNHOLD_DESC"
							,"VALUE":/*String*/ "The selected Order belongs to an order on hold.It requires confirmation from customer."
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ORIGINAL_DELIVERY_TIME"
							,"VALUE":/*String*/ "Original delivery time"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PENDING_AMOUNT"
							,"VALUE":/*String*/ "Pending Amount"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PENDING_LOADS"
							,"VALUE":/*String*/ "Pending Loads"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PENDING_VOLUME"
							,"VALUE":/*String*/ "Pending Quantity"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PICK_UP"
							,"VALUE":/*String*/ "Pick up"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PICK_UP_BK"
							,"VALUE":/*String*/ "Pick up Blocked"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANT"
							,"VALUE":/*String*/ "Plant"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANTSELECTION_MAIN_MENU"
							,"VALUE":/*String*/ "Plant Selection"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANT_ASSIGNED_TOOLTIP"
							,"VALUE":/*String*/ "Assigned"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANT_CONFIRMED_TOOLTIP"
							,"VALUE":/*String*/ "Confirmed"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANT_DELIVERED_TOOLTIP"
							,"VALUE":/*String*/ "Delivered"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANT_DELIVERED_TOOLTIP2"
							,"VALUE":/*String*/ "Plant Delivered"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANT_GROUP"
							,"VALUE":/*String*/ "Plant Group"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANT_GROUP_PLANTS_NAME"
							,"VALUE":/*String*/ "Please specify a Name for this group"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANT_OPERATIVE_MODE"
							,"VALUE":/*String*/ "Plant Operative Mode"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANT_OP_MODE_AUTONOMOUS"
							,"VALUE":/*String*/ "Autonomous Mode"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANT_OP_MODE_CENTRALIZED"
							,"VALUE":/*String*/ "Centralized Mode"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANT_OP_MODE_DISTRIBUTED"
							,"VALUE":/*String*/ "Distributed Mode"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANT_SCHEDULED_TOOLTIP"
							,"VALUE":/*String*/ "Scheduled"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANT_SELECTION_HEADER"
							,"VALUE":/*String*/ "Plant Selection"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANT_SELECT_PLANTS"
							,"VALUE":/*String*/ "You want to create a Group for Plants"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PRODUCT"
							,"VALUE":/*String*/ "Product"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PRODUCTION_ORDER_TOOLTIP"
							,"VALUE":/*String*/ "PO"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PROVISIONAL_LOAD_DETAIL1"
							,"VALUE":/*String*/ "The selected Load is in Provisional status. It requires confirmation from customer."
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PROVISIONAL_LOAD_HEADER"
							,"VALUE":/*String*/ "Provisional Load!"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PROVISIONAL_ORDER_DESC"
							,"VALUE":/*String*/ "The Order that you are trying to dispatch is not complete and requires additional information to be dispatched."
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PROVISIONAL_ORDER_HEADER"
							,"VALUE":/*String*/ "Provisional Order"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PUMP"
							,"VALUE":/*String*/ "Pump"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PUMPING_SERVICE"
							,"VALUE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PUMP_BK"
							,"VALUE":/*String*/ "Pump Blocked"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PUMP_SERVICE"
							,"VALUE":/*String*/ "Pump service"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PUSH_BACK_TIME"
							,"VALUE":/*String*/ "Push back time"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_QUALITY_ADJUSTMENT"
							,"VALUE":/*String*/ "Quality Adjustment"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_REASON"
							,"VALUE":/*String*/ "Reason"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_REDIRECT_LOAD_OPTION"
							,"VALUE":/*String*/ "Redirect Load"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_REFRESH_TOOLTIP"
							,"VALUE":/*String*/ "Refresh"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_RELEASED_VEHICLES"
							,"VALUE":/*String*/ "Released Vehicles"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_RELEASE_ACTIVE_VEHICLES_HEADER"
							,"VALUE":/*String*/ "Release Active Vehicles"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_RELEASE_VEHICLE"
							,"VALUE":/*String*/ "Release Vehicle"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_RENEGOTIATE"
							,"VALUE":/*String*/ "Renegotiate"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_RENEGOTIATED"
							,"VALUE":/*String*/ "Renegotiated"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_RENEGOTIATED_BK"
							,"VALUE":/*String*/ "Renegotiate blocked"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_RENEGOTIATION_LABEL"
							,"VALUE":/*String*/ "Renegotiate"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_RESTART_LOAD"
							,"VALUE":/*String*/ "Restart Load"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_RETURNED_LOAD_MAIN_MENU"
							,"VALUE":/*String*/ "Returned Load"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_RETURNED_LOAD_OPTION"
							,"VALUE":/*String*/ "Returned Load"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_RE_PRINT"
							,"VALUE":/*String*/ "Reprint"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_RE_USE"
							,"VALUE":/*String*/ "Reuse"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_SCHEDULED_VOLUME"
							,"VALUE":/*String*/ "Scheduled Volume"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_SEARCH_VEHICLE"
							,"VALUE":/*String*/ "Search Vehicle"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_SHOW_ALL_TOOLTIP"
							,"VALUE":/*String*/ "Show All"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_SIMULATE"
							,"VALUE":/*String*/ "Simulate"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_START_BATCHING"
							,"VALUE":/*String*/ "Start Batching"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_STATUS"
							,"VALUE":/*String*/ "Status"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_SUPPORT_PLANT"
							,"VALUE":/*String*/ "Support Plant"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_TECHNICAL_DESCRIPTION"
							,"VALUE":/*String*/ "Technical Description"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_TELEPHONE_SHORT"
							,"VALUE":/*String*/ "Tel"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_TIME_ON_HOLD"
							,"VALUE":/*String*/ "Delivery time"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_TOB_BE_CONFIRMED_BLOCKED"
							,"VALUE":/*String*/ "Tob be Confirmed Blocked"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_TOOLTIP_SEARCH_BY"
							,"VALUE":/*String*/ "Search by"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_TO_BE_CONFIRMED"
							,"VALUE":/*String*/ "To be confirmed"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_TO_BE_CONFIRMED_TOOLTIP"
							,"VALUE":/*String*/ "To be confirmed"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_TO_BE_CONF_BK"
							,"VALUE":/*String*/ "To be confirmed  Blocked"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_UNASSIGN_TO_PLANT"
							,"VALUE":/*String*/ "Unassigned to plant"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_UNDEFINED"
							,"VALUE":/*String*/ "Undefined"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_VEHICLE"
							,"VALUE":/*String*/ "Vehicle"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_VEHICLE_CONFIRMED_TOOLTIP"
							,"VALUE":/*String*/ "Confirmed"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_VEHICLE_DELIVERED_TOOLTIP"
							,"VALUE":/*String*/ "Delivered"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_VEHICLE_ID"
							,"VALUE":/*String*/ "Vehicle ID"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_VEHICLE_IN_PLANT"
							,"VALUE":/*String*/ "Vehicle in Plant"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_VEHICLE_TO_BE_CONFIRMED_TOOLTIP"
							,"VALUE":/*String*/ "To be confirmed"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_VH_IN_PLANT"
							,"VALUE":/*String*/ "Vehicle Registration"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_VISIBLE_AT_PLANT"
							,"VALUE":/*String*/ "Visible at plant"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_VISUALIZATION_MAIN_MENU"
							,"VALUE":/*String*/ "Visualization"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_VOLUME"
							,"VALUE":/*String*/ "Volume"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_WARNING"
							,"VALUE":/*String*/ "Warning"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_WORKCENTER"
							,"VALUE":/*String*/ "Work Center"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ZOOM_IN_TOOLTIP"
							,"VALUE":/*String*/ "Zoom In"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ZOOM_OUT_TOOLTIP"
							,"VALUE":/*String*/ "Zoom Out"
						}
					])
					,"FX_PARAMETERS":new ArrayCollection([
						/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "ASSI_VEHI_NOT_PL"
							,"VALUE":/*String*/ "1"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "CHANGE_ASSI_VEHI"
							,"VALUE":/*String*/ "1"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "CHANGE_VOL_CASH"
							,"VALUE":/*String*/ "1"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "COLLECT_LOAD_DIS"
							,"VALUE":/*String*/ "1"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "CONSTRUC_PRO_DIS"
							,"VALUE":/*String*/ "1"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "COST_REFRESH_TIM"
							,"VALUE":/*String*/ "11"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "DISP_ID_OR_LICEN"
							,"VALUE":/*String*/ "ID"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "DISP_PUMP_DFE"
							,"VALUE":/*String*/ "1"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "LIFE_SPAN"
							,"VALUE":/*String*/ "1"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "MAX_DISP_DELIVER"
							,"VALUE":/*String*/ "60"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "MAX_DISP_FUTURE"
							,"VALUE":/*String*/ "120"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "MAX_TIME_ANTICIP"
							,"VALUE":/*String*/ "<NO_VALUE>"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "MAX_TIME_ASSI_VE"
							,"VALUE":/*String*/ "600"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "NUM_DAY_NON_AP_T"
							,"VALUE":/*String*/ "2"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "NUM_OPC_DISP_MEP"
							,"VALUE":/*String*/ "5"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "NUM_VEHI_DISP"
							,"VALUE":/*String*/ "5"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "PLANT_VIS_AREA_L"
							,"VALUE":/*String*/ "120"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "PUNCTUAL_MEASURE"
							,"VALUE":/*String*/ "2"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "REFERENCE_PUNTU"
							,"VALUE":/*String*/ "2"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "TIME_LOAD_DELAY"
							,"VALUE":/*String*/ "59"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "TIME_REDIRECT"
							,"VALUE":/*String*/ "120"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "TIME_REFRESH_DFE"
							,"VALUE":/*String*/ "120"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "TIME_SLOTS_DFE"
							,"VALUE":/*String*/ "5"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "VEHIC_MAX_CAPACI"
							,"VALUE":/*String*/ "09"
						}
					])
					,"FX_TVARVC":new ArrayCollection([
						/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ADBO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_ADBO"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZADR"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_ADDITION"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRWA"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_ADDITION"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "AVLB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_ALL_EQUI"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TJST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_ALL_EQUI"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TOPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_ALL_EQUI"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_ASSG"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSN"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_ASSN"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_ASSN_VEHI"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_ASSN_VEHI"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_ASSN_VEHI"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ATPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_ATPL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "AVLB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_AVLB"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TJST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_AVOID_AV"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TOPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_AVOID_AV"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "AVLB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_AVOID_VHP"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "BLCK"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_BLCK"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CMPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CMPL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CMPO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CMPO"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CNCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CNCL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CNCO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CNCO"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "01"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLL_ORDE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XC2DFA9"
							,"LOW":/*String*/ "18"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "18"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XF26966"
							,"LOW":/*String*/ "19"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "19"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XDBDBDB"
							,"LOW":/*String*/ "20"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "20"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XF9E4C9"
							,"LOW":/*String*/ "21"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "21"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XD0E0EB"
							,"LOW":/*String*/ "22"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "22"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XE4DCE2"
							,"LOW":/*String*/ "23"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "23"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XEDDAF0"
							,"LOW":/*String*/ "24"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "24"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XC7E9EE"
							,"LOW":/*String*/ "25"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "25"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XCCE4E4"
							,"LOW":/*String*/ "26"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "26"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XEFEFAF"
							,"LOW":/*String*/ "27"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "27"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XE5E0C2"
							,"LOW":/*String*/ "28"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "28"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XE8E9DB"
							,"LOW":/*String*/ "29"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "29"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XCDEBAB"
							,"LOW":/*String*/ "30"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "30"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFDBA73"
							,"LOW":/*String*/ "31"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "31"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFFFFFF"
							,"LOW":/*String*/ "32"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "32"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFEF745"
							,"LOW":/*String*/ "33"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "33"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFFFC8A"
							,"LOW":/*String*/ "34"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "34"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFFFDBD"
							,"LOW":/*String*/ "35"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "35"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0X000000"
							,"LOW":/*String*/ "0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFF645D"
							,"LOW":/*String*/ "1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XBEEC9C"
							,"LOW":/*String*/ "2"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFD675C"
							,"LOW":/*String*/ "3"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFF7A71"
							,"LOW":/*String*/ "4"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFF978E"
							,"LOW":/*String*/ "5"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFAAA38"
							,"LOW":/*String*/ "6"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "6"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFFB671"
							,"LOW":/*String*/ "7"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "7"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFCCE8B"
							,"LOW":/*String*/ "8"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "8"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XF5F67E"
							,"LOW":/*String*/ "17"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "17"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XBADDDF"
							,"LOW":/*String*/ "16"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "16"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XD5E7F4"
							,"LOW":/*String*/ "15"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "15"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XC1D7F2"
							,"LOW":/*String*/ "14"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "14"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XB7CEEE"
							,"LOW":/*String*/ "13"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "13"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XA1E5FF"
							,"LOW":/*String*/ "12"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "12"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XC4F9C4"
							,"LOW":/*String*/ "11"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "11"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XAAEEA3"
							,"LOW":/*String*/ "10"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "10"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0X95D78F"
							,"LOW":/*String*/ "9"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "9"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XD0E0EB/0XD0E0EB"
							,"LOW":/*String*/ "PUMP"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_LAB"
							,"NUMB":/*String*/ "9"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XD0E0EB/0XFD675C"
							,"LOW":/*String*/ "PUMP_BK"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_LAB"
							,"NUMB":/*String*/ "10"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFFFFFF/0XFD675C"
							,"LOW":/*String*/ "NOT_START_BK"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_LAB"
							,"NUMB":/*String*/ "11"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFEF745/0XFD675C"
							,"LOW":/*String*/ "RENEGOTIATED_BK"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_LAB"
							,"NUMB":/*String*/ "12"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFEF745/0XFEF745"
							,"LOW":/*String*/ "RENEGOTIATED"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_LAB"
							,"NUMB":/*String*/ "13"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XDBDBDB/0XDBDBDB"
							,"LOW":/*String*/ "VISIBLE_AT_PLANT"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_LAB"
							,"NUMB":/*String*/ "14"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFCCE8B/0XFCCE8B"
							,"LOW":/*String*/ "DELAYED"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_LAB"
							,"NUMB":/*String*/ "15"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFCCE8B/0XFD675C"
							,"LOW":/*String*/ "DELAYED_BK"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_LAB"
							,"NUMB":/*String*/ "16"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFFFFFF/0XFD675C"
							,"LOW":/*String*/ "CONFIRM_BK"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_LAB"
							,"NUMB":/*String*/ "17"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XC4F9C4/0XC4F9C4"
							,"LOW":/*String*/ "IN_PROC_ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_LAB"
							,"NUMB":/*String*/ "8"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XAAEEA3/0XF5F67E"
							,"LOW":/*String*/ "IN_PROC_BATCH"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_LAB"
							,"NUMB":/*String*/ "7"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XC4F9C4/0XFD675C"
							,"LOW":/*String*/ "IN_PROCESS_BK"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_LAB"
							,"NUMB":/*String*/ "6"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFFFFFF/0X000000"
							,"LOW":/*String*/ "IN_PROCESS"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_LAB"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XE5E0C2/0XFD675C"
							,"LOW":/*String*/ "PICK_UP_BK"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_LAB"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XE5E0C2/0XE5E0C2"
							,"LOW":/*String*/ "PICK_UP"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_LAB"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XA1E5FF/0XFD675C"
							,"LOW":/*String*/ "TO_BE_CONF_BK"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_LAB"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XA1E5FF/0XA1E5FF"
							,"LOW":/*String*/ "TO_BE_CONFIRMED"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_LAB"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFEF745/0XC4F9C4"
							,"LOW":/*String*/ "UNDEFINED"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_LAB"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRW1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CONCRETE"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CONCRETE"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTCB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CONCRETE"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CONCRETE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LDNG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CONCRET_L"
							,"NUMB":/*String*/ "6"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "BLOCKED"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CONCRET_L"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLI"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CONCRET_L"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TJST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CONCRET_L"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CONCRET_L"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CONCRET_L"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CONCRET_L"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CONF"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CONF"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CP ONLY"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CP"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRWR"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CPTYPE"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZBR1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CPTYPE"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRM2"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CPTYPE"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZBR1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CPTYPE"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTNR"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CPTYPE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTNR"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CPTYPE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "I0002"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CSTAT_REL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "12/3|12/3"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_ADVANCE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "33/3|33/3"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_BLOCKED"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "12/12"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_CALL_BK"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "3/3|3/3"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_CANCELL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "9/9|9/9"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_COMPLET"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "6/6|6/6"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_COMPL_D"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "32/0|32/0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_CONFIRM"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "8/8|8/8"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_DELAYED"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "11/11|11/11"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_IN_PROC"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "11/3|32/3"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_ORDER_O"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "22/22"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_PUMP_SE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "33/33|33/3"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_RENEGOT"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "31/31"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_SELF_CO"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "12/0|12/12"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_TO_BE_C"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "20/20"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_VISI_PL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CMPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_DELIV_VOL"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "UNLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_DELIV_VOL"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "OJOB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_DELIV_VOL"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TJST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_DELIV_VOL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_DLY_PLL_L"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_DLY_PLL_L"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLI"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_DLY_PLL_L"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_DLY_PLL_O"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "RNGT"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_DLY_PLL_O"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ADBO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_DLY_PLL_O"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "INPC"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_DLY_PLL_O"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TCBO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_DLY_PLL_O"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CONF"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_DLY_PLL_O"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "V"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_EQTYP"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "RMS-DUMMY"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_EQ_DUMMY"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_EXT_VEHIC"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_ASL_L"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "INPC"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_ASL_O"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPO_L"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLI"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPO_L"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPO_L"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CONF"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPO_O"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPO_O"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "INPC"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPO_O"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ADBO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPO_O"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPO_O"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "RNGT"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPO_O"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LDNG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPV_L"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPV_L"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLI"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPV_L"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPV_L"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPV_L"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LDNG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPV_O"
							,"NUMB":/*String*/ "6"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "RNGT"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPV_O"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPV_O"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ADBO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPV_O"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "INPC"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPV_O"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPV_O"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CONF"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPV_O"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "MAX_TIME_ASSI_VE"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "12"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "NUM_DAY_NON_AP_T"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "13"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "NUM_OPC_DISP_MEP"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "14"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "NUM_VEHI_DISP"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "15"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "PLANT_VIS_AREA_L"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "16"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "PUNCTUAL_MEASURE"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "17"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "REFERENCE_PUNTU"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "18"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "TIME_LOAD_DELAY"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "19"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "TIME_REDIRECT"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "20"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "TIME_REFRESH_DFE"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "21"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "TIME_SLOTS_DFE"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "22"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "VEHIC_MAX_CAPACI"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "23"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "X, , ,"
							,"LOW":/*String*/ "ASSI_VEHI_NOT_PL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " , , ,X"
							,"LOW":/*String*/ "CHANGE_ASSI_VEHI"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "X, , ,"
							,"LOW":/*String*/ "CHANGE_VOL_CASH"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "COLLECT_LOAD_DIS"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "CONSTRUC_PRO_DIS"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "COST_REFRESH_TIM"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "X, , ,"
							,"LOW":/*String*/ "DISP_ID_OR_LICEN"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "6"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "DISP_PUMP_DFE"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "7"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "LIFE_SPAN"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "8"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "X, , ,"
							,"LOW":/*String*/ "MAX_DISP_DELIVER"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "9"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "X, , ,"
							,"LOW":/*String*/ "MAX_DISP_FUTURE"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "10"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "MAX_TIME_ANTICIP"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "11"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "Y"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_GPS"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "INC"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_GPS_INC"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "Z001"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_GRULG"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ATPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_GV_VEHI"
							,"NUMB":/*String*/ "10"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ARRV"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_GV_VEHI"
							,"NUMB":/*String*/ "9"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TOPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_GV_VEHI"
							,"NUMB":/*String*/ "8"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "UNLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_GV_VEHI"
							,"NUMB":/*String*/ "7"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "INST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_GV_VEHI"
							,"NUMB":/*String*/ "6"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "OJOB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_GV_VEHI"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TJST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_GV_VEHI"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LDNE"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_GV_VEHI"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LDNS"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_GV_VEHI"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSN"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_GV_VEHI"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "AVLB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_GV_VEHI"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ADBO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_HEADSTAT"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "BLCK"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_HEADSTAT"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "RNGT"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_HEADSTAT"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_HOLD"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "999"
							,"LOW":/*String*/ "950"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_HOLD_REAS"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLI"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_HOLI"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRWR"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_CHGPLN"
							,"NUMB":/*String*/ "9"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRM2"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_CHGPLN"
							,"NUMB":/*String*/ "8"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZBR1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_CHGPLN"
							,"NUMB":/*String*/ "7"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRWA"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_CHGPLN"
							,"NUMB":/*String*/ "6"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRW1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_CHGPLN"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_CHGPLN"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTCB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_CHGPLN"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTNR"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_CHGPLN"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZADR"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_CHGPLN"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_CHGPLN"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRWR"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_ONHLDB"
							,"NUMB":/*String*/ "9"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRM2"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_ONHLDB"
							,"NUMB":/*String*/ "8"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZBR1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_ONHLDB"
							,"NUMB":/*String*/ "7"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRWA"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_ONHLDB"
							,"NUMB":/*String*/ "6"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRW1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_ONHLDB"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_ONHLDB"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTCB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_ONHLDB"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTNR"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_ONHLDB"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZADR"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_ONHLDB"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_ONHLDB"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRWN"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_TOTVOL"
							,"NUMB":/*String*/ "8"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_TOTVOL"
							,"NUMB":/*String*/ "7"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRWA"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_TOTVOL"
							,"NUMB":/*String*/ "6"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZADR"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_TOTVOL"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRW1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_TOTVOL"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_TOTVOL"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTCB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_TOTVOL"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_TOTVOL"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTAT"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_IC_TOTVOL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "EQ"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_ID_OR_LIC"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "INPC"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_INPC"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_ADVANCE"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_ADVANCE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_BLOCKED"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_CALL_BK"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_CALL_BK"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_COMPLET"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_COMPLET"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_COMPL_D"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_COMPL_D"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_CONFIRM"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_CONFIRM"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_DELAYED"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_DELAYED"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_IN_PROC"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_IN_PROC"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_ORDER_O"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_ORDER_O"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_PUMP_SE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_RENEGOT"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_SELF_CO"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_SELF_CO"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_TO_BE_C"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_TO_BE_C"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_VISI_PL"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_VISI_PL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ID"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_LABEL_ID"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LP"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_LABEL_PL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LDNG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_LDNG"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "OH"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_LIFSK"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LOAD_SCOPE"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_LOAD_SCOP"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_ADVANCE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_ADVANCE"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0x92d050"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_BACHED"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_BLOCKED"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_BLOCKED"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_CALL_BK"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CNCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_CANCELL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0x7030a0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_COLLECT"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CMPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_COMPLET"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_CONFIRM"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0xffc000"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_DELA"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_DELAYED"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_DELAYED"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0x974706"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_DELIVER"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0xd8d8d8"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_DIST"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CMPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_IN_PROC"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "UNLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_IN_PROC"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "OJOB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_IN_PROC"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TJST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_IN_PROC"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LDNG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_IN_PROC"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_IN_PROC"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0x548dd4"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_MAKEUP"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0xffffff"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_NORMAL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0xb6dde8"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_ONHOLD"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLI"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_ORDER_O"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0xffff00"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_PROV"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0xffffff,0x0070c0,3"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_PUMP"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_PUMP_SE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0X00FF33"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_QUEUED"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_RENEGOT"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_RENEGOT"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_RENEGOT"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0xb6dde8,0xff0000,3"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_RESERVE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_SELF_CO"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_SELF_CO"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_STARTED"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_STARTED"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CNCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_STARTED"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0XFF0000"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_STOPPED"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_TO_BE_C"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_VISI_PL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTAT"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_MAINTYPE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ROH"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_MAT_ROH"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "N"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_NOGPS"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_NOST"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "BLCK"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_NO_CHGPL"
							,"NUMB":/*String*/ "8"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSIGN"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_NO_CHGPL"
							,"NUMB":/*String*/ "7"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LDNG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_NO_CHGPL"
							,"NUMB":/*String*/ "6"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "UNLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_NO_CHGPL"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "OJOB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_NO_CHGPL"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TJST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_NO_CHGPL"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CMPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_NO_CHGPL"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CMPO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_NO_CHGPL"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ADBO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_NO_CHGPL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_NO_ONHOLD"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LDNG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_NO_ONHOLD"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "OJOB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_OJOB"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ORDER_SCOPE"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_ORDE_SCOP"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ADBO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_ADVANCE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "BLCK"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_BLOCKED"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CONF"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_CALL_BK"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_CALL_BK"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "INPC"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_CALL_BK"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CNCO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_CANCELL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CMPO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_COMPLET"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CMPO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_COMPL_D"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CONF"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_CONFIRM"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0xffc000"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_DELA"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CONF"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_DELAYED"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_DELAYED"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "INPC"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_IN_PROC"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0xffffff"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_NORMAL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_ORDER_O"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CONF"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_PUMP_SE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "RNGT"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_RENEGOT"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0xffff00"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_RNGT"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CONF"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_SELF_CO"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_SELF_CO"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_TO_BE_C"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CONF"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_VISI_PL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZCOC"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PAYTERM"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZCOD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PAYTERM"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LDNG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PENDING_L"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "BLOCKED"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PENDING_L"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLI"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PENDING_L"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PENDING_L"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PENDING_L"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PENDING_L"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZV"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PF_DCEMEX"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "03"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PLOPM_A"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "01"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PLOPM_C"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "02"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PLOPM_D"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_ASSGND"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CNCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_ASSGND"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_CHGPLN"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_CHGPLN"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_CHGVEH"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_CHGVEH"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_CHGVEH"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TJST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_DLVTIM"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LDNG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_DLVTIM"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_DLVTIM"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_DLVTIM"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_GNERAL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_GNERAL"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_ONHLDB"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_ONHLDB"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLI"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_ONHLDB"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TJST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_ONHOLD"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LDNG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_ONHOLD"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_ONHOLD"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_ONHOLD"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZREADYMX"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PP_PROFI"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "30"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PP_STATUS"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "40"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PP_STATUS"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "50"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PP_STATUS"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "60"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PP_STATUS"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "70"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PP_STOP"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "70"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PP_STOP"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "80"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PP_S_TJST"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "90"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PP_S_TJST"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRWN"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PROD"
							,"NUMB":/*String*/ "10"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRWR"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PROD"
							,"NUMB":/*String*/ "9"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZBR1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PROD"
							,"NUMB":/*String*/ "8"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRM2"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PROD"
							,"NUMB":/*String*/ "7"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZBR1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PROD"
							,"NUMB":/*String*/ "6"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRW1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PROD"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PROD"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTCB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PROD"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTNR"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PROD"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PROD"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PROD"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "00000006"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PROFILE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "UNLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PR_ASSGND"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "OJOB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PR_ASSGND"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TJST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PR_ASSGND"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LDNG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PR_ASSGND"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PR_ASSGND"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CMPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PR_ONHOLD"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "UNLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PR_ONHOLD"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "OJOB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PR_ONHOLD"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TJST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PR_ONHOLD"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LDNG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PR_ONHOLD"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PR_ONHOLD"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PTT_L_ASS"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CNCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PTT_L_CNC"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TJST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PTT_L_CON"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LDNG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PTT_L_CON"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLI"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PTT_L_CON"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PTT_L_CON"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PTT_L_CON"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CMPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PTT_L_DEL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "OJOB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PTT_L_DEL"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "UNLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PTT_L_DEL"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PTT_L_TBC"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRWN"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PUMP"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PUMP"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "993"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PUMPTYPE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_RENEGOT_L"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_RENEGOT_L"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_RENEGOT_L"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "INPC"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_RENEGOT_O"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_RENEGOT_O"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CONF"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_RENEGOT_O"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "949"
							,"LOW":/*String*/ "900"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_RENG_REAS"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "RNGT"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_RNGT"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRMS0001"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_SD_PROFI"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "FLEET_NUM"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_SEARCH_VH"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TEL:"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_SEP_CNAME"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "02"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_SHIPCOND"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LDNE"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_SHIP_ASSN"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LDNS"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_SHIP_ASSN"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSN"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_SHIP_ASSN"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "OJOB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_SHIP_STAT"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "INST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_SHIP_STAT"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "UNLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_SHIP_STAT"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TOPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_SHIP_STAT"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TJST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_SHIP_STAT"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRM1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_SHTYPE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRM1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_SHTYPE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRM2"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_SHTYPE_PK"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "1002"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_SLOC_ROH"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "E9999"
							,"LOW":/*String*/ "E0000"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_SO_STAT"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "E0008"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STAT"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATLD"
							,"NUMB":/*String*/ "7"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLI"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATLD"
							,"NUMB":/*String*/ "6"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "UNLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATLD"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "OJOB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATLD"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TJST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATLD"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LDNG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATLD"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATLD"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATLD"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "RNGT"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATOR"
							,"NUMB":/*String*/ "6"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATOR"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ADBO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATOR"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "BLCK"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATOR"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "INPC"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATOR"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATOR"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CONF"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATOR"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "20"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATPL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "E0001"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_ST_CREATE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "E0002"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_ST_REL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_TBCL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_TBCO"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZCI4"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_TEXT_ID"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TJST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_TJST"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TOPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_TOPL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "UNLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_UNLD"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "VAA"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VAA"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "80"
							,"LOW":/*String*/ "30"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VAL_PP_ST"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "VA-M-P"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VAMP"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "VAM-S"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VAMS"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZADR"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VATYPE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZADR"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VATYPE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRWA"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VATYPE"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ARRV"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VEHATPL_D"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "Y"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VEHCAT_CX"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "V"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VEHCAT_CX"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "994"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VEHTYPE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "E9999"
							,"LOW":/*String*/ "E0016"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VEH_STAT"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TJST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VH_PLT"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ATPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VH_PLT"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TOPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VH_PLT"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CONF"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VTT_L_CON"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "OJOB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VTT_L_DEL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "UNLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VTT_L_DEL"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CMPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VTT_L_DEL"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VTT_L_TBC"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "02"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_X_SELF_CO"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_ZZ_RET"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "3"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_ZZ_RET"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
					])
					,"LCDS":/*sap.core.wd.context::WDContextNode*/{
						"LCDS_DESTINY":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"DESTINY_ID":/*String*/ "PLANT"
								,"DESTINY_NAME":/*String*/ "PlantPI"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"DESTINY_ID":/*String*/ "PRODUCTION_STATUS"
								,"DESTINY_NAME":/*String*/ "ProductionStatusPI"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"DESTINY_ID":/*String*/ "SALES_ORDER"
								,"DESTINY_NAME":/*String*/ "SalesOrderPI"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"DESTINY_ID":/*String*/ "VEHICLE"
								,"DESTINY_NAME":/*String*/ "VehiclePI"
							}
						])
						,"LCDS_ENDPOINT":/*sap.core.wd.context::WDContextNode*/{
							"CHANNEL_NAME":/*String*/ "weborb-rtmp-messaging"
							,"CHANNEL_TYPE":/*String*/ "weborb"
							,"URL":/*String*/ "rtmp://mxoccrmsrpd01.noam.cemexnet.com:1910/weborb"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "LCDS_ENDPOINT"
						}
						,"leadselection":/*String*/ "0"
						,"name":/*String*/ "LCDS"
					}
					,"PREFIXES":/*sap.core.wd.context::WDContextNode*/{
						"COLORMAP_PREFIX":/*String*/ ""
						,"FX_OTR_PREFIX":/*String*/ "ZMXCSDSLS/"
						,"FX_PARAM_PREFIX":/*String*/ ""
						,"FX_SECURITY_PREFIX":/*String*/ "DM_DISP_"
						,"FX_TVARVC_PREFIX":/*String*/ "Z_CXFM_CSDSLSMX_1005_"
						,"leadselection":/*String*/ "0"
						,"name":/*String*/ "PREFIXES"
					}
					,"SECURITY":/*sap.core.wd.context::WDContextNode*/{
						"EXEPTIONS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_START_BATCHING"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_START_BATCHING"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_PLANT"
								,"CONFIGURATION":/*String*/ "YNN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_ASSIGNED_LOADS"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_SHOW_DATA"
								,"CONFIGURATION":/*String*/ "YNN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_ASSIGNED_LOADS"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_RENEGOTIATE_LOAD"
								,"CONFIGURATION":/*String*/ "YNN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_ASSIGNED_LOADS"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ANTICIPATED_LOADS"
								,"CONFIGURATION":/*String*/ "NNN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_ASSIGNED_LOADS"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_ONE_VEHICLE"
								,"CONFIGURATION":/*String*/ "YNN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "TIP_LOAD"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_ASSIGNED_LOADS"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_START_BATCHING"
								,"CONFIGURATION":/*String*/ "NNN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_ASSIGNED_LOADS"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_UNASSIGN_PLANT"
								,"CONFIGURATION":/*String*/ "YNN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_ASSIGNED_LOADS"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ANTICIPATED_LOADS"
								,"CONFIGURATION":/*String*/ "YNN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_VEHICLE"
								,"CONFIGURATION":/*String*/ "YNN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_PLANT"
								,"CONFIGURATION":/*String*/ "YNN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_PLANT"
								,"CONFIGURATION":/*String*/ "YYN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_DELIVERY_TIME"
								,"CONFIGURATION":/*String*/ "YYN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_FREQUENCY"
								,"CONFIGURATION":/*String*/ "YYN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "DRG_ASSIGN_VEHICLE"
								,"CONFIGURATION":/*String*/ "YNN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_VOLUME"
								,"CONFIGURATION":/*String*/ "YYN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ORDER_ON_HOLD"
								,"CONFIGURATION":/*String*/ "YYN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_SHOW_DATA"
								,"CONFIGURATION":/*String*/ "YYN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "TIP_LOAD"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "DRG_UNASSIGN_VEHICLE"
								,"CONFIGURATION":/*String*/ "YNN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "DRG_CHANGE_DELIVERY_TIME"
								,"CONFIGURATION":/*String*/ "YYN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "DRG_CHANGE_PLANT"
								,"CONFIGURATION":/*String*/ "YYN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ANTICIPATED_LOADS"
								,"CONFIGURATION":/*String*/ "YNN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_VEHICLE"
								,"CONFIGURATION":/*String*/ "YNN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_ONE_VEHICLE"
								,"CONFIGURATION":/*String*/ "YNN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_PLANT"
								,"CONFIGURATION":/*String*/ "YNN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_DELIVERY_TIME"
								,"CONFIGURATION":/*String*/ "YYN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_FREQUENCY"
								,"CONFIGURATION":/*String*/ "YYN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_PLANT"
								,"CONFIGURATION":/*String*/ "YYN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_VOLUME"
								,"CONFIGURATION":/*String*/ "YYN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ORDER_ON_HOLD"
								,"CONFIGURATION":/*String*/ "YYN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_SHOW_DATA"
								,"CONFIGURATION":/*String*/ "YYN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "TIP_LOAD"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "DRG_UNASSIGN_VEHICLE"
								,"CONFIGURATION":/*String*/ "YNN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "DRG_ASSIGN_VEHICLE"
								,"CONFIGURATION":/*String*/ "YNN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "DRG_CHANGE_DELIVERY_TIME"
								,"CONFIGURATION":/*String*/ "YYN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "DRG_CHANGE_PLANT"
								,"CONFIGURATION":/*String*/ "YYN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "VEHICLE_SUPPORT_PLANT"
								,"CONFIGURATION":/*String*/ "YYN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "VEHICLE_RELEASE_VEHICLE"
								,"CONFIGURATION":/*String*/ "YYN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "VEHICLE_VEHICLE_IN_PLANT"
								,"CONFIGURATION":/*String*/ "YYN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
						])
						,"FIELDS_PER_PROCESS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,6,28,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIG_TO_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_ASSIGNED_LOADS"
								,"PSOTM":/*String*/ "20:53:24"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ANTICIPATED_LOADS"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_ASSIGNED_LOADS"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_ASSIGNED_LOADS"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_RENEGOTIATE_LOAD"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_ASSIGNED_LOADS"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_SHOW_DATA"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_ASSIGNED_LOADS"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_START_BATCHING"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_ASSIGNED_LOADS"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_UNASSIGN_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_ASSIGNED_LOADS"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "TIP_LOAD"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_ASSIGNED_LOADS"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ANTICIPATED_LOADS"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_ONE_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_BATCH"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_DELIVERY_TIME"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_FREQUENCY"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_VOLUME"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CONTINUE_PRODUCTION"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_DISPATCH"
								,"ENABLED":/*Boolean*/ false
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_DISPLAY_PO"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ false
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_MANUAL_BATCH"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ORDER_ON_HOLD"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_QUALITY_ADJUSTMENT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_RESTART_LOAD"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_RE_PRINT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_RE_USE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_SHOW_DATA"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_START_BATCHING"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_ASSIGN_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_CHANGE_DELIVERY_TIME"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_CHANGE_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_UNASSIGN_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "TIP_LOAD_PRODUCTION"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ANTICIPATED_LOADS"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_ONE_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_BATCH"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_DELIVERY_TIME"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_FREQUENCY"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_VOLUME"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CONTINUE_PRODUCTION"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_DISPATCH"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_DISPLAY_PO"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_MANUAL_BATCH"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ORDER_ON_HOLD"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_QUALITY_ADJUSTMENT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_RESTART_LOAD"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_RE_PRINT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_RE_USE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_SHOW_DATA"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_START_BATCHING"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_ASSIGN_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_CHANGE_DELIVERY_TIME"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_CHANGE_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_UNASSIGN_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "TIP_LOAD_PRODUCTION"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "VEHICLE_RELEASE_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "VEHICLE_SUPPORT_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "VEHICLE_VEHICLE_IN_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ANTICIPATED_LOADS"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_ASSIGNED_LOADS"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_ASSIGNED_LOADS"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_RENEGOTIATE_LOAD"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_ASSIGNED_LOADS"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_SHOW_DATA"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_ASSIGNED_LOADS"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_START_BATCHING"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_ASSIGNED_LOADS"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_UNASSIGN_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_ASSIGNED_LOADS"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "TIP_LOAD"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_ASSIGNED_LOADS"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ANTICIPATED_LOADS"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_ONE_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_DELIVERY_TIME"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_FREQUENCY"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_VOLUME"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ORDER_ON_HOLD"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_SHOW_DATA"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_START_BATCHING"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_ASSIGN_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_CHANGE_DELIVERY_TIME"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_CHANGE_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_UNASSIGN_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "TIP_LOAD"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ANTICIPATED_LOADS"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_ONE_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_DELIVERY_TIME"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_FREQUENCY"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_VOLUME"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ORDER_ON_HOLD"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_SHOW_DATA"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_START_BATCHING"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_ASSIGN_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_CHANGE_DELIVERY_TIME"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_CHANGE_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_UNASSIGN_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "TIP_LOAD"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "VEHICLE_RELEASE_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "VEHICLE_SUPPORT_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "VEHICLE_VEHICLE_IN_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
						])
						,"leadselection":/*String*/ "0"
						,"name":/*String*/ "SECURITY"
					}
					,"USER_ROLES":/*sap.core.wd.context::WDContextNode*/{
						"IS_AGENT_SERVICE":/*Boolean*/ false
						,"IS_BATCHER":/*Boolean*/ false
						,"IS_DISPATCHER":/*Boolean*/ true
						,"leadselection":/*String*/ "0"
						,"name":/*String*/ "USER_ROLES"
					}
					,"leadselection":/*String*/ "0"
					,"name":/*String*/ "INITIALCONFIG"
				}
			}	//
	}
}