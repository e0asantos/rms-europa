package com.cemex.rms.dispatcher.services.flashislands.impl
{
	import com.cemex.rms.common.flashislands.AbstractFlashIslandService;
	import com.cemex.rms.common.flashislands.MappingHelper;
	import com.cemex.rms.common.flashislands.vo.OTRLabel;
	import com.cemex.rms.common.flashislands.vo.TVARVC;
	import com.cemex.rms.common.push.PushConfigHelper;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.common.utils.DictionaryMap;
	import com.cemex.rms.dispatcher.constants.OTRConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.events.DispatcherConstants;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.helpers.InitializerHelper;
	import com.cemex.rms.dispatcher.services.flashislands.IDispatcherIsland;
	import com.cemex.rms.dispatcher.services.flashislands.events.FlashIslandUnfreezedEvent;
	import com.cemex.rms.dispatcher.services.flashislands.events.FlashIslandsDataEvent;
	import com.cemex.rms.dispatcher.services.flashislands.events.ReturnLogEvent;
	import com.cemex.rms.dispatcher.services.flashislands.requests.DeleteContextServerDataRequest;
	import com.cemex.rms.dispatcher.services.flashislands.requests.RefreshRequest;
	import com.cemex.rms.dispatcher.services.flashislands.vo.DateText;
	import com.cemex.rms.dispatcher.services.flashislands.vo.Equipment;
	import com.cemex.rms.dispatcher.services.flashislands.vo.FlashIslandsData;
	import com.cemex.rms.dispatcher.services.flashislands.vo.Plant;
	import com.cemex.rms.dispatcher.services.flashislands.vo.ProductionData;
	import com.cemex.rms.dispatcher.services.flashislands.vo.SecurityExceptions;
	import com.cemex.rms.dispatcher.services.flashislands.vo.SecurityFieldPerProcess;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	
	
	/**
	 * 
	 * 
	 * 
	 * 
	 */
	public class DispatcherIslandImpl extends AbstractFlashIslandService implements IDispatcherIsland
	{
		/*
		
		Esta variable ayuda a saber cuando el usuario esta situado en otra fecha y se bloquean los menus
		
		*/
		public static var areMenusAvailable:Boolean=true;
		public function DispatcherIslandImpl() {
			super(false);
			disableAppWhileEvent = false;
		}
		
		private var role:String = "Dispatcher";
		public function getUserRole():String{
			return role;
		}
		
		
		public function getColor(colorid:String):String{
			return colorMap.get(colorid);
		}
		
		private var colorMap:DictionaryMap;
		public function getColorMap():DictionaryMap{
			return colorMap;
		}
		public function setUserRole(role:String):void{
			this.role = role;
		}
		public function getPlantNames():ArrayCollection{
			var result:ArrayCollection =  new ArrayCollection();
			for (var i:int = 0 ; i < _plants.length ; i ++ ){
				var plant:Plant = _plants.getItemAt(i) as Plant;
				result.addItem(plant.plantId);
			}
			return result;
		}
		
		public function getEquipmentsPlain():ArrayCollection{
			return DispatcherIslandHelper.getEquipmentsPlain(_plants);
		}
		public function getPlantsPlain():ArrayCollection{
			return DispatcherIslandHelper.getPlantsPlain(_plants);
		}
		
		
		
		public function removeFromAll(target:Object, values:ArrayCollection, field:String):Object{
			
			if (values != null ){
				for ( var i:int = values.length - 1 ; i >= 0   ; i--) {					
					var temp:Object = values.getItemAt(i);
					if (ReflectionHelper.compareObject(target[field],temp[field])){
						return values.removeItemAt(i);
					}
				}
			}
			return null;
		}
		
		
		public function getFromAll(target:Object, values:ArrayCollection, field:String):Object{
			
			if (values != null ){
				for ( var i:int = values.length - 1 ; i >= 0   ; i--) {					
					var temp:Object = values.getItemAt(i);
					if (ReflectionHelper.compareObject(target[field],temp[field])){
						return temp;
					}
				}
			}
			return null;
		}
		public function getPlanningLoads():ArrayCollection{
			return this.getImportDisso()["PLANNING_LOADS"];
		}
		public function updateEquipment(vehicle:Equipment):Object{
			var plant:Plant = getPlantInfo(vehicle.maintplant);
			var plain:Object =  DispatcherIslandHelper.getEquipmentPlain(plant,vehicle);
			//removeFromAll(vehicle,plant.equipments,"equipNumber");
			//var temp:Object = getFromAll(vehicle,plant.equipments,"equipNumber");
			//ReflectionHelper.copySimpleParameters(vehicle,temp);
			//plant.equipments.addItem(vehicle);
			return plain;
		}
		
		
		public function getPlantInfo(plantId:String):Plant{
			var plants:ArrayCollection = getPlants();
			for (var i:int = 0 ; i < plants.length ; i ++){
				var plant:Plant = plants.getItemAt(i) as Plant;
				if (plant.plantId == plantId){
					return plant;
				}
			}
			return null;
		}
		public function getPlantType(plantId:String):String {
			var plant:Plant = getPlantInfo(plantId);
			if (plant == null){
				return "";
			}
			return plant.zoperativeMode;
		}
		
		public function setPlantType(plantId:String,plantType:String,statusCode:String):void{
			var plant:Plant = getPlantInfo(plantId);
			if (plant == null){
				return;
			}
			plant.zoperativeMode = plantType;
			plant.status=statusCode;
		}
		
		/**
		 * Esta funcion 
		 */
		public function updateVehicle(equp:Equipment):Object{
			return null;
		}
		
		public function updatePlantOperationMode(plantId:String, zoperativeMode:String):void{
			var plant:Plant = getPlantInfo(plantId);
			plant.zoperativeMode = zoperativeMode;
		}
		
		
		public function deleteEquipment(equp:Equipment):Object{
			var equipmentDeleted:Boolean = false;
			var newPlant:Plant = null;
			
			
			for (var i:int = 0 ; i < _plants.length ; i ++ ){
				var plant:Plant = _plants.getItemAt(i) as Plant;
				if (plant.plantId == equp.maintplant){
					newPlant = plant;
				}
				if(!equipmentDeleted){
					for (var j:int = 0 ; j < plant.equipments.length ; j ++ ){
						var equipment:Equipment = plant.equipments.getItemAt(j) as Equipment;
						if (equipment.equipment == equp.equipment){
							
							plant.equipments.removeItemAt(j);
							equipmentDeleted = true;
							break;
						}
					}
				}
				
				if (equipmentDeleted && newPlant != null){
					break;
				}
			}
			
			var plantPlain:Object = new Object();
			DispatcherIslandHelper.fillPlantEquipmentPlain(plantPlain,newPlant,equp);
			return plantPlain;
		}
		
		
		public function isEnabledParameter():Boolean{
			return true;
		}
		public override function unfreezed():void{
			super.unfreezed();
			dispatcher.dispatchEvent(new FlashIslandUnfreezedEvent());
		}
		//****************************
		// WebDynpro Varibles Declarations 
		// Para asegurar que estos campos estan setteados 
		public override function getIslandFields():ArrayCollection {
			return new ArrayCollection(["DS_SORDER","DATA","IMPORT_DISSO","INITIALCONFIG","ZPARAMS","ZFIELD_ATTRIBUTES"])
		}
		private function getImportDisso():Object{
			trace("getImportDisso()");
			var something:*=getBufferData()["IMPORT_DISSO"];
			return getBufferData()["IMPORT_DISSO"];
		}
		public function getInitialConfig():Object{
			return getBufferData()["INITIALCONFIG"];
		}
		
		public function getColorPalette():Object{
			return getBufferData()["INITIALCONFIG"]["CONVENTION_COLOR"];
		}
		
		private function getDSSOrder():ArrayCollection{
			return getBufferData()["DS_SORDER"];
		}
		private function getData():Object{
			return getBufferData()["DATA"];
		}
		private function getReturnLog():ArrayCollection{
			if (getData() != null){	
				return getData()["RETURN_LOG"] as ArrayCollection;
			}
			return null;
		}
		protected function getBufferData():Object{
			return buffer.data;
		}
		
		
		private function getUserRoles():Object {
			if (getInitialConfig() != null ){
				return	getInitialConfig()["USER_ROLES"]; 
			}
			return null;
		}
		
		
		
		
		/*
		public function isBatcher():Boolean {
		return _isBatcher;		
		} 
		public function isDispatcher():Boolean {
		return _isDispatcher || true;		
		} 
		*/
		private var loadedCount:int = 0;
		
		/*
		private var _isBatcher:Boolean;
		private var _isDispatcher:Boolean;
		private var _isAgentService:Boolean;
		*/
		private var _user:String;
		private var _plants:ArrayCollection;
		private var _tvarvcParameters:DictionaryMap;
		private var _tvarvcSelections:DictionaryMap;
		
		private var _otr:DictionaryMap;
		private var _rmsParams:DictionaryMap;
		private var _rmsAssignPa:DictionaryMap;
		private var _securityExceptions:DictionaryMap;
		private var _securityFieldPerProcess:DictionaryMap;
		
		public var zonaHorariaRefresh:String;
		
		public function getSecurityExceptions(view:String,event:String,operativeMode:String):SecurityExceptions {
			
			return  _securityExceptions.get(view+":"+event) as SecurityExceptions;
		}
		
		public function getSecurityFieldPerProcess(view:String,event:String,operativeMode:String):SecurityFieldPerProcess {
			if(event=="BTN_START_BATCHING"){
				//preparing to debug
				var algo:*=".";
			}
			/*var conf2:SecurityFieldPerProcess=_securityFieldPerProcess.get(view+":"+event) 	as SecurityFieldPerProcess;
			var conf3:SecurityFieldPerProcess=_securityFieldPerProcess.get("DM_"+view+":"+event) as SecurityFieldPerProcess;*/
			var resp:SecurityFieldPerProcess=null;
			if(DispatcherIslandImpl.isBatcher){
				resp= _securityFieldPerProcess.get("DM_BATC_"+view+":"+event) as SecurityFieldPerProcess;
			} else if(DispatcherIslandImpl.isAgentService){
				resp=_securityFieldPerProcess.get("DM_SAGE_"+view+":"+event) as SecurityFieldPerProcess;
			} else if(!DispatcherIslandImpl.isBatcher && !DispatcherIslandImpl.isAgentService){
				resp= _securityFieldPerProcess.get("DM_DISP_"+view+":"+event) as SecurityFieldPerProcess;
			}
			if(resp==null){
				return _securityFieldPerProcess.get(view+":"+event) as SecurityFieldPerProcess;
			}
			return resp;
			/*var conf4:SecurityFieldPerProcess=_securityFieldPerProcess.get("DM_DISP_"+view+":"+event) as SecurityFieldPerProcess;
			var conf5:SecurityFieldPerProcess=_securityFieldPerProcess.get("DM_BATC_"+view+":"+event) as SecurityFieldPerProcess;
			if(conf2!=null){
			return conf2;
			} else if(conf3!=null){
			return conf3;
			} else if(conf4!=null){
			return conf4;
			}  else if(conf5!=null){
			return conf5;
			}
			return conf2;*/
			
		}
		
		
		public function mapSecurityViewID(currView:String):String{
			
			switch (currView){
				case TVARVCPreffixes.ASSIGNED_LOADS_VIEW_ID:
					return "ASSIGNED_LOADS";
				case TVARVCPreffixes.LOADS_PER_ORDER_VIEW_ID:
					return  "LOADS_PER_ORDER";
				case TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID:
					return  "LOADS_PER_VEHICLE";
				case TVARVCPreffixes.SERVICE_AGENT_SCREEN_VIEW_ID:
					return "SERVICE_AGENT_SCREEN";
				case TVARVCPreffixes.PLANNING_SCREEN_VIEW_ID:
					return "PLANNING_SCREEN";
			}
			return "";
			
		}
		
		public function isVisibleField(view:String,event:String,operativeMode:String):Boolean{
			view = mapSecurityViewID(view);
			var exception:SecurityExceptions = getSecurityExceptions(view,event,operativeMode);
			
			if (operativeMode == null || exception == null){
				var field:SecurityFieldPerProcess = getSecurityFieldPerProcess(view,event,operativeMode);
				if (field == null){
					return false;
				}
				else if(field.land==userCountry){
					return field.visible
				} else {
					return false;
				}
			}
			else {
				var opM:Number = Number(operativeMode);
				//if (opM <= exception.configuration.length && exception.land==userCountry && !isBatcher && !isAgentService && exception.rol=="DISP"){
				//if (opM <= exception.configuration.length && exception.land==userCountry && ((!isBatcher && !isAgentService && exception.rol=="DISP") || (isBatcher && !isAgentService && exception.rol=="BATC")){
				if (opM <= exception.configuration.length && exception.land==userCountry &&
					((!isBatcher && !isAgentService && exception.rol=="DISP") ||
						(isBatcher && !isAgentService && exception.rol=="BATC") ||(!isBatcher && isAgentService && exception.rol!="BATC" && exception.rol!="DISP"))){
					return "Y" == exception.configuration.charAt(opM - 1).toUpperCase();
				}
				return false;
			}
		}
		public function isEnabledField(view:String,event:String,operativeMode:String):Boolean{
			view = mapSecurityViewID(view);
			var sec:SecurityFieldPerProcess = getSecurityFieldPerProcess(view,event,operativeMode);
			if(sec == null){
				return false;
			}
			return sec.enabled;
		}
		private var _productionDatas:DictionaryMap = null;
		
		public function getUser():String{
			return _user;
		}
		public function getPlants():ArrayCollection {
			return _plants;
		}
		
		
		public function getTVARVCParamMap():DictionaryMap {
			return _tvarvcParameters;
		}
		public function getTVARVCSelectionMap():DictionaryMap {
			return _tvarvcSelections;
		}
		
		private var _colorstvarv:ArrayCollection;
		private function getColorMapDictionary(colorsTVARV:ArrayCollection):DictionaryMap{
			var colorMap:DictionaryMap =  new DictionaryMap();
			if(colorsTVARV!=null){
				if(colorsTVARV==null){
					colorsTVARV=_colorstvarv;
				} else {
					_colorstvarv=colorsTVARV;
				}
				for (var i:int = 0 ; i < colorsTVARV.length ; i ++ ){
					var tvarv:TVARVC = colorsTVARV.getItemAt(i) as TVARVC;
					colorMap.put(tvarv.low, tvarv.high);
				}
			}
			return colorMap;
		}
		
		public function getTVARVCParam(name:String):TVARVC {
			
			if (_tvarvcParameters.get(name) == null){
				/*Alert.show("No se encuentra el Valor ["+name+"] de la TVARVC ");*/
			}
			var arr:ArrayCollection = _tvarvcParameters.get(name) as ArrayCollection;
			if (arr == null){
				return  _tvarvcParameters.get(name) as TVARVC;
			}
			return arr.getItemAt(0) as TVARVC;
		}
		public function getTVARVCSelection(name:String):ArrayCollection{
			
			if (_tvarvcSelections.get(name) == null){
					/*Alert.show("No se encuentra el Valor ["+name+"] de la TVARVC Selections ");*/
			}
			return _tvarvcSelections.get(name) as ArrayCollection;
		}
		
		public function getOTRValue(name:String):String{
			
			if (_otr == null || _otr.get(name) == null){
				/*Alert.show("No se encuentra el Valor ["+name+"] de la OTR ");*/
				if(name==OTRConstants.MULTI_DELIVERY){
					return "Multi-delivery";
				}
				return name;
			}
			//Alert.show(ReflectionHelper.object2XML(_otr,"otr"));
			return (_otr.get(name) as OTRLabel).value;
		}
		
		
		public function getParamString(param:String,plant:String=null):String{
			//verificar primero en los parametros reales
			if(_rmsAssignPa!=null){
				if(plant!=null){
					if(_rmsAssignPa.get(plant+":"+param)!=null){
						return _rmsAssignPa.get(plant+":"+param);
					} else {
						if(_rmsAssignPa.get(":"+param)!=null){
							return _rmsAssignPa.get(":"+param);
						}
					}
				} else {
					if(_rmsAssignPa.get(":"+param)!=null){
						return _rmsAssignPa.get(":"+param);
					}
				}
			}
			if (_rmsParams == null){
				return null;
			}
			return _rmsParams.get(param);
		}
		public function getParamNumber(param:String,plant:String=null):Number{
			if(_rmsAssignPa!=null){
				if(plant!=null){
					if(_rmsAssignPa.get(plant+":"+param)!=null){
						return Number(_rmsAssignPa.get(plant+":"+param));
					} else {
						if(_rmsAssignPa.get(":"+param)!=null){
							return _rmsAssignPa.get(":"+param);
						}
					}
				} else {
					if(_rmsAssignPa.get(":"+param)!=null){
						return Number(_rmsAssignPa.get(":"+param));
					}
				}
			}
			if (_rmsParams == null){
				return 0;
			}
			return _rmsParams.get(param);
		}
		
		
		public static const STATUS_UPDATE_DATA:String="UPDATE_DATA"
		public static const STATUS_INITIAL_DATA:String="INITIAL_DATA"
		public static const STATUS_READ_DATA:String="READ_DATA"
		public static const STATUS_IGNORE_DATA:String="IGNORE_DATA"
		
		public var datesTextMap:DictionaryMap =  new DictionaryMap();
		
		public function getDateText(date:String):DateText {
			return datesTextMap.get(date) as DateText;
		}
		public function getProductionData(payload:*):ProductionData{
			var pdata:ProductionData = null;
			//Alert.show("get("+payload.orderNumber+","+payload.itemNumber+")\n"+ReflectionHelper.object2XML(_productionDatas,"_productionDatas"));
			if ( _productionDatas != null ) {
				pdata = _productionDatas.get(DispatcherIslandHelper.getPDataID(payload.orderNumber,payload.itemNumber)) as ProductionData;
			}
			return pdata;
		}
		public function setProductionData(data:ProductionData):void{
			var id:String = DispatcherIslandHelper.getPDataID(data.vbeln ,""+ data.posnr);
			if (_productionDatas != null) {
				_productionDatas.put(id,data);
				logger.info("DispatcherIslandImpl received ProductionData and Inserted");
			}
			else {
				logger.info("DispatcherIslandImpl received ProductionData but ignored");
			}
			
		}
		/**
		 * Permite desplegar los valores de los costos en otro orden
		 * */
		public static var _rmsCostDisplay:Number=1;
		public static function getRMSCostDisplay():Number{
			return DispatcherIslandImpl._rmsCostDisplay;
		}
		
		/**
		 * Esta funcion valida que todos los datos esten disponibles para comenzar a activar todos los 
		 * lugares donde se requiera hacer algo con los datos
		 * 
		 * La variable de llave maestra le sirve al flash island para no repetir o recargar datos
		 */
		public static var masterKey:Boolean=true;
		public static var _fechaBase:Date;
		public static function set fechaBase(value:Date):void{
			_fechaBase=value;
		}
		public static function get fechaBase():Date{
			return _fechaBase;
		}
		public static var pushBaseDate:Date;
		public static var horaBase:int;
		public static var dataLogging:Boolean=false;
		public static var userFormat:String="?";
		private var plantas:String;
		private var fanterior:Date;
		private var initialDataLoaded:Boolean=false;
		public static var isBatcher:Boolean=false;
		public static var isAgentService:Boolean=false;
		public static var isTomorrow:Boolean=false;
		public static var currentSelector:String=null;
		public static var currentViewSelector:String=null;
		public static var planningScreenLoads:DictionaryMap;
		public static var userCountry:String;
		public static var delayMsg:String=null;
		public static var diferentBetwenDay:Number;
		public static var mapVehicles:DictionaryMap=new DictionaryMap();
		public static var extraProductsLimit:Array=[5000,5999];
		
		public static var tieneTab5:Boolean = false;
		
		public override function flashIslandDataReady():void {
			//|| (_plants.source.toString()!=plantas && plantas!=null) || DispatcherIslandImpl.masterKey
			
			if (getImportDisso() == null){
				
				return;
			}
			//hacer visible o no visible los tabs
			if(getImportDisso()["OPEN_PREFERENCES"]=="X" && DispatcherViewMediator.visualConfigurations.windowOpened==false){
				DispatcherViewMediator.currentInstance.openSelectPlantWindow();
			}
			if(getBufferData()["ZFIELD_ATTRIBUTES"]!=null){
				try{
					if (getBufferData()["ZFIELD_ATTRIBUTES"]["TAB_5"]=="X")
						tieneTab5 = true;
					else{
						/*DispatcherViewMediator.currentInstance.view.viewStack.removeChildAt(4);*/
						tieneTab5 = false;
					}
					if(getBufferData()["ZFIELD_ATTRIBUTES"]["TAB_4"]!="X"){
						/*DispatcherViewMediator.currentInstance.view.viewStack.removeChildAt(3);*/
					} else if(!initialDataLoaded){
						//DispatcherViewMediator.currentView=TVARVCPreffixes.SERVICE_AGENT_SCREEN_VIEW_ID;
					}
					if(getBufferData()["ZFIELD_ATTRIBUTES"]["TAB_3"]!="X"){
						DispatcherViewMediator.currentInstance.view.viewStack.removeChildAt(2);
					}
					if(getBufferData()["ZFIELD_ATTRIBUTES"]["TAB_2"]!="X"){
						DispatcherViewMediator.currentInstance.view.viewStack.removeChildAt(1);
					}
					if(getBufferData()["ZFIELD_ATTRIBUTES"]["TAB_1"]!="X"){
						DispatcherViewMediator.currentInstance.view.viewStack.removeChildAt(0);
						DispatcherIslandImpl.currentViewSelector="LSA";
					}
					DispatcherViewMediator.currentInstance.view.viewStack.visible=true;
				} catch(e:Error){
					
				}
			}
			var status:String = DispatcherIslandHelper.getFlashIslandStatus(getImportDisso());
			
			
			if(status=="READ_VEHICLES"){
				status=STATUS_READ_DATA;
				DispatcherIslandImpl.masterKey=true;
			}
			/*if(status==STATUS_IGNORE_DATA && masterKey==true){
			status=STATUS_READ_DATA;
			}*/
			
			var statusTimeStamp:String = DispatcherIslandHelper.getFlashIslandStatusTimeStamp(getImportDisso());
			
			
			logger.debug("Event Arrived:" + status );
			trace("Event Arrived:" + status )
			// SE convierten los datos al esquema que el Evento de FlashIslands necesita
			var event:FlashIslandsDataEvent = null;
			
			var data:FlashIslandsData =  new FlashIslandsData();
			data.baseDate = DispatcherIslandHelper.getBaseDate(getImportDisso());
			if(fanterior==null){
				fanterior=data.baseDate;
			} else if(data.baseDate.date!=fanterior.date){
				fanterior=data.baseDate;
				DispatcherIslandImpl.masterKey=true;
			}
			if(pushBaseDate!=null && pushBaseDate.date!= data.baseDate.date){
				diferentBetwenDay= (pushBaseDate.time-data.baseDate.time)/1000/60;
			}
			DispatcherIslandImpl.pushBaseDate=data.baseDate;
			DispatcherIslandImpl.fechaBase=data.baseDate;
			//DispatcherIslandImpl.fechaBase.date=(new Date()).date
			//setear las hroas de la zona horaria para recorrer la linea
			var zonaHorariaHora:String=this.getImportDisso()["TIME"];
			zonaHorariaRefresh=this.getImportDisso()["TIME"];// si trae el valor 00:00:00 significa se esta en una fecha diferente a la actual ..
			DispatcherIslandImpl.isTomorrow=this.getImportDisso()["IS_DATE_BETWEEN_FUTURE_DAYS"];
			trace("TIME ZONE");
			if(getImportDisso()["COST_TYPE_DISPLAY"]!=null){
				DispatcherIslandImpl._rmsCostDisplay= Number(getImportDisso()["COST_TYPE_DISPLAY"]);
			}
			if(getImportDisso()["DELAY_MSG"]!=null){
				DispatcherIslandImpl.delayMsg= getImportDisso()["DELAY_MSG"];
			}
			if(getImportDisso()["USER_COUNTRY"]!=null){
				DispatcherIslandImpl.userCountry= getImportDisso()["USER_COUNTRY"];
			}
			DispatcherIslandImpl.fechaBase.hours=Number(zonaHorariaHora.substr(0,2));
			DispatcherIslandImpl.fechaBase.minutes=Number(zonaHorariaHora.substr(3,2));
			//DispatcherIslandImpl.fechaBase.seconds=Number(zonaHorariaHora.substr(6,2));
			
			
			var datas:ArrayCollection = null;
			
			var statusUpper:String = status.toUpperCase();
			trace("SECURITY")
			var rmsSecurity:Object = MappingHelper.getObject(getInitialConfig(),"SECURITY");
			if (statusUpper == STATUS_UPDATE_DATA ||
				statusUpper == STATUS_READ_DATA || 
				statusUpper == STATUS_INITIAL_DATA){
				planningScreenLoads=new DictionaryMap();
				var planningLocalLoads:ArrayCollection=getPlanningLoads();
				for(var r:int=planningLocalLoads.length-1;r>-1;r--){
					planningScreenLoads.put(planningLocalLoads.getItemAt(r).VBELN+":"+planningLocalLoads.getItemAt(r).POSNR,planningLocalLoads.getItemAt(r));
				}
				if(DispatcherIslandImpl.userFormat=="?"){
					DispatcherIslandImpl.userFormat=getImportDisso()["NUMBER_FORMAT"];
				}
				if(DispatcherIslandHelper.ordersWithError==null){
					DispatcherIslandHelper.ordersWithError=[];
				}
				if (getReturnLog() != null && DispatcherIslandHelper.ordersWithError.length==0){
					trace("RETURN LOG")
					var returnLog:ArrayCollection = DispatcherIslandHelper.getReturnLog(getData());
					var returnEvent:ReturnLogEvent =  new ReturnLogEvent(returnLog);
					dispatcher.dispatchEvent(returnEvent);
				} else if(DispatcherIslandHelper.ordersWithError.length==0){
					//solo limpiarlo
					DispatcherIslandHelper.ordersWithError=[];
					var ac:ArrayCollection=new ArrayCollection();
					var returnEvent:ReturnLogEvent =  new ReturnLogEvent(ac);
					dispatcher.dispatchEvent(returnEvent);
				}
				trace("DEACTIVATE MENU")
				if(getImportDisso()["DEACTIVATE_MENU"]){
					DispatcherIslandImpl.areMenusAvailable=false;
				} else {
					DispatcherIslandImpl.areMenusAvailable=true;
				}	
				if(DispatcherIslandImpl.dataLogging){
					logger.debug("Object2AS:" + status +"\n" + ReflectionHelper.object2AS(getBufferData()));
					logger.debug("Object2AS:" + status);
				}
				if (statusUpper == STATUS_READ_DATA || 
					statusUpper == STATUS_INITIAL_DATA){
					datesTextMap = DispatcherIslandHelper.getDateTexts(getImportDisso());
					_plants = DispatcherIslandHelper.getPlants(getImportDisso());
					var plantas2:String="";
					/*if(_plants.source!=null){
					plantas=_plants.source.toString();
					}*/
					currentSelector="";
					var isPlantStillInTheList:Boolean=false;
					for(var q:int=0;q<_plants.length;q++){
						if((_plants.getItemAt(q) as Plant).plantId==DispatcherViewMediator.visualConfigurations.plantConfigured){
							isPlantStillInTheList=true;
						}
						plantas2+=(_plants.getItemAt(q) as Plant).plantId;
						//aqui verificamos si la planta que se configuro para la configuracion visual sigue vigente, 
						
						currentSelector+="PLANT like '%"+(_plants.getItemAt(q) as Plant).plantId+"%'";
						if(q<_plants.length-1){
							currentSelector+=" or ";								
						}
					}
					if(!isPlantStillInTheList){
						DispatcherViewMediator.visualConfigurations.plantConfigured=null;
					}
					if(plantas2!=plantas){
						plantas=plantas2;
						DispatcherIslandImpl.masterKey=true;
					}
					
				}
				
				if (status.toUpperCase() == STATUS_INITIAL_DATA) {
					DispatcherIslandImpl.isBatcher=getInitialConfig()["USER_ROLES"]["IS_BATCHER"];
					DispatcherIslandImpl.isAgentService=getInitialConfig()["USER_ROLES"]["IS_AGENT_SERVICE"];
					if(DispatcherIslandImpl.currentViewSelector==null/* && !tieneTab5*/){
						DispatcherIslandImpl.currentViewSelector=getImportDisso()["SELECTED_VIEW"];
						if(DispatcherIslandImpl.currentViewSelector==null || DispatcherIslandImpl.currentViewSelector==""){
							DispatcherIslandImpl.currentViewSelector="LPO";
						}
					}
					logger.debug("Got in Event:" + status );
					if(DispatcherViewMediator.show71534){
						DispatcherViewMediator.visualConfigurations.availableVehiclesButton=Number(getInitialConfig()["VISUAL_CONFIG"]["AVAILABLE_VEHICLES_BTN"]);
						if (getInitialConfig()["VISUAL_CONFIG"]["PLANT_CONFIGURED"]!=""){
							DispatcherViewMediator.visualConfigurations.plantConfigured=getInitialConfig()["VISUAL_CONFIG"]["PLANT_CONFIGURED"];
						}
						if(getInitialConfig()["VISUAL_CONFIG"]["RANGE_VISIBLE"]!=""){
							DispatcherViewMediator.visualConfigurations.rangeVisible=Number(getInitialConfig()["VISUAL_CONFIG"]["RANGE_VISIBLE"]);
						}
						var preset:Array=["ASSG","AVLB","LOAD"];
						var ipreset:Array=[];
						if(getInitialConfig()["VISUAL_CONFIG"]["SECOND_ROW"]!="" && getInitialConfig()["VISUAL_CONFIG"]["SECOND_ROW"]!=null){
							DispatcherViewMediator.visualConfigurations.secondRow=getInitialConfig()["VISUAL_CONFIG"]["SECOND_ROW"];
							if(ipreset.indexOf(getInitialConfig()["VISUAL_CONFIG"]["SECOND_ROW"])==-1){
								ipreset.push(getInitialConfig()["VISUAL_CONFIG"]["SECOND_ROW"]);
							}
						}
						if(getInitialConfig()["VISUAL_CONFIG"]["THIRD_ROW"]!="" && getInitialConfig()["VISUAL_CONFIG"]["THIRD_ROW"]!=null){
							DispatcherViewMediator.visualConfigurations.thirdRow=getInitialConfig()["VISUAL_CONFIG"]["THIRD_ROW"];
							if(ipreset.indexOf(getInitialConfig()["VISUAL_CONFIG"]["THIRD_ROW"])==-1){
								ipreset.push(getInitialConfig()["VISUAL_CONFIG"]["THIRD_ROW"]);
							}
						}
						if(getInitialConfig()["VISUAL_CONFIG"]["FIRST_ROW"]!="" && getInitialConfig()["VISUAL_CONFIG"]["FIRST_ROW"]!=null){
							DispatcherViewMediator.visualConfigurations.firstRow=getInitialConfig()["VISUAL_CONFIG"]["FIRST_ROW"];
							if(ipreset.indexOf(getInitialConfig()["VISUAL_CONFIG"]["FIRST_ROW"])==-1){
								ipreset.push(getInitialConfig()["VISUAL_CONFIG"]["FIRST_ROW"]);
							}
						}
						if(ipreset.length!=3){
							//reseteamos
							DispatcherViewMediator.visualConfigurations.secondRow="AVLB";
							DispatcherViewMediator.visualConfigurations.thirdRow="LOAD";
							DispatcherViewMediator.visualConfigurations.firstRow="ASSG";
						}
						
						
						if(getInitialConfig()["VISUAL_CONFIG"]["SHOW_ONE_PLANT"]=="true"){
							DispatcherViewMediator.visualConfigurations.showOnePlantAtATime=true;
						}
						if(getInitialConfig()["VISUAL_CONFIG"]["SORT_VEHICLE_FROM_TIME"]=="false"){
							DispatcherViewMediator.visualConfigurations.sortVehicleFromOldestToNewest=false;
						}
						DispatcherViewMediator.visualConfigurations.sortVehiclesPerTime=Number(getInitialConfig()["VISUAL_CONFIG"]["SORT_VEHICLES_PER_TIME"]);
						
						DispatcherViewMediator.visualConfigurations.selectedView=getInitialConfig()["VISUAL_CONFIG"]["SELECTED_VIEW"];
					}
					trace("STATUS_INITIAL_DAta")
					_user = DispatcherIslandHelper.getUser(getImportDisso());
					
					
					var preffix:String = DispatcherIslandHelper.getTVARVCPreffix(getInitialConfig());
					var tempTVARV:ArrayCollection = DispatcherIslandHelper.getTVARVCArrayCollection(getInitialConfig());
					
					extraProductsLimit[0]=Number((tempTVARV.getItemAt(tempTVARV.length-1) as TVARVC).low);
					extraProductsLimit[1]=Number((tempTVARV.getItemAt(tempTVARV.length-1) as TVARVC).high);
					
					_tvarvcParameters = DispatcherIslandHelper.getTVARVCParams(tempTVARV,preffix);
					_tvarvcSelections = DispatcherIslandHelper.getTVARVCSelections(tempTVARV,preffix);
					
					_otr = DispatcherIslandHelper.getOTR(getInitialConfig());
					_rmsParams = DispatcherIslandHelper.getRMSParams(getInitialConfig());
					_rmsAssignPa=DispatcherIslandHelper.getRMSAssignPa(this.buffer.data);
					
					var prefixSec:String = DispatcherIslandHelper.getSecurityPreffix(getInitialConfig());
					
					_securityFieldPerProcess = DispatcherIslandHelper.getSecurityFieldPerProcess(rmsSecurity,prefixSec);
					//Alert.show(ReflectionHelper.object2XML(_securityFieldPerProcess,"processField"));
					_securityExceptions = DispatcherIslandHelper.getSecurityExceptions(rmsSecurity,prefixSec); 
					//Alert.show(ReflectionHelper.object2XML(_securityExceptions,"exceptions"));
					
					//var colors:ArrayCollection = getTVARVCSelection(TVARVCConstants.MAP_COLOR_CATALOG);
					//colorMap = getColorMapDictionary(colors);
					/*
					_isAgentService =  DispatcherIslandHelper.getIsAgentService(getUserRoles());
					_isBatcher = DispatcherIslandHelper.getIsBatcher(getUserRoles());
					_isDispatcher = DispatcherIslandHelper.getIsDispatcher(getUserRoles());
					*/
					//Alert.show("AS:"+_isAgentService +"-B:"+ _isBatcher +"-D:"+ _isDispatcher);
					if(!initialDataLoaded){
						var lcds:Object = getInitialConfig()["LCDS"];
						
						PushConfigHelper.initLcdsEndpoint(lcds);
						PushConfigHelper.initLcdsDestinies(lcds);
						initialDataLoaded=true;
					}
					
					if (!isReady()){
						dispatchServiceLoadReady();
						InitializerHelper.initHelpers();
					}
				}
				
				if(!DispatcherIslandImpl.masterKey){
					return;
				}
				
				//if (isBatcher()) {
				var pdatas:ArrayCollection = DispatcherIslandHelper.getProductionData(getData());
				_productionDatas = DispatcherIslandHelper.getPDatasDictionary(pdatas);						
				//}
				
				DispatcherIslandHelper.ordersWithError=[];
				datas = DispatcherIslandHelper.transformAbapOrders2Plain( getDSSOrder() );
				var ordersToCollection:ArrayCollection=new ArrayCollection(DispatcherIslandHelper.ordersWithError);
				var returnEvent:ReturnLogEvent =  new ReturnLogEvent(ordersToCollection);
				dispatcher.dispatchEvent(returnEvent);
				
				if (isReady()) {
					trace("isReady()")
					if (status.toUpperCase() == STATUS_READ_DATA || status.toUpperCase() == STATUS_INITIAL_DATA) {
						event = new FlashIslandsDataEvent(FlashIslandsDataEvent.FLASHISLAND_DATA_READY,data);	
					}
					else if (status.toUpperCase() == STATUS_UPDATE_DATA) {
						event = new FlashIslandsDataEvent(FlashIslandsDataEvent.FLASHISLAND_DATA_READY,data);
					}
				}
									
			//	datesTextMap = DispatcherIslandHelper.getDateTexts(getImportDisso());
			}
			
			
			// Cuandos e lanza el evento quiere decir que hubo datos
			if (event != null){
				
				data.loads = datas;
				//Alert.show(ReflectionHelper.object2XML(datas,"loads"));
				//data.productionDatas = _productionDatas;
				data.rawData = addResoursesToRawDataXML(datas);
				logger.debug("Dispatching:" +event.type );
				dispatcher.dispatchEvent(event);
				//
				DispatcherIslandHelper.setFlashIslandStatus(getImportDisso(),STATUS_IGNORE_DATA);
				////cleanAll();
				
				deleteContextServerData();
				DispatcherIslandHelper.setFlashIslandStatus(getImportDisso(),STATUS_IGNORE_DATA);
				
				// Este no debe ir antes de despachar el evento
			}
			
			
			loadedCount = 0;
			// para fechas futuras
			if (zonaHorariaHora=="00:00:00"){
				refreshFuturo();
			}else
			{
				if(timerRefresh!=null){
					timerRefresh.stop();
				}
				timerRefresh=null;
			}
			
		}
		
		//representa el timer para hacer el refresh
		internal var timerRefresh:Timer;		
		
		
		
		//refresca el punto de partida es la zona horaria si es igual "00:00:00" se hace refersh
		private function refreshFuturo():void{
			//hay un error en este metodo
			/*timerRefresh = new Timer(5000);
			timerRefresh.addEventListener(TimerEvent.TIMER, refreshData);
			timerRefresh.start();*/
			
		}
		
		
		
		
		
		
		
		private static var counter:Number = 0;
		
		private function deleteContextServerData():void {	
			dispatchWDEvent(new DeleteContextServerDataRequest());
		}
		public function refreshData():void{
			var ref:RefreshRequest=new RefreshRequest();
			ref.vista=DispatcherViewMediator.currentView.substr(-3);
			if(DispatcherViewMediator.franceShowSinglePlant!=null){
				ref.plant=DispatcherViewMediator.franceShowSinglePlant
			}
			/*dispatchWDEvent(ref);*/
			GanttServiceReference.dispatchIslandEvent(ref);
		}
		public function sendMessage(s:String):void{
			logger.info(s);
			//Alert.show(s);
		}
		
		
		private function addResoursesToRawDataXML(datas:ArrayCollection):String{
			var rawDataXML:String = "";
			rawDataXML += "<InitialData>\n";
			rawDataXML += ReflectionHelper.object2XML(getImportDisso(),"IMPORT_DISSO","  ") + "\n";
			rawDataXML += ReflectionHelper.object2XML(getInitialConfig(),"INITIALCONFIG","  ") + "\n";
			rawDataXML += ReflectionHelper.object2XML(getDSSOrder(),"DS_SORDER","  ") + "\n";
			
			rawDataXML += "</InitialData>\n";
			rawDataXML += "<TasksData>\n";
			rawDataXML +=  ReflectionHelper.object2XML(_user,"user","  ") + "\n";
			rawDataXML +=  ReflectionHelper.object2XML(_plants,"plants","  ") + "\n";
			rawDataXML +=  ReflectionHelper.object2XML(_tvarvcParameters,"tvarvcParams","  ") + "\n";
			rawDataXML +=  ReflectionHelper.object2XML(_tvarvcSelections,"tvarvcSelections","  ") + "\n";
			rawDataXML +=  ReflectionHelper.object2XML(datas,"datas","  ") + "\n";
			
			rawDataXML += "</TasksData>\n";
			return rawDataXML;
		}
		
	}
}