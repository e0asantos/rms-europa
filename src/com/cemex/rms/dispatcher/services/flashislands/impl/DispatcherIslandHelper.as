package com.cemex.rms.dispatcher.services.flashislands.impl
{
	
	import com.cemex.rms.common.flashislands.MappingHelper;
	import com.cemex.rms.common.flashislands.vo.OTRLabel;
	import com.cemex.rms.common.flashislands.vo.TVARVC;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.common.utils.DictionaryMap;
	import com.cemex.rms.dispatcher.services.flashislands.vo.AddProd;
	import com.cemex.rms.dispatcher.services.flashislands.vo.DateText;
	import com.cemex.rms.dispatcher.services.flashislands.vo.Equipment;
	import com.cemex.rms.dispatcher.services.flashislands.vo.PlanningMovement;
	import com.cemex.rms.dispatcher.services.flashislands.vo.Plant;
	import com.cemex.rms.dispatcher.services.flashislands.vo.ProductionData;
	import com.cemex.rms.dispatcher.services.flashislands.vo.RMSAssignPa;
	import com.cemex.rms.dispatcher.services.flashislands.vo.RMSParam;
	import com.cemex.rms.dispatcher.services.flashislands.vo.ReturnLog;
	import com.cemex.rms.dispatcher.services.flashislands.vo.SecurityExceptions;
	import com.cemex.rms.dispatcher.services.flashislands.vo.SecurityFieldPerProcess;
	import com.cemex.rms.dispatcher.services.flashislands.vo.Text;
	import com.cemex.rms.dispatcher.services.flashislands.vo.Workcenter;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import mx.collections.ArrayCollection;
	import mx.messaging.management.Attribute;
	
	public class DispatcherIslandHelper
	{
		public function DispatcherIslandHelper()
		{
		}
		
		public static function getIsAgentService(userRoles:Object):Boolean {
			return userRoles["IS_AGENT_SERVICE"] as Boolean;
		}
		public static function getIsBatcher(userRoles:Object):Boolean {
			return userRoles["IS_BATCHER"] as Boolean
		}
		public static function getIsDispatcher(userRoles:Object):Boolean {
			return userRoles["IS_DISPATCHER"] as Boolean
		}
		
		
		
		public static function getReturnLog(data:Object):ArrayCollection{
			
			if (data == null || data["RETURN_LOG"] == null){
				return null;
			}
			return MappingHelper.doMapping(data,"RETURN_LOG", ReturnLog);	
		}
		
		public static function getDateTexts(importDisso:Object):DictionaryMap{
			
			
			var dates:ArrayCollection = MappingHelper.doMapping(importDisso,"DATES", DateText);
			
			var texts:DictionaryMap = new DictionaryMap();
			for (var i:int = 0 ; i < dates.length ; i ++) {
				
				var date:DateText =  dates.getItemAt(i) as DateText;
				
				texts.put(date.refDate,date);	
			}
			return texts;
		}
		
		
		
		public static function getBaseDate(importDisso:Object):Date{
			
			var fecha:Date = MappingHelper.getObject(importDisso, "DATE") as Date;
			
			if (fecha != null){
				return fecha;
			}
			throw new Error("Error[DPC-FIH-0013.02] No se encontro el Campo [DATE]");
		}
		
		
		
		public static function setFlashIslandStatus(importDisso:Object,value:String):void {
			MappingHelper.setObject(importDisso,"IGNORE_DATA_CHANGE",value);
		}
		
		public static function getFlashIslandStatus(importDisso:Object):String{
			return MappingHelper.getObject(importDisso, "FLASH_ISLAND_STATUS") as String;
		}
		public static function getFlashIslandStatusTimeStamp(importDisso:Object):String{
			return MappingHelper.getObject(importDisso, "FLASH_ISLAND_STATUS") as String;
		}
		
		public static function getUser(importDisso:Object):String{
			return MappingHelper.getObject(importDisso, "USER") as String;
		}
		public static function getPlants(importDisso:Object):ArrayCollection{
			return MappingHelper.doMapping(importDisso,"PLANT", Plant,[getEquipments,getWorkcenters,getPlanningMovements]);
		}
		
		public static function getEquipments(plant:Object, field:String):ArrayCollection{
			if (field != "EQUIPMENTS"){
				return null;
			}
			return MappingHelper.doMapping(plant,"EQUIPMENTS", Equipment,[getPlanningMovements]);
		}
		public static function getPlanningMovements(equipment:Object,field:String):ArrayCollection{
			if (field != "PLANNING_MOVEMENTS"){
				return null;
			}
			var moves:ArrayCollection=MappingHelper.doMapping(equipment,"PLANNING_MOVEMENTS", PlanningMovement);
			if(DispatcherIslandImpl.mapVehicles.get(equipment.EQUIPMENT)!=null){
				DispatcherIslandImpl.mapVehicles.remove(equipment.EQUIPMENT);
				DispatcherIslandImpl.mapVehicles.put(equipment.EQUIPMENT,moves);
			} else {
				DispatcherIslandImpl.mapVehicles.put(equipment.EQUIPMENT,moves);
			}
			return moves;
		}
		public static function getWorkcenters(plant:Object, field:String):ArrayCollection{
			if (field != "WORKCENTER"){
				return null;
			}
			return MappingHelper.doMapping(plant,"WORKCENTER", Workcenter);
		}
		
		
		
		public static function getPDatasDictionary(pdatas:ArrayCollection):DictionaryMap {
			var pds:DictionaryMap = new DictionaryMap();
			for (var i:int = 0 ; i < pdatas.length ; i ++) {
				
				var data:ProductionData =  pdatas.getItemAt(i) as ProductionData;
				
				var id:String = getPDataID(data.vbeln , ""+data.posnr);
				if (pds.get(id) == null) {
					pds.put(id,data);
				}	
			}
			return pds;
		}
		public static function getSecurityPreffix(initialConfig:Object):String{
			var preffix:String="";
			var temp:Object = MappingHelper.getObject(initialConfig,"PREFIXES");
			if (temp != null){
				temp = MappingHelper.getObject(temp,"FX_SECURITY_PREFIX");
				if (temp != null && temp.toString() != ""){
					preffix = temp.toString();
				}
			}	
			return preffix;
		}
		
		
		
		public static function getSecurityExceptions(security:Object,prefix:String):DictionaryMap{
			
			var exceptions:ArrayCollection = MappingHelper.doMapping(security,"EXEPTIONS", SecurityExceptions);
			
			var exceptionMap:DictionaryMap = new DictionaryMap();
			for (var i:int = 0 ; i < exceptions.length ; i ++) {
				
				
				var sec:SecurityExceptions =  exceptions.getItemAt(i) as SecurityExceptions;
				var name:String = sec.attributeName;
				var process:String = sec.process;
				if (process.indexOf(prefix) != -1){
					process = process.substring(process.indexOf(prefix) + prefix.length);
				}
				exceptionMap.put(process + ":" + name,sec);	
			}
			return exceptionMap;
		}
		
		
		
		public static function getSecurityFieldPerProcess(security:Object,prefix:String):DictionaryMap{
			
			
			var exceptions:ArrayCollection = MappingHelper.doMapping(security,"FIELDS_PER_PROCESS", SecurityFieldPerProcess);
			
			var exceptionMap:DictionaryMap = new DictionaryMap();
			for (var i:int = 0 ; i < exceptions.length ; i ++) {
				
				
				var sec:SecurityFieldPerProcess =  exceptions.getItemAt(i) as SecurityFieldPerProcess;
				var name:String = sec.attributeName;
				var process:String = sec.process;
				if (process.indexOf(prefix) != -1){
					process = process.substring(process.indexOf(prefix) + prefix.length);
				}
				exceptionMap.put(process + ":" + name,sec);	
			}
			return exceptionMap;
		}
		
		
		
		public static function getRMSParams(initialConfig:Object):DictionaryMap{
			
			
			var rmsParams:ArrayCollection = MappingHelper.doMapping(initialConfig,"FX_PARAMETERS", RMSParam);
			
			var params:DictionaryMap = new DictionaryMap();
			for (var i:int = 0 ; i < rmsParams.length ; i ++) {
				
				var param:RMSParam =  rmsParams.getItemAt(i) as RMSParam;
				
				params.put(param.name,param.value);	
			}
			return params;
		}
		
		public static function getRMSAssignPa(initialConfig:Object):DictionaryMap{
			
			
			var rmsParams:ArrayCollection = MappingHelper.doMapping(initialConfig,"ZPARAMS", RMSAssignPa);
			
			var params:DictionaryMap = new DictionaryMap();
			for (var i:int = 0 ; i < rmsParams.length ; i ++) {
				
				var param:RMSAssignPa =  rmsParams.getItemAt(i) as RMSAssignPa;
				
				params.put(param.werks+":"+param.alias,param.value);	
			}
			return params;
		}
		
		
		
		public static function getTVARVCPreffix(initialConfig:Object):String{
			var preffix:String="";
			var temp:Object = MappingHelper.getObject(initialConfig,"PREFIXES");
			if (temp != null){
				temp = MappingHelper.getObject(temp,"FX_TVARVC_PREFIX");
				if (temp != null && temp.toString() != ""){
					preffix = temp.toString();
				}
			}	
			return preffix;
		}
		
		public static function getTVARVCArrayCollection(initialConfig:Object):ArrayCollection{
			
			var tvarvcAbap:ArrayCollection = MappingHelper.doMapping(initialConfig,"FX_TVARVC", TVARVC);
			return tvarvcAbap;
		}
		
		
		public static function getTVARVCSelections(tvarvcAbap:ArrayCollection,prefix:String):DictionaryMap {
			var tvarvc:DictionaryMap = new DictionaryMap();
			for (var i:int = 0 ; i < tvarvcAbap.length ; i ++) {
				
				var data:TVARVC =  tvarvcAbap.getItemAt(i) as TVARVC;
				if (data.type == "S"){
					var arr:ArrayCollection;
					var name:String = data.name;
					if(data.name!=null){
						if (data.name.indexOf(prefix) != -1){
							name = data.name.substring(data.name.indexOf(prefix) + prefix.length);
						}
					}
					
					//Z_CXFM_CSDSLSMX_1005_FXD_LPV_O
					if (tvarvc.get(name) == null){
						arr =  new ArrayCollection();
						tvarvc.put(name,arr);
					}
					else {
						arr = tvarvc.get(name) as ArrayCollection;
					}
					arr.addItem(data);
				}
				
			}
			
			return tvarvc;
		}
		
		
		public static function getTVARVCParams(tvarvcAbap:ArrayCollection,prefix:String):DictionaryMap {
			var tvarvc:DictionaryMap = new DictionaryMap();
			for (var i:int = 0 ; i < tvarvcAbap.length ; i ++) {
				
				var data:TVARVC =  tvarvcAbap.getItemAt(i) as TVARVC;
				if (data.type == "P"){
					var name:String = data.name;
					if (data.name.indexOf(prefix) != -1){
						name = data.name.substring(data.name.indexOf(prefix) + prefix.length);
					}
					tvarvc.put(name,data);
				}				
			}
			return tvarvc;
		}
		
		
		
		public static function getOTR(initialConfig:Object):DictionaryMap{
			
			var prefix:String="";
			var temp:Object = MappingHelper.getObject(initialConfig,"PREFIXES");
			if (temp != null){
				temp = MappingHelper.getObject(temp,"FX_OTR_PREFIX");
				if (temp != null && temp.toString() != ""){
					prefix = temp.toString();
				}
			}	
			
			var otrAbap:ArrayCollection = MappingHelper.doMapping(initialConfig,"FX_OTR_LABELS", OTRLabel);
			
			var tvarvc:DictionaryMap  = getOTRDictionary(otrAbap,prefix);
			
			return tvarvc;
		}
		public static function getOTRDictionary(otrAbap:ArrayCollection,prefix:String):DictionaryMap {
			var otr:DictionaryMap = new DictionaryMap();
			for (var i:int = 0 ; i < otrAbap.length ; i ++) {
				
				var label:OTRLabel =  otrAbap.getItemAt(i) as OTRLabel;
				var alias:String = label.alias;
				label.name = alias;
				if (alias.indexOf(prefix) != -1){
					label.name = alias.substring(alias.indexOf(prefix) + prefix.length);
					label.matchedPrefix = true;	
				}
				otr.put(label.name,label);	
			}
			return otr;
		}
		
		
		/**
		 * Esta funcion agrega todos los recursos y tareas en base a todos los datos que se
		 * reciben de WebDynpro , pero antes de ejecutar esta funcion se debe 
		 * validar que esten todos los datos disponibles.
		 */
		public static var ordersWithError:Array;
		public static function transformAbapOrders2Plain( abapOrders:ArrayCollection):ArrayCollection {
			DispatcherIslandHelper.ordersWithError=[];
			var datas:ArrayCollection =  new ArrayCollection();
			for (var i:int = 0 ; i < abapOrders.length ; i ++ ) {
				var order:Object = abapOrders.getItemAt(i);
				var jobsite:Object =  DispatcherIslandHelper.getJobSiteABAP(order);
				var constructionProduct:Object =  DispatcherIslandHelper.getConstructionProductABAP(order);
				var firstConcrete:Object =  DispatcherIslandHelper.getFirstConcreteABAP(order);
				var loads:ArrayCollection  = DispatcherIslandHelper.getLoadsABAP(order);
				var addProd:ArrayCollection  = DispatcherIslandHelper.getAddProdABAP(order);
				var texts:ArrayCollection  = DispatcherIslandHelper.getTextsABAP(order);
				if (loads) {
					var plainLoad:OrderLoad =null;
					for (var j:int = 0 ; j < loads.length ; j ++ ){
						var load:Object = loads[j];
						//primero verificar si el objeto es valido
						plainLoad =  DispatcherIslandHelper.transformABAP2OrderLoad(order,load,jobsite,constructionProduct,firstConcrete);
						if(plainLoad.loadingDate==null
							|| plainLoad.loadingTime == null
							|| plainLoad.loadingTime==""){
							DispatcherIslandHelper.ordersWithError.push("ERROR "+plainLoad.orderNumber+"-"+plainLoad.loadNumber);
							continue;
						}
						
						
						plainLoad.addProd = addProd;
						plainLoad.texts = texts;
						plainLoad.orderLoad = plainLoad.orderNumber + "-" + plainLoad.loadNumber;
						
						datas.addItem(plainLoad);
					}
				}
			}
			return datas;
			
			
		}
		
		
		public static function getPDataID(order:String, item:String):String{
			return order + ":" + item;
		}
		
		
		
		public static function getEquipmentsPlain(plants:ArrayCollection):ArrayCollection{
			var result:ArrayCollection =  new ArrayCollection();
			for (var i:int = 0 ; i < plants.length ; i ++ ){
				var plant:Plant = plants.getItemAt(i) as Plant;
				for (var j:int = 0 ; j < plant.equipments.length ; j ++ ){
					var equipment:Equipment = plant.equipments.getItemAt(j) as Equipment;
					
					var plantPlain:Object = new Object();	
					fillPlantEquipmentPlain(plantPlain,plant,equipment);
					result.addItem(plantPlain);	
				}
			}
			return result;
		}
		
		public static function getPlantsPlain(plants:ArrayCollection):ArrayCollection{
			var result:ArrayCollection =  new ArrayCollection();
			for (var i:int = 0 ; i < plants.length ; i ++ ){
				
				var plant:Plant = plants.getItemAt(i) as Plant;
				var plantPlain:Object = new Object();
				
				fillPlantPlain(plantPlain,plant);
				result.addItem(plantPlain);	
			}
			return result;
		}
		
		public static function getEquipmentPlain(plant:Plant,vehicle:Equipment):Object {
			
			var plantPlain:Object = new Object();	
			fillPlantEquipmentPlain(plantPlain,plant,vehicle);
			return plantPlain;
		}
		
		
		public static function fillPlantPlain(plantPlain:Object,plant:Plant):void{
			
			plantPlain["plant"] = plant.plantId;	
			
			ReflectionHelper.copySimpleParameters(plant,plantPlain);
			
			
		}
		
		public static function fillPlantEquipmentPlain(plantPlain:Object,plant:Plant,equipment:Equipment):void{
			fillPlantPlain(plantPlain,plant);
			plantPlain["equipNumber"] = equipment.equipment;
			ReflectionHelper.copySimpleParameters(equipment,plantPlain);
			plantPlain["planningMovements"]=equipment.planningMovements;
			//plantPlain["equipments"] = null;
			//if (equipment.equipLabel == null || (""+equipment.equipLabel) == ""){
			//plantPlain["equipLabel"] = equipment.licenseNum;
			//}
		}
		
		/// Metodos para obtener los Datos en Modo Plain
		public static function transformABAP2OrderLoad(order:Object, load:Object,jobsite:Object,constructionProduct:Object,firstConcrete:Object):OrderLoad{
			
			var result:OrderLoad = new OrderLoad();
			
			ReflectionHelper.copySimpleParametersUpperAsCamel(order,result);
			ReflectionHelper.copySimpleParametersUpperAsCamel(load,result);
			ReflectionHelper.copySimpleParametersUpperAsCamel(jobsite,result);
			ReflectionHelper.copySimpleParametersUpperAsCamel(constructionProduct,result,"cp");
			ReflectionHelper.copySimpleParametersUpperAsCamel(firstConcrete,result,"first");
			
			
			return result;
		}
		
		
		
		public static function getJobSiteABAP(order:Object):Object{
			return MappingHelper.getObject(order,"JOB_SITE");
		}
		public static function getConstructionProductABAP(order:Object):Object{
			return MappingHelper.getObject(order,"C_PRODUCT");
		}
		public static function getFirstConcreteABAP(order:Object):Object{
			return MappingHelper.getObject(order,"FIRST_CONCRETE");
		}
		
		
		
		public static function getLoadsABAP(order:Object):ArrayCollection{
			if (order["LOADS"]){
				return order["LOADS"] as ArrayCollection;
			}
			return null;
		}
		
		public static function getAddProdABAP(order:Object):ArrayCollection{
			var result:ArrayCollection = MappingHelper.doMapping(order,"ADDPROD", AddProd);
			return result;
		}
		
		public static function getTextsABAP(order:Object):ArrayCollection{
			var result:ArrayCollection = MappingHelper.doMapping(order,"TEXT", Text);
			return result;
		}
		
		
		
		
		public static function getProductionData(data:Object):ArrayCollection{
			var result:ArrayCollection = MappingHelper.doMapping(data,"PRODUCTION_DATA", ProductionData);
			return result;
		}
		
		
		
		// TErminan metodos para obtener los datos en forma plain
		
		
		
		
		
	}
}