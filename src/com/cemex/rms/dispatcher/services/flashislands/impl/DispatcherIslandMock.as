package com.cemex.rms.dispatcher.services.flashislands.impl
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	
	import mx.controls.Alert;

	public class DispatcherIslandMock extends DispatcherIslandImpl
	{
		public function DispatcherIslandMock() {
			super();
		}
		private var mockData:MockObject =  new MockObject();
		override protected function getBufferData():Object {
			var algo:*=mockData.data;
			return mockData.data;
		}
		
		override public function dispatchWDEvent(fio:IFlashIslandEventObject):void {
			logger.info(fio.getEventName()+"(" + ReflectionHelper.object2XML(fio)+")");
			//Alert.show(fio.getEventName()+"(" + ReflectionHelper.object2XML(fio)+")");
		}
		override public function startService():void {
			
			flashIslandDataReady();
		}
		
		override public function refreshData():void{
			flashIslandDataReady();
		}
		
	}
}