package com.cemex.rms.dispatcher.services.flashislands.helper
{
	import com.cemex.rms.common.date.FlexDateHelper;
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.dispatcher.events.DispatcherConstants;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.AnticipatedLoads;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.AssignAdvance;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.AssignPlant;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.AssignVehicle;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.AutomaticDistribution;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.Bitacora;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.BringAllVehicles;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.CancelOrder;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.ChangeDeliveryTime;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.ChangeFrequency;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.ChangeItem;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.ChangePlantRequest;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.ChangeVolume;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.ClearMessagesArea;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.CommentDisp;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.CostData;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.ExchangeVehicle;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.GenericEventToWD;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.GlobalChangeInfo;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.InterchangeVehicle;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.LaunchPump;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.LaunchTicket;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.OpenOTS;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.OrderDetail;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.OrderOnHoldRequest;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.RedirectLoad;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.RenegotiateLoad;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.ResizeHeight;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.ReuseConcrete;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.SavePlanningScreenChange;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.SetPlanningPlant;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.UnassignPlant;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.UnassignQuick;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.UnassignVehicle;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.openWindowVehicle;
	import com.cemex.rms.dispatcher.services.flashislands.requests.production.BatchPerWorkcenter;
	import com.cemex.rms.dispatcher.services.flashislands.requests.production.BomRequest;
	import com.cemex.rms.dispatcher.services.flashislands.requests.production.ContinueProduction;
	import com.cemex.rms.dispatcher.services.flashislands.requests.production.Dispatch;
	import com.cemex.rms.dispatcher.services.flashislands.requests.production.DisplayPO;
	import com.cemex.rms.dispatcher.services.flashislands.requests.production.ManualBatch;
	import com.cemex.rms.dispatcher.services.flashislands.requests.production.MultiDelivery;
	import com.cemex.rms.dispatcher.services.flashislands.requests.production.QualityAdjustment;
	import com.cemex.rms.dispatcher.services.flashislands.requests.production.RePrint;
	import com.cemex.rms.dispatcher.services.flashislands.requests.production.ReUse;
	import com.cemex.rms.dispatcher.services.flashislands.requests.production.RestartLoad;
	import com.cemex.rms.dispatcher.services.flashislands.requests.production.StartBatching;
	import com.cemex.rms.dispatcher.services.flashislands.vo.Equipment;
	import com.cemex.rms.dispatcher.services.flashislands.vo.Plant;
	import com.cemex.rms.dispatcher.services.flashislands.vo.ProductionData;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class WDRequestHelper
	{
		public function WDRequestHelper()
		{
		}
		
		public static const SHOW_MULTI_DELIVERY:String="getShowMultiDelivery";
		
		public static function getShowMultiDelivery(payload:OrderLoad, productionData:ProductionData,toPlantType:String,workcenter:String):MultiDelivery{
			var bomreq:MultiDelivery=new MultiDelivery();
			bomreq.vbeln=payload.orderNumber;
			bomreq.posnr=payload.itemNumber
			return bomreq;
		}
		
		public static const BOM_REQUEST:String="getBomRequest";
		
		public static function getBomRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,workcenter:String):BomRequest{
			var bomreq:BomRequest=new BomRequest();
			bomreq.VBELN=payload.orderNumber;
			bomreq.MATNR = payload.materialId;
			bomreq.WERKS=payload.plant;
			bomreq.BOM=payload.bomValid;
			return bomreq;
		}
		
		public static const SHOW_DATA_REQUEST:String ="doShowData";
		public static const BATCH_REQUEST:String="getBatchPerWorkcenterRequest";
		public static function getBatchPerWorkcenterRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,workcenter:String):BatchPerWorkcenter{
			var request:BatchPerWorkcenter =  new BatchPerWorkcenter();
			
			var licenseNum:String = "";
			
			request.LICENSE_NUM = payload.equipLabel;
			
			request.ARBPL = workcenter;
			
			request.WERKS = payload.plant;
			request.VBELN = payload.orderNumber;
			request.POSNR = payload.itemNumber;
			request.EQUNR = payload.equipNumber;
			
			request.AUFNR = productionData.aufnr;
			return request;
		}
		
		
		
		
		
		public static const DISPLAY_PO_REQUEST:String="getDisplayPORequest";
		public static function getDisplayPORequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):DisplayPO{
			var request:DisplayPO =  new DisplayPO();
			request.AUFNR = productionData.aufnr;
			return request;
		}
		
		public static const CONTINUE_PRODUCTION_REQUEST:String="getContinueProductionRequest";
		public static function getContinueProductionRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):ContinueProduction{
			var request:ContinueProduction =  new ContinueProduction();
			
			var licenseNum:String="";
			request.AUFNR = productionData.aufnr;
			
			request.WERKS = payload.plant;
			request.VBELN = payload.orderNumber;
			request.POSNR = payload.itemNumber;
			request.EQUNR = payload.equipNumber;
			request.LICENSE_NUM = licenseNum;
			request.MATNR = payload.materialId;
			request.GAMNG ="";
			return request;
		}
		
		
		
		public static const MANUAL_BATCH_REQUEST:String="getManualBatchRequest";
		public static function getManualBatchRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):ManualBatch{
			var request:ManualBatch =  new ManualBatch();
			
			request.AUFNR = payload.prodOrder;//productionData.aufnr;
			request.STONR = payload.txtStatusProdOrder;//productionData.stonr;
			
			//
			var licenseNum:String = "";
			
			request.LICENSE_NUM = payload.equipLabel;
			
			request.ARBPL = extra;
			
			request.WERKS = payload.plant;
			request.VBELN = payload.orderNumber;
			request.POSNR = payload.itemNumber;
			request.EQUNR = payload.equipNumber;
			
			
			//
			return request;
		}
		
		
		public static const QUALITY_ADJUSTMENT_REQUEST:String="getQualityAdjustmentRequest";
		public static function getQualityAdjustmentRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):QualityAdjustment{
			var request:QualityAdjustment =  new QualityAdjustment();
			
			request.AUFNR = productionData.aufnr;
			
			return request;
		}
		
		
		
		
		public static const RE_PRINT_REQUEST:String="getRePrintRequest";
		public static function getRePrintRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):RePrint{
			var request:RePrint =  new RePrint();
			
			request.AUFNR = productionData.aufnr;
			request.STONR = productionData.stonr;
			request.VBELN = productionData.vbeln;
			request.POSNR = productionData.posnr;
			return request;
		}
		
		
		
		public static const RESTART_LOAD_REQUEST:String="getRestartLoadRequest";
		public static function getRestartLoadRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):RestartLoad{
			var request:RestartLoad =  new RestartLoad();
			
			request.AUFNR = productionData.aufnr;
			request.WERKS=payload.plant;
			request.POSNR=payload.itemNumber;
			request.VBELN=payload.orderNumber;
			//request.ARBPL=payload.
			return request;
		}
		
		
		public static const RE_USE_REQUEST:String="getReUseRequest";
		public static function getReUseRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):ReUse{
			var request:ReUse =  new ReUse();
			
			request.AUFNR = productionData.aufnr;
			request.VBELN = payload.orderNumber;
			request.POSNR = payload.itemNumber;
			request.EQUNR=payload.equipNumber;
			return request;
		}
		
		public static const START_BATCHING_REQUEST:String="getStartBatchingRequest";
		public static function getStartBatchingRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):StartBatching{
			var request:StartBatching =  new StartBatching();
			request.AUFNR = productionData.aufnr;
			request.VBELN = payload.orderNumber;
			request.POSNR = payload.itemNumber;
			return request;
		}
		
		/**
		 * Evento de cargas anticipadas
		 **/
		
		public static const ANTICIPATED_LOADS_REQUEST:String="getAnticipatedLoads";
		public static function getAnticipatedLoads(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):AnticipatedLoads{
			var request:AnticipatedLoads =  new AnticipatedLoads();
			request.WERKS = payload.plant;
			request.VBELN = payload.orderNumber;
			request.POSNR = payload.itemNumber;
			return request;
		}
		
		
		public static const DISPATCH_REQUEST:String="getDispatchRequest";
		public static function getDispatchRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):Dispatch{
			var request:Dispatch =  new Dispatch();
			request.WERKS = payload.plant;
			request.VBELN = payload.orderNumber;
			request.POSNR = payload.itemNumber;
			return request;
		}
		
		public static const CHANGE_FREQUENCY_REQUEST:String="getChangeFrequencyRequest";
		public static function getChangeFrequencyRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):ChangeFrequency{
			var frequency:ChangeFrequency = new ChangeFrequency();
			
			frequency.VBELN = payload.orderNumber;
			frequency.POSNR = payload.itemNumber;
			return frequency;
		}
		
		
		public static const CHANGE_DELIVERY_TIME_REQUEST:String="getChangeDeliveryTimeRequest";
		public static function getChangeDeliveryTimeRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):ChangeDeliveryTime {
			
			var delivery:ChangeDeliveryTime =  new ChangeDeliveryTime();
			
			
			delivery.itemNumber =  payload.itemNumber;
			delivery.orderNumber = payload.orderNumber;
			delivery.plant=payload.plant;
			var toTime:String = FlexDateHelper.getDateString(payload.startTime,"HHNNSS");
			if (int(toTime) > 240000){
				toTime = "00" +  toTime.substring(2,toTime.length);
			}
			delivery.toTime = toTime; 
			delivery.toDate = FlexDateHelper.copyDate(payload.startTime);
			delivery.toDate.setHours(0,0,0,0);
			return delivery;
		}
		
		public static const CANCEL_ORDER:String="getCancelOrder";
		public static function getCancelOrder(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):CancelOrder {
			
			var delivery:CancelOrder =  new CancelOrder();
			
			
			delivery.itemNumber =  payload.itemNumber;
			delivery.orderNumber = payload.orderNumber;
			delivery.plant=payload.plant;
			var toTime:String = FlexDateHelper.getDateString(payload.startTime,"HHNNSS");
			if (int(toTime) > 240000){
				toTime = "00" +  toTime.substring(2,toTime.length);
			}
			delivery.toTime = toTime; 
			delivery.toDate = FlexDateHelper.copyDate(payload.startTime);
			delivery.toDate.setHours(0,0,0,0);
			return delivery;
		}
		
		public static const OPEN_OT:String="getOpenOT";
		public static function getOpenOT(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):CancelOrder {
			
			var delivery:CancelOrder =  new CancelOrder();
			
			
			delivery.itemNumber =  payload.itemNumber;
			delivery.orderNumber = payload.orderNumber;
			delivery.plant=payload.plant;
			var toTime:String = FlexDateHelper.getDateString(payload.startTime,"HHNNSS");
			if (int(toTime) > 240000){
				toTime = "00" +  toTime.substring(2,toTime.length);
			}
			delivery.toTime = toTime; 
			delivery.toDate = FlexDateHelper.copyDate(payload.startTime);
			delivery.toDate.setHours(0,0,0,0);
			return delivery;
		}
		
		public static const ASSIGN_VEHICLE_REQUEST:String="getAssignVehicleRequest";
		public static function getAssignVehicleRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String,isdirect:String=""):AssignVehicle{
			var vehicle:AssignVehicle =  new AssignVehicle();
			vehicle.orderNumber = payload.orderNumber;
			vehicle.itemNumber = payload.itemNumber;
			vehicle.toPlant = payload.plant;
			vehicle.toPlantType = toPlantType;
			vehicle.equipNumber = payload.equipNumber;
			vehicle.loadStatus = payload.loadStatus;
			vehicle.isCallDirect=isdirect;
			vehicle.dosifi="z";
			vehicle.isprod="z";
			if(vehicle.equipNumber==""){
				vehicle.equipNumber=" ";
			}
			if(vehicle.isCallDirect==""){
				vehicle.isCallDirect=" ";
			}
			//Alert.show("getAssignVehicleRequest:"+payload);
			return vehicle;
		}
		
		public static const EXCHANGE_VEHICLE_REQUEST:String="getExchangeVehicleRequest";
		public static function getExchangeVehicleRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String,isdirect:String=""):ExchangeVehicle{
			var vehicle:ExchangeVehicle =  new ExchangeVehicle();
			vehicle.orderNumber = payload.orderNumber;
			vehicle.itemNumber = payload.itemNumber;
			vehicle.toPlant = payload.plant;
			vehicle.toPlantType = toPlantType;
			vehicle.equipNumber = payload.equipNumber;
			vehicle.vehicleNumber=payload.equipNumber;
			vehicle.loadStatus = payload.loadStatus;
			if(vehicle.equipNumber==""){
				vehicle.equipNumber=" ";
			}
			
			//Alert.show("getAssignVehicleRequest:"+payload);
			return vehicle;
		}
		
		public static const INTERCHANGE_VEHICLE_REQUEST:String="getInterchangeVehicleRequest";
		public static function getInterchangeVehicleRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String,isdirect:String=""):InterchangeVehicle{
			var vehicle:InterchangeVehicle =  new InterchangeVehicle();
			vehicle.orderNumber = payload.orderNumber;
			vehicle.itemNumber = payload.itemNumber;
			vehicle.toPlant = payload.plant;
			vehicle.toPlantType = toPlantType;
			vehicle.equipNumber = payload.equipNumber;
			vehicle.loadStatus = payload.loadStatus;
			vehicle.isCallDirect=isdirect;
			if(vehicle.equipNumber==""){
				vehicle.equipNumber=" ";
			}
			if(vehicle.isCallDirect==""){
				vehicle.isCallDirect=" ";
			}
			//Alert.show("getAssignVehicleRequest:"+payload);
			return vehicle;
		}
		
		public static const ADVANCE_BATCH_REQUEST:String="getAdvanceBatchRequest";
		public static function getAdvanceBatchRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String,isdirect:String="X"):AssignAdvance{
			var extras:Array=extra.split(":")
			var asad:AssignAdvance=new AssignAdvance();
			asad.batchType=extras.pop();
			asad.dosifi=extras.pop();
			asad.equipNumber=payload.equipNumber;
			asad.loadStatus=payload.loadStatus;
			asad.itemNumber=payload.itemNumber;
			asad.orderNumber=payload.orderNumber;
			asad.plantType=GanttServiceReference.getPlantType(payload.plant);
			return asad;
		}
		
		
		public static const ASSIGN_ONE_VEHICLE_REQUEST:String="getAssignOneVehicleRequest";
		public static function getAssignOneVehicleRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String,isdirect:String="X"):AssignVehicle{
			
			var split:Array = extra.split(":");
			var vehicle:AssignVehicle =  new AssignVehicle();
			vehicle.orderNumber = payload.orderNumber;
			vehicle.itemNumber = payload.itemNumber;
			vehicle.toPlant = split[0];
			vehicle.toPlantType = split[2];
			vehicle.equipNumber = split[1];
			vehicle.loadStatus = payload.loadStatus;
			vehicle.isCallDirect=isdirect;
			vehicle.dosifi="z";
			vehicle.isprod="z";
			if(String(split[split.length-1]).toLowerCase()=="x"){
				vehicle.dosifi=String(split[split.length-2]);
				vehicle.isprod="X";
			}
			if(String(split[split.length-1]).toLowerCase()=="w"){
				vehicle.isprod="W";
			}
			//Alert.show("getAssignVehicleRequest:"+payload);
			return vehicle;
		}
		
		public static function getAssignOneVehicleExtraID(plant:Plant,equipment:Object):String{
			return plant.plantId+":"
				+equipment.equipment+":"+equipment.status+":"+plant.zoperativeMode+":"
				+equipment.equipLabel+":"+equipment.equicatgry+":"+equipment.vehicleType;
		}
		
		
		public static const UNASSIGN_VEHICLE_REQUEST:String="getUnassignVehicleRequest";
		public static function getUnassignVehicleRequest(
			payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):UnassignVehicle{
			
			
			var vehicle:UnassignVehicle =  new UnassignVehicle();
			vehicle.orderNumber = payload.orderNumber;
			vehicle.itemNumber = payload.itemNumber;
			vehicle.toPlant = payload.plant;
			vehicle.toPlantType = toPlantType;
			/*vehicle.equipNumber = payload.equipNumber;*/
			vehicle.loadStatus = payload.loadStatus;
			//Alert.show("getAssignVehicleRequest:"+payload);
			
			return vehicle;
		}
		
		/**
		 * evento que sirve para notificar que WD debe limpiar el area de notificacion de mensajes
		 * */
		
		public static const CLEAR_MESSAGES_AREA:String="getClearMessagesArea";
		public static function getClearMessagesArea():ClearMessagesArea{
			var req:ClearMessagesArea=new ClearMessagesArea();
			return req;
		}
		
		public static const ORDER_ON_HOLD_REQUEST:String="getOrderOnHoldRequest";
		public static function getOrderOnHoldRequest(
			payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String
			):OrderOnHoldRequest{
			var hold:OrderOnHoldRequest =  new OrderOnHoldRequest();
			hold.orderNumber = payload.orderNumber;
			hold.itemNumber = payload.itemNumber;
			hold.deliveryBlock = payload.deliveryBlock;
			hold.plant=payload.plant;
			
			if(hold.deliveryBlock==""){
				hold.deliveryBlock=" ";
			}
			
			return hold;
		}
		
		public static const CHANGE_PLANT_REQUEST_DRAG:String="getChangePlantRequestDrag";
		public static function getChangePlantRequestDrag(payload:OrderLoad, productionData:ProductionData,toPlantType:String,scope:String,eventType:String="MENU"):ChangePlantRequest{
			return getChangePlantRequest(payload, productionData,toPlantType,scope,DispatcherConstants.EVENT_TYPE_DRAG_DROP);
		}
		
		public static const CHANGE_PLANT_REQUEST_MENU:String="getChangePlantRequestMenu";
		public static function getChangePlantRequestMenu(payload:OrderLoad, productionData:ProductionData,toPlantType:String,scope:String):ChangePlantRequest{
			return getChangePlantRequest(payload, productionData,toPlantType,scope,DispatcherConstants.EVENT_TYPE_MENU);
			
		}
		private static function getChangePlantRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,scope:String,eventType:String):ChangePlantRequest{
			
			
			var plant:ChangePlantRequest =  new ChangePlantRequest();
			
			plant.orderNumber =  payload.orderNumber;
			plant.itemNumber  = payload.itemNumber;
			if(plant.itemNumber==""){
				plant.itemNumber=" ";
			}
			//plant.itemNumber  = "";
			plant.material = payload.materialId;
			if(scope==""){
				plant.scope="O"
			} else if(scope=="LOAD"){
				plant.scope="L";
			}
			//plant.scope = scope;
			if (eventType == DispatcherConstants.EVENT_TYPE_MENU){
				plant.toPlant = " ";
				plant.toPlantType = " ";
			}
			else {
				plant.toPlant = payload.plant;
				plant.toPlantType = toPlantType;
			}
			plant.eventType = eventType;
			plant.fromView="LO";
			plant.isDistributedInsideArea=new String(" ");
			var plantInfo:Object=GanttServiceReference.getPlantInfo(plant.toPlant);
			if(plantInfo !=null){
				if(plantInfo.zoperativeMode=="02"){
					//planta distribuida
					plant.isDistributedInsideArea="X";
				}
			}
			return plant;
		}
		
		public static const CHANGE_VOLUME_REQUEST:String="getChangeVolumeRequest";
		public static function getChangeVolumeRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):ChangeVolume{
			
			var volume:ChangeVolume =  new ChangeVolume();
			volume.orderNumber =  payload.orderNumber;
			volume.itemNumber  = payload.itemNumber;
			volume.loadStatus = payload.loadStatus;
			return volume;
		}
		
		
		
		
		public static const REDIRECT_LOAD_REQUEST:String="getRedirectLoadRequest";
		public static function getRedirectLoadRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):RedirectLoad{
			
			var load:RedirectLoad =  new RedirectLoad();
			load.orderNumber =  payload.orderNumber;
			load.itemNumber  = payload.itemNumber;
			return load;
		}
		
		public static const REUSE_CONCRETE_REQUEST:String="getReuseConcreteRequest";
		
		public static function getReuseConcreteRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):ReuseConcrete{
			
			var concrete:ReuseConcrete =  new ReuseConcrete();
			concrete.orderNumber =  payload.orderNumber;
			concrete.itemNumber  = payload.itemNumber;
			concrete.werks=payload.plant;
			return concrete;
		}
		
		public static const DISP_LOAD_MODIFY_LOAD_REQ:String="getLoadModifyLoadReq";
		
		public static function getLoadModifyLoadReq(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):GenericEventToWD{
			var ngenericEvent:GenericEventToWD=new GenericEventToWD();
			ngenericEvent.itemNumber=payload.itemNumber;
			ngenericEvent.orderNumber=payload.orderNumber;
			ngenericEvent.wdevent=DISP_LOAD_MODIFY_LOAD_REQ
			return ngenericEvent;
		}
		public static const DISP_LOAD_CHANGE_VOLUME_REQ:String="getChangeVolumeReq";
		public static function getChangeVolumeReq(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):GenericEventToWD{
			var ngenericEvent:GenericEventToWD=new GenericEventToWD();
			ngenericEvent.itemNumber=payload.itemNumber;
			ngenericEvent.orderNumber=payload.orderNumber;
			ngenericEvent.wdevent=DISP_LOAD_CHANGE_VOLUME_REQ
			return ngenericEvent;
		}
		public static const DISP_LOAD_DELIVERY_HOUR_REQ:String="getLoadDeliveryHourReq";
		public static function getLoadDeliveryHourReq(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):GenericEventToWD{
			var ngenericEvent:GenericEventToWD=new GenericEventToWD();
			ngenericEvent.itemNumber=payload.itemNumber;
			ngenericEvent.orderNumber=payload.orderNumber;
			ngenericEvent.wdevent=DISP_LOAD_DELIVERY_HOUR_REQ
			return ngenericEvent;
		}
		public static const DISP_LOAD_DELIVERY_PLANT_REQ:String="getLoadDeliveryPlantReq";
		public static function getLoadDeliveryPlantReq(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):GenericEventToWD{
			var ngenericEvent:GenericEventToWD=new GenericEventToWD();
			ngenericEvent.itemNumber=payload.itemNumber;
			ngenericEvent.orderNumber=payload.orderNumber;
			ngenericEvent.wdevent=DISP_LOAD_DELIVERY_PLANT_REQ
			return ngenericEvent;
		}
		public static const DISP_LOAD_CHANGE_TRAVEL_TIME_REQ:String="getLoadChangeTravelTimeReq";
		public static function getLoadChangeTravelTimeReq(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):GenericEventToWD{
			var ngenericEvent:GenericEventToWD=new GenericEventToWD();
			ngenericEvent.itemNumber=payload.itemNumber;
			ngenericEvent.orderNumber=payload.orderNumber;
			ngenericEvent.wdevent=DISP_LOAD_CHANGE_TRAVEL_TIME_REQ
			return ngenericEvent;
		}
		public static const DISP_LOAD_CHANGE_LOAD_STATUS_REQ:String="getLoadChangeStatusReq";
		public static function getLoadChangeStatusReq(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):GenericEventToWD{
			var ngenericEvent:GenericEventToWD=new GenericEventToWD();
			ngenericEvent.itemNumber=payload.itemNumber;
			ngenericEvent.orderNumber=payload.orderNumber;
			ngenericEvent.wdevent=DISP_LOAD_CHANGE_LOAD_STATUS_REQ
			return ngenericEvent;
		}
		public static const DISP_LOAD_LAUNCH_PRODUCTION_REQ:String="getLoadLaunchProductionReq";
		public static function getLoadLaunchProductionReq(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):GenericEventToWD{
			var ngenericEvent:GenericEventToWD=new GenericEventToWD();
			ngenericEvent.itemNumber=payload.itemNumber;
			ngenericEvent.orderNumber=payload.orderNumber;
			ngenericEvent.wdevent=DISP_LOAD_LAUNCH_PRODUCTION_REQ
			return ngenericEvent;
		}
		public static const DISP_LOAD_CHANGE_FREQUENCY_REQ:String="getLoadChangeFrequencyReq";
		public static function getLoadChangeFrequencyReq(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):GenericEventToWD{
			var ngenericEvent:GenericEventToWD=new GenericEventToWD();
			ngenericEvent.itemNumber=payload.itemNumber;
			ngenericEvent.orderNumber=payload.orderNumber;
			ngenericEvent.wdevent=DISP_LOAD_CHANGE_FREQUENCY_REQ;
			return ngenericEvent;
		}
		
		
		public static const ASSIGN_PLANT_REQUEST:String="getAssignPlantRequest";
		public static function getAssignPlantRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):AssignPlant{
			
			var plant:AssignPlant =  new AssignPlant();
			plant.orderNumber =  payload.orderNumber;
			plant.itemNumber  = payload.itemNumber;
			plant.toPlant = payload.plant;
			plant.toPlantType = toPlantType;
			plant.loadStatus = payload.loadStatus;
			plant.itemCategory = payload.itemCategory;
			
			
			return plant;
		}
		
		public static const LAUNCH_PUMP_REQUEST:String="getLaunchPumpRequest";
		public static function getLaunchPumpRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):LaunchPump{
			
			var plant:LaunchPump =  new LaunchPump();
			plant.orderNumber =  payload.orderNumber;
			plant.itemNumber  = payload.itemNumber;
			plant.toPlant = payload.plant;
			plant.toPlantType = toPlantType;
			plant.loadStatus = payload.loadStatus;
			plant.itemCategory = payload.itemCategory;
			
			
			return plant;
		}
		
		
		
		public static const UNASSIGN_PLANT_REQUEST:String="getUnassignPlantRequest";
		
		public static function getUnassignPlantRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):UnassignPlant{
			
			var plant:UnassignPlant =  new UnassignPlant();
			plant.orderNumber =  payload.orderNumber;
			plant.itemNumber  = payload.itemNumber;
			plant.toPlant = payload.plant;
			plant.toPlantType = toPlantType;
			
			
			return plant;
		}
		public static const UNASSIGN_QUICK_REQUEST:String="getUnassignQuickRequest";
		
		public static function getUnassignQuickRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):UnassignQuick{
			
			var plant:UnassignQuick =  new UnassignQuick();
			plant.orderNumber =  payload.orderNumber;
			plant.itemNumber  = payload.itemNumber;
			plant.toPlant = payload.plant;
			plant.toPlantType = toPlantType;
			
			
			return plant;
		}
		
		public static const RENEGOTIATE_LOAD_REQUEST:String="getRenegotiateLoadRequest";
		public static function getRenegotiateLoadRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):RenegotiateLoad{
			
			var renegotiateLoad:RenegotiateLoad =  new RenegotiateLoad();
			renegotiateLoad.orderNumber =  payload.orderNumber;
			renegotiateLoad.itemNumber  = payload.itemNumber;
			return renegotiateLoad;
		}
		
		
		
		public static const SUPPORT_PLANT_REQUEST:String="getSupportPlantRequest";
		public static function getSupportPlantRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):RenegotiateLoad{
			
			var renegotiateLoad:RenegotiateLoad =  new RenegotiateLoad();
			renegotiateLoad.orderNumber =  payload.orderNumber;
			renegotiateLoad.itemNumber  = payload.itemNumber;
			return renegotiateLoad;
		}
		
		public static const SHOW_BITACORA_REQUEST:String="getShowBitacoraRequest";
		public static function getShowBitacoraRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):Bitacora{
			
			var bitacora:Bitacora =  new Bitacora();
			bitacora.orderNumber =  payload.orderNumber;
			//bitacora.itemNumber  = payload.itemNumber;
			return bitacora;
		}
		
		public static const SHOW_CHANGE_ITEM:String="getShowChangeItemRequest";
		public static function getShowChangeItemRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):ChangeItem{
			
			var bitacora:ChangeItem =  new ChangeItem();
			bitacora.orderNumber =  payload.orderNumber;
			bitacora.itemNumber  = payload.itemNumber;
			return bitacora;
		}
		
		
		public static const DISP_CHANGE_GLOBAL_VOLUME_EVT:String 		= "getGlobalChangeInfo";//"DISP_CHANGE_GLOBAL_VOLUME";
		public static const DISP_CHANGE_GLOBAL_DELIVERY_EVT:String 		= "getGlobalChangeInfo";//"DISP_CHANGE_GLOBAL_DELIVERY";
		public static const DISP_CHANGE_GLOBAL_FREQUENCY_EVT:String 	= "getGlobalChangeInfo";//"DISP_CHANGE_GLOBAL_FREQUENCY";
		public static const DISP_CHANGE_UNLOADING_TIME_EVT:String 		= "getGlobalChangeInfo";//"DISP_CHANGE_UNLOADING_TIME";
		public static const DISP_CHANGE_DELIVERY_PLANT_EVT:String 		= "getGlobalChangeInfo";//"DISP_CHANGE_DELIVERY_PLANT";
		public static const DISP_CHANGE_TRAVEL_TIME_EVT:String 			= "getGlobalChangeInfo";//"DISP_CHANGE_TRAVEL_TIME";
		public static const DISP_CHANGE_STATUS_ORDER_EVT:String 		= "getGlobalChangeInfo";//"DISP_CHANGE_STATUS_ORDER";
		public static const DISP_CHANGE_ADDITIONAL_SERVICE_EVT:String 	= "getGlobalChangeInfo";//"DISP_CHANGE_ADDITIONAL_SERVICE";
		public static const DISP_OPEN_OTS_EVT:String 					= "getGlobalChangeInfo";//"DISP_OPEN_OTS";
		public static const DISP_COPY_ORDER_EVT:String 					= "getGlobalChangeInfo";//"DISP_COPY_ORDER";
		public static const DISP_GLOBAL_ALL:String						= "getGlobalChangeInfo";
		public static const DISP_DISPLAY_OT:String						= "getGlobalChangeInfoPre";
		public static const DISP_COPY_ORDER:String						= "getGlobalChangeInfoPre";
		
		public static function getGlobalChangeInfoPre(payload:*, productionData:ProductionData,toPlantType:String,extra:String):GlobalChangeInfo{
			var globalChange:GlobalChangeInfo =  new GlobalChangeInfo();
			globalChange.orderNumber =  payload.orderNumber;
			globalChange.eventType=extra;
			return globalChange;
		}
		
		public static function getGlobalChangeInfo(payload:*, productionData:ProductionData,toPlantType:String,extra:String):GlobalChangeInfo{
			var globalChange:GlobalChangeInfo =  new GlobalChangeInfo();
			globalChange.orderNumber =  payload.orderNumber;
			globalChange.eventType=extra;
			return globalChange;
		}
		
		public static const OPEN_WINDOW_VEHICLE:String="getOpenWindowVehicle";
		public static function getOpenWindowVehicle(equipNumber:String):openWindowVehicle{
			
			var bitacora:openWindowVehicle =  new openWindowVehicle();
			bitacora.equipNumber=equipNumber;
			return bitacora;
		}
		
		public static const BRING_ALL_VEHICLES:String="getBringAllVehicles";
		public static function getBringAllVehicles(value:String):BringAllVehicles{
			
			var bitacora:BringAllVehicles =  new BringAllVehicles();
			bitacora.allOrNative=value;
			return bitacora;
		}
		
		public static const SET_PLANNING_PLANT:String="getSetPlanningPlant";
		public static function getSetPlanningPlant(value:String):SetPlanningPlant{
			
			var bitacora:SetPlanningPlant =  new SetPlanningPlant();
			bitacora.plant=value;
			return bitacora;
		}
		
		public static const SET_AUTOMATIC_DISTRIBUTION:String="getAutomaticDistribution";
		public static function getAutomaticDistribution(value:String):AutomaticDistribution{
			
			var bitacora:AutomaticDistribution =  new AutomaticDistribution();
			bitacora.plant=value;
			return bitacora;
		}
		
		
		public static const OPEN_OTS_ORDER:String="getOpenOtsOrder";
		public static function getOpenOtsOrder(value:String):OpenOTS{
			
			var bitacora:OpenOTS =  new OpenOTS();
			bitacora.orderNumber=value;
			return bitacora;
		}
		
		public static const SAVE_PLANNING_CHANGE:String="getSavePlanningChange";
		public static function getSavePlanningChange(payload:*):SavePlanningScreenChange{
			
			var bitacora:SavePlanningScreenChange =  new SavePlanningScreenChange();
			return bitacora;
		}
		
		public static const ORDER_DETAIL:String="getOrderDetailRequest";
		public static function getOrderDetailRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):OrderDetail{
			
			var orderDetail:OrderDetail =  new OrderDetail();
			
			orderDetail.orderNumber =  payload.orderNumber;
			orderDetail.posy=DispatcherViewMediator.currentInstance.view.mouseY.toString();
			
			return orderDetail;
		}
		
		public static const RESIZE_HEIGHT:String="getResizeHeight";
		public static function getResizeHeight(heightSize:String):ResizeHeight{
			
			var resizeHeight:ResizeHeight =  new ResizeHeight();
			resizeHeight.heightSize= heightSize;
			
			
			return resizeHeight;
		}
		
		public static const COST_DATA:String="getCostData";
		public static function getCostData(cost1:String, cost2:String):CostData{
			
			var costData:CostData =  new CostData();
			if(cost1==null || cost1==""){
				costData.cost1 =  "0";
			}else{
				costData.cost1=cost1;
			}
			
			if(cost2==null || cost2==""){
				costData.cost2 =  "0";
			}else{
				costData.cost2=cost2;
			}
			
			return costData;
		}
		
		
		public static const COMMENT_DISP_REQUEST:String="getCommentDispRequest";
		public static function getCommentDispRequest(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):CommentDisp{
			
			var comentDisp:CommentDisp =  new CommentDisp();
			comentDisp.pa_OrderNumber =  payload.orderNumber;
			
			return comentDisp;
		}
		
		
		
		public static const DISP_LAUNCHTICKET:String="getDispLaunchTicket";
		public static function getDispLaunchTicket(payload:OrderLoad, productionData:ProductionData,toPlantType:String,extra:String):LaunchTicket{
			
			var launchTiket:LaunchTicket =  new LaunchTicket();
			launchTiket.orderNumber =  payload.orderNumber;
			launchTiket.itemNumber = payload.itemNumber;
			
			return launchTiket;
		}
		
		
		
		
		
		
		
		
	}
}