package com.cemex.rms.dispatcher.views.mediators
{
	import com.adobe.fiber.runtime.lib.DateTimeFunc;
	import com.cemex.rms.common.date.FlexDateHelper;
	import com.cemex.rms.common.filters.FilterCondition;
	import com.cemex.rms.common.filters.FilterHelper;
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.common.gantt.GanttResource;
	import com.cemex.rms.common.gantt.GanttTask;
	import com.cemex.rms.common.helpers.ContextMenuHelper;
	import com.cemex.rms.common.helpers.FullScreenHelper;
	import com.cemex.rms.common.lcds.AbstractReceiverService;
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.common.services.config.IServiceModel;
	import com.cemex.rms.common.services.events.ServiceEvent;
	import com.cemex.rms.common.transformer.IDataTransformer;
	import com.cemex.rms.common.tree.GenericNode;
	import com.cemex.rms.common.tree.ResourceHierarchicalData;
	import com.cemex.rms.common.utils.DictionaryMap;
	import com.cemex.rms.common.views.GenericMediator;
	import com.cemex.rms.dispatcher.constants.ExtrasConstants;
	import com.cemex.rms.dispatcher.constants.Logger;
	import com.cemex.rms.dispatcher.constants.OTRConstants;
	import com.cemex.rms.dispatcher.constants.ParamtersConstants;
	import com.cemex.rms.dispatcher.constants.SecurityConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.constants.ValidateParameters;
	import com.cemex.rms.dispatcher.events.SearchBtnClickViewEvent;
	import com.cemex.rms.dispatcher.events.SearchBtnVehicleEvent;
	import com.cemex.rms.dispatcher.events.TogglePlaintViewEvent;
	import com.cemex.rms.dispatcher.events.lcds.LCDSReconnectEvent;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.helpers.ILogDateFormatterHelper;
	import com.cemex.rms.dispatcher.helpers.TimeZoneHelper;
	import com.cemex.rms.dispatcher.services.flashislands.events.FlashIslandsDataEvent;
	import com.cemex.rms.dispatcher.services.flashislands.events.ReturnLogEvent;
	import com.cemex.rms.dispatcher.services.flashislands.helper.WDRequestHelper;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandImpl;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.AddTruck;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.ClearMessagesArea;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.ClearPlanningChanges;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.EditTruck;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.SaveNowPlanningScreenChange;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.SavePlanningScreenChange;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.VisualConfigurationEvent;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.testComponent1;
	import com.cemex.rms.dispatcher.services.flashislands.vo.AddProd;
	import com.cemex.rms.dispatcher.services.flashislands.vo.ColorSetting;
	import com.cemex.rms.dispatcher.services.flashislands.vo.Equipment;
	import com.cemex.rms.dispatcher.services.flashislands.vo.ProductionData;
	import com.cemex.rms.dispatcher.services.flashislands.vo.ReturnLog;
	import com.cemex.rms.dispatcher.services.lcds.events.PlantOperativeModeLCDSEvent;
	import com.cemex.rms.dispatcher.services.lcds.events.ProductionStatusLCDSEvent;
	import com.cemex.rms.dispatcher.services.lcds.events.SalesOrderLCDSEvent;
	import com.cemex.rms.dispatcher.services.lcds.events.VehicleLCDSEvent;
	import com.cemex.rms.dispatcher.views.DispatcherView;
	import com.cemex.rms.dispatcher.views.SelectCurrentPlant;
	import com.cemex.rms.dispatcher.views.assignedLoads.transformers.AssignedLoadsDataTransformer;
	import com.cemex.rms.dispatcher.views.components.BottomTabNavigator;
	import com.cemex.rms.dispatcher.views.components.CanvasColor;
	import com.cemex.rms.dispatcher.views.components.ChooseOnePlantPopUp;
	import com.cemex.rms.dispatcher.views.components.DashedLine;
	import com.cemex.rms.dispatcher.views.components.DelayDismissDialog;
	import com.cemex.rms.dispatcher.views.components.HTMLToolTip;
	import com.cemex.rms.dispatcher.views.components.LabelExt;
	import com.cemex.rms.dispatcher.views.components.TestWindowSession;
	import com.cemex.rms.dispatcher.views.components.WarningPopUp;
	import com.cemex.rms.dispatcher.views.components.searchProductionOrders;
	import com.cemex.rms.dispatcher.views.factory.ButtonFactory;
	import com.cemex.rms.dispatcher.views.factory.TimeDispatcher;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.LoadPerOrderPlantOrderResourceRenderer;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.transformers.LoadPerOrderOrderDataTransformer;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.transformers.LoadPerOrderPlantOrderDataTransformer;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.transformers.LoadsPerOrderAbstractDataTransformer;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.LoadsPerVehicleTaskRenderer;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.transformers.LoadPerVehiclePlantVehicleDataTransformer;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.tree.LoadsPerVehicleResource;
	import com.cemex.rms.dispatcher.views.planningScreen.renderers.PlanningScreenTaskRenderer;
	import com.cemex.rms.dispatcher.views.planningScreen.renderers.PlanningScreenTreeResourceRenderer;
	import com.cemex.rms.dispatcher.views.planningScreen.transformers.PlanningScreenDataTransformer;
	import com.cemex.rms.dispatcher.views.planningScreen.transformers.PlanningScreenVehicleDataTransformer;
	import com.cemex.rms.dispatcher.views.planningScreen.tree.PlanningScreenResource;
	import com.cemex.rms.dispatcher.views.planningScreen.tree.PlanningScreenTask;
	import com.cemex.rms.dispatcher.views.reports.PlantReport;
	import com.cemex.rms.dispatcher.views.reports.ReportConfig;
	import com.cemex.rms.dispatcher.views.reports.VehicleReport;
	import com.cemex.rms.dispatcher.views.reports.ViewReportObject;
	import com.cemex.rms.dispatcher.views.serviceAgentScreen.transformers.ServiceAgentScreenDataTransformer;
	import com.cemex.rms.dispatcher.views.serviceAgentScreen.tree.ServiceAgentScreenTask;
	import com.cemex.rms.dispatcher.vo.DispatchViewConfiguration;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import flash.display.Graphics;
	import flash.events.ContextMenuEvent;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;
	import flash.net.SharedObject;
	import flash.system.System;
	import flash.utils.Dictionary;
	import flash.utils.Timer;
	import flash.utils.describeType;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	import flash.utils.getTimer;
	
	import mx.binding.utils.BindingUtils;
	import mx.collections.ArrayCollection;
	import mx.collections.HierarchicalCollectionView;
	import mx.collections.IHierarchicalCollectionViewCursor;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.containers.Canvas;
	import mx.containers.ViewStack;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.TextInput;
	import mx.controls.ToolTip;
	import mx.core.IFlexDisplayObject;
	import mx.core.UIComponent;
	import mx.core.UITextField;
	import mx.events.CloseEvent;
	import mx.events.EffectEvent;
	import mx.events.ListEvent;
	import mx.formatters.CurrencyFormatter;
	import mx.managers.PopUpManager;
	import mx.managers.ToolTipManager;
	import mx.messaging.ChannelSet;
	import mx.messaging.events.MessageAckEvent;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.messaging.messages.AsyncMessage;
	
	import ilog.gantt.GanttSheetEvent;
	import ilog.gantt.GanttSheetEventReason;
	import ilog.gantt.TaskItem;
	import ilog.gantt.TimeControllerGetter;
	import ilog.utils.TimeUnit;
	
	import weborb.messaging.WeborbConsumer;
	import weborb.messaging.WeborbMessagingChannel;
	import weborb.messaging.WeborbProducer;
	
	/**
	 * 
	 * DispatcherViewMediator es la clase mediador que le da funcionalidad a la vista, tambien
	 *  se encarga de hacer que los botones y la funcionalidad externa funcionen
	 * a demas que esta clase es la que se encarga de recibir los datos externos
	 * Es decir esta clase se encarga de afectar los datos y la vista del gantt
	 * 
	 * 
	 */
	public class DispatcherViewMediator extends GenericMediator {
		
		public function DispatcherViewMediator() {
			super();
			
		}
		
		
		/**
		 * La vista 
		 */
		[Inject]
		public var view : DispatcherView;
		
		private var salesOrderPushCounter:Number=0;
		private var plantPushCounter:Number=0;
		private var vehiclePushCounter:Number=0;
		private var productionPushCounter:Number=0;
		public var poConfirmationCounter:Number=0;
		
		private var salesOrderLastDate:String="";
		private var plantLastDate:String="";
		private var vehicleLastDate:String="";
		private var productionLastDate:String="";
		public var poConfirmationDate:String="";
		
		
		/**
		 * 
		 */
		private var rawData:String="Init";
		
		
		/**
		 * the logger
		 */
		private var logger:ILogger = LoggerFactory.getLogger("DispatcherViewMediator");
		
		
		/**
		 * The date selected by the user
		 */
		private var baseDate:Date;
		
		
		// 
		
		/**
		 * Timer Display Objects content
		 */
		private var content:UIComponent;
		
		/**
		 * Timer Display Objects
		 */
		private var timeIndicators:UIComponent;
		
		/**
		 * Timer Display Objects Indicator
		 */
		private var timeIndicatorTimer:Timer;
		
		/**
		 * Timer Display Objects Controller
		 */
		public static var timeController:Object; 
		
		/**
		 * Timer Display Objects Controller second screen
		 */
		public static var timeControllerSecond:Object; 
		/**
		 * this var controls all the tasks instances
		 **/
		public static var taskInstances:Dictionary;
		
		/**
		 * this var controls the instance of this class
		 **/
		public static var currentInstance:DispatcherViewMediator;
		
		/**
		 * flag for order plant is by schedule
		 * */
		public var isPerHour:Boolean=false;
		
		
		/**
		 * This method will get the View of this mediator 
		 */
		override public function getView():UIComponent{
			return view;
		}
		/**
		 * this is a static var that allows to know which plants should be shown
		 */
		public static var franceShowSinglePlant:String;
		
		/**
		 * this variable controls the delay time
		 */
		public static var hashDelay:Number=0;
		/**
		 * this variable control screenSize
		 */
		/** 
		 * this variable controls wheater to show or hide the changes to folio 71534
		 */
		public static var show71534:Boolean=true;
		
		public static var heightSize:int;
		/**
		 * This method is the first executed
		 */
		public static var currentView:String = TVARVCPreffixes.LOADS_PER_ORDER_VIEW_ID;
		/**
		 * this variable controls the configuration of the view
		 * 
		 */
		
		/**
		 * controlador de tiempo
		 */
		
		public static var timeVisualController:TimeDispatcher=new TimeDispatcher();
		public static var visualConfigurations:DispatchViewConfiguration=new DispatchViewConfiguration();
		public static var wmsg:WarningPopUp=null;
		
		override public function onRegister():void {
			
			
			// SEccion de eventos del mouse
			init();
			/*if (TimeDispatcher.getCurrentZoom()==null){
				Alert.show("creado");
				TimeDispatcher.setCurrentZoom(new Date(),new Date());
			} else {
				Alert.show(TimeDispatcher.getCurrentZoom().toString());
				
			}*/
			ToolTipManager.toolTipClass = HTMLToolTip;
			currentInstance=this;
			hashDelay=(new Date()).time;
			// SE settean los valores para que se cambie de pantalla
			view.loadsPerOrderLabel.name		= TVARVCPreffixes.LOADS_PER_ORDER_VIEW_ID;
			view.loadsPerVehicleLabel.name		= TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID;
			view.assignedLoadsLabel.name		= TVARVCPreffixes.ASSIGNED_LOADS_VIEW_ID;
			/*view.planningScreenLabel.name		= TVARVCPreffixes.PLANNING_SCREEN_VIEW_ID;
			view.serviceAgentScreenLabel.name	=TVARVCPreffixes.SERVICE_AGENT_SCREEN_VIEW_ID;*/
			
			// SE registran los eventos de usuario
			view.zoomin.addEventListener(MouseEvent.CLICK,zoomin);
			view.zoomout.addEventListener(MouseEvent.CLICK,zoomout);
			view.showAll.addEventListener(MouseEvent.CLICK,showAll);
			view.refresh.addEventListener(MouseEvent.CLICK,refresh);
			view.currentTime.addEventListener(MouseEvent.CLICK,gotoCurrentTime);
			view.btnScreenHVertical.addEventListener(MouseEvent.CLICK, resizing);
			view.btnScreenLHVertical.addEventListener(MouseEvent.CLICK, resizing);
			//view.selectPlantWindow.addEventListener(MouseEvent.CLICK,openSelectPlantWindow);
			/*view.riseEvent.addEventListener(MouseEvent.CLICK,riseEventMediator);*/
			view.editTruck.addEventListener(MouseEvent.CLICK,editTruckEvent);
			view.addTruck.addEventListener(MouseEvent.CLICK,addTruckEvent);
			view.autDistr.addEventListener(MouseEvent.CLICK,automaticDistribution);
			view.saveVisualChanges.addEventListener(MouseEvent.CLICK,savePlanningChanges);
			view.removeItems.addEventListener(MouseEvent.CLICK,removePlanningChanges);
			//view.emule.addEventListener(MouseEvent.CLICK,putShape);
			view.vehicleScheduleView.ganttSheet.minZoomFactor= TimeUnit.SECOND.milliseconds;;
			view.vehicleScheduleView.ganttSheet.maxZoomFactor=TimeUnit.DAY.milliseconds;
			
			view.vehicleScheduleView.timeScale.majorLabelFunction=dateFormatter;
			view.vehicleScheduleView.timeScale.minorLabelFunction=dateFormatter;
			// SEccion de eventos Varios
			view.viewStack.addEventListener(Event.CHANGE,changeViewHandler);
			view.gantt.ganttSheet.addEventListener(GanttSheetEvent.CHANGE,movingGantt);
			view.gantt.ganttSheet.addEventListener(GanttSheetEvent.ITEM_EDIT_END,itemEditEnd);
			view.vehicleScheduleView.ganttSheet.addEventListener(GanttSheetEvent.ITEM_ROLL_OVER,disconnectDrag);
			view.vehicleScheduleView.ganttSheet.addEventListener(GanttSheetEvent.ITEM_ROLL_OUT,reconnectDrag);
			view.vehicleScheduleView.ganttSheet.addEventListener(GanttSheetEvent.ITEM_EDIT_BEGIN,secondEditBegin);
			view.vehicleScheduleView.ganttSheet.addEventListener(GanttSheetEvent.ITEM_EDIT_END,secondEditEnd);
			view.vehicleScheduleView.ganttSheet.addEventListener(GanttSheetEvent.ITEM_EDIT_MOVE,secondEditBegin);
			view.vehicleScheduleView.ganttSheet.commitItemFunction=upperCommitItemFunction;
			view.gantt.ganttSheet.addEventListener(GanttSheetEvent.ITEM_EDIT_BEGIN,itemChangeAsDragAndDropBegin);
			view.gantt.ganttSheet.addEventListener(MouseEvent.MOUSE_DOWN,cancelAllMovements);
			view.gantt.ganttSheet.addEventListener(MouseEvent.MOUSE_UP,continueAllMovements);
			view.gantt.ganttSheet.addEventListener(GanttSheetEvent.ITEM_EDIT_MOVE,itemIsBeingMoved); 
			view.gantt.ganttSheet.addEventListener(GanttSheetEvent.ITEM_EDIT_BEGIN,startedDrag);
			view.gantt.ganttSheet.taskBackToFrontSortFunction=taskSort;
			view.gantt.ganttSheet.addEventListener(GanttSheetEvent.VISIBLE_TIME_RANGE_CHANGE,movingGantt); 
			view.vehicleScheduleView.ganttSheet.addEventListener(GanttSheetEvent.VISIBLE_TIME_RANGE_CHANGE,movingGanttVehicle); 
			view.stage.addEventListener(KeyboardEvent.KEY_DOWN,addKeyForSearch);
			
			//eventos para crear pushes
			//	view.nu.addEventListener(MouseEvent.CLICK,dummyevt)
			//view.del.addEventListener(MouseEvent.CLICK,borrarPush);
			// Collect Order Filtering 
			view.collect.addEventListener(MouseEvent.CLICK,collectFilter);
			view.cleanMsg.addEventListener(MouseEvent.CLICK,clearMessages);
			view.loadWithOutRecipe_cb.addEventListener(MouseEvent.CLICK,filterRecipes);
			
			view.LoadsPerHour.addEventListener(MouseEvent.CLICK, loadsPerHour)
			
			
			
			// -------------SEccion de eventos -----
			/// Eventos de Plash Island
			eventMap.mapListener(eventDispatcher,FlashIslandsDataEvent.FLASHISLAND_DATA_READY,islandDataEventHandler);
			eventMap.mapListener(eventDispatcher,FlashIslandsDataEvent.FLASHISLAND_DATA_CONFIRM,islandDataConfirmEventHandler);
			eventMap.mapListener(eventDispatcher,ReturnLogEvent.RETURN_LOG_EVENT,returnLogEvent);
			eventMap.mapListener(eventDispatcher,SearchBtnClickViewEvent.SEARCH_BTN_CLICK_EVENT,seekInTreeView);
			eventMap.mapListener(eventDispatcher,SearchBtnVehicleEvent.SEARCH_BTN_VEHICLE_CLICK_EVENT,expandTree);
			
			// Eventos de LCDS (Push)
			eventMap.mapListener(eventDispatcher,SalesOrderLCDSEvent.LCDS_DATA_RECEIVED,gotPushFromSalesOrder);
			eventMap.mapListener(eventDispatcher,VehicleLCDSEvent.LCDS_DATA_RECEIVED,gotPushFromVehicle);
			eventMap.mapListener(eventDispatcher,ProductionStatusLCDSEvent.LCDS_DATA_RECEIVED,gotPushFromProductionStatus);
			eventMap.mapListener(eventDispatcher,PlantOperativeModeLCDSEvent.LCDS_DATA_RECEIVED,gotPushFromPlantOperativeMode);
			
			// Eventos de pantalla especiales
			eventMap.mapListener(eventDispatcher,TogglePlaintViewEvent.TOGGLE_PLAIN_VIEW,togglePlainView);
			
			
			
			// Seccion del Timer
			content = view.gantt.ganttSheet.getChildByName("content")as UIComponent;
			timeIndicators = content.getChildByName("timeIndicators")as UIComponent;
			timeController = TimeControllerGetter.getTimeController(view.gantt.ganttSheet);
			timeControllerSecond = TimeControllerGetter.getTimeController(view.vehicleScheduleView.ganttSheet);
			
			
			timeIndicatorTimer = new Timer(0);
			DispatcherViewMediator.isUsingPush=true;
			timeIndicatorTimer.addEventListener(TimerEvent.TIMER,timeChanged)
			timeIndicatorTimer.start();
			refreshTime.addEventListener(TimerEvent.TIMER, doRefresh);
			
			// Click derecho
			FullScreenHelper.prepareFlashIslandFullScreen();
			ContextMenuHelper.addContextMenuOption("Clean log 11/Sept "+TimeDispatcher.getCurrentZoom(), cleanLog);
			ContextMenuHelper.addContextMenuOption("Copy Log 28_1/ene", copyLog);
			ContextMenuHelper.addContextMenuOption("Activate event mapping",activateMapping);
			ContextMenuHelper.addContextMenuOption("Activated test mode:"+AbstractReceiverService.isTestMode, activeTestMode);
			ContextMenuHelper.addContextMenuOption("Activated data logging:"+DispatcherIslandImpl.dataLogging, activeDataLogging);
			ContextMenuHelper.addContextMenuOption("Create new name session",createSession);
			ContextMenuHelper.addContextMenuOption("Search production orders",searchProduction);
			ContextMenuHelper.addContextMenuOption("Copy Received Data",copyReceivedData);
			ContextMenuHelper.addContextMenuOption("Copy Data", copyToClipboard);
			ContextMenuHelper.addContextMenuOption("Toggle Full Screen View",fullScreenMenuHandler);
			tracetip=ToolTipManager.createToolTip("Loading time2:",100,100,"errorTipAbove") as ToolTip;
			/*			golabTooltip=ToolTipManager.createToolTip("TooltipGlobal",100,100,"errorTipAbove") as ToolTip;*/
			tracetipAdd=ToolTipManager.createToolTip("Loading time2:",100,100,"errorTipRight") as ToolTip;
			/*			golabTooltip.visible  = false;*/
			tipTaskInformationAdd=ToolTipManager.createToolTip("Loading time2:",100,100,"errorTipBelow") as ToolTip;
			tracetip.visible=false;
			tracetipAdd.visible=false;
			tipTaskInformationAdd.visible=false;
			view.addEventListener(MouseEvent.CLICK,dropTruckLine);
			
			//selecciÃ³n de planta mediante combo
			view.cboPlantSelector.addEventListener(ListEvent.CHANGE, cboPlantSelectorChangeHandler);
			disableStack(false);
			//fakepush
			//view.btnFakePush.addEventListener(MouseEvent.CLICK, fakearPush);
			initiateInternalPush();
			if(view.viewStack.numChildren==2){
				currentView=TVARVCPreffixes.SERVICE_AGENT_SCREEN_VIEW_ID;
			}
			if(/*view.viewStack.selectedIndex==4 || */(view.viewStack.selectedIndex==1 && view.viewStack.numChildren==2)){
				 view.Options2.visible = false;
			} else {
				view.Options2.visible = true;
			}
			if(currentView==TVARVCPreffixes.SERVICE_AGENT_SCREEN_VIEW_ID && !DispatcherIslandImpl.isBatcher){
				view.collect.enabled=false;
			}
			
			
			
		}
		
		public function updatePushPanel():void{
			//actualiza el display
			var innerText1:String="Interface\n"+
				"\nSalesOrder: "+
				"\nVehicle: "+
				"\nProd. Order Status: "+
				"\nPlant mode: "+
				"\nPoConfirmation: ";
			var innerText2:String="Count\n"+
				"\n"+salesOrderPushCounter.toString()+
				"\n"+vehiclePushCounter.toString()+
				"\n"+productionPushCounter.toString()+
				"\n"+plantPushCounter.toString()+
				"\n"+poConfirmationCounter.toString();
			var innerText3:String="Last received\n"+
				"\n"+salesOrderLastDate.toString()+
				"\n"+vehicleLastDate.toString()+
				"\n"+productionLastDate.toString()+
				"\n"+plantLastDate.toString()+
				"\n"+poConfirmationDate.toString();
			view.pushMonitorTextInterface.text=innerText1;
			view.pushMonitorTextcounter.text=innerText2;
			view.pushMonitorTextDate.text=innerText3;
			
		}
		
		public function openSelectPlantWindow(evt:MouseEvent=null):void{
			var window:SelectCurrentPlant=PopUpManager.createPopUp(this.view,SelectCurrentPlant,true) as SelectCurrentPlant;
			PopUpManager.centerPopUp(window);
		}
		private function disableStack(status:Boolean):void{
			view.viewStack.enabled=status;
			view.refresh.enabled=status;
			//view.selectPlantWindow.enabled=status;
			if(view.viewStack.numChildren==3){
				try{
					view.loadsPerOrderLabel.enabled=status;	
				} catch(e){
					
				}
				try{
					view.loadsPerVehicleLabel.enabled=status;
				} catch(e){
					
				}
				try{
					view.assignedLoadsLabel.enabled=status;
				} catch(e){
					
				}
			}
			if(view.viewStack.numChildren==2){
				try{
					/*view.serviceAgentScreenLabel.enabled=status;*/
				} catch(e){
					
				}
				try{
					/*view.planningScreenLabel.enabled=status;*/
				} catch(e){
					
				}
			}
				
			
		}
		private function fakearPush(e:MouseEvent):void{
			gotPushFromProductionStatus(null);
		}
		
		private function dummyevt(evt:MouseEvent):void{
			
		}
		private function randomRange(minNum:Number, maxNum:Number):Number{
			return (Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum);
		}
		private var producerMulti:WeborbProducer;
		private var consumerMulti:WeborbConsumer;
		public function initiateInternalPush():void{
			var cs:ChannelSet = new ChannelSet();
			var cs2:ChannelSet = new ChannelSet();
			var uri:String = "rtmp://mxoccrmsrpp02.noam.cemexnet.com:1962/weborb";
			var channel:WeborbMessagingChannel = new WeborbMessagingChannel("rtmp", uri);
			var channel2:WeborbMessagingChannel = new WeborbMessagingChannel("rtmp", uri);
			channel2.addEventListener(MessageEvent.MESSAGE, receivedMessage);
			channel.addEventListener(MessageEvent.MESSAGE, receivedMessage);
			cs.addChannel(channel);
			cs2.addChannel(channel2);
			
			producerMulti = new WeborbProducer();
			producerMulti.channelSet = cs;
			producerMulti.destination = "SampleRTMPDestination";
			producerMulti.subqueue = "sub";
			producerMulti.addEventListener(MessageAckEvent.ACKNOWLEDGE, onMessageAck);
			producerMulti.addEventListener(MessageFaultEvent.FAULT, onMessageFault);
			
			consumerMulti=new WeborbConsumer();
			consumerMulti.channelSet=cs2;
			consumerMulti.addEventListener(MessageAckEvent.ACKNOWLEDGE, onMessageAck);
			consumerMulti.addEventListener(MessageFaultEvent.FAULT, onMessageFault);
			consumerMulti.subqueue="sub";
			consumerMulti.destination= "SampleRTMPDestination";
			consumerMulti.addEventListener( MessageEvent.MESSAGE, receivedMessage );
			consumerMulti.subscribe("me");
			dispatchId=String.fromCharCode(randomRange(32,127))+String.fromCharCode(randomRange(32,127))
			var msg:AsyncMessage=new AsyncMessage();
			msg.body="who:"+dispatchId;
			producerMulti.send(msg);
		}
		private var dispatchId:String;
		private var isSync:Boolean=false;
		private function receivedMessage(evt:MessageEvent):void{
			var msg:Object=evt.message.body;
			sharedo=SharedObject.getLocal("rms","/");
			if(String(msg).indexOf("who")!=-1){
				//alguien pide el id de este despacho
				//verificar que el id del who no sea este mismo
				if(String(msg)!="who:"+dispatchId){
					//enviar el id de este despacho
					var msga:AsyncMessage=new AsyncMessage();
					msga.body="w:"+dispatchId;
					producerMulti.send(msga);
				}
			} else if(String(msg).indexOf("w:")!=-1){
				//llego el id de alguien, ver si ya estamos sync
				if(!isSync){
					//solicitar los dismiss
					var msga:AsyncMessage=new AsyncMessage();
					msga.body="r:"+String(msg).substr(2);
					producerMulti.send(msga);
				}
			} else if(String(msg)=="r:"+dispatchId){
				//enviar los dismiss
				
				var msga:AsyncMessage=new AsyncMessage();
				msga.body=sharedo.data;
				producerMulti.send(msga);
			} else {
				if((msg is Object) && !isSync ){
					isSync=true;
					//llegaron todos los pedidos en dismiss y hacer concat
					for(var rr in msg){
						sharedo.data[rr]=msg[rr];
						sharedo.flush();
					}
				}
			}
			
		}
		private function onMessageAck(evt:MessageAckEvent):void{
			
		}
		private function onMessageFault(evt:MessageFaultEvent):void{
			
		}
		private function cboPlantSelectorChangeHandler(e:ListEvent):void{
			var plantSelectedTMP:String = view.cboPlantSelector.selectedItem.plantId;
			GanttServiceReference.dispatchIslandEvent(WDRequestHelper.getSetPlanningPlant(plantSelectedTMP));
			franceShowSinglePlant = plantSelectedTMP;
			refresh(null);
		}
		
		private function plantSelectorClickHandler(e:MouseEvent):void{
			trace("wait");
		}
		
		private var wasError:String=null;
		public function upperCommitItemFunction(param:Object):void{
			view.gantt.ganttSheet.visibleTimeRangeStart = minMaxDates[0];
			view.gantt.ganttSheet.visibleTimeRangeEnd = minMaxDates[1];
			view.gantt.ganttSheet.visibleTimeRangeStart=FlexDateHelper.addSeconds(view.gantt.ganttSheet.visibleTimeRangeStart,1);
			
			
			if(param.data.data.city=="SCHEDULE"){
				return;
			}
			if(wasError!=null){
				view.vehicleScheduleView.ganttSheet.invalidateDisplayList();
				view.vehicleScheduleView.ganttSheet.invalidateProperties();
				view.vehicleScheduleView.ganttSheet.validateNow();
				//showAll(new MouseEvent(MouseEvent.CLICK));//regresar qui
				/*var responseUpdateContext:* = updateContext(param.data.data,param.data.resourceId,true,secondCurrentTransformer);
				param.data.data=wasError;*/
				
				wasError=null;
			} else {
				/*param.data.data.equipNumber=param.resourceId;
				param.data.data.equipLabel=param.resource.equipLabel;*/
				var dtp:HierarchicalCollectionView=view.vehicleScheduleView.dataGridBack.dataProvider as HierarchicalCollectionView;
				//var responseUpdateContext:* = updateContext(param.data.data,param.data.data.equipNumber,true,secondCurrentTransformer);
				var responseUpdateContext:* = updateContext(param.data.data,param.data.resourceId,true,secondCurrentTransformer);
				var objdp:Object=view.gantt.taskDataProvider;
				var objdp2:Object=view.gantt.resourceDataProvider;
				//removeOldPositionData(param.data.resourceId, param.data.data.orderNumber, param.data.data.loadNumber);
				//seekAndDestroy(param.data.data,param.data.data.plant,param.data.data.equipNumber,param.data.data.equipNumber,secondCurrentTransformer);
				//var dict:DictionaryMap=secondCurrentTransformer.getTree().ctx;
				//var dtp:HierarchicalCollectionView=view.vehicleScheduleView.dataGridBack.dataProvider as HierarchicalCollectionView;
				/*secondCurrentTransformer.addData(param.data.data,GanttTask.STEP_MOVED,TimeZoneHelper.getServer());*/
				
			}
			
		}
		
		private function removeOldPositionData(oldVehicle:String, orderNumber:String, loadNumber:String):void{
			var rdp:ArrayCollection = view.vehicleScheduleView.resourceDataProvider.source.source;
			var childrens:ArrayCollection;
			for (var cuentaTreeItems:Number=0; cuentaTreeItems<rdp.length;cuentaTreeItems++){
				if (rdp[cuentaTreeItems].equipNumber==oldVehicle){
					childrens = rdp[cuentaTreeItems]._children;
					for (var cuentaChildrens:Number=0;cuentaChildrens<childrens.length;cuentaChildrens++){
						if (childrens[cuentaChildrens].data.orderNumber==orderNumber && childrens[cuentaChildrens].data.loadNumber == loadNumber){
							ArrayCollection(rdp[cuentaTreeItems]._children).removeItemAt(cuentaChildrens);
							break;
						}
					}
					break;
				}
			}
		}
		private var _lockUpperGantt:Number=0;
		private var minMaxDates:Array;
		private function movingGantt(evt:GanttSheetEvent):void{
			saveRange();
			if((getTimer()-_lockUpperGantt)>1000){
				trace("Moving _gantt1");
				var ss:String="ss";
				
				//determinar cual es el que tiene mayor campo visual
				view.vehicleScheduleView.ganttSheet.visibleTimeRangeStart=startDate;
				//view.vehicleScheduleView.ganttSheet.maxVisibleTime=view.gantt.ganttSheet.maxVisibleTime
				view.vehicleScheduleView.ganttSheet.visibleTimeRangeEnd=endDate;
				_lockDownGantt=getTimer();
				
			}
			
			var now:Date=TimeZoneHelper.getServer();
			if (isSameAsBaseDate(now) ){
				if (currentTransformer != null){
					DispatcherViewMediator.isUsingPush=true;
					currentTransformer.timeChanged(now);
				}
			}			
			
		}
		private var _lockDownGantt:Number=0;
		private function movingGanttVehicle(evt:GanttSheetEvent):void{
			
			if((getTimer()-_lockDownGantt)>1000){
				trace("Moving _gantt2");	
				var ss:String="ss";
				view.gantt.ganttSheet.visibleTimeRangeStart=view.vehicleScheduleView.ganttSheet.visibleTimeRangeStart;
				//view.gantt.ganttSheet.maxVisibleTime=view.vehicleScheduleView.ganttSheet.maxVisibleTime;
				view.gantt.ganttSheet.visibleTimeRangeEnd=view.vehicleScheduleView.ganttSheet.visibleTimeRangeEnd;
				_lockUpperGantt=getTimer();
				
			}
			//saveRange();
			
		}
		private function dateFormatter(date:Date,dateFormat:String,timeUnit:TimeUnit,timeUnitSteps:Number):String
		{
			return ILogDateFormatterHelper.formatDateComplete(date, dateFormat);
		}
		public function secondEditBegin(evt:GanttSheetEvent):void{
			var somethinghere:String="dfdf";
			var toPlantType:String = GanttServiceReference.getPlantType(franceShowSinglePlant);
			if(evt.item.data.city=="SCHEDULE"){
				view.vehicleScheduleView.ganttSheet.moveEnabled=false;
				view.vehicleScheduleView.ganttSheet.reassignEnabled=false;
			}
			//poner las validaciones
		}
		
		public function secondEditEnd(evt:GanttSheetEvent):void{
			
			var itemData:OrderLoad=evt.item.data;
			if(itemData.city=="SCHEDULE"){
				return;
			}
			originalVehicleSource=evt;
			var rowItem:Object = view.vehicleScheduleView.dataGridBack.getRowItemAtPosition(view.mouseY);
			var vehicleid:String="";
			if(rowItem!=null){
				vehicleid=rowItem.equipNumber;
				
			}
			var vehicleSelected:Object;
			for(var ve:int=0;ve<DispatcherViewMediator.vehiculosCorrectos.length;ve++){
				if(DispatcherViewMediator.vehiculosCorrectos.getItemAt(ve).equipNumber==vehicleid){
					vehicleSelected=DispatcherViewMediator.vehiculosCorrectos.getItemAt(ve);
					privateVehicleSelected=vehicleSelected;
					break;
				}
			}
			
			var mapScheduleVehicle:ArrayCollection=DispatcherIslandImpl.mapVehicles.get(vehicleSelected.equipNumber);
			var isVehicleSchedule:Boolean=true;
			for(var msv:int=0;msv<mapScheduleVehicle.length;msv++){
				var finDate:Date=mapScheduleVehicle.getItemAt(msv).finalDate;
				finDate.hours=Number(mapScheduleVehicle.getItemAt(msv).finalHour.split(":")[0]);
				finDate.minutes=Number(mapScheduleVehicle.getItemAt(msv).finalHour.split(":")[1]);
				
				var iniDate:Date=mapScheduleVehicle.getItemAt(msv).initialDate;
				iniDate.hours=Number(mapScheduleVehicle.getItemAt(msv).initialHour.split(":")[0]);
				iniDate.minutes=Number(mapScheduleVehicle.getItemAt(msv).initialHour.split(":")[1]);
				
				var iinit:Number=itemData.startTime.time;
				var iend:Number=FlexDateHelper.addSeconds(itemData.startTime,itemData.cycleTimeMillis).time;
				//cuando la carga es pequeÃ±a o entra dentro del horario
				if((iend>=iniDate.time && iend<=finDate.time) || (iinit>=iniDate.time && iinit<=finDate.time)){
					isVehicleSchedule=false;
				}
				//cuando el horario es mas pequeÃ±o
				if((iniDate.time>=iinit && iniDate.time<=iend) || (finDate.time>=iinit && finDate.time<=iend)){
					isVehicleSchedule=false;
				}
				/*if(!(iinit>finDate.time && iinit>iniDate.time)){
				isVehicleSchedule=false;
				}*/
			}
			
			
			/*var ref:DictionaryMap=overlapedItem;
			var overlaped:Boolean=false;
			for(var _msv:int=0;_msv<rowItem._children.length;_msv++){
			if(itemData.orderLoad!=rowItem._children[_msv].data.orderLoad){
			var _finDate:Date=rowItem._children[_msv].endTime;
			var _iniDate:Date=rowItem._children[_msv].startTime;
			if( _finDate!=null && _iniDate!=null){
			
			var _iinit:Number=itemData.startTime.time;
			var _iend:Number=FlexDateHelper.addSeconds(itemData.startTime,itemData.cycleTimeMillis).time;
			//cuando la carga es pequeÃ±a o entra dentro del horario
			if((_iend>=_iniDate.time && _iend<=_finDate.time) || (_iinit>=_iniDate.time && _iinit<=_finDate.time)){
			//isVehicleSchedule=false;
			//Alert.show('overlaped 1')
			overlaped=true;
			//cuando el horario es mas pequeÃ±o
			if((_iniDate.time>=_iinit && _iniDate.time<=_iend) || (_finDate.time>=_iinit && _finDate.time<=_iend)){
			overlaped=true;
			}
			}
			}
			}
			
			if (overlaped== true){
			//var rowItemfake:Object = rowItem._children[(rowItem._children as ArrayCollection).length-1];
			var rowItemfake:Object  
			rowItemfake	=originalVehicleSource.item;
			overlapedItem.put((rowItemfake['data'] as OrderLoad).orderNumber+":"+(rowItemfake['data'] as OrderLoad).orderLoad+":"+(rowItemfake['data'] as OrderLoad).itemNumber,(rowItemfake['data'] as OrderLoad).orderNumber+":"+(rowItemfake['data'] as OrderLoad).orderLoad+":"+(rowItemfake['data'] as OrderLoad).itemNumber);
			
			}
			view.vehicleScheduleView.ganttSheet.visibleTimeRangeStart=FlexDateHelper.addSeconds(view.gantt.ganttSheet.visibleTimeRangeStart,5);
			view.gantt.ganttSheet.visibleTimeRangeStart=FlexDateHelper.addSeconds(view.gantt.ganttSheet.visibleTimeRangeStart,3);
			*/
			
			
			if(isVehicleSchedule){
				if(Number(vehicleSelected.loadVol)<evt.item.data.loadVolume){
					PlanningScreenVehicleDataTransformer.ignoreAssn=true;
					(evt.itemRenderer as UIComponent).visible=true;
					evt.item.data.workAroundDestroy=null;
					wasError=vehicleSelected.equipNumber;
					privateIsConveyor=isLoadConveyor(evt.item.data);
					// Set the height and width of the Alert control.
					Alert.show(getLabel(OTRConstants.DISP_PS_VOL_EXC),"Alert",Alert.OK | Alert.CANCEL,
						this.view,
						alertListener);
					return;
				}
				//assignByDrag(isConveyor,vehicleSelected);//estaba comentado... preguntar porquÃ©?
				//assignByDrag(vehicleSelected.vehicleType == "995",vehicleSelected);
				if (canTruckContainLoad(itemData,vehicleSelected.vehicleType)){
					assignByDrag(isLoadConveyor(itemData),vehicleSelected);	
				} else {
					Alert.show(getLabel(OTRConstants.DISP_PS_LOAD_CNV),"Error");
				}
			} else {
				PlanningScreenVehicleDataTransformer.ignoreAssn=true;
				(evt.itemRenderer as UIComponent).visible=true;
				evt.item.data.workAroundDestroy=null;
				wasError=vehicleSelected.equipNumber;
				Alert.show(getLabel(OTRConstants.DISP_PS_TRUCK_NO_AV),"Error");
			}
			/*}catch(e:Error){
			if (e.errorID==1009) trace("*** Error al soltar objeto fuera de gantt de vehÃ­culos ("+ e.errorID + ", "+e.message+")");
			else trace("*** Error:"+ e.errorID + "\r\n" + e.message);
			}*/
			//view.vehicleScheduleView.ganttSheet.dispatchEvent(new GanttSheetEvent( GanttSheetEvent.ITEM_EDIT_MOVE));
			
			view.vehicleScheduleView.ganttSheet.visibleTimeRangeStart=FlexDateHelper.addSeconds(view.gantt.ganttSheet.visibleTimeRangeStart,20);
			view.gantt.ganttSheet.visibleTimeRangeStart=FlexDateHelper.addSeconds(view.gantt.ganttSheet.visibleTimeRangeStart,20);
			//rowItem
			
		}
		
		
		public function disconnectDrag(evt:GanttSheetEvent):void{
			var toPlantType:String = GanttServiceReference.getPlantType(franceShowSinglePlant);
			var disdrag:String="uno";
			if(evt.item.hasOwnProperty("data")){
				if(evt.item.data.hasOwnProperty("city")){
					if(evt.item.data.city=="SCHEDULE"){
						view.vehicleScheduleView.ganttSheet.moveEnabled=false;
						view.vehicleScheduleView.ganttSheet.reassignEnabled=false;
					}
					
				}
			}
			
		}
		public function reconnectDrag(evt:GanttSheetEvent):void{
			var disdrag:String="uno";
			var toPlantType:String = GanttServiceReference.getPlantType(franceShowSinglePlant);
			if(evt.item.hasOwnProperty("data")){
				if(evt.item.data.hasOwnProperty("city")){
					if(evt.item.data.city=="SCHEDULE"){
						
						view.vehicleScheduleView.ganttSheet.moveEnabled=false;
						view.vehicleScheduleView.ganttSheet.reassignEnabled=true;
					}
				}
			}
			
		}
		public function putShape(evt:MouseEvent):void{
			Alert.show("texto","titulo");
		}
		public function automaticDistribution(evt:MouseEvent):void{
			var toPlantType:String = GanttServiceReference.getPlantType(franceShowSinglePlant);
			if(GanttServiceReference.isVisibleField(DispatcherViewMediator.currentView,SecurityConstants.DRAG_PLANNING_ASSIGNATION,toPlantType  )  && this.currentTransformer is PlanningScreenDataTransformer){
				DispatcherIslandImpl.masterKey=true;
				GanttServiceReference.dispatchIslandEvent(WDRequestHelper.getAutomaticDistribution(franceShowSinglePlant));
			}
			
		}
		public function removePlanningChanges(evt:MouseEvent):void{
			tmpEraseItemPlanningScreen=[];
			clonedCanvas=null;
			var saveObject:ClearPlanningChanges=new ClearPlanningChanges();
			saveObject.plant=franceShowSinglePlant;
			GanttServiceReference.dispatchIslandEvent(saveObject);
			refresh(null);
			
		}
		public function savePlanningChanges(evt:MouseEvent):void{
			var mdictionary:DictionaryMap=secondCurrentTransformer.getTree().ctx;
			//recorrer todos los registros de los vehiculos y extraer sus payloads
			tmpEraseItemPlanningScreen=[];
			clonedCanvas=null;
			var payloadList:Array=new Array();
			for(var w:int=mdictionary.getAvailableKeys().length-1;w>-1;w--){
				if(String(mdictionary.getAvailableKeys().getItemAt(w)).indexOf(":")!=-1){
					//es un vehiculo y obtenemos los children
					var vehicleChildren:ArrayCollection=mdictionary.get(String(mdictionary.getAvailableKeys().getItemAt(w)))._children;
					//recorrer y guardar cargas
					for(var f:int=vehicleChildren.length-1;f>-1;f--){
						payloadList.push(vehicleChildren.getItemAt(f).data);
						/*var payl:OrderLoad=vehicleChildren.getItemAt(f).data;
						var saveObject:SavePlanningScreenChange=new SavePlanningScreenChange();
						saveObject.plant=payl.plant;
						saveObject.dateCreate="01012013";
						saveObject.vehicle=payl.equipNumber;
						saveObject.vbeln=payl.orderNumber;
						saveObject.posnr=payl.itemNumber;
						saveObject.loadVol=payl.loadVolume.toString();
						saveObject.deliveryIhour="010000";
						saveObject.deliveryFhour="010000";
						saveObject.modificationDat="01012013";
						saveObject.userCreate="e0asantos";
						//enviar evento
						GanttServiceReference.dispatchIslandEvent(saveObject);*/
					}
				}
			}
			//mandar las cargas a guardar
			var saveObject:SaveNowPlanningScreenChange=new SaveNowPlanningScreenChange();
			GanttServiceReference.dispatchIslandEvent(saveObject);
			DispatcherIslandImpl.masterKey=true;
			
		}
		public function dropTruckLine(evt:MouseEvent):void{
			var rowItem:Object = view.vehicleScheduleView.dataGridBack.getRowItemAtPosition(view.mouseY);
			if(allowClonedLabel==false && (getTimer()-lastMovedObject)>200 && vehicleCanvas!=null && rowItem!=null){
				vehicleCanvas.stopDrag();
				//buscar orden
				var orden:Object=this.currentTransformer.getTree().ctx.get(lastMovedOrder);
				var items:Array=[];
				for(var k:int=0;k<PlanningScreenTask.singlePositions.getAvailableKeys().length;k++){
					if(String(PlanningScreenTask.singlePositions.getAvailableKeys().getItemAt(k)).indexOf(lastMovedOrder)!=-1 && PlanningScreenTask.singlePositions.get(String(PlanningScreenTask.singlePositions.getAvailableKeys().getItemAt(k)))==lastFictiveVehicle){
						items.push(String(PlanningScreenTask.singlePositions.getAvailableKeys().getItemAt(k)));
					}
				}
				allowClonedLabel=true;
				//buscar
				var vehicleSelected:Object;
				for(var ve:int=0;ve<DispatcherViewMediator.vehiculosCorrectos.length;ve++){
					if(DispatcherViewMediator.vehiculosCorrectos.getItemAt(ve).equipNumber==rowItem.equipNumber){
						vehicleSelected=DispatcherViewMediator.vehiculosCorrectos.getItemAt(ve);
					}
				}
				var conveyorLoads:Boolean=false;
				
				for(var tr:int=0;tr<orden._children.length;tr++){
					for(var tt:int=0;tt<items.length;tt++){
						if(orden._children.getItemAt(tr).data.itemNumber==String(items[tt]).substr(-4) && canTruckContainLoad(orden._children.getItemAt(tr).data,vehicleSelected.vehicleType)){
							var mapScheduleVehicle:ArrayCollection=DispatcherIslandImpl.mapVehicles.get(vehicleSelected.equipNumber);
							var isVehicleSchedule:Boolean=true;			
							for(var msv:int=0;msv<mapScheduleVehicle.length;msv++){
								var finDate:Date=mapScheduleVehicle.getItemAt(msv).finalDate;
								finDate.hours=Number(mapScheduleVehicle.getItemAt(msv).finalHour.split(":")[0]);
								finDate.minutes=Number(mapScheduleVehicle.getItemAt(msv).finalHour.split(":")[1]);
								
								var iniDate:Date=mapScheduleVehicle.getItemAt(msv).initialDate;
								iniDate.hours=Number(mapScheduleVehicle.getItemAt(msv).initialHour.split(":")[0]);
								iniDate.minutes=Number(mapScheduleVehicle.getItemAt(msv).initialHour.split(":")[1]);
								
								var iinit:Number=orden._children.getItemAt(tr).startTime.time;
								var iend:Number=FlexDateHelper.addSeconds(orden._children.getItemAt(tr).data.startTime,orden._children.getItemAt(tr).data.cycleTimeMillis).time;
								//cuando la carga es pequeÃ±a o entra dentro del horario
								if((iend>=iniDate.time && iend<=finDate.time) || (iinit>=iniDate.time && iinit<=finDate.time)){
									isVehicleSchedule=false;
								}
								//cuando el horario es mas pequeÃ±o
								if((iniDate.time>=iinit && iniDate.time<=iend) || (finDate.time>=iinit && finDate.time<=iend)){
									isVehicleSchedule=false;
								}
								
							}
							/*for(var msv:int=0;msv<rowItem._children.length;msv++){
							var finDate:Date=rowItem._children[msv].endTime;
							var iniDate:Date=rowItem._children[msv].startTime;
							if(finDate!=null && iniDate!=null){
							
							var iinit:Number=orden._children.getItemAt(tr).data.startTime.time;
							var iend:Number=FlexDateHelper.addSeconds(orden._children.getItemAt(tr).data.startTime,orden._children.getItemAt(tr).data.cycleTimeMillis).time;
							//cuando la carga es pequeÃ±a o entra dentro del horario
							if((iend>=iniDate.time && iend<=finDate.time) || (iinit>=iniDate.time && iinit<=finDate.time)){
							isVehicleSchedule=false;
							}
							//cuando el horario es mas pequeÃ±o
							if((iniDate.time>=iinit && iniDate.time<=iend) || (finDate.time>=iinit && finDate.time<=iend)){
							isVehicleSchedule=false;
							}
							}
							}*/
							
							if(isVehicleSchedule && (Number(vehicleSelected.loadVol)>=orden._children.getItemAt(tr).data.loadVolume)){
								originalVehicleSource=new GanttSheetEvent(GanttSheetEvent.ITEM_EDIT_END);
								originalVehicleSource.item=new Object();
								originalVehicleSource.item["data"]=orden._children.getItemAt(tr).data;
								originalVehicleSource.itemRenderer=orden._children.getItemAt(tr).data.graphicReference;
								tmpEraseItemPlanningScreen.push(orden._children.getItemAt(tr).data.orderLoad);
								assignByDrag(false,vehicleSelected);
								var posiciones:DictionaryMap=PlanningScreenTask.singlePositions;
								posiciones.remove(orden._children.getItemAt(tr).data.plant+":"+orden._children.getItemAt(tr).data.orderNumber+":"+orden._children.getItemAt(tr).data.itemNumber);
								conveyorLoads=true;
							}
						}
					}
				}
				if(!conveyorLoads){
					Alert.show(getLabel(OTRConstants.DISP_PS_VEHI_ERR),"Error");
				}
				tmpRefresh=true;
				islandDataEventHandler(tmpErase);
			}
			if(vehicleCanvas!=null && (getTimer()-lastMovedObject)>200){
				
				vehicleCanvas.visible=false;
				view.removeChild(vehicleCanvas);
				vehicleCanvas=null;
				allowClonedLabel=true;
			}
			
		}
		public function canTruckContainLoad(mload:OrderLoad,vehicleType:String):Boolean{
			//buscar item en servicios adicionales
			var allowToAssign:Boolean=true;
			for(var q:int=mload.addProd.length-1;q>-1;q--){
				if((mload.addProd.getItemAt(q) as AddProd).itemNumber==mload.itemNumber){
					//checar que los addprod sean conveyor
					if(((mload.addProd.getItemAt(q) as AddProd).conveyor=="X" && vehicleType!="995")
						/*|| ((mload.addProd.getItemAt(q) as AddProd).conveyor=="" && vehicleType!="995")
					|| ((mload.addProd.getItemAt(q) as AddProd).conveyor=="" && vehicleType=="995")*/){
						allowToAssign=false;
					}
				}
			}
			return allowToAssign;
		}
		public function isLoadConveyor(mload:OrderLoad):Boolean{
			//buscar item en servicios adicionales
			var allowToAssign:Boolean=false;
			for(var q:int=mload.addProd.length-1;q>-1;q--){
				if((mload.addProd.getItemAt(q) as AddProd).itemNumber==mload.itemNumber){
					//checar que los addprod sean conveyor
					if(((mload.addProd.getItemAt(q) as AddProd).conveyor=="X")
						/*|| ((mload.addProd.getItemAt(q) as AddProd).conveyor=="" && vehicleType!="995")
					|| ((mload.addProd.getItemAt(q) as AddProd).conveyor=="" && vehicleType=="995")*/){
						allowToAssign=true;
					}
				}
			}
			return allowToAssign;
		}
		public function addTruckEvent(evt:MouseEvent):void{
			var evet:AddTruck=new AddTruck();
			GanttServiceReference.dispatchIslandEvent(evet);
		}
		
		public function editTruckEvent(evt:MouseEvent):void{
			var evet:EditTruck=new EditTruck();
			GanttServiceReference.dispatchIslandEvent(evet);
		}
		
		public function startedDrag(evt:GanttSheetEvent):void{
			
		}
		public var showNoRecipesOnly:Boolean=false;
		public function filterRecipes(evt:MouseEvent):void{
			DispatcherIslandImpl.masterKey=true;
			showNoRecipesOnly=!showNoRecipesOnly;
			requestRefresh();
		}
		public function riseEventMediator(evt:MouseEvent):void{
			var fakeEvent:testComponent1=new testComponent1();
			GanttServiceReference.dispatchIslandEvent(fakeEvent);
		}
		public function activateMapping(event:ContextMenuEvent):void{
			/*CemexDispatcherMonitor.activateMap=!CemexDispatcherMonitor.activateMap;*/
			/*var datos:ArrayCollection=new ArrayCollection([createDummySalesOrder("I241","1"),createDummySalesOrder("I241","L01")]);
			var evt:SalesOrderLCDSEvent=new SalesOrderLCDSEvent(datos);
			gotPushFromSalesOrder(evt);*/
			Alert.show("Ejecuciones: "+dumCo,"Titulo");
		}
		/*public function createDummySalesOrder(nom:String="PAULA HERNANDEZ",fecha:String="18:45:00",planta="I241",carga:String="L001"):OrderLoad{
			var var1:OrderLoad=new OrderLoad();
			var1.cycleTimeMillis=16500;
			var1.cycleTime="04:35:00";
			var1.soldToName="N-tek";
			var1.orderVolume=17;
			//var1.startTime=new Date(2011,8,2,8);
			var1.materialId="000000000010001991";
			var1.valueAdded="";
			var1.shipConditions="01";
			var1.loadFrequency="0";
			var1.mepTotcost=102125.8;
			var1.cpQuantity=0;
			var1.loadingDate=new Date(2014,8,5);
			var1.loadingTime=fecha;
			var1.loadingTimestamp=new Date(2014,8,5);
			var1.unloadingTime="02:50:00"
			var1.vehicleMaxVol=7;
			var1.totCost=89241.5;
			var1.loadNumber=carga;
			var1.itemCategory="ZTC1";
			var1.idHlevelItem="";
			var1.hlevelItem=1000;
			var1.additionalFlag=false;
			var1.contactName1=nom;
			var1.orderReason="";
			var1.contactTel="";
			var1.cpDescription="";
			var1.shipmentStatus="";
			var1.constructionProduct="";
			var1.itemNumber="1001";
			var1.jobSiteId="0065010066";
			var1.paytermLabel="";
			var1.timeFixIndicator="";
			var1.orderStatus="CMPO";
			var1.itemCurrency="MXN";
			var1.contactName2="";
			var1.equipNumber="";
			var1.loadUom="M3";
			var1.deliveryGroup=1;
			var1.deliveryTime="03:43:00"
			var1.firstDescription="CCO-1-100-28-1";
			var1.firstDeliveryTime="03:43:00";
			
			var1.orderNumber="0000171721";
			var1.hlevelItem=1000;
			var1.confirmQty=7;
			var1.orderUom="M3";
			var1.jobSiteName="RESIDENCIAL CUITLAHUAC (SQC)";
			var1.postalCode="65670";
			var1.isDelayed=false;
			var1.remainFlag=false;
			var1.orderLoad="0000171721-L1";
			var1.houseNoStreet="CUITLAHUAC 134";
			var1.firstOrderNumber="0000031150";
			var1.city="TLALPAN";
			var1.deliveryAmount="";
			var1.cpUom="";
			var1.loadStatus="CMPL";
			var1.equipStatus=ExtrasConstants.LOAD_UNASSIGNED;
			var1.warning="";
			var1.materialDes="CCO-1-100-28-1";
			var1.renegFlag=false;
			var1.cpOrderNumber="";
			var1.completedDelayed=false;
			var1.soldToNumber="0050160376";
			var1.plant=planta;
			var1.loadVolume=7;
			return var1;
		}*/
		public static var tracetip:ToolTip=null;
		public static var golabTooltip:ToolTip=null;
		public static var tracetipAdd:ToolTip=null;
		public static var tipTaskInformationAdd:ToolTip=null;
		public function itemIsBeingMoved(evt:GanttSheetEvent):void{
			var itemmoved:Object=evt;
			//view.cleanMsg.label=getTimer().toString();
			//tracetip.text="Loading time:"+evt.item.startTime.toString();
			if(evt.item.payload.graphicReference!=null){
				var calculatedDate:Date=timeController.getTime(evt.item.payload.graphicReference.x);
				tracetip.text=calculatedDate.toString();
			}
			
			if(clonedCanvas!=null){
				/*clonedCanvas.x=view.mouseX-(clonedCanvas.width/2);
				clonedCanvas.y=view.mouseY-mousePoint.y;*/
				clonedCanvas.alpha=.6;
				evt.item.data.workAroundDestroy="yes";
				
			}
			
		}
		
		public function addKeyForSearch(evt:KeyboardEvent):void{
			eventDispatcher.dispatchEvent(evt);
		}
		
		
		public function loadsPerHour(event:MouseEvent):void{
			isPerHour = !isPerHour;
			DispatcherIslandImpl.masterKey=true;
			//changeViews(currentView);
			requestRefresh();
			
		}
		
		
		
		protected function expandTree(evn:SearchBtnVehicleEvent):void{
			view.gantt.dataGridBack.expandAll();
			view.callLater(seekInTreeViewVehicleRunner, [evn]);
			
		}
		private function seekInTreeView(evt:SearchBtnClickViewEvent):void{
			view.gantt.dataGridBack.expandAll();                           				   
			view.callLater(seekInTreeViewRunner, [evt.searchString,evt.refTextInput]);
		}
		public static var textToSearch:String="";
		private function seekInTreeViewVehicleRunner(evnt:SearchBtnVehicleEvent):void{
			var cursoresVechicle:Array=[];
			var labl:String;
			var ref:TextInput;
			labl= evnt.searchString;
			
			ref=evnt.refTextInput;
			var totalCursors:int=0;
			var dataCursor:IHierarchicalCollectionViewCursor = view.gantt.dataGridBack.dataProvider.createCursor();
			while (dataCursor.current != null)
			{
				totalCursors++;
				var added:Boolean=false;
				if(dataCursor.current["label"].toString().toLowerCase().indexOf(labl.toLowerCase())>-1 || dataCursor.current["id"].toString().toLowerCase().indexOf(labl.toLowerCase())>-1){
					added=true;
					cursoresVechicle.push(dataCursor["index"]);
				}
				if(!added && dataCursor.current["labelField"]!="plant" && dataCursor.current["label"]!=ExtrasConstants.VEHICLE_DISPONIBLE && dataCursor.current["label"]!=ExtrasConstants.VEHICLE_ASSIGNED  && dataCursor.current["label"]!=ExtrasConstants.LOAD_UNASSIGNED){
					//buscar el vehiculo y/o la orden
					for(var res:int=0;res<dataCursor.current._children.length;res++){
						if((dataCursor.current._children.getItemAt(res).payload.equipNumber.toLowerCase().indexOf(labl.toLowerCase())>-1
							|| dataCursor.current._children.getItemAt(res).payload.equipLabel.toLowerCase().indexOf(labl.toLowerCase())>-1
							|| dataCursor.current._children.getItemAt(res).payload.orderLoad.toLowerCase().indexOf(labl.toLowerCase())>-1)){
							cursoresVechicle.push(dataCursor["index"]);
						}
					}
				}
				dataCursor.moveNext();
			}
			if(labl!=""){
				view.gantt.dataGridBack.scrollToIndex(cursoresVechicle[0]);
				view.gantt.dataGridBack.selectedIndices=cursoresVechicle;
			} else {
				view.gantt.dataGridBack.selectedIndices=[];
			}
			
			cursoresVechicle = null; //quita referencia al array
			
			view.gantt.dataGridBack.validateDisplayList();
			view.gantt.dataGridBack.validateProperties();
			view.gantt.dataGridBack.validateNow();
			if(ref!=null){
				ref.setFocus();
				ref.setSelection(ref.text.length,ref.text.length);
			}
			/*			if(view.viewStack.selectedIndex==4 || (view.viewStack.selectedIndex==1 && view.viewStack.numChildren==2))
			view.callLater(seekInTasks, [evnt.searchString]);
			*/
			
		}
		
		private function seekInTasks(param:String):void{
			var orderTMP:OrderLoad = new OrderLoad();
			//Iterar gannt unnasigned
			for (var cuenta:Number=0;cuenta<view.gantt.taskDataProvider.length;cuenta++){
				orderTMP = view.gantt.taskDataProvider[cuenta].payload as OrderLoad;
				if ((orderTMP.orderNumber.toLowerCase().indexOf(param)>-1||orderTMP.orderLoad.toLowerCase().indexOf(param)>-1)&&param!="")
					orderTMP.mustGlowBySearch = true;
				else
					orderTMP.mustGlowBySearch = false;
			}
			view.gantt.ganttSheet.visibleTimeRangeStart=FlexDateHelper.addSeconds(view.gantt.ganttSheet.visibleTimeRangeStart,3);
			if(view.viewStack.selectedIndex==4 || (view.viewStack.selectedIndex==1 && view.viewStack.numChildren==2)){
				//Iterar gannt assigned
				for (var cuentaVehic:Number=0;cuentaVehic<view.vehicleScheduleView.taskDataProvider.length;cuentaVehic++){
					orderTMP = view.vehicleScheduleView.taskDataProvider[cuentaVehic].payload as OrderLoad;
					if ((orderTMP.orderNumber.toLowerCase().indexOf(param)>-1||orderTMP.orderLoad.toLowerCase().indexOf(param)>-1)&&param!="")
						orderTMP.mustGlowBySearch = true;
					else
						orderTMP.mustGlowBySearch = false;
				}
				view.vehicleScheduleView.ganttSheet.visibleTimeRangeStart=FlexDateHelper.addSeconds(view.vehicleScheduleView.ganttSheet.visibleTimeRangeStart,3);
			}
			
		}
		
		public var cursores:Array=[];
		private function seekInTreeViewRunner(labl:String,ref:TextInput):void{
			cursores=[];
			var totalCursors:int=0;
			var dataCursor:IHierarchicalCollectionViewCursor = view.gantt.dataGridBack.dataProvider.createCursor();
			while (dataCursor.current != null)
			{
				totalCursors++;
				var added:Boolean=false;
				if(dataCursor.current["label"].toString().toLowerCase().indexOf(labl.toLowerCase())!=-1 || dataCursor.current["id"].toString().toLowerCase().indexOf(labl.toLowerCase())!=-1){
					//view.gantt.dataGridBack.selectedItem(dataCursor.current);
					added=true;
					cursores.push(dataCursor["index"]);
				}
				if(!added && dataCursor.current["labelField"]!="plant" && dataCursor.current["label"]!=ExtrasConstants.VEHICLE_DISPONIBLE && dataCursor.current["label"]!=ExtrasConstants.VEHICLE_ASSIGNED  && dataCursor.current["label"]!=ExtrasConstants.LOAD_UNASSIGNED){
					//buscar el vehiculo y/o la orden
					for(var res:int=0;res<dataCursor.current._children.length;res++){
						if((dataCursor.current._children.getItemAt(res).payload.equipNumber.toLowerCase().indexOf(labl.toLowerCase())>-1
							|| dataCursor.current._children.getItemAt(res).payload.equipLabel.toLowerCase().indexOf(labl.toLowerCase())>-1
							|| dataCursor.current._children.getItemAt(res).payload.orderLoad.toLowerCase().indexOf(labl.toLowerCase())>-1)){
							cursores.push(dataCursor["index"]);
						}
					}
				}
				dataCursor.moveNext();
			}
			if(labl!=""){
				view.gantt.dataGridBack.scrollToIndex(cursores[0]);
				view.gantt.dataGridBack.selectedIndices=cursores;
				//var scrollIndexPosition:int=(view.gantt.dataGridBack.maxVerticalScrollPosition/totalCursors)*cursores[0];
				//view.gantt.dataGridBack.verticalScrollPosition=int(scrollIndexPosition);
			} else {
				view.gantt.dataGridBack.selectedIndices=[];
			}
			
			cursores= new Array(); 
			
			if(view.viewStack.selectedIndex==4 || (view.viewStack.numChildren==2 && view.viewStack.selectedIndex==1)){
				totalCursors=0;
				dataCursor = view.vehicleScheduleView.dataGridBack.dataProvider.createCursor();
				while (dataCursor.current != null)
				{
					totalCursors++;
					added=false;
					if(dataCursor.current["label"].toString().toLowerCase().indexOf(labl.toLowerCase())!=-1 || dataCursor.current["id"].toString().toLowerCase().indexOf(labl.toLowerCase())!=-1){
						//view.gantt.dataGridBack.selectedItem(dataCursor.current);
						added=true;
						cursores.push(dataCursor["index"]);
					}
					if(!added && dataCursor.current["labelField"]!="plant" && dataCursor.current["label"]!=ExtrasConstants.VEHICLE_DISPONIBLE && dataCursor.current["label"]!=ExtrasConstants.VEHICLE_ASSIGNED  && dataCursor.current["label"]!=ExtrasConstants.LOAD_UNASSIGNED){
						//buscar el vehiculo y/o la orden
						for(var res2:int=0;res2<dataCursor.current._children.length;res2++){
							if((dataCursor.current._children.getItemAt(res2).payload.equipNumber.toLowerCase().indexOf(labl.toLowerCase())!=-1
								|| dataCursor.current._children.getItemAt(res2).payload.equipLabel.toLowerCase().indexOf(labl.toLowerCase())!=-1
								|| dataCursor.current._children.getItemAt(res2).payload.orderLoad.toLowerCase().indexOf(labl.toLowerCase())!=-1)){
								cursores.push(dataCursor["index"]);
							}
						}
					}
					dataCursor.moveNext();
				}
				if(labl!=""){
					view.vehicleScheduleView.dataGridBack.scrollToIndex(cursores[0]);
					view.vehicleScheduleView.dataGridBack.selectedIndices=cursores;
				} else {
					view.vehicleScheduleView.dataGridBack.selectedIndices=[];
				}
				
				
			}			
			view.callLater(seekInTasks, [labl]);
			if(ref!=null){
				ref.setFocus();
				ref.setSelection(ref.text.length,ref.text.length);
			}
		}
		
		public function resizing(e:MouseEvent):void{
			var event:IFlashIslandEventObject
			if(e.currentTarget["name"]=='btnSHV'){
				heightSize=1;
				
			}
			if(e.currentTarget["name"]=='btnSLHV'){
				heightSize=-1;
			}
			/*view.btnScreenHVertical.toolTip=heightSize.toString();
			view.btnScreenLHVertical.toolTip=heightSize.toString();*/
			event=WDRequestHelper.getResizeHeight(heightSize.toString() );
			GanttServiceReference.dispatchIslandEvent( event );
			
		}
		
		
		private function activeTestMode(evt:ContextMenuEvent):void{
			AbstractReceiverService.isTestMode=!AbstractReceiverService.isTestMode;
		}
		private function activeDataLogging(evt:ContextMenuEvent):void{
			DispatcherIslandImpl.dataLogging=!DispatcherIslandImpl.dataLogging;
		}
		private function createSession(evt:ContextMenuEvent):void{
			//PopUpManager.createPopUp(view,TestWindowSession,true);
			
			PopUpManager.centerPopUp(PopUpManager.createPopUp(view,TestWindowSession,true));
		}
		private function searchProduction(evt:ContextMenuEvent):void{
			var ventana:searchProductionOrders=PopUpManager.createPopUp(view,searchProductionOrders,true) as searchProductionOrders;
			ventana.flexContext=currentTransformer.getTree().ctx;   
			PopUpManager.centerPopUp(ventana);
			
		}
		public static var isUsingPush:Boolean=false;
		public static var isDragging:Boolean=true;
		public function cancelAllMovements(evt:MouseEvent):void{
			DispatcherViewMediator.isDragging=false;
			var now:Date=TimeZoneHelper.getServer();
			if (isSameAsBaseDate(now) ){
				DispatcherViewMediator.isUsingPush=true;
				currentTransformer.timeChanged(now);
			}
			logger.debug("CANCELA MOVS");
		}
		public function continueAllMovements(evt:MouseEvent):void{
			logger.debug("CONTINUA MOVS");
			DispatcherViewMediator.isDragging=true;
		}
		
		public static function newSibling(sourceObj:Object):* {
			if(sourceObj) {
				var objSibling:*;
				try {
					var classOfSourceObj:Class = getDefinitionByName(getQualifiedClassName(sourceObj)) as Class;
					objSibling = new classOfSourceObj();
				}
				
				catch(e:Object) {}
				
				return objSibling;
			}
			return null;
		}
		
		public static function clone(source:Object):Object {
			
			var clone:Object;
			if(source) {
				clone = newSibling(source);
				
				if(clone) {
					copyData(source, clone);
				}
			}
			
			return clone;
		}
		public static function copyData(source:Object, destination:Object):void {
			
			//copies data from commonly named properties and getter/setter pairs
			if((source) && (destination)) {
				
				try {
					var sourceInfo:XML = describeType(source);
					var prop:XML;
					
					for each(prop in sourceInfo.variable) {
						
						if(destination.hasOwnProperty(prop.@name)) {
							destination[prop.@name] = source[prop.@name];
						}
						
					}
					
					for each(prop in sourceInfo.accessor) {
						if(prop.@access == "readwrite") {
							if(destination.hasOwnProperty(prop.@name)) {
								destination[prop.@name] = source[prop.@name];
							}
							
						}
					}
				}
				catch (err:Object) {
				}
			}
		}
		/**
		 * Este metodo permite saber cuando un task renderer esta siendo cambiado por drag and drop
		 * esto permite que ejecutemos o no la animacion que corre cuando se cambian las fechas de los renders
		 * 
		 **/
		public function itemChangeAsDragAndDrop(evt:GanttSheetEvent):void{
			return;
		}
		
		/**
		 * Metodo para saber cuando empieza a editarse un item por drag&drop
		 *
		 **/
		internal var clonedCanvas:PlanningScreenTaskRenderer;
		internal var mousePoint:Point=new Point();
		private var previousHeight:Number;
		public function itemChangeAsDragAndDropBegin(evt:GanttSheetEvent):void{
			var toPlantType:String = GanttServiceReference.getPlantType(franceShowSinglePlant);
			(evt.itemRenderer as UIComponent).stopDrag();
			//var flagvisible:Boolean= GanttServiceReference.isVisibleField(DispatcherViewMediator.currentView,SecurityConstants.DRAG_PLANNING_ASSIGNATION,toPlantType  ); 
			//	var flagEnable:Boolean= GanttServiceReference.isEnabledField(DispatcherViewMediator.currentView,SecurityConstants.DRAG_PLANNING_ASSIGNATION,toPlantType )
			if(GanttServiceReference.isEnabledField(DispatcherViewMediator.currentView,SecurityConstants.DRAG_PLANNING_ASSIGNATION,toPlantType  )  && this.currentTransformer is PlanningScreenDataTransformer){
				if(this.currentTransformer is PlanningScreenDataTransformer/* && !DispatcherIslandImpl.isBatcher*/){
					clonedCanvas=new PlanningScreenTaskRenderer();
					view.addChild(clonedCanvas);
					/*clonedCanvas.x=(evt.itemRenderer as UIComponent).x+view.gantt.dataGridBack.width;
					clonedCanvas.y=view.mouseY-50;
					mousePoint.x=view.mouseX-clonedCanvas.x-view.gantt.dataGridBack.width;
					mousePoint.y=view.mouseY-clonedCanvas.y;*/
					clonedCanvas.startDrag(true);
					clonedCanvas.width=(evt.itemRenderer as UIComponent).width;
					clonedCanvas.data=(evt.itemRenderer as UIComponent)["data"];
					evt.item.data.workAroundDestroy="yes";
					var fecha:Object=getCoordinate(new Date());
					/*view.animateDrag.fromValue=view.gantt.height;
					view.animateDrag.play();*/
					previousHeight=view.vehicleScheduleView.height;
					/*TweenLite.to(view.vehicleScheduleView,2,{
					height:600
					});*/
				}
				DispatcherViewMediator.isDragging=true;
				
			}else{
				return;
				//view.gantt.ganttSheet.moveEnabled=false;
			}
			
		}
		private var clonedLabel:LabelExt;
		private var allowClonedLabel:Boolean=true;
		private var vehicleCanvas:Canvas;
		private var lastMovedObject:Number=0;
		public static var lastMovedOrder:String;
		public static var lastFictiveVehicle:Number=-1;
		public function clonedLabelEvent(evt:MouseEvent):void{
			var toPlantType:String = GanttServiceReference.getPlantType(franceShowSinglePlant);
			if(GanttServiceReference.isVisibleField(DispatcherViewMediator.currentView,SecurityConstants.DRAG_PLANNING_ASSIGNATION,toPlantType  )  && this.currentTransformer is PlanningScreenDataTransformer)
			{	
				if(this.currentTransformer is PlanningScreenDataTransformer && allowClonedLabel){
					vehicleCanvas=new Canvas();
					//dibujar dashed line
					lastMovedObject=getTimer();
					view.addChild(vehicleCanvas);
					vehicleCanvas.width=720;
					vehicleCanvas.height=60;
					clonedLabel=new LabelExt();
					clonedLabel.text=(evt.target as UITextField).text;
					vehicleCanvas.addChild(clonedLabel);
					var g:Graphics=vehicleCanvas.graphics;
					g.clear();
					g.lineStyle(2, 0x0000dd, 0.5);
					var arbol:DictionaryMap=currentTransformer.getTree().ctx;
					var posiciones:DictionaryMap=PlanningScreenTask.singlePositions;
					DashedLine.drawLine(g,new Point(0,0),new Point(vehicleCanvas.width,0),true);
					DashedLine.drawLine(g,new Point(vehicleCanvas.width,0),new Point(vehicleCanvas.width,vehicleCanvas.height),true);
					DashedLine.drawLine(g,new Point(vehicleCanvas.width,vehicleCanvas.height),new Point(0,vehicleCanvas.height),true);
					DashedLine.drawLine(g,new Point(0,vehicleCanvas.height),new Point(0,0),true);
					//vehicleCanvas.setStyle("backgroundColor",0x999999);
					//vehicleCanvas.x=(evt.target as UIComponent).x+view.gantt.dataGridBack.width;
					vehicleCanvas.y=view.mouseY;
					vehicleCanvas.startDrag(true);
					allowClonedLabel=false;
				}
			}else{
				return;
			}
		}
		
		/**
		 * metodo que permite limpiar los mensajes del area de notificacion
		 * se manda un evento a WD para decirle que los limpie
		 * */
		public function clearMessages(evt:MouseEvent):void{
			DispatcherIslandHelper.ordersWithError=[];
			view.logMessage.dataProvider=new ArrayCollection();
			GanttServiceReference.dispatchIslandEvent(new ClearMessagesArea());
		}
		/**
		 * Sort de los elementos
		 * **/
		public function taskSort(value:Array):Array{
			
			//se puede usar el startTime como ratificador
			if(currentTransformer is LoadPerVehiclePlantVehicleDataTransformer){
				if(value.length==2){
					if(value[0].startTime.time<value[1].startTime.time){
						return value.reverse()
					} else {
						return value;
					}
				}
			}
			var resultados:Array=new Array();
			for(var q:int=0;q<value.length;q++){
				//recorrer para recalcular
				var indice:int=0;
				for(var w:int=0;w<resultados.length;w++){
					/*var res:int=ObjectUtil.dateCompare(value[q].startTime,resultados[w].startTime);*/
					var res:int=0;
					if(value[q].startTime.time<resultados[w].startTime.time){
						res=-1
					}
					if(res==-1){
						//si fecha de q<w
						indice=w;
					}
				}
				resultados.splice(indice,0,value[q]);
			}
			return resultados;
		}
		/**
		 * mock de pushes
		 * /
		 * */
		public function crearNuevoPush(evt:MouseEvent):void{
			
			var mheader:Object=new Object();
			mheader["equipStatus"]=ExtrasConstants.VEHICLE_ASSIGNED;
			mheader["plant"]="D084";
			mheader["plantId"]="D084";
			currentTransformer.addHeader(mheader);
			mheader["equipStatus"]=ExtrasConstants.VEHICLE_DISPONIBLE;
			mheader["plant"]="D084";
			mheader["plantId"]="D084";
			currentTransformer.addHeader(mheader);
			mheader["equipStatus"]=ExtrasConstants.LOAD_UNASSIGNED;
			mheader["plant"]="D084";
			mheader["plantId"]="D084";
			currentTransformer.addHeader(mheader);
		}
		public function borrarPush(evt:MouseEvent):void{
			
		}
		
		
		public function fullScreenMenuHandler(event:ContextMenuEvent) : void{  
			FullScreenHelper.toggleFullScreen();
		}
		public function copyReceivedData(event:ContextMenuEvent) : void{  
			
		}
		
		public function cleanLog(e:ContextMenuEvent):void{
			TimeDispatcher.clearZoom();
		}
		
		
		
		/**
		 * Este metodo valida que la fecha seleccionada por el usuario sea la misma que la fecha que recibe como parametro
		 * 
		 */
		public function isSameAsBaseDate(now:Date):Boolean {
			var firstDateToDate:Boolean=(FlexDateHelper.getDateString(now,"YYYY/MM/DD") == FlexDateHelper.getDateString(baseDate,"YYYY/MM/DD"));
			if(DispatcherIslandImpl.pushBaseDate==null){
				return false;
			}
			var secondDateToDate:Boolean=(new Date()).date==DispatcherIslandImpl.pushBaseDate.date;
			if(DispatcherIslandImpl.pushBaseDate.date>(new Date()).date){
				//entonces se esta entrando desde una computadora con fecha anterior debido al usuario
				secondDateToDate=true;
			}
			return secondDateToDate && firstDateToDate;
		}
		
		public var lastRenderedMinute:String="";
		public var lastRenderedSeconds:String="";
		
		/**
		 * ESte metodo es el que permite hacer acciones cuando cambia el tiempo
		 * y va a mandar a llamar a diferentes metodos segun se requiera hacer actualizaciones de los datos
		 * @reference  processTimeChangedEveryEvent
		 * @reference  processTimeChangedEverySeconds
		 * @reference  processTimeChangedEveryMinute
		 */
		public function timeChanged(e:TimerEvent=null):void{
			
			//var now:Date = new Date();
			var now:Date=TimeZoneHelper.getServer();
			if (isSameAsBaseDate(now)){//&& isDataLoaded){
				processTimeChangedEveryEvent(now);
				now.seconds = int(now.seconds /10) * 10;
				if (FlexDateHelper.parseTimeString(now) != lastRenderedSeconds){
					processTimeChangedEverySeconds(now);
					lastRenderedSeconds = FlexDateHelper.parseTimeString(now);
				}
				now.setSeconds(0);
				if (FlexDateHelper.parseTimeString(now) != lastRenderedMinute){
					processTimeChangedEveryMinute(now);
					lastRenderedMinute = FlexDateHelper.parseTimeString(now);
				}
			}
		}
		
		/**
		 * Este metodos se ejecuta cada Evento que tiene el Timer
		 * @reference timeChanged
		 */
		public function processTimeChangedEveryEvent(time:Date):void {
			if(view.viewStack.numChildren==3 || view.viewStack.numChildren==5){
				drawTimeIndicators();
			}
			var now:Date=TimeZoneHelper.getServer();
			currentTransformer.timeChanged(now);
		}
		
		private var sharedo:SharedObject
		/**
		 * Este metodos se ejecuta cada Minuto
		 * @reference timeChanged
		 */
		public function processTimeChangedEveryMinute(time:Date):void {
			var tareas:ArrayCollection=currentTransformer.getTasksTree();
			sharedo=SharedObject.getLocal("rms","/");
			//recorrer cada planta
			
			for(var q:int=0;q<tareas.length;q++){
				if(tareas.getItemAt(q).data.isDelayed!="" && GanttServiceReference.getParamString("RMS_LATE_ORD_MSG",tareas.getItemAt(q).data.plant)!="0" && GanttServiceReference.getParamString("RMS_LATE_ORD_MSG",tareas.getItemAt(q).data.plant)!=null && /*Number(tareas.getItemAt(q).data.codStatusProdOrder)<20*/ (tareas.getItemAt(q).data.loadStatus=="NOST" || tareas.getItemAt(q).data.loadStatus=="TBCL" || tareas.getItemAt(q).data.loadStatus=="HOLI")){
					//guardar en sharedObject
					if(sharedo.data[tareas.getItemAt(q).data.orderLoad]!=null){
						var justNow:Date=new Date();
						if(sharedo.data[tareas.getItemAt(q).data.orderLoad]!="X"){
							if(sharedo.data[tareas.getItemAt(q).data.orderLoad]<justNow.time && sharedo.data["w:"+tareas.getItemAt(q).data.orderLoad]==null){
								sharedo.data["w:"+tareas.getItemAt(q).data.orderLoad]="open";
								sharedo.flush();
								var delayWindow:DelayDismissDialog=PopUpManager.createPopUp(this.view,DelayDismissDialog,true) as DelayDismissDialog;
								delayWindow.pedido=tareas.getItemAt(q).data;
								PopUpManager.centerPopUp(delayWindow);		
							}
						}
					} else if(sharedo.data["w:"+tareas.getItemAt(q).data.orderLoad]==null){
						sharedo.data["w:"+tareas.getItemAt(q).data.orderLoad]="open";
						sharedo.flush();
						var delayWindow:DelayDismissDialog=PopUpManager.createPopUp(this.view,DelayDismissDialog,true) as DelayDismissDialog;
						delayWindow.pedido=tareas.getItemAt(q).data;
						PopUpManager.centerPopUp(delayWindow);
					}
				}
				//creamos ventanas
				
			}
			if(currentTimeFlag){
				move2CurrentTime(false);
			}
			/*if (isSameAsBaseDate(time) ){
			if(secondCurrentTransformer!=null){
			secondCurrentTransformer.timeChanged(time);
			
			}
			currentTransformer.timeChanged(time);
			}*/
		}
		
		
		/**
		 * Este metodos se ejecuta cada 10 segundos que tiene el Timer
		 * @reference timeChanged
		 */
		public function processTimeChangedEverySeconds(time:Date):void {
			
		}
		
		
		public var lastRenderedDate:String = "";
		
		
		
		
		/**
		 * Este metodo agenda el siguiente timer que se necesita
		 * @reference timeChanged
		 */
		private function scheduleTimeIndicatorTimer():void
		{
			var loc1:Date = new Date();
			timeIndicatorTimer.delay = ilog.utils.TimeUnit.MINUTE.milliseconds - (loc1.seconds * 1000 + loc1.milliseconds);
			timeIndicatorTimer.start();
			return;
		}
		
		
		/**
		 * Este metodo dibuja las lineas que se necesiten 
		 * 
		 */
		private function drawTimeIndicators():void {
			/*var saveObject:SavePlanningScreenChange=new SavePlanningScreenChange(); 
			saveObject.plant="pp";
			saveObject.dateCreate="01012013";
			saveObject.vehicle="..";
			saveObject.vbeln="22343";
			saveObject.posnr="3243";
			saveObject.loadVol="32423";
			saveObject.deliveryIhour="010000";
			saveObject.deliveryFhour="010000";
			saveObject.modificationDat="01012013";
			saveObject.userCreate="e0asantos";
			//enviar evento
			GanttServiceReference.dispatchIslandEvent(saveObject);*/
			var g:Graphics=timeIndicators.graphics;
			g.clear();
			if(currentView!=TVARVCPreffixes.PLANNING_SCREEN_VIEW_ID && currentView!=TVARVCPreffixes.SERVICE_AGENT_SCREEN_VIEW_ID && view.viewStack.numChildren>2){
				
				if (timeIndicators && timeController && timeController.configured) {
					var g:Graphics=timeIndicators.graphics;
					g.clear();
					if(currentView!=TVARVCPreffixes.PLANNING_SCREEN_VIEW_ID && currentView!=TVARVCPreffixes.SERVICE_AGENT_SCREEN_VIEW_ID){
						if (isCurrentDateData){
							drawLine(g,0,0x000099);
							drawLine(g,getPlantVisibilityLineMinutes(),0x990000);
						}
					}
				}
			}
		}
		
		/**
		 * esta funcio regresa el valor que tenga el Paramtero PLANT_VIS_AREA_L
		 */
		public function getPlantVisibilityLineMinutes():Number{
			return GanttServiceReference.getParamNumber(ParamtersConstants.PLANT_VIS_AREA_L);
		}
		
		/**
		 * Este metodo regresa las coordenadas de cierta fecha 
		 * 
		 */
		public static function getCoordinate(date:Date):Number{
			return timeController.getCoordinate(date) as Number
		} 
		
		/**
		 * Este metodo dibuja sobre el lienzo una linea deacuerdo a los parametros que se obtienen
		 */
		public function drawLine(g:Graphics,minutes:int,color:Number,thinkness:int=2):void{
			
			//var timeController:Object = TimeControllerGetter.getTimeController(view.gantt.ganttSheet);
			//var fecha:Date = view.gantt.ganttSheet.calendar.floor(FlexDateHelper.addMinutes(new Date(),minutes), ilog.utils.TimeUnit.MINUTE, 1);
			var fechaZonaHorariaOffSet:Date=TimeZoneHelper.getServer();
			//fechaZonaHorariaOffSet=FlexDateHelper.addSeconds(fechaZonaHorariaOffSet,getTimer()/1000);
			DispatcherViewMediator.endDate=FlexDateHelper.addMinutes(fechaZonaHorariaOffSet,minutes);
			var posicion:Number = timeController.getCoordinate(FlexDateHelper.addMinutes(fechaZonaHorariaOffSet,minutes)) as Number;
			if (posicion >= 0 && posicion < timeIndicators.width) 
			{
				g.lineStyle(thinkness,color , view.gantt.ganttSheet.getStyle("currentTimeIndicatorAlpha"));
				g.moveTo(posicion, 0);
				g.lineTo(posicion, timeIndicators.height);
			}
		}
		
		//////////////////
		//  funciones de estatus de Servicios
		//////////////////
		// Funciones de ayuda para dibujar
		
		[Bindable] [Embed(source="/assets/s_s_ledg.gif")] private var green:Class;             
		[Bindable] [Embed(source="/assets/s_s_ledr.gif")] private var red:Class;
		[Bindable] [Embed(source="/assets/s_s_ledy.gif")] private var yellow:Class;            
		
		/**
		 * Este metodo valida que icono se quiere pintar, pero esto es para los Eventos de los servicios
		 */
		override public function getIcon(status:String):Class {
			if (status == CONNECTED){
				return green;
			}
			else if (status == FAULT){
				return yellow;
			}
			else {
				return red;
			}
		}
		
		/**
		 * Este metodo regresa el modelos de los servicios, 
		 * para que se dibuje los estatus de
		 */
		override public function getModel():IServiceModel{
			return GanttServiceReference.getModel();
		}
		
		
		
		
		/**
		 * Este metodo es el que nos dice que etiqueta se debe pintar 
		 * Segun los datos de la OTR
		 */
		public function getLabel(label:String):String{
			return GanttServiceReference.getLabel(label);
		}
		/**
		 * Este metodo actualiza los servicios
		 * 
		 */
		override protected function serviceReady(e:ServiceEvent):void{
			super.serviceReady(e);
			setLabels(e.service.config.name);
		}
		
		/**
		 * ESte metodo settea las etiquetas deacuerdo al idioma en que se haya entrado, 
		 * 
		 * Este metodo se ejecuta en la carga inicial cuando el servicio esta listo
		 * 
		 */
		private var colorsSet:Boolean=false;
		public function setLabels(service:String):void{
			
			if (service == "DispatchIsland"){
				
				
				view.zoomin.toolTip = getLabel(OTRConstants.ZOOM_IN_TOOLTIP);
				view.zoomout.toolTip = getLabel(OTRConstants.ZOOM_OUT_TOOLTIP);
				view.showAll.toolTip = getLabel(OTRConstants.SHOW_ALL_TOOLTIP);
				view.currentTime.toolTip = getLabel(OTRConstants.CURRENT_TIME_TOOLTIP);
				view.refresh.toolTip = getLabel(OTRConstants.REFRESH_TOOLTIP);
				if(view.viewStack.numChildren==3 || view.viewStack.numChildren==5){
					view.loadsPerOrderLabel.label = getLabel(OTRConstants.LOADS_PER_ORDER_HEADER);
					view.loadsPerVehicleLabel.label = getLabel(OTRConstants.LOADS_PER_VEHICLE_HEADER);
					view.assignedLoadsLabel.label = getLabel(OTRConstants.ASSIGNED_LOADS_HEADER);
				}
				try{
					if (view.viewStack.numChildren==2 || view.viewStack.numChildren==5){
						
						/*view.serviceAgentScreenLabel.label=getLabel(OTRConstants.SERVICE_AGENT_SCREEN);
						view.planningScreenLabel.label=getLabel(OTRConstants.PLANNING_SCREEN);*/
					}
				} catch(e:Error){
					
				}
				//view.loadWithOutRecipe_cb.label=getLabel(OTRConstants.SHOW_NO_RECIPE);
				view.loadWithOutRecipe_cb.label=getLabel(OTRConstants.DISP_LOADS_WITHOUT_RECIPE);
				
				view.collect.label=getLabel(OTRConstants.COLLECT_ORDERS);
				/*view.currentCostLabel.text = getLabel(OTRConstants.CURRENT_COST_LABEL);
				view.currentCostCurr.text = getLabel(OTRConstants.CURRENT_COST_CURRENCY);
				
				view.optimalCostLabel.text = getLabel(OTRConstants.OPTIMAL_COST_LABEL);
				view.optimalCostCurr.text = getLabel(OTRConstants.OPTIMAL_COST_CURRENCY);*/
				view.addTruck.toolTip=getLabel(OTRConstants.DISP_TRUCK_DATABASE);
				view.editTruck.toolTip=getLabel(OTRConstants.DISP_TRUCK_REPORT);
				view.warning.text = getLabel(OTRConstants.WARNING);
				view.logMessage.text = "";
				view.LoadsPerHour.label=getLabel(OTRConstants.LOADS_PER_HOUR);
				//view.collect.label=getLabel(OTRConstants.COLLECT_ORDERS);				
				
				
				GanttServiceReference.setCollectFilterStatus(GanttServiceReference.getParamNumber(ParamtersConstants.COLLECT_LOAD_DIS) != 0);
				view.collect.selected = GanttServiceReference.getCollectFilterStatus();
				view.autDistr.toolTip= getLabel(OTRConstants.DISP_TT_AUT_DISP);
				view.saveVisualChanges.toolTip= getLabel(OTRConstants.DISP_TT_SAVE_CHANGES);
				
				ValidateParameters.validateOTR();
				ValidateParameters.validateParams();
				ValidateParameters.validateTVARV();
				GanttServiceReference.dispatchIslandEvent( WDRequestHelper.getCostData("0.00", "0.00"));
			}
			if(!colorsSet){
				var coloresNuevos:ArrayCollection=GanttServiceReference.getColorPalette() as ArrayCollection;
				for(var q:int=0;q<coloresNuevos.length;q++){
					var canvasc:CanvasColor=new CanvasColor();
					/*view.colorReference.colores.addChild(canvasc);*/
					canvasc.setStyle("backgroundColor",coloresNuevos.getItemAt(q).FILL_COLOR);
					canvasc.setStyle("borderThickness",2); 
					canvasc.setStyle("borderColor",coloresNuevos.getItemAt(q).BORDER_COLOR);
					canvasc.setStyle("borderStyle","solid");
					//canvasc.texto.text=coloresNuevos.getItemAt(q).DESCRIPTION;
					canvasc.toolTip=coloresNuevos.getItemAt(q).DESCRIPTION+"\n"+coloresNuevos.getItemAt(q).CONCEPT;
					canvasc.addEventListener(MouseEvent.MOUSE_OVER,cambiarColor);
				}
				//view.colorReference.addEventListener(MouseEvent.ROLL_OUT,resetColors);
				colorsSet=true;
			}
			
		}
		
		public function cambiarColor(evt:MouseEvent):void{
			BottomTabNavigator.currentColor=String(evt.currentTarget.getStyle("backgroundColor"));
			var ac:ArrayCollection=currentTransformer.getTasksTree();
			for(var tsk:int=0;tsk<ac.length;tsk++){
				if(ac.getItemAt(tsk).payload.graphicReference!=null){
					var uno:String=String(evt.currentTarget.getStyle("backgroundColor"));
					var dos:String=String(ac.getItemAt(tsk).payload.graphicReference.getStyle("backgroundColor"));
					
					var tres:String=String(evt.currentTarget.getStyle("borderColor"));
					var cuatro:String=(ac.getItemAt(tsk).payload.graphicReference.getStyle("borderColor"));
					
				}
			}
		}
		private function resetColors(evt:MouseEvent):void{
			var ac:ArrayCollection=currentTransformer.getTasksTree();
			for(var tsk:int=0;tsk<ac.length;tsk++){
				if(ac.getItemAt(tsk).payload.graphicReference!=null){
					ac.getItemAt(tsk).payload.graphicReference.alpha=1;
				}
			}
		}
		public function setColorToCanvas(color:ColorSetting, canvas:Canvas):void{
			
			canvas.setStyle("backgroundColor",color.fillColor); 
			//canvas.setStyle("backgroundAlpha",0.7); 
			canvas.setStyle("borderThickness",color.borderThickness); 
			canvas.setStyle("borderColor",color.borderColor);
			canvas.setStyle("borderStyle","solid");
		}
		
		////////////////////////////////////
		/// SECCION DE EVENTOS DEL MOUSE
		////////////////////////////////////
		/**
		 * Evento de volver a conectar al LCDS
		 */
		override public function clickStatus(service:String):void{
			dispatch(new LCDSReconnectEvent());
		}
		
		/*		public function dispatchSearchAux():void{
		var evntSearchTMP:SearchBtnClickViewEvent = new SearchBtnClickViewEvent();
		evntSearchTMP.searchString = textToSearch;
		eventDispatcher.dispatchEvent(evntSearchTMP);
		}
		*/		
		
		//public var collectFilterFlag:Boolean = false;
		public function collectFilter(e:MouseEvent):void{
			DispatcherIslandImpl.masterKey=true;
			//collectFilterFlag = view.collect.selected;
			GanttServiceReference.setCollectFilterStatus(view.collect.selected);
			
			
			requestRefresh();
		}
		
		
		/**
		 * Solcitud de refresh hecha por el usuario
		 * Solo el boton puede actualizar los datos usando la variable de llave maestra
		 */
		
		private var masterKey:Boolean=false;
		public function refresh(e:MouseEvent):void{
			saveRange();
			/*view.pushNotifier.ordenes = "";
			view.pushNotifier.setVisibility(false);*/
			DispatcherIslandImpl.masterKey=true;
			requestRefresh();
		}
		/**
		 * Este metodo es un refresh por otra causa que no es explicitamente la solicitud del usuario 
		 */
		public function requestRefresh():void{
			/*if(!DispatcherIslandImpl.masterKey){
			return;
			}*/
			//view.gantt.resourceDataProvider = new ArrayCollection();
			//view.gantt.taskDataProvider = new ArrayCollection();
			disableStack(false);
			clonedCanvas=null;
			tmpEraseItemPlanningScreen=[];
			GanttServiceReference.refreshData();
		}
		
		/**
		 * Esta funcion es para mostrar en pantalla todas las cargas que estan visibles
		 */
		public function showAll(e:MouseEvent):void{
			if(currentTransformer.getTasksTree().length>GanttServiceReference.getPlanningLoads().length){
				view.gantt.ganttSheet.showAll();	
			} else {
				view.vehicleScheduleView.ganttSheet.showAll();
			}
			
			//view.vehicleScheduleView.ganttSheet.showAll();
			//comparar quien tiene la menor fecha
			/*if(view.gantt.ganttSheet.visibleTimeRangeStart.time<view.vehicleScheduleView.ganttSheet.visibleTimeRangeStart.time){
			view.vehicleScheduleView.ganttSheet.visibleTimeRangeStart=view.gantt.ganttSheet.visibleTimeRangeStart;
			} else {
			view.gantt.ganttSheet.visibleTimeRangeStart=view.vehicleScheduleView.ganttSheet.visibleTimeRangeStart;
			}
			if(view.gantt.ganttSheet.visibleTimeRangeEnd.time>view.vehicleScheduleView.ganttSheet.visibleTimeRangeEnd.time){
			view.vehicleScheduleView.ganttSheet.visibleTimeRangeEnd=view.gantt.ganttSheet.visibleTimeRangeEnd;
			} else {
			view.gantt.ganttSheet.visibleTimeRangeEnd=view.vehicleScheduleView.ganttSheet.visibleTimeRangeEnd;
			}*/
			
		}
		/**
		 * Esta es la solicitud del usuario de hacer zoom in
		 * Si se tiene apretado el boton de Control hace todo el zoomin que se pueda
		 */
		public function zoomin(e:MouseEvent):void{
			
			var factor:Number  = 0.5;
			if (e.ctrlKey){
				factor=0.001;
			}
			if (isCurrentDateData){
				//view.gantt.ganttSheet.zoom(factor, new Date(), true );
				view.gantt.ganttSheet.zoom(factor, null, true );
				view.vehicleScheduleView.ganttSheet.zoom(factor, null, true );
			}
			else {
				view.gantt.ganttSheet.zoom(factor, null, true );
			}
			//var now:Date = new Date();
			var now:Date=TimeZoneHelper.getServer();
			if (isSameAsBaseDate(now) ){
				DispatcherViewMediator.isUsingPush=true;
				currentTransformer.timeChanged(now);
				if(secondCurrentTransformer!=null){
					secondCurrentTransformer.timeChanged(now);
				}
			}
			saveRange();
		} 
		
		/**
		 * Esta es la solicitud del usuario de hacer zoom out
		 * Si se tiene apretado el boton de Control hace todo el zoomou que se pueda
		 */
		public function zoomout(e:MouseEvent):void{
			
			var factor:Number  = 2;
			if (e.ctrlKey){
				factor=1000;
			}
			if (isCurrentDateData){
				view.gantt.ganttSheet.zoom(factor, null, true );
				view.vehicleScheduleView.ganttSheet.zoom(factor, null, true );
				view.gantt.ganttSheet.addEventListener(EffectEvent.EFFECT_END,endAnimation)
				//view.gantt.ganttSheet.zoom(factor, new Date(), true );
			}
			else {
				view.gantt.ganttSheet.zoom(factor, null, true );
			}
			//var now:Date = new Date();
			var now:Date=TimeZoneHelper.getServer();
			if (isSameAsBaseDate(now) ){
				DispatcherViewMediator.isUsingPush=true;
				currentTransformer.timeChanged(now);
				if(secondCurrentTransformer!=null){
					secondCurrentTransformer.timeChanged(now);
				}
			}
			saveRange();
		}
		
		private function endAnimation(evt:EffectEvent):void{
			logger.debug("efecto terminado");
		}
		
		private var currentTimeFlag:Boolean=true;
		
		/**
		 * Este evento cacha cuando el usuario ya no quiere que e mueva automaticamente la pantalla
		 */
		public function gotoCurrentTime(e:MouseEvent):void{
			currentTimeFlag = !currentTimeFlag;
			if (currentTimeFlag){
				//view.gantt.ganttSheet.moveTo(
				move2CurrentTime(true);
			}
		}
		
		
		public static var overlapedItem:DictionaryMap= new DictionaryMap();
		
		
		
		
		
		/**
		 * Esta funcion se manda a llamar cuando se termina
		 */
		private var originalVehicleSource:GanttSheetEvent;
		private var privateIsConveyor:Boolean=false;
		private var privateVehicleSelected:Object;
		private function itemEditEnd(e:GanttSheetEvent):void{
			//Alert.show("itemEditEnd","info");
			//conveyor 995
			//mixer 994
			//pumping 993
			//992pump mixer
			originalVehicleSource=e;
			DispatcherViewMediator.isDragging=false;
			if (e.reason == GanttSheetEventReason.CANCELLED) {
				var task:GanttTask = e.item as GanttTask;
				addData(task.data,GanttTask.STEP_MOVED);
			}
			if(clonedCanvas!=null){
				
				//50 pixel margin and 70
				clonedCanvas.visible=false;
				clonedCanvas.stopDrag();
				/*TweenLite.to(view.vehicleScheduleView,2,{
				height:previousHeight
				})
				*/				
				var scroll:Number=view.vehicleScheduleView.dataGridBack.verticalScrollPosition;
				var posy:Number=0;
				if(scroll==0){
					posy=Math.floor((view.vehicleScheduleView.mouseY-90)/70)-1;
				} else {
					posy=Math.floor((view.vehicleScheduleView.mouseY)/70)-1;
				}
				var maxscroll:Number=view.vehicleScheduleView.dataGridBack.maxVerticalScrollPosition;
				var entidades:Number=view.vehicleScheduleView.dataGridBack.dataProvider.length-1;
				var numitems:Number=Math.floor((view.vehicleScheduleView.dataGridBack.height-90)/70);
				var something:Object=PlanningScreenTreeResourceRenderer.xyPositions;
				var posicionCero:Number=Math.floor((scroll*entidades)/maxscroll)-1;
				var rowItem:Object = view.vehicleScheduleView.dataGridBack.getRowItemAtPosition(view.mouseY);
				var vehicleid:String="";
				if(rowItem!=null){
					vehicleid=rowItem.equipNumber;
					
				}
				//buscar
				var vehicleSelected:Object;
				for(var ve:int=0;ve<DispatcherViewMediator.vehiculosCorrectos.length;ve++){
					if(DispatcherViewMediator.vehiculosCorrectos.getItemAt(ve).equipNumber==vehicleid){
						vehicleSelected=DispatcherViewMediator.vehiculosCorrectos.getItemAt(ve);
						privateVehicleSelected=vehicleSelected;
						break;
					}
				}
				/*if(scroll>(maxscroll/2)){
				posicionCero=posicionCero-1;
				}*/
				if(posy<0){
					posy=0;
				}
				if(posicionCero<0){
					posicionCero=0;
				}
				var finalRealIndex:Number=posicionCero+posy;
				if(finalRealIndex>DispatcherViewMediator.vehiculosCorrectos.length-1){
					finalRealIndex=DispatcherViewMediator.vehiculosCorrectos.length-1;
				}
				if(vehicleSelected!=null){
					var conveyorString:String=" is NOT "
					//buscar si es conveyor
					var isConveyor:Boolean=false;
					var adds:ArrayCollection=e.item.data.addProd;
					for(var h:int=0;h<adds.length;h++){
						if(adds.getItemAt(h).itemNumber==e.item.data.itemNumber && adds.getItemAt(h).conveyor=="X"){
							isConveyor=true;
							privateIsConveyor=true;
							conveyorString=" is ";
							break;
						}
					}
					//validar volumen y tipo de vehiculo
					
					var itemData:OrderLoad=originalVehicleSource.item.data
					var mapScheduleVehicle:ArrayCollection=DispatcherIslandImpl.mapVehicles.get(vehicleSelected.equipNumber);
					var isVehicleSchedule:Boolean=true;
					for(var msv:int=0;msv<mapScheduleVehicle.length;msv++){
						var finDate:Date=mapScheduleVehicle.getItemAt(msv).finalDate;
						finDate.hours=Number(mapScheduleVehicle.getItemAt(msv).finalHour.split(":")[0]);
						finDate.minutes=Number(mapScheduleVehicle.getItemAt(msv).finalHour.split(":")[1]);
						
						var iniDate:Date=mapScheduleVehicle.getItemAt(msv).initialDate;
						iniDate.hours=Number(mapScheduleVehicle.getItemAt(msv).initialHour.split(":")[0]);
						iniDate.minutes=Number(mapScheduleVehicle.getItemAt(msv).initialHour.split(":")[1]);
						
						var iinit:Number=itemData.startTime.time;
						var iend:Number=FlexDateHelper.addSeconds(itemData.startTime,itemData.cycleTimeMillis).time;
						//cuando la carga es pequeÃ±a o entra dentro del horario
						if((iend>=iniDate.time && iend<=finDate.time) || (iinit>=iniDate.time && iinit<=finDate.time)){
							isVehicleSchedule=false;
						}
						//cuando el horario es mas pequeÃ±o
						if((iniDate.time>=iinit && iniDate.time<=iend) || (finDate.time>=iinit && finDate.time<=iend)){
							isVehicleSchedule=false;
						}
						/*if(!(iinit>finDate.time && iinit>iniDate.time)){
						isVehicleSchedule=false;
						}*/
					}
					//verificar el horario de la carga
					
					//verificar el horario de la carga
					
					var overlaped:Boolean=false;
					for(var _msv:int=0;_msv<rowItem._children.length;_msv++){
						
						var _finDate:Date=rowItem._children[_msv].endTime;
						var _iniDate:Date=rowItem._children[_msv].startTime;
						if( _finDate!=null && _iniDate!=null){
							
							var _iinit:Number=itemData.startTime.time;
							var _iend:Number=FlexDateHelper.addSeconds(itemData.startTime,itemData.cycleTimeMillis).time;
							//cuando la carga es pequeña o entra dentro del horario
							if((_iend>=_iniDate.time && _iend<=_finDate.time) || (_iinit>=_iniDate.time && _iinit<=_finDate.time)){
								overlaped=true;
							}
							//cuando el horario es mas pequeño
							if((_iniDate.time>=_iinit && _iniDate.time<=_iend) || (_finDate.time>=_iinit && _finDate.time<=_iend)){
								overlaped=true;
							}
						}
						
					}
					/*if (overlaped== true){
					
					
					var rowItemfake:Object  
					rowItemfake	=originalVehicleSource.item;
					var dictioninspected:DictionaryMap= overlapedItem;
					if (overlaped== true){ // si es overlap: agrega el item arrastrado y agrega todos los children de la columna destino 
					for(var _msv1:int=0;_msv1<rowItem._children.length;_msv1++){
					if(overlapedItem.get(rowItem._children[_msv1].data.orderNumber+":"+rowItem._children[_msv1].data.orderLoad+":"+rowItem._children[_msv1].data.itemNumber)==null){
					
					overlapedItem.put(rowItem._children[_msv1].data.orderNumber+":"+rowItem._children[_msv1].data.orderLoad+":"+rowItem._children[_msv1].data.itemNumber,rowItem._children[_msv1].data.orderNumber+":"+rowItem._children[_msv1].data.orderLoad+":"+rowItem._children[_msv1].data.itemNumber);
					}
					}
					var rowItemfake:Object = rowItem._children[(rowItem._children as ArrayCollection).length-1];
					overlapedItem.put((rowItemfake['data'] as OrderLoad).orderNumber+":"+(rowItemfake['data'] as OrderLoad).orderLoad+":"+(rowItemfake['data'] as OrderLoad).itemNumber,(rowItemfake['data'] as OrderLoad).orderNumber+":"+(rowItemfake['data'] as OrderLoad).orderLoad+":"+(rowItemfake['data'] as OrderLoad).itemNumber);
					
					}*/
					
					
					/*	for (var cuentaVehic:Number=0;cuentaVehic<view.vehicleScheduleView.taskDataProvider.length;cuentaVehic++){
					orderTMP = view.vehicleScheduleView.taskDataProvider[cuentaVehic].payload as OrderLoad;
					if ((orderTMP.orderNumber.toLowerCase().indexOf(param)>-1||orderTMP.orderLoad.toLowerCase().indexOf(param)>-1)&&param!="")
					orderTMP.mustGlowBySearch = true;
					else
					orderTMP.mustGlowBySearch = false;
					}
					
					}*/
					
					
					
					if(isVehicleSchedule){
						if(Number(vehicleSelected.loadVol)<e.item.data.loadVolume){
							PlanningScreenVehicleDataTransformer.ignoreAssn=true;
							(e.itemRenderer as UIComponent).visible=true;
							e.item.data.workAroundDestroy=null;
							privateIsConveyor=isLoadConveyor(e.item.data);
							// Set the height and width of the Alert control.
							Alert.show(getLabel(OTRConstants.DISP_PS_VOL_EXC),"Alert",Alert.OK | Alert.CANCEL,
								this.view,
								alertListener);
							return;
						}
						var resa:Boolean=assignByDrag(isConveyor,vehicleSelected);
						if(resa){
							tmpRefresh=true;
							islandDataEventHandler(tmpErase);
						}
					} else {
						PlanningScreenVehicleDataTransformer.ignoreAssn=true;
						(e.itemRenderer as UIComponent).visible=true;
						e.item.data.workAroundDestroy=null;
						Alert.show(getLabel(OTRConstants.DISP_PS_TRUCK_NO_AV),"Error");
					}
					
					/*if((isConveyor==true && vehicleSelected.vehicleType=="995")
					|| (isConveyor==false && (vehicleSelected.vehicleType=="994" || vehicleSelected.vehicleType=="992"))){
					e.item.data.equipNumber=vehicleSelected.equipNumber;
					e.item.data.equipLabel=vehicleSelected.equipLabel;
					e.item.data.equipStatus="Not Assigned";
					PlanningScreenVehicleDataTransformer.ignoreAssn=true;
					var inow:Date=TimeZoneHelper.getServer();
					secondCurrentTransformer.addData(e.item.data,GanttTask.STEP_INITIAL,inow);
					e.item.data.workAroundDestroy="yes";
					(e.itemRenderer as UIComponent).visible=false;
					//
					var payl:OrderLoad=e.item.data;
					var saveObject:SavePlanningScreenChange=new SavePlanningScreenChange();
					saveObject.plant=payl.plant;
					saveObject.dateCreate="01012013";
					saveObject.vehicle=payl.equipNumber;
					saveObject.vbeln=payl.orderNumber;
					saveObject.posnr=payl.itemNumber;
					saveObject.loadVol=payl.loadVolume.toString();
					saveObject.deliveryIhour="010000";
					saveObject.deliveryFhour="010000";
					saveObject.modificationDat="01012013";
					saveObject.userCreate="e0asantos";
					//enviar evento
					GanttServiceReference.dispatchIslandEvent(saveObject);
					} else {
					PlanningScreenVehicleDataTransformer.ignoreAssn=true;
					(e.itemRenderer as UIComponent).visible=true;
					e.item.data.workAroundDestroy=null;
					Alert.show("The load "+e.item.data.itemNumber+" cannot be assigned to this vehicle "+String(vehicleSelected.equipLabel)+"("+String(parseInt(vehicleSelected.equipNumber))+"). The load"+conveyorString+" conveyor","Error");
					}*/
				} else {
					//fuera de un vehiculo
					Alert.show(getLabel(OTRConstants.DISP_PS_DRAG_VEH),"Error");
					(e.itemRenderer as UIComponent).visible=true;
					e.item.data.workAroundDestroy=null;
					view.gantt.ganttSheet.visibleTimeRangeStart=FlexDateHelper.addSeconds(view.vehicleScheduleView.ganttSheet.visibleTimeRangeStart,3);
				}
				/*for(var w:int;w<DispatcherViewMediator.vehiculosCorrectos.length;w++){
				
				}*/
				clonedCanvas=null;
				view.vehicleScheduleView.ganttSheet.visibleTimeRangeStart=FlexDateHelper.addSeconds(view.gantt.ganttSheet.visibleTimeRangeStart,5);	
			}
		}
		private function assignByDrag(isConveyor:Boolean,vehicleSelected:Object,makeGlow:Boolean=false):Boolean{
			var resultOfResult:Boolean=false;
			if((isConveyor==true && vehicleSelected.vehicleType=="995")
				|| (isConveyor==false && (vehicleSelected.vehicleType=="994" || vehicleSelected.vehicleType=="992" || vehicleSelected.vehicleType=="995"))){
				var factor:Object=view.gantt.ganttSheet.maxZoomFactor
				minMaxDates = [view.gantt.ganttSheet.visibleTimeRangeStart, view.gantt.ganttSheet.visibleTimeRangeEnd];				
				originalVehicleSource.item.data.equipNumber=vehicleSelected.equipNumber;
				originalVehicleSource.item.data.equipLabel=vehicleSelected.equipLabel;
				originalVehicleSource.item.data.equipStatus="Not Assigned";
				originalVehicleSource.item.data.useGlow=makeGlow;
				PlanningScreenVehicleDataTransformer.ignoreAssn=true;
				var inow:Date=TimeZoneHelper.getServer();
				if((originalVehicleSource.itemRenderer as UIComponent)!=null){
					/*(originalVehicleSource.itemRenderer as UIComponent)["personalMark"]=true;*/
					(originalVehicleSource.itemRenderer as UIComponent).visible=false;
				}
				/*originalVehicleSource.item.data.graphicReference.personalMark=true;*/
				secondCurrentTransformer.addData(originalVehicleSource.item.data,GanttTask.STEP_INITIAL,inow);
				//generar un evento
				/* */
				originalVehicleSource.item.data.workAroundDestroy="yes";
				
				var posiciones:DictionaryMap=PlanningScreenTask.singlePositions;
				posiciones.remove(originalVehicleSource.item.data.plant+":"+originalVehicleSource.item.orderNumber+":"+originalVehicleSource.item.data.itemNumber);
				//
				var payl:OrderLoad=originalVehicleSource.item.data;
				var saveObject:SavePlanningScreenChange=new SavePlanningScreenChange(); 
				saveObject.plant=payl.plant;
				saveObject.dateCreate="01012013";
				saveObject.vehicle=payl.equipNumber;
				saveObject.vbeln=payl.orderNumber;
				saveObject.posnr=payl.itemNumber;
				saveObject.loadVol=payl.loadVolume.toString();
				saveObject.deliveryIhour="010000";
				saveObject.deliveryFhour="010000";
				saveObject.modificationDat="01012013";
				saveObject.userCreate="e0asantos";
				//enviar evento y eliminar de la vista errores
				GanttServiceReference.dispatchIslandEvent(saveObject);
				var objdp:ArrayCollection=view.gantt.taskDataProvider as ArrayCollection;
				var objdp2:Object=view.gantt.resourceDataProvider;
				if(originalVehicleSource.target==view.gantt.ganttSheet){
					for(var ccq:int=0;ccq<objdp.length;ccq++){
						if(objdp.getItemAt(ccq).data.orderLoad==payl.orderLoad){
							tmpEraseItemPlanningScreen.push(payl.orderLoad);
							resultOfResult=true;
							PlanningScreenTaskRenderer.personalMarkTime[originalVehicleSource.itemRenderer]=true;
							var responseUpdateContext:* = updateContext(objdp.getItemAt(ccq).data,objdp.getItemAt(ccq).resourceId,true,currentTransformer);
							break;
						}
					}
					/*var hd:ResourceHierarchicalData = new ResourceHierarchicalData(currentTransformer.getResourcesTree());
					gH=hd;
					hd.childrenField = "children";
					var sort:Sort=new Sort();
					view.gantt.resourceDataProvider=hd
					view.gantt.taskDataProvider=currentTransformer.getTasksTree();*/
					var dict:Object=currentTransformer.getTree().ctx;
				}
				/*var busqueda:SearchBtnClickViewEvent=new SearchBtnClickViewEvent();
				busqueda.searchString="-_-";
				busqueda.refTextInput=null;
				eventDispatcher.dispatchEvent(busqueda);*/
				if(clonedCanvas!=null){
					view.removeChild(clonedCanvas);
				}
				clonedCanvas=null;
			} else {
				PlanningScreenVehicleDataTransformer.ignoreAssn=true;
				if((originalVehicleSource.itemRenderer as UIComponent)!=null){
					(originalVehicleSource.itemRenderer as UIComponent).visible=true;
					originalVehicleSource.item.data.workAroundDestroy=null;
				}
				
				Alert.show(originalVehicleSource.item.data.itemNumber+getLabel(OTRConstants.DISP_PS_LOAD_ASS_ERR)+String(vehicleSelected.equipLabel)+"("+String(parseInt(vehicleSelected.equipNumber))+").","Error");
				view.gantt.ganttSheet.visibleTimeRangeStart=FlexDateHelper.addSeconds(view.vehicleScheduleView.ganttSheet.visibleTimeRangeStart,3);
			}
			return resultOfResult;
		}
		private function alertListener(eventObj:CloseEvent):void {
			// Check to see if the OK button was pressed.
			if (eventObj.detail==Alert.OK) {
				
				var resa:Boolean=assignByDrag(privateIsConveyor,privateVehicleSelected,true);
				if(resa){
					tmpRefresh=true;
					islandDataEventHandler(tmpErase);
				}
			}
		}
		
		
		
		
		
		
		
		
		public function copyToClipboard(e:ContextMenuEvent):void {
			if (currentTransformer != null && currentTransformer.getResourcesTree() != null){
				System.setClipboard(
					"<Data>\n"+
					rawData+
					"<tasksTree>\n"+
					ReflectionHelper.object2XML(currentTransformer.getTasksTree(),"tasksTree","  ")+
					"</tasksTree>\n"+
					"<resourcesTree>\n"+
					ReflectionHelper.object2XML(currentTransformer.getResourcesTree(),"resourcesTree","  ")+
					"</resourcesTree>\n"+
					"</Data>"
				);
			}
		}
		
		public function returnLogEvent(e:ReturnLogEvent):void{
			var msg:String="";
			for(var q:int=0;q<e.returnLog.length;q++){
				msg+=(e.returnLog.getItemAt(q) as ReturnLog).message+"\n"
				if(e.returnLog.getItemAt(q).toString().indexOf("ERROR")!=-1){
					e.returnLog.setItemAt(e.returnLog.getItemAt(q)+" "+getLabel(OTRConstants.DISP_ERROR_DELIVERY_DATE),q);
				}
			}
			if(msg!=""){
				/*Alert.show(msg,"Info");*/
			}
			view.logMessage.dataProvider = e.returnLog.source.reverse();
			
			
		}
		
		// Funciones 
		public function islandDataConfirmEventHandler(e:FlashIslandsDataEvent):void{
			addDatas(e.data.loads,GanttTask.STEP_MOVED);
		}
		
		/**
		 * Se cambia el modo operativo en los ervicios y despues se manda a 
		 */
		public function gotPushFromPlantOperativeMode(e:PlantOperativeModeLCDSEvent):void {
			var thisDatetmp:Date=new Date();
			plantLastDate=thisDatetmp.date+"/"+thisDatetmp.month+"/"+thisDatetmp.fullYear+"  "+thisDatetmp.hours+":"+thisDatetmp.minutes+":"+thisDatetmp.seconds
			plantPushCounter+=1
			updatePushPanel();
			DispatcherViewMediator.isUsingPush=true;
			if(Logger.inDebug){
				Logger.pushPlantOperativeCounter++;
				//view.gotPush.text="PUSH PLANT "+e.plantId;//+"  "+((new Date().minutes)+""+(new Date().seconds))+"/"+Logger.pushPlantOperativeCounter+"/"+Logger.pushProduction+"/"+Logger.pushSalesOrderCounter+"/"+Logger.pushVehicleCounter;
				Logger.incomingPush("Plant operative push",e.plantId,this.view)
			}
			if(GanttServiceReference.getPlantInfo(e.plantId)){
				
				GanttServiceReference.setPlantType(e.plantId,e.zoperativeMode,e.statusCode);
				view.gantt.dataGridBack.invalidateDisplayList();
				view.gantt.dataGridBack.invalidateList();
				/*view.gantt.dataGridBack.invalidateProperties();
				view.gantt.dataGridBack.invalidateSize();*/
				//var now:Date = new Date();
				var now:Date=TimeZoneHelper.getServer();
				if (isSameAsBaseDate(now) ){
					DispatcherViewMediator.isUsingPush=true;
					currentTransformer.timeChanged(now);
				}
			}
			view.gantt.dataGridBack.invalidateDisplayList();
			view.gantt.dataGridBack.invalidateList();
			
		}
		public static var issales:Boolean=false;
		public function createDummySalesOrder(planta:String,itemNumberr:String):OrderLoad{
			var var1:OrderLoad=new OrderLoad();
			var1.additionalFlag=false;
			var1.addProd=new ArrayCollection();
			var1.bomValid="";
			var1.borderColor="0XE8C427";
			var1.fillColor="0XFFFD1C";
			var1.city="France la rue";
			var1.codStatusProdOrder="10";
			var1.comingFromPush=false;
			var1.completedDelayed=false;
			var1.confirmQty=7;
			var1.constructionProduct="";
			var1.contactName1="prueba4 prueba4";
			var1.contactName2="prueba4 prueba4";
			var1.contactTel="23423424";
			var1.cpDescription="";
			var1.cpOrderNumber="";
			var1.cpQuantity=0;
			var1.cpUom="";
			var1.cycleTime="03:40:00";
			var1.cycleTimeMillis=13200;
			var1.deliveryAmount="";
			var1.deliveryBlock="";
			var1.deliveryGroup=1;
			var1.deliveryTime="08:40:00";
			var1.endTime=null;
			var1.equipLabel="000000000011000451";
			var1.equipNumber="000000000011000451";
			var1.equipStatus="Assigned";
			var1.firstDeliveryTime="08:40:00";
			var1.firstDescription="2-0100-G0-A-003-000-0-3000-000";
			var1.firstOrderNumber="0000128710";
			var1.flagRecipe="";
			var1.graphicReference=null;
			var1.hlevelItem=1000;
			var1.houseNoStreet="125 rue";
			var1.idHlevelItem="";
			var1.indexPosition=0;
			var1.isDelayed=false;
			var1.itemCategory="ZTC1";
			var1.itemCurrency="EUR";
			var1.itemNumber="100"+itemNumberr;
			var1.jobSiteId="0065001158";
			var1.jobSiteName="Job Site02";
			var1.loadFrequency="0";
			var1.loadingDate=new Date(2014,7,5);
			var1.loadingTime="07:05:00";
			var1.loadingTimestamp=new Date(2014,7,5,7,5);
			var1.loadNumber="L00"+itemNumberr;
			var1.loadStatus="CMPL";
			var1.loadUom="M3";
			var1.loadVolume=7;
			var1.materialDes="2-0100-G0-A-003-000-0-3000-000";
			var1.materialId="000000000010001973";
			var1.mepTotCost=551811.26;
			var1.numberTimeStamp=0;
			var1._numericDate=-762434720;
			var1.numProdOrder=null;
			var1.orderLoad="0000128710-L00"+itemNumberr;
			var1.orderNumber="0000128710";
			var1.orderReason="";
			var1.orderStatus="CONF";
			var1.orderUom="M3";
			var1.orderVolume=19;
			var1.paytermLabel="";
			var1.pborderColor="";
			var1.pfillColor="";
			var1.plant=planta;
			var1.postalCode="14526";
			var1.prodOrder="000065204334";
			var1.remainFlag=false;
			var1.renegFlag=false;
			var1.SAPStamp=null;
			var1.shipConditions="01";
			var1.shipmentStatus="";
			var1.slump="";
			var1.soldToName="cust02";
			var1.soldToNumber="0050003570";
			var1.startTime=null;
			var1.taskNumber="";
			var1.text=[];
			var1.texts=null;
			var1.timeFixIndicator="";
			var1.timePumpJobs="00:00:00";
			var1.timeStamp=null;
			var1.totalLoads="3";
			var1.totCost=815699.22;
			var1.txtStatusProdOrder="ABIE";
			var1.unloadingTime="00:20:00";
			var1.valueAdded="";
			var1.vehicleMaxVol=7;
			var1.vehicleType="";
			var1.visible="";
			var1.warning="";
			var1.workAroundDestroy=null;
			var1.orderStatus="CMPO";
			return var1;
		}
		public static var nonVisilbleItems:DictionaryMap=new DictionaryMap();
		public function gotPushFromSalesOrder(e:SalesOrderLCDSEvent):void{
			var thisDatetmp:Date=new Date();
			salesOrderLastDate=thisDatetmp.date+"/"+thisDatetmp.month+"/"+thisDatetmp.fullYear+"  "+thisDatetmp.hours+":"+thisDatetmp.minutes+":"+thisDatetmp.seconds
			salesOrderPushCounter+=1;
			updatePushPanel();
			DispatcherViewMediator.isUsingPush=true;
			var cargas:ArrayCollection=e.datas;
			if (view.viewStack.selectedIndex==4){
/*				if (view.pushNotifier.ordenes.indexOf(cargas[0].orderNumber)>-1){
					view.pushNotifier.ordenes += " "+cargas[0].orderNumber;
					view.pushNotifier.setVisibility(true);
				}*/
			}
			if (currentView == TVARVCPreffixes.PLANNING_SCREEN_VIEW_ID){
				return;
			}
			if(currentStampLoads[(cargas.getItemAt(0) as OrderLoad).orderNumber]!=null){
				if(currentStampLoads[(cargas.getItemAt(0) as OrderLoad).orderNumber]>(cargas.getItemAt(0) as OrderLoad).numberTimeStamp){
					//doesnt need to continue, is old
					return;
				} else {
					currentStampLoads[(cargas.getItemAt(0) as OrderLoad).orderNumber]=(cargas.getItemAt(0) as OrderLoad).numberTimeStamp;
				}
			}
			if(Logger.inDebug){
				Logger.pushSalesOrderCounter++;
				//view.gotPush.text="PUSH SALESORDER:";//+((new Date().minutes)+""+(new Date().seconds))+"/"+Logger.pushPlantOperativeCounter+"/"+Logger.pushProduction+"/"+Logger.pushSalesOrderCounter+"/"+Logger.pushVehicleCounter;
				Logger.incomingPush("Sales Order push",(cargas.getItemAt(0) as OrderLoad).orderLoad+"/"+(cargas.getItemAt(0) as OrderLoad).orderNumber,this.view)
			}
			
			DispatcherViewMediator.issales=true;
			//var continuar:Boolean=false;
			//aqui descartar el push de las plantas que no estan permitidas
			
			
			//descartar los pedidos de bombeo que vienen por push en vista de cargas por vehiculo -- ZTX0
			//if(currentTransformer is LoadPerVehiclePlantVehicleDataTransformer){
			//var arc:ArrayCollection=currentTransformer.getTasksTree();
			for(var b:int=cargas.length-1;b>-1;b--){
				if((cargas.getItemAt(b) as OrderLoad).loadingDate==null){
					(cargas.getItemAt(b) as OrderLoad).loadingDate=new Date();
					(cargas.getItemAt(b) as OrderLoad).loadingDate.hours=0;
					(cargas.getItemAt(b) as OrderLoad).loadingDate.minutes=0;
				}
				if((cargas.getItemAt(b) as OrderLoad).totalLoads==null){
					(cargas.getItemAt(b) as OrderLoad).totalLoads= cargas.length.toString();
				}
				var dateone:Number=(cargas.getItemAt(b) as OrderLoad).loadingTimeStampFix().time;
				var datetwo:Number=getDayStart().time;
				var datefour:Number=getDayEnd().time;
				//	(cargas.getItemAt(b) as OrderLoad).totalLoads= cargas.length.toString();
				if((cargas.getItemAt(b) as OrderLoad).loadStatus=="CMPL" ||
					(cargas.getItemAt(b) as OrderLoad).loadStatus=="DELE"
					|| (cargas.getItemAt(b) as OrderLoad).loadStatus=="CNCL"
					|| dateone<datetwo 
					|| dateone>datefour){
					if(!(currentTransformer is ServiceAgentScreenDataTransformer)){
						nonVisilbleItems.put((cargas.getItemAt(b) as OrderLoad).orderNumber+":"+(cargas.getItemAt(b) as OrderLoad).itemNumber,(cargas.getItemAt(b) as OrderLoad).orderNumber+":"+(cargas.getItemAt(b) as OrderLoad).itemNumber)	
					}
				} else if(nonVisilbleItems.get((cargas.getItemAt(b) as OrderLoad).orderNumber+":"+(cargas.getItemAt(b) as OrderLoad).itemNumber)!=null) {
					nonVisilbleItems.remove((cargas.getItemAt(b) as OrderLoad).orderNumber+":"+(cargas.getItemAt(b) as OrderLoad).itemNumber);
				}
				//(cargas.getItemAt(b) as OrderLoad).comingFromPush=true;
				if(((cargas.getItemAt(b) as OrderLoad).loadNumber==null || (cargas.getItemAt(b) as OrderLoad).loadNumber=="") && (cargas.getItemAt(b) as OrderLoad).itemCategory!="ZTX0"){
					(cargas.getItemAt(b) as OrderLoad).loadNumber="L"+(b+1);
					(cargas.getItemAt(b) as OrderLoad).orderLoad=(cargas.getItemAt(b) as OrderLoad).orderNumber+"-"+(cargas.getItemAt(b) as OrderLoad).loadNumber;
				} else if(((cargas.getItemAt(b) as OrderLoad).loadNumber==null || (cargas.getItemAt(b) as OrderLoad).loadNumber=="") && (cargas.getItemAt(b) as OrderLoad).itemCategory=="ZTX0"){
					(cargas.getItemAt(b) as OrderLoad).loadNumber="B";
					(cargas.getItemAt(b) as OrderLoad).orderLoad=(cargas.getItemAt(b) as OrderLoad).orderNumber+"-"+(cargas.getItemAt(b) as OrderLoad).loadNumber;
				}
				var seekAndDestroyResult:Number=-1;
				var useTaskForce:Boolean=true;
				if(LoadsPerVehicleTaskRenderer.globalWorkingLoads[(cargas.getItemAt(b) as OrderLoad).orderNumber+":"+(cargas.getItemAt(b) as OrderLoad).itemNumber]!=null && currentTransformer is LoadPerVehiclePlantVehicleDataTransformer){
					/*verify the load has the same order status*/
					if((LoadsPerVehicleTaskRenderer.globalWorkingLoads[(cargas.getItemAt(b) as OrderLoad).orderNumber+":"+(cargas.getItemAt(b) as OrderLoad).itemNumber] as OrderLoad).loadStatus==(cargas.getItemAt(b) as OrderLoad).loadStatus && (LoadsPerVehicleTaskRenderer.globalWorkingLoads[(cargas.getItemAt(b) as OrderLoad).orderNumber+":"+(cargas.getItemAt(b) as OrderLoad).itemNumber] as OrderLoad).equipStatus==(cargas.getItemAt(b) as OrderLoad).equipStatus){
						useTaskForce=false;
						seekAndDestroyResult=0;
						var ggloads:Object=LoadsPerVehicleTaskRenderer.globalWorkingLoads;
						ReflectionHelper.copyParameters(cargas.getItemAt(b),LoadsPerVehicleTaskRenderer.globalWorkingLoads[(cargas.getItemAt(b) as OrderLoad).orderNumber+":"+(cargas.getItemAt(b) as OrderLoad).itemNumber]);
					}
				}
				if(useTaskForce){
					seekAndDestroyResult=seekAndDestroy((cargas.getItemAt(b) as OrderLoad),(cargas.getItemAt(b) as OrderLoad).plant,(cargas.getItemAt(b) as OrderLoad).equipStatus,(cargas.getItemAt(b) as OrderLoad).orderNumber);
				}
				
				var alg002:Date=DispatcherIslandImpl.fechaBase;
				var omitChange:Boolean=false;
				var listaPlantas:ArrayCollection=GanttServiceReference.getPlantsPlain();
				var isMyPlantHere:Boolean=false;
				for(var bb:int=0;bb<listaPlantas.length;bb++){
					if((cargas.getItemAt(b) as OrderLoad).plant==listaPlantas.getItemAt(bb).plantId){
						//se encontro la planta, se recomienda seguir
						isMyPlantHere=true;
					}
				}
				if((((cargas.getItemAt(b) as OrderLoad).itemCategory=="ZTX0" || (cargas.getItemAt(b) as OrderLoad).itemCategory=="ZRWN") && currentTransformer is LoadPerVehiclePlantVehicleDataTransformer) 
					|| seekAndDestroyResult==0 || seekAndDestroyResult==2 
					|| (((cargas.getItemAt(b) as OrderLoad).loadStatus=="ASSG" || (cargas.getItemAt(b) as OrderLoad).loadStatus=="TJST" || (cargas.getItemAt(b) as OrderLoad).loadStatus=="LDNG") && currentTransformer is LoadsPerOrderAbstractDataTransformer) 
					|| dateone<datetwo 
					|| dateone>datefour
					|| (GanttServiceReference.getParamString("DISP_PUMP_DFE")=="0" && ((cargas.getItemAt(b) as OrderLoad).itemCategory=="ZTX0" || (cargas.getItemAt(b) as OrderLoad).itemCategory=="ZRWN") && currentTransformer is LoadsPerOrderAbstractDataTransformer)
					|| ((cargas.getItemAt(b) as OrderLoad).equipStatus==ExtrasConstants.LOAD_UNASSIGNED && (currentTransformer is AssignedLoadsDataTransformer))
					|| ((cargas.getItemAt(b) as OrderLoad).loadStatus=="ASSG" && !(currentTransformer is LoadPerVehiclePlantVehicleDataTransformer))
					|| !isMyPlantHere
					|| ((cargas.getItemAt(b) as OrderLoad).loadStatus=="DELE")
					|| isRedirect(cargas.getItemAt(b) as OrderLoad)
					|| (visualConfigurations.plantConfigured!=null && (cargas.getItemAt(b) as OrderLoad).plant!=visualConfigurations.plantConfigured && visualConfigurations.showOnePlantAtATime)
					|| (Number((cargas.getItemAt(b) as OrderLoad).codStatusProdOrder)>=20 && currentView==TVARVCPreffixes.LOADS_PER_ORDER_VIEW_ID)
					/*|| (DispatcherIslandImpl.currentViewSelector=="LPO" && Number((cargas.getItemAt(b) as OrderLoad).codStatusProdOrder)>=20)*/){
					logger.debug("SALESORDER PUSH ERASE:"+(cargas.getItemAt(b) as OrderLoad).orderNumber+(cargas.getItemAt(b) as OrderLoad).loadNumber);
					logger.debug("REASON:"+seekAndDestroyResult+"  fecha es futura:"+((cargas.getItemAt(b) as OrderLoad).loadingDate.date!=DispatcherIslandImpl.pushBaseDate.date && (cargas.getItemAt(b) as OrderLoad).loadingDate.date!=getDayEnd().date));
					//nonVisilbleItems.put((cargas.getItemAt(b) as OrderLoad).orderNumber+":"+(cargas.getItemAt(b) as OrderLoad).itemNumber,(cargas.getItemAt(b) as OrderLoad).orderNumber+":"+(cargas.getItemAt(b) as OrderLoad).itemNumber)
					cargas.removeItemAt(b);
					omitChange=true;
					
				}
				if(!omitChange){
					for(var v:int=0;v<DispatcherViewMediator.vehiculosCorrectos.length;v++){
						//en realidad no hay q borrar nada, solo registrarlo en los encabezados
						if((cargas.getItemAt(b) as OrderLoad).equipNumber==DispatcherViewMediator.vehiculosCorrectos.getItemAt(v).equipNumber && DispatcherViewMediator.vehiculosCorrectos.getItemAt(v).equipStatus!=(cargas.getItemAt(b) as OrderLoad).equipStatus){
							logger.debug("SALESORDER PUSH VEHICLE ASSIGN:"+DispatcherViewMediator.vehiculosCorrectos.getItemAt(v).equipNumber+"   "+DispatcherViewMediator.vehiculosCorrectos.getItemAt(v).equipLabel);
							var clonVehiculo:Object=ReflectionHelper.cloneObject(DispatcherViewMediator.vehiculosCorrectos.getItemAt(v));
							
							seekAndDestroyVehicle(DispatcherViewMediator.vehiculosCorrectos.getItemAt(v),null,(cargas.getItemAt(b) as OrderLoad).equipLabel);
							clonVehiculo.equipStatus=(cargas.getItemAt(b) as OrderLoad).equipStatus;
							clonVehiculo.maintplant=(cargas.getItemAt(b) as OrderLoad).plant;
							clonVehiculo.plant=(cargas.getItemAt(b) as OrderLoad).plant;
							clonVehiculo.plantId=(cargas.getItemAt(b) as OrderLoad).plant;
							currentTransformer.addHeader(clonVehiculo);
						}
					}
				}
				//(cargas.getItemAt(b) as OrderLoad).comingFromPush=true;
				//antes de mandar las nuevas cargas comprobar que los vehiculos esten marcados como asignados
				
				/*else {
				//this.seekAndDestroy(cargas.getItemAt(b),(cargas.getItemAt(0) as OrderLoad).plant,(cargas.getItemAt(0) as OrderLoad).equipStatus,(cargas.getItemAt(0) as OrderLoad).orderNumber);
				}*/
				//} 
			}
			logger.debug("SALESORDER CARGAS PASAN:"+cargas.length);
			addDatas(cargas,GanttTask.STEP_MOVED);
			//actualizar los titulos de los elementos
			if(cargas.length==0){
				currentTransformer.sortView();
			}
			DispatcherViewMediator.isUsingPush=true;
			currentTransformer.timeChanged(TimeZoneHelper.getServer());
			abstractUpdateByTimerPush();
			
		}
		private function updateByTimerPush(evt:TimerEvent):void{
			DispatcherViewMediator.isUsingPush=true;
			
			currentTransformer.addHeaders(DispatcherViewMediator.vehiculosCorrectos);
			currentTransformer.timeChanged(TimeZoneHelper.getServer());
			view.gantt.ganttSheet.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
			if(currentTransformer is ServiceAgentScreenDataTransformer || currentTransformer is PlanningScreenDataTransformer){
				/*				view.gantt.ganttSheet.showAll();
				view.vehicleScheduleView.ganttSheet.showAll();*/
				view.gantt.ganttSheet.visibleTimeRangeStart=FlexDateHelper.addSeconds(view.vehicleScheduleView.ganttSheet.visibleTimeRangeStart,3);
				view.vehicleScheduleView.ganttSheet.visibleTimeRangeStart=FlexDateHelper.addSeconds(view.vehicleScheduleView.ganttSheet.visibleTimeRangeStart,3);
			}
			disableStack(true);
			
		}
		private var tcTimer:Timer;
		
		private function seekAndDestroyEmpty():void{
			var dataCursor:IHierarchicalCollectionViewCursor = view.gantt.dataGridBack.dataProvider.createCursor();
			while (dataCursor.current != null)
			{
				dataCursor.moveNext();
			}
		}
		private function seekAndDestroyVehicle(equipo:Object,planta:String=null,equipLabel:String=null):int{
			var dataCursor:IHierarchicalCollectionViewCursor = view.gantt.dataGridBack.dataProvider.createCursor();
			var regresa:int=0;
			var alg:*=DispatcherViewMediator.vehiculosCorrectos;
			
			
			if(equipo.status=="ASSN" || equipo.status=="AVLB" || equipo.status=="ATPL"){
				/*
				if context key is the same as the new key, then there will not be changes
				but if they are different, then, changes apply
				*/
				var llaves:ArrayCollection=currentTransformer.getTree().ctx.getAvailableKeys();
				var vehicleFound:String="";
				for(var c:int=llaves.length-1;c>-1;c--){
					if(llaves.getItemAt(c).toString().indexOf(equipo.equipNumber)!=-1){
						var childNode:GenericNode=currentTransformer.getTree().ctx.get(llaves.getItemAt(c).toString());
						var childParent:ArrayCollection=childNode.getParent()["_children"];
						//buscar las cargas asignadas porque este vehiculo acaba de liberarse y liberar sus cargas
						for(var qi:int=childNode["_children"].length-1;qi>-1;qi--){
							childNode["_children"].getItemAt(qi)["payload"].workAroundDestroy="DELE";
							childNode["_children"].getItemAt(qi)["data"].workAroundDestroy="DELE";
							//and erase the loads inside
							(childNode["_children"] as ArrayCollection).removeItemAt(qi);
						}
						//equipo.maintplant+":"+equipo.status+":"+equipo.equipNumber
						vehicleFound=equipo.equipNumber;
						//look for the vehicle in the parent
						for(var q:int=childParent.length-1;q>-1;q--){
							if(childParent.getItemAt(q)==childNode){
								/*hay que verificar que se pueda borrar usando la lista interna
								*/
								var invokeError=0;
								
								if(childParent.list.length>q){
									try {
										if (invokeError==1){
											childParent.removeItemAt(1000);//<<<-----error detectado
										} else {
											childParent.removeItemAt(q);//<<<-----error detectado
										}
									} catch(e){
										wmsg=new WarningPopUp();
										view.addChild(wmsg);
										wmsg.y=35;
									}
								} else {
									var v:Number=0;
									//poner mensaje de alerta
									/*wmsg=new WarningPopUp();
									view.addChild(wmsg);
									wmsg.y=30;*/
								}
							}
						}
						currentTransformer.getTree().ctx.remove(llaves.getItemAt(c).toString());
					}
				}
				if(vehicleFound!="" || equipo.equipNumber!=""){
					for(var d:int=DispatcherViewMediator.vehiculosCorrectos.length-1;d>-1;d--){
						if(DispatcherViewMediator.vehiculosCorrectos.getItemAt(d)["equipNumber"]==vehicleFound || DispatcherViewMediator.vehiculosCorrectos.getItemAt(d)["equipNumber"]==equipo.equipNumber){
							DispatcherViewMediator.vehiculosCorrectos.removeItemAt(d);
						}
					}
				}
			}
			
			
			var eraseItem:ArrayCollection=currentTransformer.getResourcesTree();
			/*
			If the tree is probably contracted, the vehicle will not be seeing, for this case
			lets search in the context
			*/
			while (dataCursor.current != null)
			{
				if(dataCursor.current["label"].toString()==ExtrasConstants.VEHICLE_DISPONIBLE){
					
				}
				if(dataCursor.current["label"].toString().indexOf(equipLabel)!=-1){
					currentTransformer.getTree().ctx.remove(dataCursor.current.payload["plant"]+":"+dataCursor.current.payload["equipStatus"]+":"+equipo.equipNumber);
					//comparar contra la planta, si NO esta en la misma planta o su estatus es DIFERENTE
					if((dataCursor.current as LoadsPerVehicleResource).id!=equipo.maintplant+":"+equipo.status+":"+equipo.equipNumber){
						//buscarlo y quitarlo de los vehiculos correctos
						
						
						view.gantt.dataGridBack.dataProvider.removeChild( view.gantt.dataGridBack.dataProvider.getParentItem(dataCursor.current), dataCursor.current );
						//buscar y destruirlo del mapa interno
					}
				}
				dataCursor.moveNext();
			}
			return regresa;
		}
		/**
		 * Regresa 0 cuando el item/carga se encuentra ya creado y solo se actualizaron sus datos-done
		 * regresa 1 cuando el item se encuentra ya creado pero en un nivel diferente, eso es, se borra del nivel anterior
		 * regresa 2 cuando el item se encuentra en una planta que no esta en la vista o que se cancelo /borrado completo
		 * regresa 3 cuando el item no se encontro-done
		 * */
		var dumCo:int=0;
		private function seekAndDestroy(item:OrderLoad,planta:String=null,equip:String=null,lablToErase:String=null,transformerToUpdate:IDataTransformer=null):Number{
			//buscar el dataprovider
			var workingDataTransformer:IDataTransformer=this.currentTransformer;
			if(transformerToUpdate!=null){
				workingDataTransformer=transformerToUpdate;
			}
			var regresa:Number=-1;
			var isPlantPresent:Boolean=false;
			var listaPlantas:ArrayCollection=GanttServiceReference.getPlantsPlain();
			for(var b:int=0;b<listaPlantas.length;b++){
				if(item.plant==listaPlantas.getItemAt(b).plantId){
					//se encontro la planta, se recomienda seguir
					regresa=3;
					isPlantPresent=true;
				}
			}
			if(regresa==-1){
				//no se encontro en las cargas, asi q hay q borrarlo
				regresa=2;
			}
			var currentListObject:Object;
			var currentListIndex:int=0;
			var dataCursor:IHierarchicalCollectionViewCursor=null;
			try{
				dataCursor = view.gantt.dataGridBack.dataProvider.createCursor();
			} catch(e:Error){
				dataCursor=null;
				logger.debug("ERROR ON DATACURSOR:"+planta+":"+equip+":"+lablToErase);
			}
			if(dataCursor==null){
				return regresa;
			}
			var alg001:Boolean=false;
			while (currentListIndex<workingDataTransformer.getTree().ctx.getAvailableKeys().length/*dataCursor.current != null*/)
			{
				dumCo++;
				trace(dumCo);
				currentListObject=workingDataTransformer.getTree().ctx.get(workingDataTransformer.getTree().ctx.getAvailableKeys().getItemAt(currentListIndex).toString());
				var fullID:String=String(planta+":"+equip+":"+lablToErase);
				var shortID:String=String(planta+":"+lablToErase);
				
				if(workingDataTransformer is LoadsPerOrderAbstractDataTransformer){
					if(this.isPerHour){
						fullID=String(equip+":"+lablToErase);
						shortID=String(lablToErase);
					}else{
						fullID=String(planta+":"+equip+":"+lablToErase);
						shortID=String(planta+":"+lablToErase);
					}
				} else if(workingDataTransformer is LoadPerVehiclePlantVehicleDataTransformer){
					fullID=String(planta+":"+equip+":"+item.equipNumber);
					shortID=String(planta+":"+equip+":"+lablToErase);
				} else if(workingDataTransformer is AssignedLoadsDataTransformer){
					fullID=String(planta)+":"+item.loadNumber;
					shortID=String(planta)+":"+item.loadNumber;
				} else if(workingDataTransformer is PlanningScreenVehicleDataTransformer){
					fullID=item.equipNumber;
					shortID=item.equipNumber;
				} else if(workingDataTransformer is ServiceAgentScreenDataTransformer){
					fullID=String(planta)+":"+item.orderNumber;
					shortID=String(planta)+":"+item.orderNumber;
				}
				var dateone:Number=item.loadingTimeStampFix().time;
				var datetwo:Number=getDayStart().time;
				var datethree:Number=getDayEnd().time;
				//when a vehicle has more than one charge, we need to remove every single one identifying the view
				if(/*dataCursor.current*/currentListObject is LoadsPerVehicleResource){
					if(currentListObject.labelField=="equipNumber"){
						// buscar en los hijos y ver si ya no esta asignado
						var truckChildren:ArrayCollection=currentListObject._children;
						for(var cin:int=truckChildren.length-1;cin>-1;cin--){
							if(((truckChildren.getItemAt(cin).payload as OrderLoad).orderNumber==item.orderNumber && (truckChildren.getItemAt(cin).payload as OrderLoad).itemNumber==item.itemNumber) && (item.equipNumber=="" || (item.codStatusProdOrder=="80" && item.itemCategory=="ZTC1" && item.loadStatus=="CMPL"))){
								//borrar
								updateContext(item,currentListObject["equipNumber"],true,workingDataTransformer);
								regresa=1;
							}
						}
					}
					var vehi:ArrayCollection=DispatcherViewMediator.vehiculosCorrectos;
				}
				if((currentListObject["id"]/*dataCursor.current["id"]*/==fullID || currentListObject["id"]/*dataCursor.current["id"]*/==shortID) 
					&& (item.loadStatus!="CNCL" && item.orderStatus!="CNCO") 
					&& (dateone>=datetwo && dateone<=datethree)){
					regresa=0;
					//actualizar en dataCursor.current["payload"]
					
					//si son el mismo pegar valores y actualizar el current transformer y currenttree
					//buscar en sus children
					var childs:ArrayCollection=(currentListObject/*dataCursor.current*/ as GanttResource).getChildren();
					//in order to get updated the sort function
					if(childs!=null){
						for(var q:int=0;q<childs.length;q++){
							//com.cemex.rms.common.gantt
							if(childs.getItemAt(q) is GanttTask){
								var it:GanttTask=childs.getItemAt(q) as GanttTask;
								if((item.itemNumber==(it["payload"] as OrderLoad).itemNumber || item.loadNumber==(it["payload"] as OrderLoad).loadNumber) && item.orderNumber==(it["payload"] as OrderLoad).orderNumber){
									//to do apply erase changes
									//borrar el cambio pues puede ocacionar errores
									
									//currentTransformer.getTre(e).ctx.remove(dataCursor.current["id"]);
									alg001=true;
									var cumsFromPush:Boolean=false;
									var expandedIndex:int=(it["payload"] as OrderLoad).indexPosition;
									eraseSpecialCaseContext(item,workingDataTransformer);
									if((it["payload"] as OrderLoad).loadingTime!=item.loadingTime){
										cumsFromPush=true;
										
									}
									ReflectionHelper.copyParameters(item,it["payload"]);
									it.data=item;
									(it["payload"] as OrderLoad).indexPosition=expandedIndex;
									if(cumsFromPush){
										item.comingFromPush=true;
										(it["payload"] as OrderLoad).comingFromPush=true;
									}
									//sale un error aqui hay que debuggear
									try{
										(dataCursor.current as GanttResource).minDateNumber=item._numericDate;
									} catch(e){
										trace("ATENCION: UN ERROR inner en SEEKandDestroy");
									}
								}
							} else if(childs.getItemAt(q) is GanttResource){
								var its:GanttResource=childs.getItemAt(q) as GanttResource;
								if(item.itemNumber==its["payload"].itemNumber || item.loadNumber==its["payload"].loadNumber){
									//currentTransformer.getTree().ctx.remove(dataCursor.current["id"]);
									alg001=true;
									eraseSpecialCaseContext(item,workingDataTransformer);
									ReflectionHelper.copyParameters(item,its["payload"]);
									its.data=item;
									if(its["payload"].loadingTime!=item.loadingTime){
										item.comingFromPush=true;
										its["payload"].comingFromPush=true;
										
									}
								}
								(currentListObject/*dataCursor.current*/ as GanttResource).minDateNumber=item._numericDate;
							}
							ReflectionHelper.copyParameters(item,currentListObject["payload"]/*dataCursor.current["payload"]*/);
						}
					}
					if(!alg001){
						//no se encontro y comunicamos que se agregue
						regresa=3;
					}
					
				} else if(currentListObject/*dataCursor.current*/["label"].toString().indexOf(lablToErase)!=-1 || !(dateone>=datetwo && dateone<=datethree)){
					//se encuentra en una planta que no tenemos pero que antes estuvo aqui
					regresa=2;
					if((item.loadStatus=="ASSG" || item.loadStatus=="LDNG" || item.loadStatus=="TJST") && (workingDataTransformer is AssignedLoadsDataTransformer)){
						return regresa;
					}
					
					if(isPlantPresent){
						regresa=1;
					}
					
					var isItemPresent:int=0;
					//necesitamos ver si en efecto fue un cambio de planta, qizas es una partida viviendo en otra planta
					//primero buscamos si existe esa otra partida en la otra planta, si existe ya llegaremos a ella
					//si no existe fue un cambio de planta y hay que borrarla de donde la encontremos
					if(!(dateone>=datetwo && dateone<=datethree)){
						isItemPresent=updateContext(item,"?",true,workingDataTransformer);
					} else {
						isItemPresent=updateContext(item,"?",false,workingDataTransformer);
					}
					
					if(isItemPresent==0){
						//ya no se encontro el elemento
						//si no se encuentra pero tiene estatus asignado o TJST, significa que lo cambiaron de vehiculo
						if(item.loadStatus=="ASSG" || item.loadStatus=="TJST"){
							//regresa 1 que es presente en planta, pongamoslo
							regresa=1;
							if(alg001){
								regresa=0;
							}
						} else {
							regresa=0;
						}
					} else if(isItemPresent==1) {
						regresa=0;
						/*if(isPlantPresent){
							regresa=1;
						}*/
					} else if(isItemPresent==2){
						regresa=3;
					}
					if(item.orderStatus=="CNCO" || item.orderStatus=="CMPO" || (!isPlantPresent && currentListObject._children.length<1)){
						regresa=2;
						//buscar las llaves
						
						
						//LA SIGUIENTE LINEA BORRA TODO UN PEDIDO, se reepondran al llegar las cargas del pedido
						try{
							
							var childs:ArrayCollection=(currentListObject/*dataCursor.current*/ as GanttResource).getChildren();
							if(childs!=null){
								for(var q:int=0;q<childs.length;q++){
									//com.cemex.rms.common.gantt 
									var it:GanttTask=childs.getItemAt(q) as GanttTask;
									this.lastOptimalCost=this.lastOptimalCost-(it["payload"] as OrderLoad).mepTotcost;
									this.lastCostFormat=this.lastCostFormat-(it["payload"] as OrderLoad).totCost;
									//currentTransformer.getTree().ctx.remove((it["payload"] as OrderLoad).plant+":"+(it["payload"] as OrderLoad).equipStatus+":"+item.orderNumber);
									//ReflectionHelper.copyParameters(item,dataCursor.current["payload"]);
								}
							}
							//no borramos los de cargas asignadas
							
							if(!(workingDataTransformer is AssignedLoadsDataTransformer)){
								/*antes de borrar hay que asegurarnos que efectivamente el item que deseamos borrar es el mismo que esta dentro del vehiculo*/
								if(currentListObject.hasOwnProperty("_children")){
									var m2children:ArrayCollection=currentListObject._children;
									for(var df:int=0;df<m2children.length;df++){
										if((m2children.getItemAt(df).data as OrderLoad).orderLoad.indexOf(item.orderLoad)!=-1){
											view.gantt.dataGridBack.dataProvider.removeChild( view.gantt.dataGridBack.dataProvider.getParentItem(currentListObject/*dataCursor.current*/), currentListObject/*dataCursor.current*/ );		
										}
									}
								} else {
									view.gantt.dataGridBack.dataProvider.removeChild( view.gantt.dataGridBack.dataProvider.getParentItem(currentListObject/*dataCursor.current*/), currentListObject/*dataCursor.current*/ );
								}
								logger.debug("SEEKANDDESTROY:"+planta+":"+equip+":"+lablToErase);
							} else {
								currentListObject./*dataCursor.current.*/children.removeItemAt(0);
							}
							//si se borra quitamos su costo
						} catch(e2:Error){
							
							logger.debug("ERROR ON SEEKANDDESTROY:"+e2.message);
						}
					}
				}
				currentListIndex++;
				//dataCursor.moveNext();
			}
			
			//dataProvider.refresh();
			if (isSameAsBaseDate(TimeZoneHelper.getServer()) ){
				move2CurrentTime(false);
			}
			
			
			if((item.loadStatus=="CMPL" || item.loadStatus=="CNCL" || item.orderStatus=="CNCO" || item.orderStatus=="CMPO" || (item.orderStatus=="" && item.loadStatus=="")) && regresa==3){
				regresa=2;
			}
			//antes de continuar verificar que no este en el contexto
			
			return regresa;
		}
		public function eraseSpecialCaseContext(item:Object,workingTransformer:IDataTransformer):void{
			if(((item as OrderLoad).loadStatus=="ASSG"
				|| (item as OrderLoad).loadStatus=="TJST"
				|| (item as OrderLoad).loadStatus=="LDNG"
				|| (item as OrderLoad).loadStatus=="DELE"
				|| isRedirect(item as OrderLoad))
				&& workingTransformer is LoadsPerOrderAbstractDataTransformer){
				updateContext(item,"?",true,workingTransformer);	
			} else if(((item as OrderLoad).loadStatus=="CMPL" && currentView!=TVARVCPreffixes.SERVICE_AGENT_SCREEN_VIEW_ID)
				|| isRedirect(item as OrderLoad)){
				updateContext(item,"?",true,workingTransformer);
			}
		}
		public function updateContext(item:Object,numEquipoDummy:String="?",erase:Boolean=true,workingTransformer:IDataTransformer=null):int{
			if(workingTransformer==null){
				workingTransformer=currentTransformer;
			}
			var llaves:ArrayCollection=workingTransformer.getTree().ctx.getAvailableKeys();
			var isItemPresent:int=0;
			for(var n:int=0;n<llaves.length;n++){
				//si encontramos el numero de pedido buscamos a quien debemos borrar
				if(llaves.getItemAt(n).indexOf(item.orderNumber)!=-1 || llaves.getItemAt(n).indexOf(numEquipoDummy)!=-1){
					//a) buscar el elemento a nivel de orden y quitarlo del nivel de orden
					var resourceObjectinit:Object=workingTransformer.getTree().ctx.get(llaves.getItemAt(n).toString());
					for(var ch:int=resourceObjectinit._children.length-1;ch>-1;ch--){
						if(resourceObjectinit._children.getItemAt(ch).payload!=null){
							
							if((resourceObjectinit._children.getItemAt(ch).payload as OrderLoad).itemNumber.indexOf(item.itemNumber)!=-1 && (resourceObjectinit._children.getItemAt(ch).payload as OrderLoad).orderNumber.indexOf(item.orderNumber)!=-1){
								//quitarlo del nivel de la orden
								if(!erase){
									if((resourceObjectinit._children.getItemAt(ch).payload as OrderLoad).plant!=item.plant || (resourceObjectinit._children.getItemAt(ch).payload as OrderLoad).equipStatus!=item.equipStatus){
										(resourceObjectinit._children.getItemAt(ch).payload as OrderLoad).workAroundDestroy="DELE";
										//fue un cambio de planta y por tal motivo, hay que eliminarlo
										resourceObjectinit._children.removeItemAt(ch);
										isItemPresent=2;
									} else {
										isItemPresent=0;
										
									}
								} else {
									resourceObjectinit._children.removeItemAt(ch);
									isItemPresent=1;
									
								}
								//regresa=0;
								if(resourceObjectinit.hasOwnProperty("totalChildrenCount")){
									resourceObjectinit.totalChildrenCount=resourceObjectinit._children.length;
								}
							}
						}
						
					}
					if(resourceObjectinit._children.length==0 && workingTransformer!=secondCurrentTransformer){
						//borrar todo el elemento
						
						//b) buscarlo en el padre y borrarlo del padre
						var idString:String=llaves.getItemAt(n).substr(0,String(llaves.getItemAt(n)).lastIndexOf(":"));
						var resourceObject:Object=workingTransformer.getTree().ctx.get(idString);
						if(resourceObject!=null){
							for(var ch:int=resourceObject._children.length-1;ch>-1;ch--){
								//commented ItemNumber to allow erasing the parent node
								if(resourceObject._children.getItemAt(ch).payload!=null){
									
									var obj:Object=resourceObject._children.getItemAt(ch);
									if(resourceObject._children.getItemAt(ch).payload.orderNumber==item.orderNumber && !(workingTransformer is AssignedLoadsDataTransformer)/* && resourceObject._children.getItemAt(ch).payload.itemNumber==item.itemNumber*/){
										resourceObject._children.removeItemAt(ch);
										if(numEquipoDummy!="?"){
											//search the vehicle and reassign
											for(var e:int=DispatcherViewMediator.vehiculosCorrectos.length-1;e>-1;e--){
												if(DispatcherViewMediator.vehiculosCorrectos.getItemAt(e).equipNumber==numEquipoDummy){
													DispatcherViewMediator.vehiculosCorrectos.getItemAt(e).equipStatus=ExtrasConstants.VEHICLE_DISPONIBLE;
												}
											}
										}
									} else if(obj.payload.hasOwnProperty("orderLoad")){
										if(String(obj.payload["orderLoad"]).indexOf(item.orderLoad)!=-1
											&& (workingTransformer is AssignedLoadsDataTransformer)){
											resourceObject._children.removeItemAt(ch);
										}
									}
								}
								//regresa=0;
							}
						}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
						workingTransformer.getTree().ctx.remove(llaves.getItemAt(n).toString());
					}
				}
			}
			return isItemPresent;
		}
		
		public function gotPushFromVehicle(e:VehicleLCDSEvent):void{
			DispatcherViewMediator.isUsingPush=true;
			var thisDatetmp:Date=new Date();
			vehicleLastDate=thisDatetmp.date+"/"+thisDatetmp.month+"/"+thisDatetmp.fullYear+"  "+thisDatetmp.hours+":"+thisDatetmp.minutes+":"+thisDatetmp.seconds
			vehiclePushCounter+=1;
			updatePushPanel();
			if(Logger.inDebug){
				Logger.pushVehicleCounter++;
				//view.gotPush.text="PUSH VEHICLE:"+e.vehicle//((new Date().minutes)+""+(new Date().seconds))+"/"+Logger.pushPlantOperativeCounter+"/"+Logger.pushProduction+"/"+Logger.pushSalesOrderCounter+"/"+Logger.pushVehicleCounter;
				Logger.incomingPush("Vehicle push",e.vehicle.equipment,this.view)
			}
			
			var equipment:Equipment = e.vehicle;
			//ignore if the equiment is dummy
			if(equipment.fleetNum==GanttServiceReference.getTVARVCParam("EQ_DUMMY").low){
				return;
			}
			var seekanddestroy:int=this.seekAndDestroyVehicle(equipment,null,equipment.equipLabel);
			//ignore if the 
			if(equipment.status=="AVLB" || equipment.status=="ASSN"){
				
				//buscar vehiculo y borrarlo D136:Asignadas:000000000011004264
				//this.seekAndDestroy(null,equipment.maintplant,equipment.equipLabel);
				if (GanttServiceReference.getPlantInfo(equipment.maintplant) != null){
					
					if (currentView == TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID){
						
						/*var plain:Object = GanttServiceReference.updateEquipment(equipment);
						currentTransformer.addHeader(plain);*/
						
						//DispatcherViewMediator.vehiculosCorrectos.addItem(plain);
						//currentTransformer.addHeader(equipment);
						/*view.gantt.dataGridBack.invalidateDisplayList();
						view.gantt.dataGridBack.invalidateList();*/
						//var vehicle:Equipment = GanttServiceReference.removeVehicle(equipment);
						//currentTransformer.deleteHeader(vehicle);
						
					}
				}
			} 
			var plain:Object=new Object();
			plain["plant"] = equipment.maintplant;
			plain["plantId"]=equipment.maintplant;
			ReflectionHelper.copySimpleParameters(equipment,plain);
			//buscar si tenemos la planta
			if(GanttServiceReference.getPlantInfo(equipment.maintplant) ==null){
				return;
			}
			if((equipment.status=="AVLB" || equipment.status=="ASSN") && (seekanddestroy==0 || seekanddestroy==3)){
				/*var plain:Object = GanttServiceReference.updateEquipment(equipment);
				currentTransformer.addHeader(plain);*/
				if((equipment.maintplant==visualConfigurations.plantConfigured && visualConfigurations.plantConfigured!=null) || visualConfigurations.plantConfigured==null){
					DispatcherViewMediator.vehiculosCorrectos.addItem(plain);
					
				}
			}
			currentTransformer.addHeaders(DispatcherViewMediator.vehiculosCorrectos);
			abstractUpdateByTimerPush();
			/**/
		}
		
		private function abstractUpdateByTimerPush():void{
			if(tcTimer==null){
				tcTimer=new Timer(2000,1);
				tcTimer.addEventListener(TimerEvent.TIMER_COMPLETE,updateByTimerPush);
			}
			tcTimer.delay=2000;
			tcTimer.reset();
			tcTimer.stop();
			tcTimer.start();
			
		}
		public function getFakeProduction():ProductionData{
			var prod:ProductionData=new ProductionData();
			prod.aufnr="11223344";
			prod.vbeln="0000147871";
			prod.posnr=1001;
			prod.stonr="20";
			return prod;
		}
		public function gotPushFromProductionStatus(e:ProductionStatusLCDSEvent):void{
			productionPushCounter+=1;
			var thisDatetmp:Date=new Date();
			productionPushCounter=productionPushCounter+1;
			productionLastDate=thisDatetmp.date+"/"+thisDatetmp.month+"/"+thisDatetmp.fullYear+"  "+thisDatetmp.hours+":"+thisDatetmp.minutes+":"+thisDatetmp.seconds
			updatePushPanel();
			DispatcherViewMediator.isUsingPush=true;
			if(Logger.inDebug){
				Logger.pushProduction++;
				//view.gotPush.text="PUSH PRODUCTION:"+e.productionData;//((new Date().minutes)+""+(new Date().seconds))+"/"+Logger.pushPlantOperativeCounter+"/"+Logger.pushProduction+"/"+Logger.pushSalesOrderCounter+"/"+Logger.pushVehicleCounter;
				Logger.incomingPush("Production push",e.productionData.aufnr,this.view)
			}
			if (currentTransformer is LoadPerVehiclePlantVehicleDataTransformer || currentTransformer is ServiceAgentScreenDataTransformer) {
				var productionData:ProductionData;
				if (e==null){
					productionData = new ProductionData();
					productionData.aufnr = "000065215280";
					productionData.stonr = "70";
				}else{
					productionData = e.productionData;
				}
				
				productionData.txt04=translateProductionStatus(productionData.stonr);
				var task:GanttTask = currentTransformer.getTask (productionData);
				var tasks:Array=currentTransformer.getTasks(productionData);
				//identificamos el estatus y aplicamos el cambio de color
				/*
				No          Stat        Text MORADO
				
				40           QUEU   QUEUED
				
				50           INPR      IN PRODUCTION
				
				60           PCNF     PARTIAL CONFIRMATION
				
				80           CNF       FINAL CONFIRMATION
				
				No          Stat        Text Rojo
				
				70           STOP     STOP PROD ORDER
				*/
				
				
				GanttServiceReference.setProductionData(productionData);
				view.gantt.dataGridBack.invalidateDisplayList();
				view.gantt.dataGridBack.invalidateList();
				
				if(task==null){
					return;
				}
				if(productionData.txt04!="200" && productionData.txt04!="100"){
					(task.data as OrderLoad).txtStatusProdOrder=productionData.txt04;
					(task.data as OrderLoad).prodOrder=productionData.aufnr;
					(task.data as OrderLoad).codStatusProdOrder=productionData.stonr;
					//se recorren todas las cargas cuando es multidelivery
					for(var taskc:int=0;taskc<tasks.length;taskc++){
						tasks[taskc].data.txtStatusProdOrder=productionData.txt04;
						tasks[taskc].data.prodOrder=productionData.aufnr;
						tasks[taskc].data.codStatusProdOrder=productionData.stonr;
						if(productionData.stonr=="20"){
							tasks[taskc].data.loadStatus="ASSG";
						}
						if(productionData.stonr=="70"){
							//solo si es 70 le cambiamos el visible al resto
							for(var ctareas:int=0;ctareas<currentTransformer.getTasksTree().length;ctareas++){
								if(currentTransformer.getTasksTree().getItemAt(ctareas).data.orderNumber==productionData.vbeln){
									currentTransformer.getTasksTree().getItemAt(ctareas).data.visible="c";
									currentTransformer.getTasksTree().getItemAt(ctareas).data.loadStatus="ASSG";
								}
							}
							tasks[taskc].data.visible="B";
							
						}
					}
				}
				switch(Number(productionData.stonr)){
					case 70:
						for(var taskc:int=0;taskc<tasks.length;taskc++){
							tasks[taskc].data.pborderColor="0XFD675C";
							tasks[taskc].data.pfillColor="0XFD675C";
						}
						(task.data as OrderLoad).pborderColor="0XFD675C";
						(task.data as OrderLoad).pfillColor="0XFD675C";
						break;
					case 30:
					case 40:
					case 50:
					case 60:
						for(var taskc:int=0;taskc<tasks.length;taskc++){
							tasks[taskc].data.pborderColor="0XEDDAF0";
							tasks[taskc].data.pfillColor="0XEDDAF0";
							tasks[taskc].data.loadStatus="LDNG";
						}
						(task.data as OrderLoad).pfillColor="0XEDDAF0";
						(task.data as OrderLoad).pborderColor="0XEDDAF0";
						(task.data as OrderLoad).loadStatus="LDNG";
						break;
					case 80:
						for(var taskc:int=0;taskc<tasks.length;taskc++){
							tasks[taskc].data.pborderColor="0XF5F67E";
							tasks[taskc].data.pfillColor="0XAAEEA3";
						}
						(task.data as OrderLoad).pfillColor="0XAAEEA3";
						(task.data as OrderLoad).pborderColor="0XF5F67E";
						break;
					case 20:
						for(var taskc:int=0;taskc<tasks.length;taskc++){
							tasks[taskc].data.pborderColor="0XC4F9C4";
							tasks[taskc].data.pfillColor="0XC4F9C4";
						}
						(task.data as OrderLoad).pfillColor="0XC4F9C4";
						(task.data as OrderLoad).pborderColor="0XC4F9C4";
						break;
				}
				if (isSameAsBaseDate(TimeZoneHelper.getServer()) ){
					DispatcherViewMediator.isUsingPush=true;
					currentTransformer.timeChanged(TimeZoneHelper.getServer());
				}
			}
			currentTransformer.sortView();
			DispatcherViewMediator.isUsingPush=true;
			currentTransformer.timeChanged(TimeZoneHelper.getServer());
			abstractUpdateByTimerPush();			
			
		}
		
		
		public function translateProductionStatus(STONR:String):String{
			if(STONR==null){
				return "";
			} else if(STONR==""){
				return "";
			}
			
			var translation:String="";
			if(DispatcherIslandImpl.userCountry!="MX"){
				//traducir
				switch(STONR){
					case "10":
						translation="CRTD";
						break;
					case "20":
						translation="REL";
						break;
					case "30":
						translation="SENT";
						break;
					case "40":
						translation="QUEU";
						break;
					case "50":
						translation="INPR";
						break;
					case "60":
						translation="PCNF";
						break;
					case "70":
						translation="STOP";
						break;
					case "80":
						translation="CNF";
						break;
				}
			} else {
				switch(STONR){
					case "10":
						translation="ABIE";
						break;
					case "20":
						translation="LIB.";
						break;
					case "30":
						translation="ENVI";
						break;
					case "40":
						translation="ENCO";
						break;
					case "50":
						translation="ENPR";
						break;
					case "60":
						translation="NOTP";
						break;
					case "70":
						translation="PARO";
						break;
					case "80":
						translation="NOTI";
						break;
				}
			}
			return translation;
		}
		/**
		 * Esta funcion se manda allamar cuando los datos que llegan del Flash Island tienene que sobre escribir todos los datos
		 * 
		 * las siguientes variables controlan las pantallas para que no se regresen, no se muevan y permitan visualizar
		 */
		private var tmpErase:FlashIslandsDataEvent
		private var tmpEraseItemPlanningScreen:Array=[];
		private var alreadyLoaded:Boolean=false;
		public function islandDataEventHandler(e:FlashIslandsDataEvent):void{
			//do translations
			if(wmsg!=null){
				view.removeChild(wmsg);
				wmsg=null;
			}
			tmpErase=e;
			nonVisilbleItems=new DictionaryMap();
			LoadsPerVehicleTaskRenderer.globalWorkingLoads=new Dictionary();
			if(DispatcherIslandImpl.currentSelector!=AbstractReceiverService.SQL_SELECTOR){
				AbstractReceiverService.SQL_SELECTOR=DispatcherIslandImpl.currentSelector;
				dispatch(new LCDSReconnectEvent());
			}
			if(view.viewStack.numChildren==2 && !alreadyLoaded){
				view.viewStack.selectedIndex=0;
				currentView=TVARVCPreffixes.SERVICE_AGENT_SCREEN_VIEW_ID;
				DispatcherIslandImpl.currentViewSelector="LSA";
				alreadyLoaded=true;
			}
			ServiceAgentScreenTask.franceTaskProcessed=[];
			ServiceAgentScreenTask.franceTaskAcomodationRows=new DictionaryMap();
			PlanningScreenTask.franceTaskProcessed=[];
			PlanningScreenTask.franceTaskProcessedAux=[];
			PlanningScreenTask.singlePositions=new DictionaryMap();
			PlanningScreenTreeResourceRenderer.mixerOrConveyor=new DictionaryMap();
			view.cboPlantSelector.dataProvider = null;
			var arrPlantsTMP_plain:ArrayCollection = GanttServiceReference.getPlantsPlain();
			view.cboPlantSelector.dataProvider = arrPlantsTMP_plain;
			PlanningScreenTask.franceTaskAcomodationRows=new DictionaryMap();
			PlanningScreenTask.franceTaskAcomodationRowsAux=new DictionaryMap();
			setPlantSelected();
			view.cleanMsg.label=getLabel(OTRConstants.DISP_CLEAN);
			view.plantSelectorLbl.text=getLabel(OTRConstants.DISP_PS_PLANNING_PLANT);
			view.removeItems.label=getLabel(OTRConstants.DISP_PS_CLEAR_PLAN);
			view.saveVisualChanges.label=getLabel(OTRConstants.DISP_PS_SAVE_PLAN)
			var availableKeys:ArrayCollection=LoadPerOrderPlantOrderResourceRenderer.expandableItems.getAvailableKeys();
			for(var len:int=availableKeys.length-1;len>-1;len--){
				LoadPerOrderPlantOrderResourceRenderer.expandableItems.remove(availableKeys.getItemAt(len).toString());
			}
			logger.info("islandDataEventHandler() init");
			/*if(!DispatcherIslandImpl.masterKey){
			return;
			}*/
			DispatcherIslandImpl.masterKey=false;
			setBaseDate(e.data.baseDate);
			//changeViews(currentView);
			// Set the CurrentTransformer
			updateTransformers();
			/*if(pauseLoading){
			return;
			}*/
			// SEt the time
			drawTimeIndicators();
			//move2CurrentTime(true);
			
			// prepare the DataFiltering and reporting
			setFilters();
			var cargas:ArrayCollection=e.data.loads;
			
			/*if(this.currentTransformer is LoadPerVehiclePlantVehicleDataTransformer){
			//descartar todas los pedidos de bombas de la vista de loadsPerVehicle
			for(var g:int=cargas.length-1;g>-1;g--){
			if((cargas.getItemAt(g) as OrderLoad).itemCategory=="ZTX0"){
			//borrarlo por ser de bombeo
			cargas.removeItemAt(g);
			}
			}
			}*/
			initRootNodes(cargas);
			
			// STart Adding data to the transformer
			addDatas(cargas,GanttTask.STEP_INITIAL);
			assignDataProviders();
			rawData = e.data.rawData;
			/*view.validateNow();
			view.validateDisplayList();
			view.validateProperties();
			DispatcherViewMediator.isUsingPush=true;
			currentTransformer.timeChanged(TimeZoneHelper.getServer());
			view.gantt.validateSize();*/
			// Start moving the time
			//refreshTime.start();
			//move2CurrentTime(true);
			//view.gantt.ganttSheet.showAll();
			currentTransformer.sortView();
			abstractUpdateByTimerPush();
			logger.info("islandDataEventHandler() end");
			if(currentTransformer is PlanningScreenDataTransformer){
				view.gantt.callLater(forceGridToDisplay);
			}
			view.loadWithOutRecipe_cb.enabled = !(currentTransformer is ServiceAgentScreenDataTransformer);
			view.LoadsPerHour.enabled=!(currentTransformer is ServiceAgentScreenDataTransformer);
			if(startDate== null && endDate==null){
				startDate=TimeDispatcher.getInitialZoom(getDayStart());
				endDate=TimeDispatcher.getFinalZoom(getDayEnd());
			}
			if(DispatcherIslandImpl.diferentBetwenDay!=0){
				startDate=FlexDateHelper.addMinutes(startDate, startDate.date -DispatcherIslandImpl.diferentBetwenDay)
				endDate=FlexDateHelper.addMinutes(endDate, startDate.date -DispatcherIslandImpl.diferentBetwenDay)
				
			}
			if (tmpRefresh){
				var factor:Object=view.gantt.ganttSheet.maxZoomFactor;
				view.gantt.ganttSheet.visibleTimeRangeStart = minMaxDates[0];
				view.gantt.ganttSheet.visibleTimeRangeEnd = minMaxDates[1];
				view.gantt.ganttSheet.visibleTimeRangeStart=FlexDateHelper.addSeconds(minMaxDates[0],1);
			} else {
				//configurar las horas de las cargas visibles
				if(startDate!= null && endDate!=null){
					view.gantt.ganttSheet.visibleTimeRangeStart = startDate//visibleRangeDate[0];
					view.gantt.ganttSheet.visibleTimeRangeEnd = endDate//visibleRangeDate[1];
					view.gantt.ganttSheet.visibleTimeRangeStart=FlexDateHelper.addSeconds(startDate,1);
				}
			}
			tmpRefresh=false;
			DispatcherIslandImpl.diferentBetwenDay=0;
			/*GanttServiceReference.dispatchIslandEvent(WDRequestHelper.getSetPlanningPlant(fra));*/
			//	view.gantt.callLater(setRangeVisible);
			view.gantt.ganttSheet.zoomFactor=((64*DispatcherViewMediator.visualConfigurations.rangeVisible)/60)*1000;
			/*if(currentView==TVARVCPreffixes.SERVICE_AGENT_SCREEN_VIEW_ID && !DispatcherIslandImpl.isBatcher){
				view.collect.selected=false;
			} else {
				view.collect.enabled=true;
			}*/
		}
		
		private function forceGridToDisplay():void{
			/*			view.gantt.dataGridBack.invalidateDisplayList();
			view.gantt.dataGridBack.invalidateProperties();
			view.gantt.dataGridBack.invalidateSize();
			view.gantt.dataGridBack.validateNow();*/
			
		}	
		
		/**
		 * Esta funcion actualiza la fecha actual configurada por el usuario pero el el 
		 * WebDynpro de tal forma que se pueda 
		 * 
		 */
		public function setBaseDate(baseDate:Date):void{
			this.baseDate = baseDate;
			logger.info("setBaseDate()   init    "+(new Date()));
			//var now:Date = new Date();
			var now:Date=TimeZoneHelper.getServer();
			var nowDateString:String = FlexDateHelper.getDateString(now,"YYYY/MM/DD");
			var baseDateString:String = FlexDateHelper.getDateString(baseDate,"YYYY/MM/DD");
			
			if (nowDateString == baseDateString) {
				
				isCurrentDateData = true;
				move2CurrentTime(true);
			}
			else {
				isCurrentDateData = false;
				if(visibleRangeDate[0].time<getDayStart().time){
					view.gantt.ganttSheet.minVisibleTime		 	= visibleRangeDate[0];
				} else {
					view.gantt.ganttSheet.minVisibleTime		 	= getDayStart();
				}
				if(visibleRangeDate[1].time>getDayEnd().time){
					view.gantt.ganttSheet.maxVisibleTime		 	= visibleRangeDate[1];
				} else {
					view.gantt.ganttSheet.maxVisibleTime		 	= getDayEnd();
				}
				/*				view.gantt.ganttSheet.showAll();*/
				view.gantt.ganttSheet.visibleTimeRangeStart=FlexDateHelper.addSeconds(view.vehicleScheduleView.ganttSheet.visibleTimeRangeStart,1);
				view.vehicleScheduleView.ganttSheet.visibleTimeRangeStart=FlexDateHelper.addSeconds(view.vehicleScheduleView.ganttSheet.visibleTimeRangeStart,1);
				
				
			}
			logger.info("setBaseDate()   init    "+(new Date()));
		}
		
		
		
		
		
		
		// Funciones que cambian las vistas
		
		private var isCurrentDateData:Boolean = false;
		
		
		/**
		 * Esta funcion sirve para seleccionar el Transformer de datos que se quiere utilizar con respecto  
		 * a los botones que se hayan utilizado
		 */
		private var initialBatcherView:Boolean=false;
		private var pauseLoading:Boolean=false;
		private var viewAlreadyLoaded:Boolean=false;
		private var lastView:String;
		public function updateTransformers():void{
			var toPlantType:String = GanttServiceReference.getPlantType(franceShowSinglePlant);
			view.gantt.ganttSheet.moveEnabled=true;
			view.vehicleScheduleView.ganttSheet.moveEnabled=false;
			view.gantt.ganttSheet.reassignEnabled=true;
			view.currentTime.visible=true;
			PlanningScreenTask.globalConveyorCounter=1;
			var isBatcher:Boolean = DispatcherIslandImpl.isBatcher;
			/*if(!initialBatcherView && DispatcherIslandImpl.isBatcher && !DispatcherIslandImpl.tieneTab5){
			currentView = TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID;
			initialBatcherView=true;
			view.viewStack.selectedIndex=1;
			view.doubleScreen.getDividerAt(0).visible=false;
			}*/
			var cview:String=DispatcherIslandImpl.currentViewSelector;
			if ((currentView == TVARVCPreffixes.LOADS_PER_ORDER_VIEW_ID && viewAlreadyLoaded) || (DispatcherIslandImpl.currentViewSelector=="LPO" && !viewAlreadyLoaded)){
				makeVisiblePlanningButtons(false);
				view.viewStack.selectedIndex=0;
				franceShowSinglePlant=null;
				view.gantt.showHideTimeScale();
				viewAlreadyLoaded=true;
				view.vehicleScheduleView.height=0;
				view.doubleScreen.getDividerAt(0).visible=false;
				if (isPlain){
					currentTransformer = loadsPerHourPlain;
				}else if(isPerHour) {
					(loadsPerHourGroupedPlants as LoadPerOrderPlantOrderDataTransformer).loadPerOrderByScheduleField();
					
					currentTransformer = loadsPerHourGroupedPlants;
					
				}
					
				else{
					(loadsPerHourGroupedPlants as LoadPerOrderPlantOrderDataTransformer).loadPerOrderByPlantField();
					currentTransformer = loadsPerHourGroupedPlants;
				}	
			}
			else if ((currentView == TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID  && viewAlreadyLoaded) || (DispatcherIslandImpl.currentViewSelector=="LPV" && !viewAlreadyLoaded)){
				makeVisiblePlanningButtons(false);
				view.viewStack.selectedIndex=1;
				view.gantt.showHideTimeScale();
				franceShowSinglePlant=null;
				viewAlreadyLoaded=true;
				view.doubleScreen.getDividerAt(0).visible=false;
				view.vehicleScheduleView.height=0;
				currentTransformer = loadsPerVehicle;
			}
			else if ((currentView == TVARVCPreffixes.ASSIGNED_LOADS_VIEW_ID && viewAlreadyLoaded)  || (DispatcherIslandImpl.currentViewSelector=="LAL" && !viewAlreadyLoaded)){
				makeVisiblePlanningButtons(false);
				view.viewStack.selectedIndex=2;
				view.gantt.showHideTimeScale();
				viewAlreadyLoaded=true;
				view.doubleScreen.getDividerAt(0).visible=false;
				franceShowSinglePlant=null;
				view.vehicleScheduleView.height=0;
				currentTransformer = assignedLoads;
			} else if((currentView == TVARVCPreffixes.SERVICE_AGENT_SCREEN_VIEW_ID && viewAlreadyLoaded) || (DispatcherIslandImpl.currentViewSelector=="LSA" && !viewAlreadyLoaded)){
				makeVisiblePlanningButtons(false);
				if(currentView==TVARVCPreffixes.SERVICE_AGENT_SCREEN_VIEW_ID && !DispatcherIslandImpl.isBatcher && lastView!=currentView){
					view.collect.selected=false;
				} 
				GanttServiceReference.setCollectFilterStatus(view.collect.selected);
				view.viewStack.selectedIndex=3;
				if(view.viewStack.numChildren>2){
					view.viewStack.selectedIndex=3;
				} else {
					view.viewStack.selectedIndex=0;
				}
				view.gantt.showHideTimeScale();
				viewAlreadyLoaded=true;
				view.doubleScreen.getDividerAt(0).visible=false;
				view.vehicleScheduleView.height=0;
				currentTransformer=serviceAgentDataTransformer;
			} else if((currentView==TVARVCPreffixes.PLANNING_SCREEN_VIEW_ID && viewAlreadyLoaded && !tmpRefresh) || (DispatcherIslandImpl.currentViewSelector=="LPS" && !viewAlreadyLoaded && !tmpRefresh)){
				if(visibleRangeDate[0].time<getDayStart().time){
					view.gantt.ganttSheet.minVisibleTime		 	= visibleRangeDate[0];
				} else {
					view.gantt.ganttSheet.minVisibleTime		 	= getDayStart();
				}
				if(visibleRangeDate[1].time>getDayEnd().time){
					view.gantt.ganttSheet.maxVisibleTime		 	= visibleRangeDate[1];
				} else {
					view.gantt.ganttSheet.maxVisibleTime		 	= getDayEnd();
				}
				//view.vehicleScheduleView.ganttSheet.showAll();
				view.currentTime.visible=false;
				makeVisiblePlanningButtons(true, DispatcherIslandImpl.isAgentService);
				if(DispatcherIslandImpl.isBatcher){
					makeVisiblePlanningButtons(true, DispatcherIslandImpl.isBatcher);	
				}
				view.gantt.ganttSheet.moveEnabled=false;
				if(view.viewStack.numChildren>2){
					view.viewStack.selectedIndex=4;
				} else {
					view.viewStack.selectedIndex=1;
				}
				viewAlreadyLoaded=true;
				view.gantt.showHideTimeScale(false);
				view.doubleScreen.getDividerAt(0).visible=true;
				if (view.vehicleScheduleView.height<2){
					view.vehicleScheduleView.height=150;
				}				
				currentTransformer=planningDataTransformer;
				secondCurrentTransformer=planningVehicleDataTransformer;
			}
			
			if(!GanttServiceReference.isVisibleField(DispatcherViewMediator.currentView,SecurityConstants.DRAG_PLANNING_ASSIGNATION,toPlantType) && this.currentTransformer is PlanningScreenDataTransformer){
				view.gantt.ganttSheet.moveEnabled=false;
				view.gantt.ganttSheet.reassignEnabled=false;
			}
			lastView=currentView;
		}
		
		private function makeVisiblePlanningButtons(areVisible:Boolean=true, isAgent:Boolean=true):void{
			view.editTruck.visible=areVisible;
			view.addTruck.visible=areVisible;
			view.autDistr.visible=areVisible;
			view.saveVisualChanges.visible=areVisible;
			view.removeItems.visible=areVisible;
			view.cboPlantSelector.visible = areVisible;
			if(DispatcherIslandImpl.isBatcher){				
				view.editTruck.enabled=true;
				view.addTruck.enabled=false;
			} else {
				view.editTruck.enabled=!isAgent;
				view.addTruck.enabled=!isAgent;
			}
			
			view.autDistr.enabled=!isAgent;
			view.saveVisualChanges.enabled=!isAgent;
			view.removeItems.enabled=!isAgent;
			
			view.saveVisualChanges.enabled=!isAgent;
		}
		
		/**
		 * Esta funcion calcula el limite inferior de fecha que estara visible en la sabana de desapach
		 * 
		 */
		public function getDayStart():Date {
			var returnDate:Date=null;
			var initDay:Date = FlexDateHelper.getDateWithHours(baseDate,0,0,0);
			var param:Number = GanttServiceReference.getParamNumber(ParamtersConstants.MAX_DISP_DELIVER);
			//return DateTimeFunc.dateAdd("n",-param,initDay);
			/*
			Esta linea verifica que los menus no esten bloqueados debido a la fecha
			actual
			*/
			if(!DispatcherIslandImpl.areMenusAvailable){
				returnDate=initDay;
			} else{
				returnDate=DateTimeFunc.dateAdd("n",-param,initDay);
			}
			if(returnDate.time>visibleRangeDate[0].time){
				returnDate=visibleRangeDate[0]
			}
			return returnDate;
		}
		
		/**
		 * Esta funcion calcula el limite superior de fecha que estara visible en la sabana de desapach
		 * 
		 */
		public static var endDate:Date;
		public function getDayEnd():Date {
			var returnDate:Date=null;
			var endDay:Date = FlexDateHelper.getDateWithHours(baseDate,23,59,59);
			var param:Number = GanttServiceReference.getParamNumber(ParamtersConstants.MAX_DISP_FUTURE);
			/*
			Esta linea verifica que los menus no esten bloqueados debido a la fecha
			actual
			*/
			if(!DispatcherIslandImpl.areMenusAvailable){
				returnDate=endDay;
			} else{
				returnDate=DateTimeFunc.dateAdd("n",param,endDay);
			}
			if(returnDate.time<visibleRangeDate[1].time){
				returnDate=visibleRangeDate[1]
			}
			
			return returnDate;
			
		}
		
		
		/**
		 * Esta funcion calcula el Settea los limites visibles segun la vista en donde este
		 * 
		 */
		public function move2CurrentTime(initial:Boolean=false):void {
			
			if (isCurrentDateData) {
				
				
				var delayStartTime:Date = TimeZoneHelper.getServer();
				//var delayStartTime:Date=new Date();
				//delayStartTime.setTime(delayStartTime.getTime()+getTimer());
				delayStartTime.setSeconds(0);
				var obj:String=GanttServiceReference.getParamString(ParamtersConstants.LATE_SERVICES);
				if(obj!="0" && obj!=null){
					delayStartTime=FlexDateHelper.subSeconds(delayStartTime,Number(obj)*60);
				}
				// En base a la vista es el tiempo que se debe ver 
				if (currentView == TVARVCPreffixes.LOADS_PER_ORDER_VIEW_ID && (view.viewStack.numChildren==3 || view.viewStack.numChildren==5)){
					
					// En base a la vista es el tiempo que se debe ver
					if(visibleRangeDate[0].time<FlexDateHelper.getDateWithHours(delayStartTime,delayStartTime.getHours(),delayStartTime.getMinutes(),0).time){
						view.gantt.ganttSheet.minVisibleTime=visibleRangeDate[0];
						view.vehicleScheduleView.ganttSheet.minVisibleTime=visibleRangeDate[0];
					} else {
						view.gantt.ganttSheet.minVisibleTime				= FlexDateHelper.getDateWithHours(delayStartTime,delayStartTime.getHours(),delayStartTime.getMinutes(),0);
						view.vehicleScheduleView.ganttSheet.minVisibleTime	=FlexDateHelper.getDateWithHours(delayStartTime,delayStartTime.getHours(),delayStartTime.getMinutes(),0);
					}
					
					if (initial || currentTimeFlag){
						if(obj!="0" && obj!=null){
							delayStartTime=FlexDateHelper.addMinutes(delayStartTime,Number(obj));
						}
						//comento estas dos lineas para evitar que se regrese 
						/*view.gantt.ganttSheet.visibleTimeRangeStart		= FlexDateHelper.getDateWithHours(delayStartTime,delayStartTime.getHours(),delayStartTime.getMinutes(),0);
						view.gantt.ganttSheet.visibleTimeRangeEnd  	= FlexDateHelper.addMinutes(FlexDateHelper.getDateWithHours(delayStartTime,delayStartTime.getHours(),delayStartTime.getMinutes(),0),15);*/
						if(visibleRangeDate[1].time>getDayEnd()){
							view.gantt.ganttSheet.maxVisibleTime		 	= visibleRangeDate[1];
						} else {
							view.gantt.ganttSheet.maxVisibleTime		 	= getDayEnd();
						}
					}
					
				}
				else if (currentView == TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID 
					|| currentView == TVARVCPreffixes.ASSIGNED_LOADS_VIEW_ID 
					|| currentView==TVARVCPreffixes.PLANNING_SCREEN_VIEW_ID  
					|| currentView==TVARVCPreffixes.SERVICE_AGENT_SCREEN_VIEW_ID){
					
					if (initial){
						
							if(visibleRangeDate[0].time<getDayStart().time){
								view.gantt.ganttSheet.minVisibleTime		 	= visibleRangeDate[0];
								view.vehicleScheduleView.ganttSheet.minVisibleTime		 	= visibleRangeDate[0];
							} else {
								view.gantt.ganttSheet.minVisibleTime		 	= getDayStart();
								view.vehicleScheduleView.ganttSheet.minVisibleTime		 	= getDayStart();
							}
							if(visibleRangeDate[1].time>getDayEnd().time){
								view.gantt.ganttSheet.maxVisibleTime		 	= visibleRangeDate[1];
								view.vehicleScheduleView.ganttSheet.maxVisibleTime= visibleRangeDate[1];
							} else {
								view.gantt.ganttSheet.maxVisibleTime		 	= getDayEnd();
								view.vehicleScheduleView.ganttSheet.maxVisibleTime=getDayEnd();
							}
						
						if(currentView!=TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID){
							view.gantt.ganttSheet.visibleTimeRangeStart		= FlexDateHelper.getDateWithHours(delayStartTime,delayStartTime.getHours(),delayStartTime.getMinutes(),0);
							if(endDate!=null){
								view.gantt.ganttSheet.visibleTimeRangeEnd=endDate;
							} else {
								view.gantt.ganttSheet.visibleTimeRangeEnd  	= FlexDateHelper.addMinutes(FlexDateHelper.getDateWithHours(delayStartTime,delayStartTime.getHours(),delayStartTime.getMinutes(),0),180);
							}
						} else {
							
							//startDate.setDate(baseDate.date);
							///endDate.day= 
								
							view.gantt.ganttSheet.visibleTimeRangeStart = startDate//visibleRangeDate[0];
							view.gantt.ganttSheet.visibleTimeRangeEnd = endDate//visibleRangeDate[1];
							
							
						}
						
						
						//se comenta para prueba de drag. 
						/*if(currentView==TVARVCPreffixes.PLANNING_SCREEN_VIEW_ID && visibleRangeDate.length>0 ){ // si esta en planning screen pasa los minimos y maximos de la fechas calculadas :lgespinosa
						view.gantt.ganttSheet.minVisibleTime= visibleRangeDate[0]; 
						view.gantt.ganttSheet.maxVisibleTime= visibleRangeDate[1];
						}*/
					}
				}
				/*view.vehicleScheduleView.ganttSheet.minVisibleTime=getDayStart();
				view.vehicleScheduleView.ganttSheet.visibleTimeRangeStart=FlexDateHelper.getDateWithHours(delayStartTime,delayStartTime.getHours(),delayStartTime.getMinutes(),0);
				view.vehicleScheduleView.ganttSheet.maxVisibleTime=getDayEnd();
				view.vehicleScheduleView.ganttSheet.visibleTimeRangeEnd=FlexDateHelper.addMinutes(FlexDateHelper.getDateWithHours(delayStartTime,delayStartTime.getHours(),delayStartTime.getMinutes(),0),180);
				*/
			}
			
		}
		
		
		private var isPlain:Boolean=false;
		
		//public var currentGrouping:int = 0;
		
		// Esta es la seccion de las vistas
		
		
		
		// Funciones para modificar datos en todos los modelos al mismo tiempo
		/**
		 * Estas variables es la vista actual del gantt
		 */
		public var currentTransformer:IDataTransformer;
		
		/**
		 * esta variable controla las pantallas que tienen doble gantt
		 */
		public var secondCurrentTransformer:IDataTransformer;
		/**
		 * este es el transformador que hace la vista de aroles Order-> Plant 
		 */
		//public var orderPlantTransformer:IDataTransformer = new OrderPlantDataTransformer();
		
		
		/**
		 * este es el transformador que hace la vista de aroles Plant->Order 
		 */
		//public var plantOrderTransformer:IDataTransformer = new PlantOrderDataTransformer();
		
		/**
		 * este es el transformador que hace la vista de aroles Order
		 */
		//public var orderTransformer:IDataTransformer = new OrderDataTransformer();
		
		
		
		public var loadsPerHourPlain:IDataTransformer = new LoadPerOrderOrderDataTransformer();
		public var loadsPerHourGroupedPlants:IDataTransformer = new LoadPerOrderPlantOrderDataTransformer();
		
		public var loadsPerVehicle:IDataTransformer = new LoadPerVehiclePlantVehicleDataTransformer();
		
		public var assignedLoads:IDataTransformer = new AssignedLoadsDataTransformer();
		public var serviceAgentDataTransformer:IDataTransformer=new ServiceAgentScreenDataTransformer();
		public var planningDataTransformer:IDataTransformer=new PlanningScreenDataTransformer();
		public var planningVehicleDataTransformer:IDataTransformer=new PlanningScreenVehicleDataTransformer();
		
		
		public var reorderOnAddData:Boolean = false;
		
		public function addData(data:Object,step:String):void{
			//this.seekAndDestroy(null);
			if (currentTransformer != null){
				//var now:Date = new Date();
				var now:Date=TimeZoneHelper.getServer();
				if (!isSameAsBaseDate(now)){
					now = null;
				}
				currentTransformer.addData(data,step,now);
				//scores.addData(data);
				if (reorderOnAddData){
					doRefreshOrder();
				}
			}
			if(currentTransformer is PlanningScreenDataTransformer){
				var now:Date=TimeZoneHelper.getServer();
				if (!isSameAsBaseDate(now)){
					now = null;
				}
				secondCurrentTransformer.addData(data,step,now);
				//scores.addData(data);
				if (reorderOnAddData){
					doRefreshOrder();
				}
			}
		}
		
		
		public function addDatas(datas:ArrayCollection,step:String):void{
			//updatingDatas  = true;
			if (currentTransformer != null){
				//now es relativo a la fecha del servidor
				//var now:Date = new Date();
				var now:Date=TimeZoneHelper.getServer();
				if (!isSameAsBaseDate(now)){
					now = null;
				}
				currentTransformer.addDatas(datas,step,now);
				if (reorderOnAddData){
					doRefreshOrder();
				}
			}
			if(currentTransformer is PlanningScreenDataTransformer){
				if (secondCurrentTransformer != null){
					//now es relativo a la fecha del servidor
					//var now:Date = new Date();
					var now:Date=TimeZoneHelper.getServer();
					if (!isSameAsBaseDate(now)){
						now = null;
					}
					secondCurrentTransformer.addDatas(datas,step,now);
					
					if (reorderOnAddData){
						doRefreshOrder();
					}
				}	
			}
			//scores.addDatas(datas);
		}
		/**
		 * Esta funcion cambia la planta actual del usuario por la seleccionada en el boton
		 */
		public function changeViewToOnePlant(evt:MouseEvent):void{
			visualConfigurations.plantConfigured=evt.currentTarget.label;
			var saveEvent:VisualConfigurationEvent=new VisualConfigurationEvent();
			saveEvent.availableVehiclesButton=DispatcherViewMediator.visualConfigurations.availableVehiclesButton;
			saveEvent.firstRow=DispatcherViewMediator.visualConfigurations.firstRow;
			saveEvent.plantConfigured=DispatcherViewMediator.visualConfigurations.plantConfigured;
			if(saveEvent.plantConfigured==null){
				saveEvent.plantConfigured="none";
			}
			saveEvent.rangeVisible=DispatcherViewMediator.visualConfigurations.rangeVisible;
			saveEvent.secondRow=DispatcherViewMediator.visualConfigurations.secondRow;
			saveEvent.showOnePlantAtATime=DispatcherViewMediator.visualConfigurations.showOnePlantAtATime;
			saveEvent.sortVehicleFromOldestToNewest=DispatcherViewMediator.visualConfigurations.sortVehicleFromOldestToNewest;
			saveEvent.sortVehiclesPerTime=DispatcherViewMediator.visualConfigurations.sortVehiclesPerTime;
			saveEvent.thirdRow=DispatcherViewMediator.visualConfigurations.thirdRow;
			if(saveEvent.thirdRow==null){
				saveEvent.thirdRow="LOAD";
			}
			saveEvent.selectedView=DispatcherViewMediator.visualConfigurations.selectedView;
			
			GanttServiceReference.dispatchIslandEvent(saveEvent);
			requestRefresh();
		}
		/**
		 * este listener ayuda a que sepamos en que status se encuentra el boton de agregar o quitar la vista de vehiculos
		 */
		public function alterAdditions(evt:MouseEvent):void{
			
			/*if(evt.currentTarget.label=="-"){
				evt.currentTarget.label="+";
			} else {
				evt.currentTarget.label="-";
			}*/
			if(evt.currentTarget.name=="assn" && (visualConfigurations.availableVehiclesButton & 1)==1){
				visualConfigurations.availableVehiclesButton&=6;
			} else if(evt.currentTarget.name=="assn" && (visualConfigurations.availableVehiclesButton & 1)==0){
				visualConfigurations.availableVehiclesButton|=1;
			}
			
			if(evt.currentTarget.name=="avlb" && (visualConfigurations.availableVehiclesButton & 2)==2){
				visualConfigurations.availableVehiclesButton&=5;
			} else if(evt.currentTarget.name=="avlb" && (visualConfigurations.availableVehiclesButton & 2)==0){
				visualConfigurations.availableVehiclesButton|=6;
			}
			for(var tcounter:int=0;tcounter<view.plantSelectionHorizontalV.numChildren;tcounter++){
				(view.plantSelectionHorizontalV.getChildAt(tcounter) as UIComponent).enabled=false;
			}
			evt.currentTarget.enabled=false;
			//enviar evento
			var saveEvent:VisualConfigurationEvent=new VisualConfigurationEvent();
			saveEvent.availableVehiclesButton=DispatcherViewMediator.visualConfigurations.availableVehiclesButton;
			saveEvent.firstRow=DispatcherViewMediator.visualConfigurations.firstRow;
			saveEvent.plantConfigured=DispatcherViewMediator.visualConfigurations.plantConfigured;
			if(saveEvent.plantConfigured==null){
				saveEvent.plantConfigured="none";
			}
			saveEvent.rangeVisible=DispatcherViewMediator.visualConfigurations.rangeVisible;
			saveEvent.secondRow=DispatcherViewMediator.visualConfigurations.secondRow;
			saveEvent.showOnePlantAtATime=DispatcherViewMediator.visualConfigurations.showOnePlantAtATime;
			saveEvent.sortVehicleFromOldestToNewest=DispatcherViewMediator.visualConfigurations.sortVehicleFromOldestToNewest;
			saveEvent.sortVehiclesPerTime=DispatcherViewMediator.visualConfigurations.sortVehiclesPerTime;
			saveEvent.thirdRow=DispatcherViewMediator.visualConfigurations.thirdRow;
			if(saveEvent.thirdRow==null){
				saveEvent.thirdRow="LOAD";
			}
			saveEvent.selectedView=DispatcherViewMediator.visualConfigurations.selectedView;
			
			GanttServiceReference.dispatchIslandEvent(saveEvent);
			
			requestRefresh();
		}
		/* Variable que contiene los vehiculos correctos para usarlos en otros sitios como menus emergentes*/
		public var visibleRangeDate:Array=[new Date(2035,11,29),new Date(2000,11,29)];
		public static var vehiculosCorrectos:ArrayCollection;
		public static var reportesPlantas:Array;
		public static var reportesTrucks:Array;
		public static var visibleLoadItemsPlain:Array=new Array();
		public static var visibleVehicleItemsPlain:Array=new Array();
		public static var currentStampLoads:Array=[];
		public static var loadsByPlants:DictionaryMap;
		public function initRootNodes(loads:ArrayCollection):void{
			//if(currentView==TVARVCPreffixes.SERVICE_AGENT_SCREEN_VIEW_ID || currentView==TVARVCPreffixes.PLANNING_SCREEN_VIEW_ID){
				visibleRangeDate=[new Date(2035,11,20),new Date(2000,11,29)];	
			//}
			
			loadsByPlants=new DictionaryMap();
			logger.info("initRootNodes()   init    "+(new Date()));
			DispatcherViewMediator.reportesPlantas=[];
			DispatcherViewMediator.reportesTrucks=[];
			var plants:ArrayCollection = GanttServiceReference.getPlantsPlain();
			if(currentTransformer is ServiceAgentScreenDataTransformer){
				franceShowSinglePlant=null;
			}
			view.plantSelectionHorizontal.removeAllChildren();
			view.plantSelectionHorizontalV.removeAllChildren();
			var tmpHolderPlant:Object=null;
			/*visualConfigurations.plantConfigured=null;*/
			//creamos aqui los botones de agregar o quitar
			if(visualConfigurations.availableVehiclesButton>0 && currentView==TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID){
				var btnViewAssn:Button;
				var btnViewAvlb:Button;
				if((visualConfigurations.availableVehiclesButton & 1)==1){
					btnViewAssn=ButtonFactory.createShowAssigned("-");
				} else {
					btnViewAssn=ButtonFactory.createShowAssigned("+");
				}
				if((visualConfigurations.availableVehiclesButton & 2)==2){
					btnViewAvlb=ButtonFactory.createShowAvailable("-");
				} else {
					btnViewAvlb=ButtonFactory.createShowAvailable("+");
				}
				view.plantSelectionHorizontalV.addChild(btnViewAssn);
				view.plantSelectionHorizontalV.addChild(btnViewAvlb);
				btnViewAssn.addEventListener(MouseEvent.CLICK,alterAdditions);
				btnViewAvlb.addEventListener(MouseEvent.CLICK,alterAdditions);
			}
			//btnPlant.addEventListener(MouseEvent.CLICK,changeViewToOnePlant);
			
			if(plants.length>1){
				for(var v:int=plants.length-1;v>-1;v--){
					if (currentView==TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID){
						if(visualConfigurations.plantConfigured==null && visualConfigurations.showOnePlantAtATime && plants.length==1){
							//no plant configured yet, lets take the first one
							visualConfigurations.plantConfigured=plants.getItemAt(v).plantId;
							var btnPlant:Button=ButtonFactory.createPlantButton(plants.getItemAt(v).plantId);
							btnPlant.enabled=false;
							view.plantSelectionHorizontal.addChild(btnPlant);
							continue;
						} else if(visualConfigurations.showOnePlantAtATime && !DispatcherIslandImpl.isBatcher){
							//before erasing lets add them to the button list
							var btnPlant:Button=ButtonFactory.createPlantButton(plants.getItemAt(v).plantId);
							btnPlant.addEventListener(MouseEvent.CLICK,changeViewToOnePlant);
							view.plantSelectionHorizontal.addChild(btnPlant);
							
							if(plants.getItemAt(v).plantId==visualConfigurations.plantConfigured){
								btnPlant.enabled=false;
								tmpHolderPlant=plants.getItemAt(v);
							}
							plants.removeItemAt(v);
							continue;
						}
					}
					if(plants.getItemAt(v).plantId!=franceShowSinglePlant && franceShowSinglePlant!=null && plants.length>1 && currentView==TVARVCPreffixes.PLANNING_SCREEN_VIEW_ID){
						plants.removeItemAt(v);
						continue;
					}
					DispatcherViewMediator.createPlantReport(plants.getItemAt(v).plantId);
				}
			} else {
				franceShowSinglePlant=plants.getItemAt(0).plantId;
				if(visualConfigurations.plantConfigured==null && visualConfigurations.showOnePlantAtATime && currentView==TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID){
					//no plant configured yet, lets take the first one
					tmpHolderPlant=plants.getItemAt(0);
					visualConfigurations.plantConfigured=plants.getItemAt(0).plantId;
					plants.removeItemAt(0);
				}
			}
			/*if(visualConfigurations.plantConfigured!=null && visualConfigurations.showOnePlantAtATime && currentView==TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID){
				var btnPlant:Button=ButtonFactory.createPlantButton(visualConfigurations.plantConfigured);
				btnPlant.toggle=true;
				btnPlant.enabled=false;
				view.plantSelectionHorizontal.addChild(btnPlant);
			}*/
			
			logger.info("initRootNodes()   creo reporte    "+(new Date()));
			//recorrer las cargas
			
			reports=null;
			if(plants.length==0 && tmpHolderPlant!=null){
				plants.addItem(tmpHolderPlant);
			}
			currentTransformer.initHeaders(plants,getReports());
			if((currentTransformer is PlanningScreenDataTransformer) && !tmpRefresh){
				secondCurrentTransformer.initHeaders(plants,getReports());	
			}
			var equipments:ArrayCollection = GanttServiceReference.getEquipmentsPlain();
			var equiposCorrectos:Array=[];
			/**
			 * Este fix permite visualizar los vehiculos correctamente
			 * y evitar que aparezcan como "undefined"
			 *
			 * */
			var buenos:Array=[];
			var buenos2:Array=[];
			if(currentView != TVARVCPreffixes.PLANNING_SCREEN_VIEW_ID){
				for(var e:int=0;e<equipments.length;e++){
					equipments.getItemAt(e).equipStatus=ExtrasConstants.VEHICLE_DISPONIBLE;
					/*if(String(equipments.getItemAt(e).equipNumber).toLowerCase().indexOf("needed")!=-1){
						equipments.getItemAt(e).equipNumber=equipments.getItemAt(e).equipLabel;
					}*/
					DispatcherViewMediator.createTruckReport(equipments.getItemAt(e).equipNumber);
					for(var w:int=loads.length-1;w>-1;w--){
						if(loads.getItemAt(w).equipNumber!=""){
							if((String(equipments.getItemAt(e).equipNumber).indexOf(loads.getItemAt(w).equipNumber)!=-1 || Number(equipments.getItemAt(e).equipNumber)==Number(loads.getItemAt(w).equipLabel)) && loads.getItemAt(w).itemCategory!="ZTX0"){
								buenos2.push(loads.getItemAt(w).equipNumber);
								equipments.getItemAt(e).maintplant=loads.getItemAt(w).plant;
								equipments.getItemAt(e).plant=loads.getItemAt(w).plant;
								equipments.getItemAt(e).plantId=loads.getItemAt(w).plant;
								break;
							}
						}
					}
				}
			}
			logger.info("initRootNodes()   creo vehiculos    "+(new Date()));
			currentStampLoads=[];
			var visibleLoadItems:ArrayCollection=GanttServiceReference.getTVARVCSelection(TVARVCConstants.LOADS_PER_ORDER_VIEW_ID_L);
			//var visibleLoadItemsPlain:Array=new Array();
			for(var vli:int=0;vli<visibleLoadItems.length;vli++){
				visibleLoadItemsPlain.push(visibleLoadItems.getItemAt(vli)["low"]);
			}
			var visibleVehicleItems:ArrayCollection=GanttServiceReference.getTVARVCSelection(TVARVCConstants.LOADS_PER_VEHICLE_VIEW_ID_L);
			//var visibleVehicleItemsPlain:Array=new Array();
			for(var vli:int=0;vli<visibleVehicleItems.length;vli++){
				visibleVehicleItemsPlain.push(visibleVehicleItems.getItemAt(vli)["low"]);
			}
			for(var w:int=loads.length-1;w>-1;w--){
				// saca minimo y maximo date de cargas
				var ceload:OrderLoad= (loads.getItemAt(w) as OrderLoad);
				if(currentView==TVARVCPreffixes.SERVICE_AGENT_SCREEN_VIEW_ID || currentView==TVARVCPreffixes.PLANNING_SCREEN_VIEW_ID){
					if(ceload.loadingTimeStampFix().time<visibleRangeDate[0].time){
						visibleRangeDate[0]=ceload.loadingTimeStampFix();
					}
					var endDateLoad:Date=FlexDateHelper.addSeconds((loads.getItemAt(w) as OrderLoad).loadingTimeStampFix(),(loads.getItemAt(w) as OrderLoad).cycleTimeMillis);
					if(endDateLoad.time>visibleRangeDate[1].time){
						visibleRangeDate[1]=endDateLoad;
					}
				}
				(loads.getItemAt(w) as OrderLoad).graphicReference=null;
				if(this.currentTransformer is PlanningScreenDataTransformer){
					//eliminar los servicios de bombeo
				}
				if(loadsByPlants.get((loads.getItemAt(w) as OrderLoad).orderNumber)==null){
					loadsByPlants.put((loads.getItemAt(w) as OrderLoad).orderNumber,new Array());
				}
				var orderPlant:Array=(loadsByPlants.get((loads.getItemAt(w) as OrderLoad).orderNumber) as Array);
				if(orderPlant.length==0){
					orderPlant[0]=new Array();
					orderPlant[1]=new Array();
				}
				if(orderPlant[0].indexOf((loads.getItemAt(w) as OrderLoad).plant)==-1){
					orderPlant[0].push((loads.getItemAt(w) as OrderLoad).plant);
					orderPlant[(loads.getItemAt(w) as OrderLoad).plant]=new Array();
					
				}
				
				if((loads.getItemAt(w) as OrderLoad).itemCategory!="ZTX0"){
					orderPlant[(loads.getItemAt(w) as OrderLoad).plant].push((loads.getItemAt(w) as OrderLoad).itemNumber);
					orderPlant[1].push((loads.getItemAt(w) as OrderLoad).plant+":"+(loads.getItemAt(w) as OrderLoad).itemNumber);	
				}
				if(currentStampLoads[(loads.getItemAt(w) as OrderLoad).orderNumber]==null){
					currentStampLoads[(loads.getItemAt(w) as OrderLoad).orderNumber]=(loads.getItemAt(w) as OrderLoad).numberTimeStamp;
				}
				(loads.getItemAt(w) as OrderLoad).txtStatusProdOrder=translateProductionStatus(loads.getItemAt(w).codStatusProdOrder);
				if((loads.getItemAt(w) as OrderLoad).txtStatusProdOrder=="100" || (loads.getItemAt(w) as OrderLoad).txtStatusProdOrder=="200"){
					(loads.getItemAt(w) as OrderLoad).txtStatusProdOrder="";
				}
				if((((loads.getItemAt(w) as OrderLoad).loadStatus=="ASSG" || (loads.getItemAt(w) as OrderLoad).loadStatus=="TJST") && currentTransformer is LoadsPerOrderAbstractDataTransformer)
					|| (((loads.getItemAt(w) as OrderLoad).itemCategory=="ZTX0" || (loads.getItemAt(w) as OrderLoad).itemCategory=="ZRWN") && (currentTransformer is LoadPerVehiclePlantVehicleDataTransformer || currentTransformer is PlanningScreenDataTransformer))
					|| (GanttServiceReference.getParamString("DISP_PUMP_DFE")=="0" && ((loads.getItemAt(w) as OrderLoad).itemCategory=="ZTX0" || (loads.getItemAt(w) as OrderLoad).itemCategory=="ZRWN") && currentTransformer is LoadsPerOrderAbstractDataTransformer)
					|| isRedirect(loads.getItemAt(w) as OrderLoad)
					|| ((loads.getItemAt(w) as OrderLoad).plant!=franceShowSinglePlant && franceShowSinglePlant!=null)
					//recipes
					|| (tmpEraseItemPlanningScreen.indexOf((loads.getItemAt(w) as OrderLoad).orderLoad)!=-1) 
					|| ((loads.getItemAt(w) as OrderLoad).bomValid!="1" && showNoRecipesOnly)
					//eliminar los selfcollect
					|| (currentTransformer is PlanningScreenDataTransformer && Number((loads.getItemAt(w) as OrderLoad).shipConditions)!=1)
					|| (currentTransformer is LoadPerVehiclePlantVehicleDataTransformer && visualConfigurations.plantConfigured!=(loads.getItemAt(w) as OrderLoad).plant && visualConfigurations.showOnePlantAtATime)
					|| (Number((loads.getItemAt(w) as OrderLoad).codStatusProdOrder)>=20 && currentView==TVARVCPreffixes.LOADS_PER_ORDER_VIEW_ID)){
					loads.removeItemAt(w);
				}
			}
			
			
			
			logger.info("initRootNodes()   descarto bombas    "+(new Date()));
			//hacer otro barrido para buscar los vehiculos asignados
			var pasados:Array=[];
			if(currentView != TVARVCPreffixes.PLANNING_SCREEN_VIEW_ID){
				for(var s:int=0;s<buenos2.length;s++){
					for(var e:int=0;e<equipments.length;e++){
						if(equipments.getItemAt(e).equipNumber==buenos2[s]){
							equipments.getItemAt(e).equipStatus=ExtrasConstants.VEHICLE_ASSIGNED;						
							
						} 
					}
				}
			}
			logger.info("initRootNodes()   acomodo arreglo    "+(new Date()));
			//y otro barrido para quitar los vehiculos que sean W y q tengan estatus diferente de asignado
			for(var e:int=equipments.length-1;e>-1;e--){
				if((equipments.getItemAt(e).equicatgry=="W" && equipments.getItemAt(e).equipStatus==ExtrasConstants.VEHICLE_DISPONIBLE && !(currentTransformer is PlanningScreenDataTransformer))
					|| (equipments.getItemAt(e).status!="AVLB" && equipments.getItemAt(e).equipStatus==ExtrasConstants.VEHICLE_DISPONIBLE && !(currentTransformer is PlanningScreenDataTransformer))
					|| (equipments.getItemAt(e).equicatgry=="Y" && equipments.getItemAt(e).equipStatus==ExtrasConstants.VEHICLE_DISPONIBLE && !(currentTransformer is PlanningScreenDataTransformer))
					|| (franceShowSinglePlant!=equipments.getItemAt(e).maintplant && franceShowSinglePlant!=null)
					|| (visualConfigurations.plantConfigured!=equipments.getItemAt(e).maintplant && visualConfigurations.showOnePlantAtATime==true)){
					/*visualConfigurations.plantConfigured==null && visualConfigurations.showOnePlantAtATime && plants.length==1){
					Yo del futuro, quitamos esta validacion por que aparecian vehiculos asignados como disponibles solo los 992
					if(equipments.getItemAt(e).vehicleType!="992"&&equipments.getItemAt(e).vehicleType!="993" && currentView!=TVARVCPreffixes.SERVICE_AGENT_SCREEN_VIEW_ID){
					equipments.removeItemAt(e);
					} else {*/
					equipments.removeItemAt(e);
					//}
				} 
			}
			DispatcherViewMediator.vehiculosCorrectos=equipments;
			var dataSortField:SortField = new SortField("turnTimeStampNumber",false,!DispatcherViewMediator.visualConfigurations.sortVehicleFromOldestToNewest,true);
			
			var dataSort:Sort = new Sort();
			dataSort.fields = [dataSortField];
			if(currentView != TVARVCPreffixes.PLANNING_SCREEN_VIEW_ID){
				DispatcherViewMediator.vehiculosCorrectos.sort = dataSort;
				DispatcherViewMediator.vehiculosCorrectos.refresh();
			}else{
				DispatcherViewMediator.vehiculosCorrectos.sort=null;
			}
			
			currentTransformer.addHeaders(equipments);
			if(currentTransformer is PlanningScreenDataTransformer){
				secondCurrentTransformer.addHeaders(equipments);
				
			}
			
			view.callLater(setRangeVisible);
			
			logger.info("initRootNodes()   end    "+(new Date()));
		}
		public function isRedirect(carga:OrderLoad):Boolean{
			return ((carga.loadStatus=="TJST" || carga.loadStatus=="ASSG") && carga.constructionProduct.indexOf("P")!=-1 && carga.visible!="");
		}
		public static function createTruckReport(truckLabel:String):void{
			if(DispatcherViewMediator.reportesTrucks[truckLabel]==null && truckLabel!=""){
				DispatcherViewMediator.reportesTrucks[truckLabel]=new Object();
				DispatcherViewMediator.reportesTrucks[truckLabel].DeliveredVolume=0;
				DispatcherViewMediator.reportesTrucks[truckLabel].PendingConfirmedVolume=0;
				DispatcherViewMediator.reportesTrucks[truckLabel].PendingToBeConfirmedVolume=0;
			}
		}
		public static function createPlantReport(plantLabel:String):void{
			if(DispatcherViewMediator.reportesPlantas[plantLabel]==null){
				DispatcherViewMediator.reportesPlantas[plantLabel]=new Object();
				DispatcherViewMediator.reportesPlantas[plantLabel].ScheduledVolume=0;
				DispatcherViewMediator.reportesPlantas[plantLabel].DeliveredVolume=0;
				DispatcherViewMediator.reportesPlantas[plantLabel].ConfirmedVolume=0;
				DispatcherViewMediator.reportesPlantas[plantLabel].ToBeConfirmedVolume=0;
			}
		}
		public static function reportTruck(carga:Object):void{
			DispatcherViewMediator.createTruckReport(carga.equipNumber);
			switch(carga.loadStatus){
				case "OJOB":
				case "UNLD":
				case "TJST":
					DispatcherViewMediator.reportesTrucks[carga.equipNumber].DeliveredVolume+=carga.loadVolume;	
					break;
				case "NOST":
				case "ASSG":
				case "HOLI":
				case "LDNG":
					DispatcherViewMediator.reportesTrucks[carga.equipNumber].PendingConfirmedVolume+=carga.loadVolume;
					break;
				case "TBCL":
					DispatcherViewMediator.reportesTrucks[carga.equipNumber].PendingToBeConfirmedVolume+=carga.loadVolume;
					break;
			}
		}
		public static function reportPlant(carga:Object):void{
			DispatcherViewMediator.createPlantReport(carga.plant);
			switch(carga.loadStatus){
				case "OJOB":
				case "UNLD":
				case "TJST":
					DispatcherViewMediator.reportesPlantas[carga.plant].DeliveredVolume+=carga.loadVolume;
					DispatcherViewMediator.reportesPlantas[carga.plant].ScheduledVolume+=carga.loadVolume;
					break;
				case "NOST":
				case "ASSG":
				case "HOLI":
				case "LDNG":
					DispatcherViewMediator.reportesPlantas[carga.plant].ConfirmedVolume+=carga.loadVolume;
					DispatcherViewMediator.reportesPlantas[carga.plant].ScheduledVolume+=carga.loadVolume;
					break;
				case "TBCL":
					DispatcherViewMediator.reportesPlantas[carga.plant].ToBeConfirmedVolume+=carga.loadVolume;
					DispatcherViewMediator.reportesPlantas[carga.plant].ScheduledVolume+=carga.loadVolume;
					break;
			}
		}
		private var reports:DictionaryMap;
		public function getReports():DictionaryMap{
			if (reports == null){
				reports = new DictionaryMap();
				reports.put("",getRootReport());
				reports.put("plant",getPlantReport());
				reports.put("equipNumber",getEquipmentReport());
				
			}
			
			return reports;
		}
		
		public function getRootReport():ViewReportObject{
			var report:ViewReportObject = new ViewReportObject();
			var reportConfig:ArrayCollection =  new ArrayCollection();
			
			
			reportConfig.addItem(getReportConfig("optimalCost",	"mepTotcost"   , ReportConfig.ACCUMULATE));
			reportConfig.addItem(getReportConfig("currentCost",	"totCost", ReportConfig.ACCUMULATE));
			report.singleton = true;
			
			BindingUtils.bindProperty(this,"currentCostFormatter",report,"currentCost");
			BindingUtils.bindProperty(this,"optimalCostFormatter",report,"optimalCost");
			report.setReportsConfig(reportConfig);
			view.callLater(costData);
			return report;
		}
		public var optimalCostString:String;
		[Bindable]
		var currentCostString:String;
		public function costData():void{
			
			GanttServiceReference.dispatchIslandEvent( WDRequestHelper.getCostData(optimalCostString ,  currentCostString  ));
			
		}
		/**
		 * this function will reformat the currency in digits
		 **/
		public static var mtcost:Number
		public var lastCostFormat:Number=0;//totCost
		public function set currentCostFormatter(quantity:Number):void{
			//TOTAL COST
			//quitar decimales
			
			var num:CurrencyFormatter=new CurrencyFormatter();
			num.precision=2;
			num.currencySymbol="";
			//here we will define the user number format from backend
			if(DispatcherIslandImpl.userFormat.toLowerCase()=="x"){
				//nothing to do here
			} else if(DispatcherIslandImpl.userFormat.toLowerCase()=="y") {
				num.decimalSeparatorFrom=",";
				num.decimalSeparatorTo=",";
				num.thousandsSeparatorFrom=" ";
				num.thousandsSeparatorTo=" ";
			} else {
				num.decimalSeparatorFrom=",";
				num.decimalSeparatorTo=",";
				num.thousandsSeparatorFrom=".";
				num.thousandsSeparatorTo=".";
			}
			//aqui traemos el parametro q va a servir para mostrar
			if(DispatcherIslandImpl.getRMSCostDisplay()==1){
				currentCostString=num.format(quantity);
			} else if(DispatcherIslandImpl.getRMSCostDisplay()==2){
				currentCostString="";
			} else if(DispatcherIslandImpl.getRMSCostDisplay()==3){
				currentCostString=num.format(quantity-DispatcherViewMediator.mtcost);
			}
			
			lastCostFormat=quantity;
		}
		public var lastOptimalCost:Number=0;//mepTotcost
		public function set optimalCostFormatter(quantity:Number):void{
			lastOptimalCost=quantity;
			//MEP COST
			DispatcherViewMediator.mtcost=quantity;
			var num:CurrencyFormatter=new CurrencyFormatter();
			num.precision=2;
			num.currencySymbol=""
			
			if(DispatcherIslandImpl.getRMSCostDisplay()==1){
				optimalCostString=num.format(quantity);
			} else if(DispatcherIslandImpl.getRMSCostDisplay()==2){
				optimalCostString="";
			} else if(DispatcherIslandImpl.getRMSCostDisplay()==3){
				optimalCostString="0";
				
			} 
		}
		
		public function getPlantReport():PlantReport{
			var report:PlantReport = new PlantReport();
			var reportConfig:ArrayCollection =  new ArrayCollection();
			
			reportConfig.addItem(getReportConfig("assignedLoads",	"loadVolume"   , ReportConfig.ACCUMULATE,		getAssignedLoadsFilter()));
			reportConfig.addItem(getReportConfig("CanceledVolume",	"loadVolume"   , ReportConfig.ACCUMULATE,		getCanceledVolumeFilter()));
			reportConfig.addItem(getReportConfig("ConfirmedVolume",	"loadVolume"   , ReportConfig.ACCUMULATE,	getConfirmedVolumeFilter()));
			reportConfig.addItem(getReportConfig("DeliveredVolume",	"loadVolume"   , ReportConfig.ACCUMULATE,	getDeliveredVolumeFilter()));
			reportConfig.addItem(getReportConfig("ScheduledVolume",	"loadVolume"   , ReportConfig.ACCUMULATE,	getScheduledVolumeFilter()));
			reportConfig.addItem(getReportConfig("ToBeConfirmedVolume",	"loadVolume"   , ReportConfig.ACCUMULATE,getToBeConfirmedVolumeFilter()));
			reportConfig.addItem(getReportConfig("UOM",				"loadUom"   , ReportConfig.KEEP_LAST_VALUE));
			report.singleton = false;
			report.setReportsConfig(reportConfig);
			var valor:*=report;
			return report;
		}
		
		public function getAssignedLoadsFilter():ArrayCollection{
			return new ArrayCollection([
				FilterHelper.getFilterCondition("loadStatus",GanttServiceReference.getTVARVCSelection(TVARVCConstants.PLANT_TOOLTIP_LOAD_FILTER_ASSIGNED),FilterCondition.EQUALS)
			]);
		}
		public function getCanceledVolumeFilter():ArrayCollection{
			return new ArrayCollection([
				FilterHelper.getFilterCondition("loadStatus",GanttServiceReference.getTVARVCSelection(TVARVCConstants.PLANT_TOOLTIP_LOAD_FILTER_CANCELED),FilterCondition.EQUALS)
			]);
		}
		public function getConfirmedVolumeFilter():ArrayCollection{
			return new ArrayCollection([
				FilterHelper.getFilterCondition("loadStatus",GanttServiceReference.getTVARVCSelection(TVARVCConstants.PLANT_TOOLTIP_LOAD_FILTER_CONFIRMED),FilterCondition.EQUALS)
			]);
		}
		public function getDeliveredVolumeFilter():ArrayCollection{
			return new ArrayCollection([
				FilterHelper.getFilterCondition("loadStatus",GanttServiceReference.getTVARVCSelection(TVARVCConstants.PLANT_TOOLTIP_LOAD_FILTER_DELIVERIED),FilterCondition.EQUALS)
			]);
		}
		public function getScheduledVolumeFilter():ArrayCollection{
			return new ArrayCollection([
				//FilterHelper.getFilterCondition("loadStatus",GanttServiceReference.getTVARVCSelection(TVARVCConstants.PLANT_TOOLTIP_LOAD_FILTER_),FilterCondition.EQUALS)
			]);
		}
		public function getToBeConfirmedVolumeFilter():ArrayCollection{
			return new ArrayCollection([
				FilterHelper.getFilterCondition("loadStatus",GanttServiceReference.getTVARVCSelection(TVARVCConstants.PLANT_TOOLTIP_LOAD_FILTER_TO_BE_CONFIRMED),FilterCondition.EQUALS)
			]);
		}
		
		public function getEquipmentReport():VehicleReport{
			var report:VehicleReport = new VehicleReport();
			var reportConfig:ArrayCollection =  new ArrayCollection();
			var o:OrderLoad;
			
			var delivered:ArrayCollection = getEquipmentDeliveredFilter();
			var confirmed:ArrayCollection = getEquipmentConfirmedFilter();
			var tobeconfirmed:ArrayCollection = getEquipmentToBeConfirmedFilter();
			
			reportConfig.addItem(getReportConfig("DeliveredLoadsCount"			,	"loadVolume"   , ReportConfig.COUNT			,delivered));
			reportConfig.addItem(getReportConfig("DeliveredVolume"				,	"loadVolume"   , ReportConfig.ACCUMULATE	,delivered));
			reportConfig.addItem(getReportConfig("PendingConfirmedLoadsCount"	,	"loadVolume"   , ReportConfig.COUNT			,confirmed));
			reportConfig.addItem(getReportConfig("PendingConfirmedVolume"		,	"loadVolume"   , ReportConfig.ACCUMULATE	,confirmed));
			reportConfig.addItem(getReportConfig("PendingToBeConfirmedLoadsCount",	"loadVolume"   , ReportConfig.COUNT			,tobeconfirmed));
			reportConfig.addItem(getReportConfig("PendingToBeConfirmedVolume"	,	"loadVolume"   , ReportConfig.ACCUMULATE	,tobeconfirmed));
			reportConfig.addItem(getReportConfig("UOM"							,	"loadUom"   , ReportConfig.KEEP_LAST_VALUE));
			report.singleton = false;
			report.setReportsConfig(reportConfig);
			return report;
		}
		
		public function getEquipmentDeliveredFilter():ArrayCollection{
			return new ArrayCollection([
				FilterHelper.getFilterCondition("loadStatus",GanttServiceReference.getTVARVCSelection(TVARVCConstants.VEHICLE_TOOLTIP_LOAD_FILTER_DELIVERIED),FilterCondition.EQUALS)
			]);
		}
		public function getEquipmentConfirmedFilter():ArrayCollection{
			return new ArrayCollection([
				FilterHelper.getFilterCondition("loadStatus",GanttServiceReference.getTVARVCSelection(TVARVCConstants.VEHICLE_TOOLTIP_LOAD_FILTER_CONFIRMED),FilterCondition.EQUALS)
			]);
		}
		public function getEquipmentToBeConfirmedFilter():ArrayCollection{
			return new ArrayCollection([
				FilterHelper.getFilterCondition("loadStatus",GanttServiceReference.getTVARVCSelection(TVARVCConstants.VEHICLE_TOOLTIP_LOAD_FILTER_TO_BE_CONFIRMED),FilterCondition.EQUALS)
			]);
		}
		
		public function getReportConfig(reportField:String,dataField:String,type:String,filters:ArrayCollection=null):ReportConfig{
			var config:ReportConfig =  new ReportConfig();
			
			//config.resourceField = resourceField;
			config.reportField = reportField;
			config.name = reportField;
			config.dataField = dataField;
			config.type = type;
			config.filters = filters;
			return config;
		}
		
		public function setFilters():void{
			logger.info("setFilters()   init    "+(new Date()));
			var filters:ArrayCollection = new ArrayCollection();
			//if (currentView != "FXD_ASL"){
			
			filters.addItem(FilterHelper.getFilterCondition("loadingTimestamp",new ArrayCollection([null]),FilterCondition.NOT_EQUALS));
			//DELICATE CHANGE COLORS
			//filters.addItem(new ArrayCollection([FlexDateHelper.getDateString(baseDate,"YYYYMMDD")]),FilterCondition.EQUALS);
			var orderFilterString:String=currentView+TVARVCPreffixes.ORDER_SUFFIX;
			var loadFilterString:String=currentView+TVARVCPreffixes.LOAD_SUFFIX;
			filters.addItem(FilterHelper.getFilterConditionTVARVC("orderStatus",GanttServiceReference.getTVARVCSelection(currentView+TVARVCPreffixes.ORDER_SUFFIX),FilterCondition.EQUALS));
			filters.addItem(FilterHelper.getFilterConditionTVARVC("loadStatus",GanttServiceReference.getTVARVCSelection(currentView+TVARVCPreffixes.LOAD_SUFFIX),FilterCondition.EQUALS));
			//filters.addItem(FilterHelper.getFilterCondition("loadingDate",new ArrayCollection([FlexDateHelper.getDateString(baseDate,"YYYYMMDD")]),FilterCondition.EQUALS));
			
			// Si no esta la bandera, quiere decir que se tiene que filtrar
			
			if(!GanttServiceReference.getCollectFilterStatus()){
				
				filters.addItem(FilterHelper.getFilterConditionTVARVC("shipConditions",GanttServiceReference.getTVARVCSelection(TVARVCConstants.COLLECT_ORDER),FilterCondition.EQUALS));
			}
			
			var displPumpService:Boolean = GanttServiceReference.getParamNumber(ParamtersConstants.DISP_PUMP_DFE) != 0 ;
			if(!displPumpService){
				new OrderLoad().itemCategory;
				filters.addItem(FilterHelper.getFilterConditionTVARVC("itemCategory",GanttServiceReference.getTVARVCSelection(TVARVCConstants.PUMP_SERVICE),FilterCondition.NOT_EQUALS));
			}
			
			var lower:Date =  getDayStart();
			var higher:Date = getDayEnd();
			filters.addItem(FilterHelper.getFilterCondition("loadingTimestamp",new ArrayCollection([lower]),FilterCondition.GREATER_THAN));
			filters.addItem(FilterHelper.getFilterCondition("loadingTimestamp",new ArrayCollection([higher]),FilterCondition.LOWER_THAN));
			currentTransformer.setFilters(filters);
			if(currentTransformer is PlanningScreenDataTransformer){
				secondCurrentTransformer.setFilters(filters);
			}
			logger.info("setFilters()   init    "+(new Date()));
		}
		
		public function ordernar(e:MouseEvent):void {
			//view.gantt.resourceDataProvider.refresh();
			refreshTime.start();
		}
		
		/**
		 *  Esta funcion camia los parametros de la vista, de tal forma que 
		 * 
		 */
		public function changeViewHandler(e:Event):void{
			disableStack(false);
			franceShowSinglePlant=null;
			var fio:IFlashIslandEventObject;
			ServiceAgentScreenTask.franceTaskProcessed=[];
			PlanningScreenTreeResourceRenderer.mixerOrConveyor=new DictionaryMap();
			ServiceAgentScreenTask.franceTaskAcomodationRows=new DictionaryMap();
			PlanningScreenTask.franceTaskProcessed=[];
			PlanningScreenTask.franceTaskProcessedAux=[];
			PlanningScreenTask.franceTaskAcomodationRows=new DictionaryMap();
			PlanningScreenTask.franceTaskAcomodationRowsAux=new DictionaryMap();
			DispatcherIslandImpl.masterKey=true;
			var viewStack:ViewStack = e.target as ViewStack;
			var viewName:String = viewStack.selectedChild["name"];
			var arrPlantsTMP:ArrayCollection = new ArrayCollection();
			var arrPlantsTMP_plain:ArrayCollection = new ArrayCollection();
			
			currentView = viewName;
			
			if(view.viewStack.selectedIndex==4 || (view.viewStack.selectedIndex==1 && view.viewStack.numChildren==2)){
				//esconder botones
				 view.Options2.visible = false;
				fio = WDRequestHelper.getBringAllVehicles("X") as IFlashIslandEventObject;
				view.cboPlantSelector.dataProvider = null;
				arrPlantsTMP_plain = GanttServiceReference.getPlantsPlain();
				view.cboPlantSelector.dataProvider = arrPlantsTMP_plain;
				arrPlantsTMP_plain = GanttServiceReference.getPlants();
				setPlantSelected();
				//GanttServiceReference.dispatchIslandEvent(fio);
				if(GanttServiceReference.getPlants().length>1){
					//Alert.show("Planning screen can only work with one plant, please modify your plan selection criteria","Error");
					if(franceShowSinglePlant==null){
						pauseLoading=true;
						var choosePlantPopUp:IFlexDisplayObject=PopUpManager.createPopUp(this.view,ChooseOnePlantPopUp,true);
						PopUpManager.centerPopUp(choosePlantPopUp);
						return;
					} else {
						pauseLoading=false;
					}
					
				} else {
					franceShowSinglePlant=GanttServiceReference.getPlants().getItemAt(0).plantId;
					//setear item selected con planta seleccionada evh
					setPlantSelected();
				}
				
				/*if(franceShowSinglePlant!=null){
				GanttServiceReference.dispatchIslandEvent(WDRequestHelper.getSetPlanningPlant(franceShowSinglePlant));
				}*/
			} else {
				fio= WDRequestHelper.getBringAllVehicles("") as IFlashIslandEventObject;
				//aparecer botones
				view.Options2.visible = true;
				
			}
			/*			GanttServiceReference.dispatchIslandEvent(fio);*/
			//changeViews(viewName);
			requestRefresh();
			view.gantt.ganttSheet.visibleTimeRangeStart=FlexDateHelper.addSeconds(view.vehicleScheduleView.ganttSheet.visibleTimeRangeStart,3);
			view.vehicleScheduleView.ganttSheet.visibleTimeRangeStart=FlexDateHelper.addSeconds(view.vehicleScheduleView.ganttSheet.visibleTimeRangeStart,3);
			
			isPlain = false;
			
			if (currentView == TVARVCPreffixes.LOADS_PER_ORDER_VIEW_ID){
				
				view.LoadsPerHour.visible=true;
				
			}else{				
				
				view.LoadsPerHour.visible=false;
			}
			disableStack(false);
		}
		
		public function setPlantSelected():void{
			if (franceShowSinglePlant!=null&&franceShowSinglePlant!=""){
				var ccounter:Number=0;
				for each(var tempObject : Object in view.cboPlantSelector.dataProvider){
					if(tempObject.plantId == franceShowSinglePlant){
						view.cboPlantSelector.selectedIndex=ccounter;
						break;
					}
					ccounter++;
				}
			}				
		}
		
		public function togglePlainView(e:TogglePlaintViewEvent):void{
			isPlain = !isPlain;
			//changeViews(currentView);
			requestRefresh();
		}
		/**
		 * Esta funcion sirve para actualizar los transformers y DataPRoviders 
		 * al mismo tiempo, uno despues de otro
		 
		 private function updateTransformerDataProvider():void{
		 
		 }
		 */
		
		
		
		//public var currentView:String = LoadPerVehiclePlantVehicleDataTransformer.LOADS_PER_VEHICLE_VIEW_ID;
		
		
		/**
		 * Esta funcion sirve para asignarle los datos al Gantt
		 *  Utilizando el Tranformer que se haya seleccionado
		 * 
		 * 
		 */
		public static var gH:ResourceHierarchicalData;
		public var tmpRefresh:Boolean=false;
		private function assignDataProviders():void{
			logger.info("assignDataProviders()   init    "+(new Date()));
			var dataSources:Object =  new Object();
			var secondDataSources:Object=new Object();
			view.gantt.resourceDataProvider = new ArrayCollection();
			view.gantt.taskDataProvider = new ArrayCollection();
			var rstree:ArrayCollection=currentTransformer.getResourcesTree();
			var hd:ResourceHierarchicalData = new ResourceHierarchicalData(currentTransformer.getResourcesTree());
			var rstreeSecond:ArrayCollection;
			var hdSecond:ResourceHierarchicalData; 
			if((currentTransformer is PlanningScreenDataTransformer) && !tmpRefresh){
				rstreeSecond=secondCurrentTransformer.getResourcesTree();
				hdSecond= new ResourceHierarchicalData(secondCurrentTransformer.getResourcesTree());
				try{
					view.vehicleScheduleView.resourceDataProvider=new ArrayCollection();
					view.vehicleScheduleView.taskDataProvider = new ArrayCollection();
				} catch(e:Error){
					
				}
				
			}
			
			// se crea para un arbol
			gH=hd;
			hd.childrenField = "children";
			var sort:Sort=new Sort();
			sort.fields=[new SortField("children")];
			logger.info("setFilters()   init    "+(new Date()));
			
			dataSources.resources  = hd;
			dataSources.tasks  = currentTransformer.getTasksTree();
			currentTransformer.setColumns(view.gantt.dataGrid,view.gantt.ganttSheet);
			if((currentTransformer is PlanningScreenDataTransformer) && !tmpRefresh){
				secondDataSources.resources=hdSecond;
				secondDataSources.tasks=secondCurrentTransformer.getTasksTree();	
				secondCurrentTransformer.setColumns(view.vehicleScheduleView.dataGrid,view.vehicleScheduleView.ganttSheet);
			}
			
			view.gantt.resourceDataProvider = dataSources.resources;
			view.gantt.taskDataProvider = dataSources.tasks;
			if((currentTransformer is PlanningScreenDataTransformer) && !tmpRefresh){
				//try{
				if(secondDataSources.tasks.length>0){
					view.vehicleScheduleView.resourceDataProvider= secondDataSources.resources;
					//ordenaResourcesByStartTime(secondDataSources.resources, secondDataSources.tasks);//metodo ordena por taskview.vehicleScheduleView.resourceDataProvider = secondDataSources.resources//
				}else{
					view.vehicleScheduleView.resourceDataProvider = secondDataSources.resources; // lo hace como en un inicio
				}
				view.vehicleScheduleView.taskDataProvider = secondDataSources.tasks;
				//} catch(e:Error){
				
				//	}d
				
			}
			
			doRefreshOrder();
			logger.info("assignDataProviders()   init    "+(new Date()));
		}
		
		private var refreshTime:Timer = new Timer(100,1);
		public function doRefresh(e:TimerEvent):void{
			//doRefreshOrder();
		}
		
		//public var isDataLoaded:Boolean = false;
		public function doRefreshOrder():void{
			view.gantt.resourceDataProvider.refresh();
			try {
				
			}
			catch (e:*){
				logger.error(e);
				throw e;
			}
			view.gantt.dataGridBack.invalidateDisplayList();
			view.gantt.dataGridBack.invalidateList();
			//}
		}
		
		
		//ordena por startTime y regresa el mismo ResourceHierarchicalData
		
		public function ordenaResourcesByStartTime(resource:ResourceHierarchicalData ,task:ArrayCollection ):ResourceHierarchicalData{
			var hdOrdenado:ResourceHierarchicalData; 
			//var iterator:IViewCursor = resource.source.createCursor(); //iterator
			var arrayTemp:ArrayCollection= new ArrayCollection();
			
			for ( var j:int=0 ; j<resource.source.length;j++ ){ //itera los resources
				var min:Number=0;
				var sortr:PlanningScreenResource= new PlanningScreenResource();
				
				var index:int=0;
				var cont:int= 0 
				for ( var i:int=0 ; i<task.length;i++ ){// itera las task 
					if(resource.source.getItemAt(j)['equipNumber']== task.getItemAt(i).payload.equipNumber){ // si tienen el mismo equipNumber son cargas planeadas para ese vehiculo
						if(cont==0)	{ //flag para compara el primer elemento y de ahi partir con el minimo
							min = task.getItemAt(i).startTime.getTime();
							index= i
							cont++	//solo saca del flag
						}else{
							if(task.getItemAt(i).startTime.getTime()<min){// las siguientes veces compara el minimo startTime de las cargas asignadas a un vehiculo 
								min = task.getItemAt(i).startTime.getTime();//asigna si es el menor y se guarda para comparara  startTime.getTime()<min
								index= i;// guarda indice 
							}
							
						}
					}
				} 
				if(resource.source.getItemAt(j)['equipNumber']== task.getItemAt(index).payload.equipNumber){ // al  mismo resource le agrega el valor taskMin para hacerle el order 
					resource.source.getItemAt(j).taskMin=task.getItemAt(index).startTime;  
					
				}
				
			}	
			
			
			var dataSortField:SortField = new SortField("taskMin",false,false,false);
			//dataSortField.numeric
			var dataSort:Sort = new Sort();
			dataSort.fields = [dataSortField];
			resource.source.sort = dataSort;
			//refresh the collection to sort
			resource.source.refresh();
			resource.source.sort=null;
			return  new ResourceHierarchicalData(resource.source)
		}
		
		
		
		var startDate:Date 
		var endDate:Date
		
		public function saveRange():void{
			if (view.viewStack.enabled && viewAlreadyLoaded){
				startDate = view.gantt.ganttSheet.visibleTimeRangeStart;
				endDate =view.gantt.ganttSheet.visibleTimeRangeEnd;
				TimeDispatcher.setFinalZoom(endDate);
				TimeDispatcher.setInitialZoom(startDate);
				visibleRangeDate[0]=view.gantt.ganttSheet.minVisibleTime;
				visibleRangeDate[1]=view.gantt.ganttSheet.maxVisibleTime;
			}
		}
		
		public function setRangeVisible():void{
			
			/*var range:SharedObject = SharedObject.getLocal("rangeVisible","/");
			if(range != null ){*/
			if(startDate!=null){
				//view.gantt.ganttSheet.taskVisibleTimeRangeStartFunction
				/*view.gantt.ganttSheet.visibleTimeRangeStart= startDate ;
				view.gantt.ganttSheet.visibleTimeRangeEnd = endDate;
				if(currentTransformer is PlanningScreenDataTransformer){
					view.vehicleScheduleView.ganttSheet.visibleTimeRangeStart=startDate;
					view.vehicleScheduleView.ganttSheet.visibleTimeRangeEnd= endDate;
					
				}*/
			}
		}
		
		
		
	}
	
}

