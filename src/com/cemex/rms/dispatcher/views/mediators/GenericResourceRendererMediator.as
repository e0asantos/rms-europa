package com.cemex.rms.dispatcher.views.mediators

{
	import mx.core.UIComponent;
	import mx.events.ToolTipEvent;
	import mx.managers.ToolTipManager;
	import mx.controls.Alert;
	import org.robotlegs.mvcs.Mediator;
	
	
	public class GenericResourceRendererMediator extends Mediator
	{
		public function GenericResourceRendererMediator()
		{
			super();
		}
			
			import flash.events.MouseEvent;
		public function getView():UIComponent{
			throw new Error("Este metodo debe ser Sobreescrito");
		}
		
		public function clickDato(e:MouseEvent):void{
			processClickDato();
		}
		public function clickAction(e:MouseEvent):void{
			processClickAction();
		}
		public function processClickAction():void{
			Alert.show("Actions:view.toolTip\n");
		}
		
		public function processClickDato():void{
			Alert.show("DATOS:view.toolTip\n");
		}
		
		
		
		override public function onRegister():void {
			
			//getView()["dato"].addEventListener(MouseEvent.CLICK,clickDato);
			//getView()["actions"].addEventListener(MouseEvent.CLICK,clickAction);
			
	
			getTooltiper().addEventListener(ToolTipEvent.TOOL_TIP_SHOW,tooltipShow);
			getTooltiper().addEventListener(ToolTipEvent.TOOL_TIP_START,tooltipStart);
			getTooltiper().toolTip="X";
			
				
		}
		
		
		public function getTooltiper():UIComponent{
			return getView();
		}
		
		public function tooltipStart(e:ToolTipEvent):void{
			
			if (getGanttResource() != null){
				var labelField:String = getLabeField();
				if (getTooltiper() != null && labelField != null){
					getTooltiper().toolTip = showTooltip(labelField);
				}
			}
		}
		
		public function getLabeField():String{
			if (getGanttResource() != null){
				 return getGanttResource()["labelField"];
			}
			return null;
		}
		public function showTooltip(field:String):String{
			
			return "Generic tooltip for ("+field+")";			
		}
		
		
		public function tooltipShow(e:ToolTipEvent):void{
			if (getTooltiper() != null){
				e.toolTip.text = getTooltiper().toolTip ;
			}
		}
		
		
		
		
		
		protected function getGanttResource():Object {
			if (getView()["data"] != null){
				return getView()["data"];
			}
			return null;
		}
		
	}
}