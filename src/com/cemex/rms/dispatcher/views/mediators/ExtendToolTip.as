package com.cemex.rms.dispatcher.views.mediators
{
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.utils.Timer;
	
	import mx.containers.HBox;
	import mx.containers.VBox;
	import mx.controls.Alert;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.Spacer;
	import mx.controls.Text;
	import mx.core.Application;
	import mx.core.IToolTip;
	import mx.core.UIComponent;
	import mx.managers.ISystemManager;
	import mx.managers.PopUpManager;
	import mx.managers.ToolTipManager;
	
	public class ExtendToolTip extends HBox implements IToolTip 
	{
		
		
		private var image:Bitmap = new Bitmap();
		private var lbl:Label;
		private var imageHolder:Image;
		private var tipText:String;
		public var mouseOverToolTipFlag:Boolean=false;
		
		
		
		[Bindable]
		public function set ImageTip(img:*):void{
			if(img is Class){
				imageHolder.source = new img().bitmapDat;
			}
			if(img as String){
				imageHolder.load(img);
			}
		}
		
		[Bindable]
		public function set TipText(txt:String):void{
			lbl.text = txt;
		}
		public function get TipText():String{
			return tipText;
		}
		private var spacer:Spacer;
		
		public function ExtendToolTip()
		{
			imageHolder = new Image();
			lbl  = new  Text();
			spacer= new Spacer();
			this.addEventListener(MouseEvent.MOUSE_OVER, toolTipMouseOver);
			this.addEventListener(MouseEvent.MOUSE_OUT, toolTipMouseOut);
			/*mouseEnabled = false;
			mouseChildren = false;
			backgroundAlpha: .7;*/
			cornerRadius: 5;
			color: 0x00000;
			setStyle("backgroundColor",0x000000)
			setStyle("backgroundAlpha",0.7);
			setStyle("color",0xffffff);
			imageHolder.addEventListener(MouseEvent.CLICK, mouseClik)
			imageHolder.source = image;
			spacer.height= (this.height-imageHolder.height)/2
			
			//	imageHolder.addEventListener(MouseEvent.MOUSE_OVER, mouseOver)
			addChild(spacer);
			addChild(lbl);
			addChild(imageHolder);
		}
		
		public var popup:titleWindow
		private function mouseClik(event:MouseEvent):void{
			//if(popup==null){
			//popup = new titleWindow();
			PopUpManager.createPopUp(Application.application as DisplayObject,titleWindow,true) as titleWindow
			PopUpManager.centerPopUp(UIComponent(this.parentApplication) )
			//
			
			
			
		}
		
		public function get text():String
		{
			return null;
		}
		
		[Bindable]
		public function set text(value:String):void    {
			tipText = value;
		}
		
		public function toolTipMouseOver(event:MouseEvent):void{
			mouseOverToolTipFlag=true;
			
		}
		
		public function toolTipMouseOut(event:MouseEvent):void{
			mouseOverToolTipFlag=false;
		}
		
		private var tm:Timer= new Timer(300);
		public var haytooltip:Boolean=false
		public function createCustomTooltip(imageToolTip:ExtendToolTip):void{
			
			mouseOverToolTipFlag= true;
			haytooltip=true;
			var context:UIComponent=null;
			var pos:Point;
			var sm:ISystemManager = context ?
				context.systemManager as ISystemManager:
				Application.application.systemManager as ISystemManager;
			sm.topLevelSystemManager.addChildToSandboxRoot("toolTipChildren", imageToolTip as DisplayObject);
			imageToolTip.move(contentMouseX+11  ,contentMouseY+22);
			tm.addEventListener(TimerEvent.TIMER , removeAfter)
			tm.start();
		}
		
		private function removeAfter(event:TimerEvent):void{
			if (!mouseOverToolTipFlag  ){
				destroyToolTip();
				tm.stop();
			}
			
			
		}
		
		
		public function destroyToolTip():void{
			haytooltip=false;
			var sm:ISystemManager = this.systemManager as ISystemManager;
			//	imageToolTip.systemManager as ISystemManager;
			sm.topLevelSystemManager.removeChildFromSandboxRoot("toolTipChildren", DisplayObject(this));
			//this=null;
			/* 	validateNow();
			validateDisplayList(); */
			ToolTipManager.enabled=true;
		}
		
	}
	
	
	
	
}