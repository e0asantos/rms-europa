package com.cemex.rms.dispatcher.views.mediators
{
	import com.cemex.rms.common.date.FlexDateHelper;
	import com.cemex.rms.common.flashislands.AbstractFlashIslandService;
	import com.cemex.rms.common.gantt.GanttTask;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.helpers.GanttTaskMoveHelper;
	import com.cemex.rms.dispatcher.helpers.ILogDateFormatterHelper;
	import com.cemex.rms.dispatcher.services.flashislands.events.FlashIslandUnfreezedEvent;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandImpl;
	import com.cemex.rms.dispatcher.views.GanttDispatcherView;
	
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import mx.controls.Alert;
	import mx.core.UIComponent;
	
	import ilog.core.DataItem;
	import ilog.gantt.GanttSheetUtils;
	import ilog.gantt.TaskItem;
	import ilog.gantt.TimeControllerGetter;
	import ilog.utils.TimeUnit;
	
	import org.robotlegs.mvcs.Mediator;
	
	/**
	 * 
	 * GanttDispatcherViewMediator Es la clase que sirve de mediador con la interfaz grafica del gantt
	 * Esta clase se encarga de hacer todas las customizaciones del iLog
	 *  
	 * 
	 */
	public class GanttDispatcherViewMediator extends Mediator
	{
		
		[Inject]
		public var view: GanttDispatcherView;
		
		public function GanttDispatcherViewMediator()
		{
			super();
		}
		
		
		override public function onRegister():void {
					
			
			//ToolTipManager.hideDelay;
			//ToolTipManager.scrubDelay = 2000;
			
			//view.ganttSheet.addEventListener(
			
			
			view.ganttSheet.moveEnabledFunction=moveEnabledFunction;
			view.ganttSheet.reassignEnabledFunction=reassignEnabledFunction;
			view.ganttSheet.resizeEnabledFunction=resizeEnabledFunction;
			view.ganttSheet.commitItemFunction=commitItemFunction;
				
			// SE deshabilitan los tooltips
			view.ganttSheet.showDataTips = false;
			view.ganttSheet.dataTipFunction=customDataTip;
			view.ganttSheet.editingTipFunction=customDataTip;
			
			view.ganttSheet.minZoomFactor= TimeUnit.SECOND.milliseconds;;
			view.ganttSheet.maxZoomFactor=TimeUnit.DAY.milliseconds;
			
			view.timeScale.majorLabelFunction=dateFormatter;
			view.timeScale.minorLabelFunction=dateFormatter;
			eventDispatcher.addEventListener(FlashIslandUnfreezedEvent.FLASHISLAND_UNFREEZED_ACKNOWLEDGE,unfreezed);
			
			ganttSheetUtils = new GanttSheetUtils(view.ganttSheet);
			
			
		}
		
		public function unfreezed(e:FlashIslandUnfreezedEvent):void{
			view.ganttSheet.enabled = true;
			ganttSheetUtils.releaseDragDrop();
			ganttSheetUtils.releaseDragDrop();
			//Alert.show("unfreezed");
			AbstractFlashIslandService.disableAppWhileEvent = temp;
			
		}
		
		private var ganttSheetUtils:GanttSheetUtils;
		private function customDataTip(event:DataItem):String {
			var taskItem:TaskItem = event as TaskItem;
			var tooltip:String = "e\n";
			
			if (!taskItem) {
				return null;
			}
			var task:GanttTask = taskItem.data as GanttTask;
			// SE regresa null para que no se tenga el tooltip durante la edicion
			//return GanttTaskRendererHelper.getTaskTooltip(task);
			return null;
		}
			
			
	
		private static var temp:Boolean = AbstractFlashIslandService.disableAppWhileEvent;
		private function commitItemFunction(data:DataItem):void{
			if (data is TaskItem){
				var event:Event = GanttTaskMoveHelper.commitItem(data as TaskItem);
				if (event != null && DispatcherViewMediator.currentView!=TVARVCPreffixes.PLANNING_SCREEN_VIEW_ID){
					//temp = AbstractFlashIslandService.disableAppWhileEvent;
					AbstractFlashIslandService.disableAppWhileEvent = false;
					dispatch(event);
					//AbstractFlashIslandService.disableAppWhileEvent = temp;
					//ganttSheetUtils.releaseDragDrop();
				}
			}
		}
	
		/**
		 * aumentar tamaño 
		 */
		private function resizeEnabledFunction(data:DataItem):Boolean {
			return false;
		}
		
		/**
		 * Movimiento vertical 
		 */
		private function reassignEnabledFunction(data:DataItem):Boolean{
			var result:Boolean = false;
			
			if (data is TaskItem){
				
				result = GanttTaskMoveHelper.getReassign(data as TaskItem,eventDispatcher);
			}
			return result;
		}
		
	
		
		/**
		 * Movimiento horizontal 
		 */
		private function moveEnabledFunction(data:DataItem):Boolean{
			var result:Boolean = false;
			
			if (data is TaskItem){
				result = GanttTaskMoveHelper.getMove(data as TaskItem,eventDispatcher);
				
			}
			return result;
		}
		
		
		private function dateFormatter(date:Date,dateFormat:String,timeUnit:TimeUnit,timeUnitSteps:Number):String
		{
			return ILogDateFormatterHelper.formatDateComplete(date, dateFormat);
		}
		
	}
}