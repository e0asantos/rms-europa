package com.cemex.rms.dispatcher.views.factory
{
	import flash.net.SharedObject;

	public class TimeDispatcher
	{
		private static var so:SharedObject=SharedObject.getLocal("rmsZoom","/");
		public static function setInitialZoom(value:Date):void{
			so.data["inicial"]=value;
			so.flush();
		}
		
		public static function clearZoom():void{
			so.data["inicial"]=null;
			so.data["final"]=null;
			so.flush();
		}
		
		public static function setFinalZoom(value:Date):void{
			so.data["final"]=value;
			so.flush();
		}
		
		public static function getInitialZoom(value:Date):Date{
			//value parameter is the limit, if we are below the limit then return the limit itself
			if(so.data["inicial"]!=null){
				if(so.data["inicial"].time>=value.time){
					return so.data["inicial"];
				} else {
					//the date is older than expected
					setInitialZoom(value);
					return value;
				}
			} else {
				setInitialZoom(value);
				return value;
			}
		}
		
		public static function getFinalZoom(value:Date):Date{
			if(so.data["final"]!=null){
				if(so.data["final"].time<=value.time){
					return so.data["final"];
				} else {
					//the date is older than expected
					setFinalZoom(value);
					return value;
				}
			} else {
				setFinalZoom(value);
				return value;
			}	
		}
		
		public static function getCurrentZoom():String{
			if (so.data["inicial"]!=null){
				return so.data["inicial"].toString()+"/"+so.data["final"].toString();
			} else {
				return "NOT";
			}
		}
	}
}