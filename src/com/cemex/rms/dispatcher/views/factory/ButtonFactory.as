package com.cemex.rms.dispatcher.views.factory
{
	import com.cemex.rms.dispatcher.constants.TVARVCConstants;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Button;

	public class ButtonFactory
	{
		
		/*[Bindable]
		[Embed(source="../../../../../../assets/s_b_plnt_centr.gif")]
		public static var plant:Class;*/
		
		[Embed(source="../../../../../../assets/s_b_plnt_distr.gif")]            
		[Bindable]             
		public static var plant_distribuida:Class;
		
		[Embed(source="../../../../../../assets/s_b_plnt_centr.gif")]            
		[Bindable]             
		public static var plant_centralizada:Class;
		
		[Embed(source="../../../../../../assets/s_b_plnt_auton.gif")]            
		[Bindable]             
		public static var plant_autonoma:Class;
		
		[Embed(source="../../../../../../assets/s_s_wspo_green.png")]            
		[Bindable]             
		public static var order_green:Class;
		
		[Embed(source="../../../../../../assets/s_b_plnt_centr_stop.png")]            
		[Bindable]             
		public static var plant_centralizada_stop:Class;
		
		[Embed(source="../../../../../../assets/s_b_plnt_distr_closed2.png")]            
		[Bindable]             
		public static var plant_distribuida_stop:Class;
		
		
		
		[Embed(source="../../../../../../assets/s_b_trns_green.gif")]            
		[Bindable]             
		public static var vehicle_green:Class;
		
		[Embed(source="../../../../../../assets/s_b_trns_red.gif")]            
		[Bindable]             
		public static var vehicle_red:Class;
		
		public static function createPlantButton(labelString:String):Button{
			var btn:Button=new Button();
			btn.label=labelString;
			
			
			var plantId:String = labelString;
			var plantType:String = GanttServiceReference.getPlantType(plantId);
			var allothem:String=GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_D).low+"_"+GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_C).low+"_"+GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_A).low;
			var plantFull:ArrayCollection=GanttServiceReference.getPlants();
			var currPlant:Object;
			for(var vi:int=0;vi<plantFull.length;vi++){
				if(plantId==plantFull.getItemAt(vi).plantId){
					currPlant=plantFull.getItemAt(vi);
				}
			}
			if (plantType == "01" && currPlant.status=="E0001"){
				btn.setStyle("icon",plant_centralizada);
			} else if(plantType == "01" && currPlant.status=="E0002"){
				btn.setStyle("icon",plant_distribuida_stop);
			} else if(plantType=="01" && currPlant.status=="E0003"){
				btn.setStyle("icon",plant_centralizada_stop);
			} else if (Number(plantType) == Number(GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_D).low)){
				btn.setStyle("icon",plant_distribuida);	
			}
			else if (Number(plantType) == Number(GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_C).low)){
				btn.setStyle("icon",plant_centralizada);
			}
			else if (Number(plantType) == Number(GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_A).low)){
				btn.setStyle("icon",plant_autonoma);
			}
			
			btn.setStyle("styleName","sapButtonStyle");
			btn.height=17;
			btn.width=70;
			return btn;
		}
		
		public static function createShowAssigned(labelString:String):Button{
			var btn:Button=new Button();
			btn.label=labelString;
			btn.name="assn";
			btn.setStyle("icon",vehicle_red);
			return btn;
		}
		
		public static function createShowAvailable(labelString:String):Button{
			var btn:Button=new Button();
			btn.name="avlb";
			btn.label=labelString;
			btn.setStyle("icon",vehicle_green);
			return btn;
		}
	}
}