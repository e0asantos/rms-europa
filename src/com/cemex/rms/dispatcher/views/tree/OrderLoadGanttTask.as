package com.cemex.rms.dispatcher.views.tree
{
	import com.cemex.rms.common.gantt.GanttTask;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	public class OrderLoadGanttTask extends GanttTask
	{
		public function OrderLoadGanttTask()
		{
			super();
		}
		
		public function get payload():OrderLoad{
			return _data as OrderLoad;
		}
		public function set payload(_payload:OrderLoad):void{
			this._data = _payload;
		}
	}
}