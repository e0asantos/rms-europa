package com.cemex.rms.dispatcher.views.planningScreen.tree
{
	import com.cemex.rms.common.date.FlexDateHelper;
	import com.cemex.rms.common.utils.DictionaryMap;
	import com.cemex.rms.dispatcher.constants.OTRConstants;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.helpers.StatusHelper;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandImpl;
	import com.cemex.rms.dispatcher.views.planningScreen.renderers.PlanningScreenResourceRenderer;
	import com.cemex.rms.dispatcher.views.planningScreen.renderers.PlanningScreenTaskRenderer;
	import com.cemex.rms.dispatcher.views.planningScreen.renderers.PlanningScreenTreeResourceRenderer;
	import com.cemex.rms.dispatcher.views.serviceAgentScreen.renderers.ServiceAgentScreenResourceRenderer;
	import com.cemex.rms.dispatcher.views.tree.OrderLoadGanttTask;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import mx.collections.ArrayCollection;
	
	public class PlanningScreenTask extends OrderLoadGanttTask
	{
		public static var franceTaskAcomodationRows:DictionaryMap=new DictionaryMap();
		public static var franceTaskProcessed:Array=[];
		public static var franceTaskAcomodationRowsAux:DictionaryMap=new DictionaryMap();
		public static var franceTaskProcessedAux:Array=[];
		public static var singlePositions:DictionaryMap=new DictionaryMap();
		public function PlanningScreenTask()
		{
			super();
			//create the special france effect
		}
		private var _view:PlanningScreenTaskRenderer;
		public function setView(_view:PlanningScreenTaskRenderer):void{
			this._view = _view;
			initExtraValues();
		}
		
		
		public function get view ():PlanningScreenTaskRenderer{
			return _view;
		}
		public override function get startTime():Date{
			if (payload == null){
				return null;
			}
			/*
			if (payload.startTime == null ){
			payload.startTime = getRawStartTime();
			}*/
			return payload.startTime;
		}
		
		public override function set startTime(_startTime:Date):void{
			if (payload != null){
				payload.startTime = _startTime;
			}
		}
		public override function get endTime():Date {
			if (payload == null){
				return null;
			}
			return getRawEndTime();
		}
		public override function set endTime(_endTime:Date):void{
			if (payload != null){
				payload.endTime = _endTime;
			}
		}
		
		public function get vehicleNumber():String {
			if (payload == null){
				return null;
			}
			return "-";
		}
		
		/*
		public override function getRawStartTime():Date{
		return getStartTime();
		}
		public override function getRawEndTime():Date{
		return getEndTime();
		}
		*/
		
		
		public override function getRawStartTime():Date{
			//var payload:OrderLoad = data as OrderLoad;
			
			//return FlexDateHelper.parseStringTime(payload.loadingDate,payload.loadingTime);
			
			return FlexDateHelper.parseTime(payload.loadingDate,payload.loadingTime);
			
			//return FlexDateHelper.parseTime(FlexDateHelper.getDateObject(payload.schedLineDate),payload.loadingTime);
		}
		public override function getRawEndTime():Date{
			//var payload:OrderLoad = data as OrderLoad;
			var payload:OrderLoad = data as OrderLoad;
			
			var fecha:Date = FlexDateHelper.addSeconds(startTime,payload.cycleTimeMillis);
			var fechaNow:Date=FlexDateHelper.addMinutes(startTime,GanttServiceReference.getLoadMinutes());
			
			return fecha;
		}
		
		public function get orderNumber():String{
			return payload.orderNumber;
		}
		public function get valueAddedCP():String {
			var result:String = "";
			if (payload.valueAdded != null){
				result = payload.valueAdded;	
			}
			if(payload.constructionProduct != null){
				if (result != ""){
					result += "-";
				}
				result += payload.constructionProduct;
			}
			return result;
		}
		
		public static var globalConveyorCounter:Number=1;
		public override function initExtraValues():void {
			super.initExtraValues();
			
			
			
			if (payload == null){
				return ;
			}
			payload.startTime = getRawStartTime();
			
			
			if (StatusHelper.isRenegotiated(payload)){
				renegotiationLabel = GanttServiceReference.getLabel(OTRConstants.RENEGOTIATION_LABEL);
			}
			
			if (StatusHelper.isConstructionProduct(payload)){
				cpWidth = "100%";
				pumpWidth = "0";
				loadWidth = "0";
				
			}
			else if (StatusHelper.isPumpingService(payload)){
				cpWidth = "0";
				pumpWidth = "100%";
				loadWidth = "0";			
			}
			else  {
				cpWidth = "0";
				pumpWidth =  "0";
				loadWidth = "100%";
			}
			//para determinar si el llamado es desde el planning (arriba)
			if(DispatcherIslandImpl.planningScreenLoads.get(payload.orderNumber+":"+payload.itemNumber)!=null){
				//overlap en PS
				if(franceTaskProcessedAux.indexOf(payload.orderNumber+payload.loadNumber)==-1){
					//var numOrder:String=payload.orderNumber;
					var itemNumber:String=payload.itemNumber;
					var currentOrderArray:Array=franceTaskAcomodationRowsAux.get(payload.equipNumber);
					var currentOrderVehicle:Array=PlanningScreenTreeResourceRenderer.mixerOrConveyor.get(payload.equipNumber);
					var horariosVehic:Array=franceTaskAcomodationRowsAux.get(payload.equipNumber);
					if(currentOrderVehicle==null){
						//PlanningScreenTreeResourceRenderer.mixerOrConveyor.put(payload.orderNumber,new Array());
						//currentOrderVehicle=PlanningScreenTreeResourceRenderer.mixerOrConveyor.get(payload.orderNumber);
					}
					if(horariosVehic==null){
						franceTaskAcomodationRowsAux.put(payload.equipNumber,new Array());
						horariosVehic=franceTaskAcomodationRowsAux.get(payload.equipNumber);
					}
					var isDisplayed:Boolean=false;
					var sePuedeAgregar:Boolean = false;
					var ll:Number = 0;
					for(var l:int=0;l<horariosVehic.length;l++){
						for (var countHV:Number=0;countHV<horariosVehic[l].length;countHV++){
							if(getRawStartTime().time>horariosVehic[l][countHV][1] ||
								(getRawStartTime().time<horariosVehic[l][countHV][1] && getRawEndTime().time<horariosVehic[l][countHV][0])){
								//create new row
								
								singlePositions.put(payload.plant+":"+payload.equipNumber+":"+payload.itemNumber,l);

								sePuedeAgregar = true;
								isDisplayed=true;
							}
							else{
								sePuedeAgregar = false;
								break;
							}
						}
						if (sePuedeAgregar){
							ll=l;
							break;
						}
					}
					if (sePuedeAgregar){
						payload.indexPositionAux=ll;
						horariosVehic[ll].push([getRawStartTime().time,getRawEndTime().time]);
						isDisplayed=true;
					}else{
						horariosVehic.push(new Array([getRawStartTime().time, getRawEndTime().time]));
						payload.indexPositionAux=horariosVehic.length-1;
						isDisplayed=true;
					}					
					if(!isDisplayed){
						horariosVehic.push([getRawStartTime().time, getRawEndTime().time])
						//currentOrderArray.push(getRawEndTime().time);
						//payload.indexPositionAux=currentOrderArray.length-1;
						payload.indexPositionAux=horariosVehic.length-1;
						singlePositions.put(payload.plant+":"+payload.equipNumber+":"+payload.itemNumber,horariosVehic.length-1);
						var addserv:ArrayCollection=payload.addProd;
						//currentOrderVehicle.push("Fictive Mixer "+globalConveyorCounter.toString());
						
						//globalConveyorCounter++;
					}
					/*					if(currentOrderArray.length==0){
					currentOrderArray.push(getRawEndTime().time);
					var addserv:ArrayCollection=payload.addProd;
					//currentOrderVehicle.push("Fictive Mixer "+globalConveyorCounter.toString());
					for(var addi:int=0;addi<addserv.length;addi++){
					if(addserv.getItemAt(addi).conveyor!="" && addserv.getItemAt(addi).itemNumber==payload.itemNumber){
					//currentOrderVehicle[currentOrderVehicle.length-1]="Fictive Conveyor "+globalConveyorCounter.toString();
					break;
					}
					}
					//globalConveyorCounter++;
					}*/
					franceTaskProcessedAux.push(payload.orderNumber+payload.loadNumber);
					PlanningScreenResourceRenderer.expandableItems.put(payload.equipNumber,horariosVehic.length);
				}			
				
				
				
			}else{
				
				if(franceTaskProcessed.indexOf(payload.orderNumber+payload.loadNumber)==-1){
					var numOrder:String=payload.orderNumber;
					var itemNumber:String=payload.itemNumber;
					var currentOrderArray:Array=franceTaskAcomodationRows.get(payload.orderNumber);
					var currentOrderVehicle:Array=PlanningScreenTreeResourceRenderer.mixerOrConveyor.get(payload.orderNumber);
					if(currentOrderVehicle==null){
						PlanningScreenTreeResourceRenderer.mixerOrConveyor.put(payload.orderNumber,new Array());
						currentOrderVehicle=PlanningScreenTreeResourceRenderer.mixerOrConveyor.get(payload.orderNumber);
					}
					if(currentOrderArray==null){
						franceTaskAcomodationRows.put(payload.orderNumber,new Array());
						currentOrderArray=franceTaskAcomodationRows.get(payload.orderNumber);
					}
					var isDisplayed:Boolean=false;
					for(var l:int=0;l<currentOrderArray.length;l++){
						if(getRawStartTime().time>currentOrderArray[l]){
							//create new row
							payload.indexPosition=l;
							singlePositions.put(payload.plant+":"+payload.orderNumber+":"+payload.itemNumber,l);
							currentOrderArray[l]=getRawEndTime().time;
							var addserv:ArrayCollection=payload.addProd;
							currentOrderVehicle[l]=GanttServiceReference.getLabel(OTRConstants.FICTIVE_MIXER)+" ";
							for(var addi:int=0;addi<addserv.length;addi++){
								if(addserv.getItemAt(addi).conveyor!="" && addserv.getItemAt(addi).itemNumber==payload.itemNumber){
									currentOrderVehicle[l]=GanttServiceReference.getLabel(OTRConstants.FICTIVE_CONVEYOR)+" ";;
									break;
								}
							}
							globalConveyorCounter++;
							//vemos si es conveyor por los add serv
							isDisplayed=true;
							break;
						} /*else if(getRawStartTime().time<currentOrderArray[l]){
						
						break;
						}*/
					}
					if(!isDisplayed){
						currentOrderArray.push(getRawEndTime().time);
						payload.indexPosition=currentOrderArray.length-1;
						singlePositions.put(payload.plant+":"+payload.orderNumber+":"+payload.itemNumber,currentOrderArray.length-1);
						var addserv:ArrayCollection=payload.addProd;
						currentOrderVehicle.push(GanttServiceReference.getLabel(OTRConstants.FICTIVE_MIXER)+" ");
						for(var addi:int=0;addi<addserv.length;addi++){
							if(addserv.getItemAt(addi).conveyor!="" && addserv.getItemAt(addi).itemNumber==payload.itemNumber){
								currentOrderVehicle[currentOrderVehicle.length-1]=GanttServiceReference.getLabel(OTRConstants.FICTIVE_CONVEYOR)+" ";
								break;
							}
						}
						globalConveyorCounter++;
					}
					if(currentOrderArray.length==0){
						currentOrderArray.push(getRawEndTime().time);
						var addserv:ArrayCollection=payload.addProd;
						currentOrderVehicle.push(GanttServiceReference.getLabel(OTRConstants.FICTIVE_MIXER)+" ");
						for(var addi:int=0;addi<addserv.length;addi++){
							if(addserv.getItemAt(addi).conveyor!="" && addserv.getItemAt(addi).itemNumber==payload.itemNumber){
								currentOrderVehicle[currentOrderVehicle.length-1]=GanttServiceReference.getLabel(OTRConstants.FICTIVE_CONVEYOR)+" ";
								break;
							}
						}
						globalConveyorCounter++;
					}
					franceTaskProcessed.push(payload.orderNumber+payload.loadNumber);
					ServiceAgentScreenResourceRenderer.expandableItems.put(payload.orderNumber,currentOrderArray.length);
				}
			}
		}
		
		
		public var cpLabel:String;
		
		[Bindable]
		public var pumpWidth:String = "0";
		
		[Bindable]
		public var loadWidth:String = "0";
		
		[Bindable]
		public var cpWidth:String = "0";
	}
}