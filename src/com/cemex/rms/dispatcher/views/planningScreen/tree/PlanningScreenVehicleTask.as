package com.cemex.rms.dispatcher.views.planningScreen.tree
{
	import com.cemex.rms.common.date.FlexDateHelper;
	import com.cemex.rms.common.tree.NodeTask;
	import com.cemex.rms.common.utils.DictionaryMap;
	import com.cemex.rms.dispatcher.constants.OTRConstants;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.helpers.StatusHelper;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandImpl;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	import com.cemex.rms.dispatcher.views.planningScreen.renderers.PlanningScreenResourceRenderer;
	import com.cemex.rms.dispatcher.views.planningScreen.renderers.PlanningScreenTaskRenderer;
	import com.cemex.rms.dispatcher.views.planningScreen.renderers.PlanningScreenTreeResourceRenderer;
	import com.cemex.rms.dispatcher.views.planningScreen.renderers.PlanningScreenVehicleTaskRenderer;
	import com.cemex.rms.dispatcher.views.serviceAgentScreen.renderers.ServiceAgentScreenResourceRenderer;
	import com.cemex.rms.dispatcher.views.serviceAgentScreen.renderers.ServiceAgentScreenTaskRenderer;
	import com.cemex.rms.dispatcher.views.tree.OrderLoadGanttTask;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import mx.collections.ArrayCollection;
	
	public class PlanningScreenVehicleTask extends OrderLoadGanttTask
	{
		public static var franceTaskAcomodationRows:DictionaryMap=new DictionaryMap();
		public static var franceTaskProcessed:Array=[];
		public static var singlePositions:DictionaryMap=new DictionaryMap();
		public function PlanningScreenVehicleTask()
		{
			super();
			//create the special france effect
		}
		private var _view:PlanningScreenVehicleTaskRenderer;
		public function setView(_view:PlanningScreenVehicleTaskRenderer):void{
			this._view = _view;
			initExtraValues();
		}
		public override function get startTime():Date{
			if (payload == null){
				return null;
			}
			/*
			if (payload.startTime == null ){
			payload.startTime = getRawStartTime();
			}*/
			return payload.startTime;
		}
		
		public override function set startTime(_startTime:Date):void{
			if (payload != null){
				payload.startTime = _startTime;
			}
		}
		public override function get endTime():Date {
			if (payload == null){
				return null;
			}
			return getRawEndTime();
		}
		public override function set endTime(_endTime:Date):void{
			if (payload != null){
				payload.endTime = _endTime;
			}
		}
		
		public function get vehicleNumber():String {
			if (payload == null){
				return null;
			}
			return "-";
		}
		
		/*
		public override function getRawStartTime():Date{
		return getStartTime();
		}
		public override function getRawEndTime():Date{
		return getEndTime();
		}
		*/
		
		
		public override function getRawStartTime():Date{
			//var payload:OrderLoad = data as OrderLoad;
			
			//return FlexDateHelper.parseStringTime(payload.loadingDate,payload.loadingTime);
			
			return FlexDateHelper.parseTime(payload.loadingDate,payload.loadingTime);
			
			//return FlexDateHelper.parseTime(FlexDateHelper.getDateObject(payload.schedLineDate),payload.loadingTime);
		}
		public override function getRawEndTime():Date{
			//var payload:OrderLoad = data as OrderLoad;
			//return FlexDateHelper.addMinutes(startTime,GanttServiceReference.getLoadMinutes());
			var payload:OrderLoad = data as OrderLoad;
			
			var fecha:Date = FlexDateHelper.addSeconds(startTime,payload.cycleTimeMillis);
			var fechaNow:Date=FlexDateHelper.addMinutes(startTime,GanttServiceReference.getLoadMinutes());
			
			return fecha;
		}
		
		
		public function get valueAddedCP():String {
			var result:String = "";
			if (payload.valueAdded != null){
				result = payload.valueAdded;	
			}
			if(payload.constructionProduct != null){
				if (result != ""){
					result += "-";
				}
				result += payload.constructionProduct;
			}
			return result;
		}
		
		
		public override function initExtraValues():void {
			super.initExtraValues();
			
			
			
			if (payload == null){
				return ;
			}
			payload.startTime = getRawStartTime();
			
			
			if (StatusHelper.isRenegotiated(payload)){
				renegotiationLabel = GanttServiceReference.getLabel(OTRConstants.RENEGOTIATION_LABEL);
			}
			
			if (StatusHelper.isConstructionProduct(payload)){
				cpWidth = "100%";
				pumpWidth = "0";
				loadWidth = "0";
				
			}
			else if (StatusHelper.isPumpingService(payload)){
				cpWidth = "0";
				pumpWidth = "100%";
				loadWidth = "0";			
			}
			else  {
				cpWidth = "0";
				pumpWidth =  "0";
				loadWidth = "100%";
			}
			
			if(franceTaskProcessed.indexOf(payload.orderNumber+payload.loadNumber)==-1){
				//var numOrder:String=payload.orderNumber;
				var itemNumber:String=payload.itemNumber;
				var currentOrderArray:Array=franceTaskAcomodationRows.get(payload.equipNumber);
				var currentOrderVehicle:Array=PlanningScreenTreeResourceRenderer.mixerOrConveyor.get(payload.equipNumber);
				if(currentOrderVehicle==null){
					//PlanningScreenTreeResourceRenderer.mixerOrConveyor.put(payload.orderNumber,new Array());
					//currentOrderVehicle=PlanningScreenTreeResourceRenderer.mixerOrConveyor.get(payload.orderNumber);
				}
				if(currentOrderArray==null){
					franceTaskAcomodationRows.put(payload.equipNumber,new Array());
					currentOrderArray=franceTaskAcomodationRows.get(payload.equipNumber);
				}
				var isDisplayed:Boolean=false;
				for(var l:int=0;l<currentOrderArray.length;l++){
					if(getRawStartTime().time>currentOrderArray[l]){
						//create new row
						payload.indexPositionAux=l;
						singlePositions.put(payload.plant+":"+payload.equipNumber+":"+payload.itemNumber,l);
						currentOrderArray[l]=getRawEndTime().time;
						var addserv:ArrayCollection=payload.addProd;
						//currentOrderVehicle[l]="Fictive Mixer "+globalConveyorCounter.toString();
						for(var addi:int=0;addi<addserv.length;addi++){
							if(addserv.getItemAt(addi).conveyor!="" && addserv.getItemAt(addi).itemNumber==payload.itemNumber){
								//currentOrderVehicle[l]="Fictive Conveyor "+globalConveyorCounter.toString();
								break;
							}
						}
						//globalConveyorCounter++;
						//vemos si es conveyor por los add serv
						isDisplayed=true;
						break;
					} /*else if(getRawStartTime().time<currentOrderArray[l]){
					
					break;
					}*/
				}
				if(!isDisplayed){
					currentOrderArray.push(getRawEndTime().time);
					payload.indexPositionAux=currentOrderArray.length-1;
					singlePositions.put(payload.plant+":"+payload.equipNumber+":"+payload.itemNumber,currentOrderArray.length-1);
					var addserv:ArrayCollection=payload.addProd;
					//currentOrderVehicle.push("Fictive Mixer "+globalConveyorCounter.toString());
					for(var addi:int=0;addi<addserv.length;addi++){
						if(addserv.getItemAt(addi).conveyor!="" && addserv.getItemAt(addi).itemNumber==payload.itemNumber){
							//currentOrderVehicle[currentOrderVehicle.length-1]="Fictive Conveyor "+globalConveyorCounter.toString();
							break;
						}
					}
					//globalConveyorCounter++;
				}
				if(currentOrderArray.length==0){
					currentOrderArray.push(getRawEndTime().time);
					var addserv:ArrayCollection=payload.addProd;
					//currentOrderVehicle.push("Fictive Mixer "+globalConveyorCounter.toString());
					for(var addi:int=0;addi<addserv.length;addi++){
						if(addserv.getItemAt(addi).conveyor!="" && addserv.getItemAt(addi).itemNumber==payload.itemNumber){
							//currentOrderVehicle[currentOrderVehicle.length-1]="Fictive Conveyor "+globalConveyorCounter.toString();
							break;
						}
					}
					//globalConveyorCounter++;
				}
				franceTaskProcessed.push(payload.equipNumber+payload.loadNumber);
				ServiceAgentScreenResourceRenderer.expandableItems.put(payload.equipNumber,currentOrderArray.length);
			}			
			
			
			
			
			
		}
		
		
		public var cpLabel:String;
		
		[Bindable]
		public var pumpWidth:String = "0";
		
		[Bindable]
		public var loadWidth:String = "0";
		
		[Bindable]
		public var cpWidth:String = "0";
	}
}