package com.cemex.rms.dispatcher.views.planningScreen.tree
{
	import com.cemex.rms.common.gantt.GanttResource;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	
	import mx.collections.ArrayCollection;
	
	public class PlanningScreenResource extends GanttResource
	{
		public var plant:String;
		public var orderLoad:String;
		public var orderNumber:String;
		public var equipStatus:String;
		public var equipNumber:String;
		public var equipLabel:String;
		public var taskMin:Date;
		public function PlanningScreenResource()
		{
			super();
		}
		
		override public function get label():String{
			var etiqueta:String = super.label;
			if(super.labelField=="plant" && super.data.hasOwnProperty("name")){
				etiqueta=etiqueta+" "+super.data["name"];
			} else if(super.labelField=="equipNumber"){
				var vehi:ArrayCollection=DispatcherViewMediator.vehiculosCorrectos;
				var str:String="";
				for(var q:int=0;q<vehi.length;q++){
					if(vehi.getItemAt(q).equipNumber==this.equipNumber){
					//	str+=vehi.getItemAt(q).equipLabel+"\n";
						this.equipLabel=vehi.getItemAt(q).equipLabel;
						if(isNaN(parseInt(String(vehi.getItemAt(q).equipNumber)))){
							if(String(vehi.getItemAt(q).equipNumber).toLowerCase().indexOf("needed")!=-1){
								var equipNumberSplitedFua:Array=String(vehi.getItemAt(q).equipNumber).split(" ")
								str+=String(vehi.getItemAt(q).equipLabel)+" "+equipNumberSplitedFua[equipNumberSplitedFua.length-2]+" "+equipNumberSplitedFua[equipNumberSplitedFua.length-1]+" ";
							} else {
								str+=String(vehi.getItemAt(q).equipNumber)+" ";
							}
							str+=vehi.getItemAt(q).loadVol;
						} else {
							str+=parseInt(String(vehi.getItemAt(q).equipNumber)).toString()+"|"  ;
							str+=vehi.getItemAt(q).loadVol+" "+vehi.getItemAt(q).volUnit+ "  " +vehi.getItemAt(q).vehicleType
						}
							//+"\n"+vehi.getItemAt(q).hauler;
						/*str+=vehi.getItemAt(q).hauler;*/
						//str+=vehi.getItemAt(q).vehicleType+"\n";
					}
				}
				return str;
			}
			return super.label;
		}
		
		/*override public function set label():String{*/
		
	}
}