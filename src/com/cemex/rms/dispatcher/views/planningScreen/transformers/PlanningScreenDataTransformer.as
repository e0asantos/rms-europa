package com.cemex.rms.dispatcher.views.planningScreen.transformers
{
	import com.cemex.rms.common.gantt.GanttTask;
	import com.cemex.rms.common.gantt.GanttTree;
	import com.cemex.rms.common.transformer.FieldColumn;
	import com.cemex.rms.common.transformer.GenericDataTransformer;
	import com.cemex.rms.common.transformer.IDataTransformer;
	import com.cemex.rms.common.utils.DictionaryMap;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandImpl;
	import com.cemex.rms.dispatcher.views.planningScreen.headers.PlanningScreenHeaderView;
	import com.cemex.rms.dispatcher.views.planningScreen.renderers.PlanningScreenResourceRenderer;
	import com.cemex.rms.dispatcher.views.planningScreen.renderers.PlanningScreenTaskRenderer;
	import com.cemex.rms.dispatcher.views.planningScreen.renderers.PlanningScreenTreeResourceRenderer;
	import com.cemex.rms.dispatcher.views.planningScreen.tree.PlanningScreenTask;
	import com.cemex.rms.dispatcher.views.planningScreen.tree.PlanningScreenTree;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.core.ClassFactory;
	import mx.core.IFactory;
	
	import ilog.gantt.GanttDataGrid;
	import ilog.gantt.GanttSheet;
	
	public class PlanningScreenDataTransformer extends com.cemex.rms.common.transformer.GenericDataTransformer
	{
		
		[Embed(source="/assets/plus.gif")]
		public var plus:Class;
		
		[Embed(source="/assets/minus.gif")]
		public var minus:Class; 
		
		[Embed(source="/assets/s_b_plnt.gif")]            
		[Bindable]             
		private var plant:Class;
		
		public function PlanningScreenDataTransformer()
		{
			//TODO: implement function
			super();
			PlanningScreenTask.globalConveyorCounter=1;
			PlanningScreenTask.franceTaskAcomodationRows=new DictionaryMap();
			PlanningScreenTask.franceTaskAcomodationRowsAux=new DictionaryMap();
			PlanningScreenTask.franceTaskProcessed=[];
			PlanningScreenTask.franceTaskProcessedAux=[];
			setFields(new ArrayCollection(["orderNumber"])
			);//,AssignedLoadsTooltipHelper.getResourceData());
			
			setFieldColumns(new ArrayCollection([
				new FieldColumn("label",null,"Tree",160,PlanningScreenTreeResourceRenderer,PlanningScreenHeaderView),
				new FieldColumn("label",null,"",0,PlanningScreenResourceRenderer)
			]));
		}
		public override function getTaskRenderer():IFactory{
			return new ClassFactory(PlanningScreenTaskRenderer);
		}
		public override function getResourceRenderer():IFactory{
			return null;
		}
		public override function getViewID():String{
			return TVARVCPreffixes.PLANNING_SCREEN_VIEW_ID;
		}
		
		public override function addDatas(datas:ArrayCollection, step:String, date:Date):void{
			var reconfiguredData:ArrayCollection=new ArrayCollection();
			for(var r:int=datas.length-1;r>-1;r--){
				if(datas.getItemAt(r) is OrderLoad){
					if(DispatcherIslandImpl.planningScreenLoads.get(datas.getItemAt(r).orderNumber+":"+datas.getItemAt(r).itemNumber)==null){
						reconfiguredData.addItem(datas.getItemAt(r));
					}
				}
			}
			//darle vuelta
			reconfiguredData.source.reverse();
			reconfiguredData.refresh();
			super.addDatas(reconfiguredData,step,date);
		}
		public override function setTreePlusMinus(dataGrid:GanttDataGrid):void{
			
			dataGrid.setStyle("disclosureOpenIcon", minus);
			dataGrid.setStyle("disclosureClosedIcon", plus);
			
		}
		
		public override function setFields(fields:ArrayCollection):void{
			this.fields =  fields;
			tree = new PlanningScreenTree(fields);
		}
		
		public override function setColumnsDataGridCustomization(dataGrid:GanttDataGrid):void{
			//super.setColumnsDataGridCustomization(dataGrid);
			//dataGrid.styleFunction = styleFunction;
			dataGrid.iconFunction = null;
			tree.currentDataGrid=dataGrid;
			
			
		}
		
		override public function getSorter():Sort
		{
			//TODO: implement function
			return null;
		}
		
		override public function getSorterFunction(field:String):Function
		{
			//TODO: implement function
			return null;
		}
	}
}