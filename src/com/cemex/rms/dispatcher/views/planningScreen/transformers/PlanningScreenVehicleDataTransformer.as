package com.cemex.rms.dispatcher.views.planningScreen.transformers
{
	import com.cemex.rms.common.gantt.GanttTask;
	import com.cemex.rms.common.gantt.GanttTree;
	import com.cemex.rms.common.transformer.FieldColumn;
	import com.cemex.rms.common.transformer.GenericDataTransformer;
	import com.cemex.rms.common.transformer.IDataTransformer;
	import com.cemex.rms.common.utils.DictionaryMap;
	import com.cemex.rms.dispatcher.constants.ExtrasConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.helpers.TimeZoneHelper;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandImpl;
	import com.cemex.rms.dispatcher.services.flashislands.vo.PlanningMovement;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	import com.cemex.rms.dispatcher.views.planningScreen.headers.PlanningScreenHeaderView;
	import com.cemex.rms.dispatcher.views.planningScreen.renderers.PlanningScreenResourceRenderer;
	import com.cemex.rms.dispatcher.views.planningScreen.renderers.PlanningScreenTaskRenderer;
	import com.cemex.rms.dispatcher.views.planningScreen.renderers.PlanningScreenTreeResourceRenderer;
	import com.cemex.rms.dispatcher.views.planningScreen.renderers.PlanningScreenVehicleTaskRenderer;
	import com.cemex.rms.dispatcher.views.planningScreen.tree.PlanningScreenTask;
	import com.cemex.rms.dispatcher.views.planningScreen.tree.PlanningScreenTree;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.core.ClassFactory;
	import mx.core.IFactory;
	
	import ilog.gantt.GanttDataGrid;
	import ilog.gantt.GanttSheet;
	
	public class PlanningScreenVehicleDataTransformer extends com.cemex.rms.common.transformer.GenericDataTransformer
	{
		
		[Embed(source="/assets/plus.gif")]
		public var plus:Class;
		
		[Embed(source="/assets/minus.gif")]
		public var minus:Class; 
		
		[Embed(source="/assets/s_b_plnt.gif")]            
		[Bindable]             
		private var plant:Class;
		
		public function PlanningScreenVehicleDataTransformer()
		{
			//TODO: implement function
			super();
			PlanningScreenTask.franceTaskAcomodationRows=new DictionaryMap();
			PlanningScreenTask.franceTaskProcessed=[];
			setFields(new ArrayCollection(["equipNumber"])
			);//,AssignedLoadsTooltipHelper.getResourceData());
			
			setFieldColumns(new ArrayCollection([
				new FieldColumn("label",null,"Tree",160,PlanningScreenTreeResourceRenderer,PlanningScreenHeaderView),
				/*new FieldColumn("label",null,"",0,PlanningScreenResourceRenderer)*/
			]));
		}
		public override function getTaskRenderer():IFactory{
			return new ClassFactory(PlanningScreenVehicleTaskRenderer);
		}
		public override function getResourceRenderer():IFactory{
			return null;
		}
		public override function getViewID():String{
			return TVARVCPreffixes.PLANNING_SCREEN_VIEW_ID;
		}
		
		
		public override function setTreePlusMinus(dataGrid:GanttDataGrid):void{
			
			dataGrid.setStyle("disclosureOpenIcon", minus);
			dataGrid.setStyle("disclosureClosedIcon", plus);
			
		}
		
		public override function addHeaders(datas:ArrayCollection):void{
			
			super.addHeaders(datas);
		}
		public var vehiclesPlanning:ArrayCollection=new ArrayCollection();
		
		public override function addHeader(data:Object):void{
			
			
			
			if(data.hasOwnProperty("driver")){
				
				if(data['equipment'] && data['equipment']!=""){
					vehiclesPlanning.addItem(data);
				}
				//verify the plant
				if(data.hasOwnProperty("plant")){
					if(data["plant"]==DispatcherViewMediator.franceShowSinglePlant){
						super.addHeader(data);
				//it is probably a equipment object
						var movements:ArrayCollection=DispatcherIslandImpl.mapVehicles.get(data.equipment);
					//	vehiclesPlanning=movements;
						for(var q:int=0;q<movements.length;q++){
							PlanningScreenVehicleDataTransformer.ignoreAssn=true;
							var now:Date=TimeZoneHelper.getServer();
							var payload:OrderLoad=createDummySalesOrder(data.equipment,movements.getItemAt(q) as PlanningMovement,data.plant);
							this.addData(payload,GanttTask.STEP_INITIAL,now);
						//	vehiclesPlanning= payload;
							
						}
					}
				}
			} 
		}
		public function createDummySalesOrder(vehicleid:String,movement:PlanningMovement,plantFrom:String):OrderLoad{
			var var1:OrderLoad=new OrderLoad();
			var1.additionalFlag=false;
			var1.addProd=new ArrayCollection();
			var1.bomValid="";
			if(movement.plant=="NOAV"){
				var1.borderColor="0XC3C5E8";
				var1.fillColor="0XC4D9E8";
			} else {
				var1.borderColor="0XE8C427";
				var1.fillColor="0XFFFD1C";
			}
			var1.city="SCHEDULE";
			var1.codStatusProdOrder="10";
			var1.comingFromPush=false;
			var1.completedDelayed=false;
			var1.confirmQty=7;
			var1.constructionProduct="";
			var1.contactName1="prueba4 prueba4";
			var1.contactName2="prueba4 prueba4";
			var1.contactTel="23423424";
			var1.cpDescription="";
			var1.cpOrderNumber="";
			var1.cpQuantity=0;
			var1.cpUom="";
			var1.cycleTime=movement.durationMov;
			/*var1.cycleTimeMillis=13200;*/
			var1.deliveryAmount="";
			var1.deliveryBlock="";
			var1.deliveryGroup=1;
			var1.deliveryTime="08:40:00";
			var1.endTime=null;
			var1.equipLabel=vehicleid;
			var1.equipNumber=vehicleid;
			var1.equipStatus="Assigned";
			var1.firstDeliveryTime="08:40:00";
			var1.firstDescription="2-0100-G0-A-003-000-0-3000-000";
			var1.firstOrderNumber=randomIntBetween(1000,50000).toString();
			var1.flagRecipe="";
			var1.graphicReference=null;
			var1.hlevelItem=1000;
			var1.houseNoStreet="125 rue";
			var1.idHlevelItem="";
			var1.indexPosition=0;
			var1.isDelayed=false;
			var1.itemCategory="ZTC1";
			var1.itemCurrency=movement.plant;
			var1.itemNumber="1001";
			var1.jobSiteId="0065001158";
			var1.jobSiteName="Job Site02";
			var1.loadFrequency="0";
			var1.loadingDate=movement.initialDate
			var1.loadingTime=movement.initialHour;
			var1.loadingTimestamp=movement.initialDate;
			var horas:Array=movement.initialHour.split(":");
			var1.loadingTimestamp.hours=Number(horas[0]);
			var1.loadingTimestamp.minutes=Number(horas[1]);
			var1.loadNumber="L001";
			var1.loadStatus="NOST";
			var1.loadUom="M3";
			var1.loadVolume=7;
			var1.materialDes="2-0100-G0-A-003-000-0-3000-000";
			var1.materialId="000000000010001973";
			var1.mepTotCost=551811.26;
			var1.numberTimeStamp=0;
			var1._numericDate=-762434720;
			var1.numProdOrder=null;
			var1.orderLoad=var1.firstOrderNumber+"-L001";
			var1.orderNumber=var1.firstOrderNumber;
			var1.orderReason="";
			var1.orderStatus="CONF";
			var1.orderUom="M3";
			var1.orderVolume=19;
			var1.paytermLabel="";
			var1.pborderColor="";
			var1.pfillColor="";
			var1.plant=plantFrom;
			var1.postalCode="14526";
			var1.prodOrder="000065204334";
			var1.remainFlag=false;
			var1.renegFlag=false;
			var1.SAPStamp=null;
			var1.shipConditions="01";
			var1.shipmentStatus="";
			var1.slump="";
			var1.soldToName="cust02";
			var1.soldToNumber="0050003570";
			var1.startTime=null;
			var1.taskNumber="";
			var1.text=[];
			var1.texts=null;
			var1.timeFixIndicator="";
			var1.timePumpJobs="00:00:00";
			var1.timeStamp=null;
			var1.totalLoads="3";
			var1.totCost=815699.22;
			var1.txtStatusProdOrder="ABIE";
			var1.unloadingTime="00:20:00";
			var1.valueAdded="";
			var1.vehicleMaxVol=7;
			var1.vehicleType="";
			var1.visible="";
			var1.warning="";
			var1.workAroundDestroy=null;
				
				
			return var1;
		}
		private function randomIntBetween(min:int, max:int):int {
			return Math.round(Math.random() * (max - min) + min);
		}
		public override function setFields(fields:ArrayCollection):void{
			this.fields =  fields;
			tree = new PlanningScreenTree(fields);
		}
		public override function timeChanged(time:Date):void{
			var algo:String="jdfnj";
		}
		public override function setColumnsDataGridCustomization(dataGrid:GanttDataGrid):void{
			//super.setColumnsDataGridCustomization(dataGrid);
			//dataGrid.styleFunction = styleFunction;
			dataGrid.iconFunction = null;
			tree.currentDataGrid=dataGrid;
			
		}
		public override function addDatas(datas:ArrayCollection, step:String, date:Date):void{
			var reconfiguredData:ArrayCollection=new ArrayCollection();
			for(var r:int=datas.length-1;r>-1;r--){
				if(datas.getItemAt(r) is OrderLoad){
					if(DispatcherIslandImpl.planningScreenLoads.get(datas.getItemAt(r).orderNumber+":"+datas.getItemAt(r).itemNumber)!=null){
						var obj:Object=DispatcherIslandImpl.planningScreenLoads.get(datas.getItemAt(r).orderNumber+":"+datas.getItemAt(r).itemNumber);
						datas.getItemAt(r).equipNumber=obj["VEHICLE"];
						datas.getItemAt(r).equipLabel=obj["VEHICLE"];
						datas.getItemAt(r).equipStatus="Not Assigned";
						reconfiguredData.addItem(datas.getItemAt(r));
					}
				}
			}
			super.addDatas(reconfiguredData,step,date);
			
		}
		public static var ignoreAssn:Boolean=false;
		public override function addData(data:Object, step:String, date:Date):void{
			if((data is OrderLoad)){
				if((data as OrderLoad).equipNumber==""
					|| PlanningScreenVehicleDataTransformer.ignoreAssn==true
				|| DispatcherIslandImpl.planningScreenLoads.get((data as OrderLoad).orderNumber+":"+(data as OrderLoad).itemNumber)!=null){
					ignoreAssn=false;
					super.addData(data, step, date);
				}
			} else {
				super.addData(data, step, date);
			}
		}
		
		override public function getSorter():Sort
		{
			//TODO: implement function
			return null;
		}
		
		override public function getSorterFunction(field:String):Function
		{
			//TODO: implement function
			return null;
		}
	}
}