package com.cemex.rms.dispatcher.views.planningScreen.headers
{
	import com.cemex.rms.dispatcher.constants.OTRConstants;
	import com.cemex.rms.dispatcher.events.SearchBtnClickViewEvent;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	
	import flash.events.Event;
	
	import org.robotlegs.mvcs.Mediator;
	
	public class PlanningScreenHeaderViewMediator extends Mediator
	{
		public function PlanningScreenHeaderViewMediator()
		{
			super();
		}
		
		[Inject]
		public var view:PlanningScreenHeaderView;
		
		public function getLabel(label:String):String{
			return GanttServiceReference.getLabel(label);
		}
		
		override public function onRegister():void {
			view.loadsPerVehicle.text=getLabel(OTRConstants.DISP_SEARCH);
			view.searchText.addEventListener(Event.CHANGE,searchBtnClick);
			view.searchText.text="";
		}
		
/*		public function setTextGlobalSearch(evt:KeyboardEvent):void{
			if(evt.charCode==8){
				view.searchText.text=view.searchText.text.substr(0,-1);
			} else {
				if(evt.charCode!=Keyboard.ENTER){
					view.searchText.text+=String.fromCharCode(evt.charCode);
				}
			}
			
			view.searchText.setSelection(view.searchText.text.length,view.searchText.text.length);
		}*/

		public function searchBtnClick(evt:Event):void{
			DispatcherViewMediator.textToSearch=view.searchText.text;
			var busqueda:SearchBtnClickViewEvent=new SearchBtnClickViewEvent();
			busqueda.searchString=view.searchText.text;
			busqueda.refTextInput=view.searchText;
			eventDispatcher.dispatchEvent(busqueda);
		}		
		
	}
}