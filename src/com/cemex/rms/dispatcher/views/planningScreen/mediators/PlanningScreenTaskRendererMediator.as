package com.cemex.rms.dispatcher.views.planningScreen.mediators
{
	import com.cemex.rms.common.gantt.GanttTask;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.events.SearchBtnClickViewEvent;
	import com.cemex.rms.dispatcher.services.IGanttServiceFactory;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.LoadPerOrderTaskRenderer;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.tree.LoadsPerVehicleTask;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	import com.cemex.rms.dispatcher.views.mediators.GenericTaskRendererMediator;
	import com.cemex.rms.dispatcher.views.planningScreen.renderers.PlanningScreenTaskRenderer;
	import com.cemex.rms.dispatcher.views.planningScreen.tree.PlanningScreenTask;
	import com.cemex.rms.dispatcher.views.serviceAgentScreen.renderers.ServiceAgentScreenTaskRenderer;
	import com.cemex.rms.dispatcher.views.serviceAgentScreen.tree.ServiceAgentScreenTask;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import flash.display.Graphics;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	
	import mx.core.UIComponent;
	
	public class PlanningScreenTaskRendererMediator extends GenericTaskRendererMediator
	{
		public function PlanningScreenTaskRendererMediator()
		{
			super();
		}
		
		[Inject]
		public var view:PlanningScreenTaskRenderer;
		[Inject]
		public var services: IGanttServiceFactory;
		
		protected override function getView():UIComponent{
			return view;
		}
		override public function onRegister():void {
			super.onRegister();
			var task:PlanningScreenTask = getGanttTask() as PlanningScreenTask;
			if (task != null){
				task.setView(view);
			}
			eventDispatcher.addEventListener(SearchBtnClickViewEvent.SEARCH_BTN_CLICK_EVENT,searchHandler);
		}
		
		private function searchHandler(event:SearchBtnClickViewEvent):void{
			if((getGanttTask().data as OrderLoad).orderLoad.toLowerCase().indexOf(event.searchString.toLowerCase())>-1&&event.searchString!=""){
				view.filters = [_shadow, _searchHiglightFilter];
			}
			else{
				view.filters = [_shadow];
			}
		}
		
		//efecto borde azul para búsquedas
		private var _searchHiglightFilter:GlowFilter = new GlowFilter(0x23A1FF, 4, 6, 6, 6, BitmapFilterQuality.HIGH, false, false);
		//efecto sombra
		private var _shadow:DropShadowFilter = new DropShadowFilter(4,72,0,0.3,4,4);
		
		
		public override function getViewName():String{
			view.invalidateDisplayList();
			view.invalidateProperties();
			view.validateNow();
			return TVARVCPreffixes.PLANNING_SCREEN_VIEW_ID;
		}
		
		public override function getExtraInfoIfOverlap(task:GanttTask):String {
			var payload:OrderLoad = task.data as OrderLoad;
			return payload.orderNumber+":"+payload.itemNumber+":"+payload.loadNumber + "("+payload.loadingTime+")";
		}
	}
}