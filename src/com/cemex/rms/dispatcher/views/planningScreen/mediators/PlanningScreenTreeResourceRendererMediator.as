package com.cemex.rms.dispatcher.views.planningScreen.mediators
{
	import com.cemex.rms.common.gantt.GanttTask;
	import com.cemex.rms.common.helpers.PopupHelper;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.services.IGanttServiceFactory;
	import com.cemex.rms.dispatcher.views.mediators.GenericTaskRendererMediator;
	import com.cemex.rms.dispatcher.views.serviceAgentScreen.renderers.ServiceAgentScreenTaskRenderer;
	import com.cemex.rms.dispatcher.views.serviceAgentScreen.tree.ServiceAgentScreenTask;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import flash.events.MouseEvent;
	
	import mx.binding.utils.BindingUtils;
	import mx.controls.Menu;
	import mx.core.UIComponent;
	import mx.events.MenuEvent;
	import mx.events.ToolTipEvent;
	
	import org.robotlegs.mvcs.Mediator;
	
	public class PlanningScreenTreeResourceRendererMediator extends GenericTaskRendererMediator
	{
		[Inject]
		public var view:ServiceAgentScreenTaskRenderer;
		[Inject]
		public var services: IGanttServiceFactory;
		public function PlanningScreenTreeResourceRendererMediator()
		{
			super();
		}
		
		override protected function getView():UIComponent{
			return view;
		}
		
		
		override public function getViewName():String{
			view.invalidateDisplayList();
			view.invalidateProperties();
			view.validateNow();
			return TVARVCPreffixes.PLANNING_SCREEN_VIEW_ID;
		}
	}
}