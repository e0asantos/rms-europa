
package com.cemex.rms.dispatcher.views.loadsPerVehicle.headers
{
	
	import com.cemex.rms.dispatcher.constants.OTRConstants;
	import com.cemex.rms.dispatcher.events.FocusInSearchEvent;
	import com.cemex.rms.dispatcher.events.SearchBtnClickViewEvent;
	import com.cemex.rms.dispatcher.events.SearchBtnVehicleEvent;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	
	import mx.controls.Alert;
	import mx.utils.StringUtil;
	
	import org.robotlegs.mvcs.Mediator;
	
	
	public class LoadsPerVehicleHeaderMediator extends Mediator
	{
		public function LoadsPerVehicleHeaderMediator()
		{
			super();
		}
		
		[Inject]
		public var view:LoadsPerVehicleHeader;
		
		public var searchMyLabel:String="";
		public function getLabel(label:String):String{
			return GanttServiceReference.getLabel(label);
		}
		
		override public function onRegister():void {
			
			view.loadsPerVehicle.text = getLabel(OTRConstants.DISP_SEARCH);
			//view.searchTextVehicle.text="eitt!";
			view.searchTextVehicle.text="";
			view.loadsPerVehicle.toolTip = getLabel(OTRConstants.LOADS_PER_VEHICLE_HEADER_TOOLTIP);
			view.searchTextVehicle.addEventListener(Event.CHANGE,searchBtnClick);
		}
		
		public function elementFocusChange(ebt:FocusEvent):void{
			var focusIn:FocusInSearchEvent= new FocusInSearchEvent(FocusInSearchEvent.FOCUS_IN_SEARCH);
			focusIn.refTextInput=view.searchTextVehicle;
			eventDispatcher.dispatchEvent(focusIn);
		}
		public function searchBtnClick(evt:Event=null):void{
			DispatcherViewMediator.textToSearch=view.searchTextVehicle.text;
			var busqueda:SearchBtnVehicleEvent=new SearchBtnVehicleEvent(SearchBtnVehicleEvent.SEARCH_BTN_VEHICLE_CLICK_EVENT);
			busqueda.searchString=view.searchTextVehicle.text;
			busqueda.refTextInput=view.searchTextVehicle;
			eventDispatcher.dispatchEvent(busqueda);
		}
		
		/*public function  mouseeventHandler(e:MouseEvent):void{
			if (e.ctrlKey){
				e.stopPropagation();
			}
		}*/
	}
}