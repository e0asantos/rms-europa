package com.cemex.rms.dispatcher.views.loadsPerVehicle.tree
{
	import com.cemex.rms.common.date.FlexDateHelper;
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.dispatcher.constants.OTRConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.helpers.ColorHelper;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.helpers.StatusHelper;
	import com.cemex.rms.dispatcher.services.flashislands.vo.ColorSetting;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.LoadsPerVehicleTaskRenderer;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	import com.cemex.rms.dispatcher.views.tree.OrderLoadGanttOverlapTask;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import mx.core.UIComponent;

	public class LoadsPerVehicleTask extends OrderLoadGanttOverlapTask
	{
		public function LoadsPerVehicleTask()
		{
			
			super();
		}
		public override function get startTime():Date{
			if (payload == null){
				return null;
			}
			/*
			if (payload.startTime == null ){
				payload.startTime = getStartTime(payload);
			}
			*/
			return payload.startTime;
		}
		override public function getCoordinate(date:Date):Number{
			return DispatcherViewMediator.getCoordinate(date);
		}
		override public function getView():UIComponent{
			return _view;
		}
		
		private var _view:LoadsPerVehicleTaskRenderer;
		public function setView(_view:LoadsPerVehicleTaskRenderer):void{
			this._view = _view;
			initExtraValues();
		}
		
		public override function set startTime(_startTime:Date):void{
			if (data != null){
				data.startTime = _startTime;
			}
		}
		public override function get endTime():Date {
			if (payload == null){
				return null;
			}
			return getEndTime(startTime,payload);
		}
		public override function set endTime(_endTime:Date):void{
			if (data != null){
				data.endTime = _endTime;
			}
		}
		
		public function getStartTime(data:Object):Date{
			var payload:OrderLoad = data as OrderLoad;
			return FlexDateHelper.parseTime(payload.loadingDate,payload.loadingTime);
		}
		public function getEndTime(startTime:Date,data:Object):Date{
			var payload:OrderLoad = data as OrderLoad;
			
			var fecha:Date = FlexDateHelper.addSeconds(startTime,payload.cycleTimeMillis);
			
			
			return fecha;
		}
		
		
		private var logger:ILogger = LoggerFactory.getLogger("LoadsPerVehicle");
		
		
		public override function initExtraValues():void {
			super.initExtraValues();
			
			
			if (payload == null){
				return ;
			}
			payload.startTime = getStartTime(payload);
			
			if (StatusHelper.isRenegotiated(payload)){
				renegotiationLabel = GanttServiceReference.getLabel(OTRConstants.RENEGOTIATION_LABEL);
			}
			var currColor:ColorSetting = ColorHelper.getColor(payload,TVARVCPreffixes.LOAD_SCOPE);
			
			/*color = currColor.fillColor;
			border = currColor.borderColor;
			thickness = currColor.borderThickness;*/
			
		
		}
		
		public override function canRenderField(field:String):Boolean{
			//if( data != null && field == "equipLabel" && data.hasOwnProperty(field) && data[field] != null && data[field] != ""){
			//	return true;
			//}
			return false;
		}
		
		
		
		
	
	}
}