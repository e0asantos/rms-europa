package com.cemex.rms.dispatcher.views.loadsPerVehicle.tree
{
	import com.cemex.rms.common.gantt.GanttTree;
	import com.cemex.rms.common.tree.NodeResource;
	import com.cemex.rms.common.tree.NodeTask;
	import com.cemex.rms.common.utils.DictionaryMap;
	import com.cemex.rms.dispatcher.helpers.StatusHelper;
	import com.cemex.rms.dispatcher.helpers.TimeZoneHelper;
	import com.cemex.rms.dispatcher.services.flashislands.vo.Equipment;
	
	import mx.collections.ArrayCollection;

	/**
	 * Se utiliza otro Tree para que se puedan especificar diferentes Task y Resources y poder cutomizar
	 * las etiquetas y comportamientos
	 * 
	 * 
	 */
	public class LoadsPerVehicleTree extends GanttTree
	{
		public function LoadsPerVehicleTree(fields:ArrayCollection)
		{
			super(fields);
		}
		/*
		protected override function hasSameValues(target:Object,temp:Object):Boolean {
			var targetTask:LoadsPerVehicleTask = target as LoadsPerVehicleTask;
			var tempTask:LoadsPerVehicleTask = temp as LoadsPerVehicleTask;
			
			return targetTask != null && tempTask != null &&
				(targetTask.payload.itemNumber == tempTask.payload.itemNumber) &&
				(targetTask.payload.orderNumber == tempTask.payload.orderNumber);
		}*/
		protected override function hasSameValues(target:Object,temp:Object):Boolean {
			var targetTask:LoadsPerVehicleTask = target as LoadsPerVehicleTask;
			var tempTask:LoadsPerVehicleTask = temp as LoadsPerVehicleTask;
			return targetTask != null && tempTask != null &&
				(""+targetTask.payload.itemNumber) == (""+tempTask.payload.itemNumber) &&
				(""+targetTask.payload.orderNumber) == (""+tempTask.payload.orderNumber);
		}
		
		/*
		public override function processHeaders(node:NodeResource,field:String):void{
			if (field == "equipLabel" && node.data != null){
				
				var equipmentNode:NodeResource = equipmentNodes.get(node.data[""]);
				equipmentNodes.put(node.		
			}
		}
		*/
		private var equipmentNodes:DictionaryMap = new DictionaryMap(); 
		
		protected override function newNode():NodeResource{
			return new LoadsPerVehicleResource();
		}
		protected override function newTask():NodeTask {
			return new LoadsPerVehicleTask();
		}
		
		
		override public function deleteOldNode(oldNode:NodeResource,node:NodeResource,field:String):Boolean{
			
			var oldValue:String = ""+oldNode.data[field];
			var newValue:String = ""+node.data[field];
			var equals:Boolean = oldValue == newValue;
			var equals2:Boolean = (""+oldNode.id) == (""+node.id) ;
			switch(field){
				//case "orderNumber":
				case "equipNumber":
					return equals && !equals2;
					
			}
			return false;
		}
		
		
		
		
		
		public override function timeChangedTask(time:Date,nodeTask:NodeTask):Boolean{
			var nowTime:Number = time.getTime();
			var task:LoadsPerVehicleTask = nodeTask as LoadsPerVehicleTask; 
			
			var startingTime:Number = task.startTime.getTime();
			var loadingTime:Number = task.getRawStartTime().getTime();		
			
			var timeTemp:Number = (nowTime - loadingTime) / (60*1000);
			
			//task.isDelayed = StatusHelper.isDelayed(task.payload);
			task.initExtraValues();
		
			
			
			if (task.payload.isDelayed) {
				task.delayTimeLabel = "D"+ Math.ceil(timeTemp) ;
				var delayed:Date = TimeZoneHelper.getServer();;
				delayed.setSeconds(0);
				if (task.getTaskItem() != null && Number(task.data.codStatusProdOrder)<20){
					task.startTime = delayed;
					task.getTaskItem().startTime = delayed;
					task.getTaskItem().endTime = task.endTime;
				}
			}else {
				
				task.delayTimeLabel = "";
			}
			getTasksTree().itemUpdated(task);
			
			return true;
		}
		
		
		
	}
}