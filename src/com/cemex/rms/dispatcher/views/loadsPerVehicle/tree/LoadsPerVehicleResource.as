package com.cemex.rms.dispatcher.views.loadsPerVehicle.tree
{
	import com.cemex.rms.common.gantt.GanttResource;
	import com.cemex.rms.dispatcher.constants.OTRConstants;
	import com.cemex.rms.dispatcher.constants.ParamtersConstants;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.helpers.StatusHelper;

	public dynamic class LoadsPerVehicleResource extends GanttResource
	{
		public function LoadsPerVehicleResource() {
			super();
			
		}
		
		public override function get label():String {
			var etiqueta:String = super.label;
			var additionalExtra:String="";
			if(super.labelField=="plant" && super.data.hasOwnProperty("name")){
				etiqueta=etiqueta+" "+super.data["name"];
			}
			if (getLabelField() == "orderNumber"){
				var extra:String = "";
				if(data.hasOwnProperty("renegFlag")){
					if(data["renegFlag"]==true){
						extra = "\n"+GanttServiceReference.getLabel(OTRConstants.RENEGOTIATION_LABEL);
					}
				}
				if (StatusHelper.isOrderRenegotiated(data)){
					extra = "\n"+GanttServiceReference.getLabel(OTRConstants.RENEGOTIATION_LABEL);
				}
				if(super.data["constructionProduct"]=="" && super.data["loadNumber"]=="") {
					extra += GanttServiceReference.getLabel(OTRConstants.PUMPING_SERVICE);
				}
				if (this.data[ "paytermLabel"] != null && this.data[ "paytermLabel"] != ""){
					if (extra == ""){
						extra +="\n";
					}
					else {
						extra +=" - ";
					}
					extra += this.data["paytermLabel"]
				}
				if(String(this.data["prodOrder"]).toLowerCase().indexOf("res")!=-1){
					if (extra == ""){
						extra +="\n";
					}
					else {
						extra +=" - ";
					}
					extra += this.data["prodOrder"];
				}
				etiqueta += extra;
				if(GanttServiceReference.getParamString("DS_DISP_SO_CUST",this.data.plant)=="NUMCUS"){
					additionalExtra="\n"+super.data["soldToName"];
				}
			} 
			
			else if (getLabelField() == "equipNumber"){
				
				/*
				if (this.data["equipLabel"] == null || this.data["equipLabel"] == ""){
					var idOrLicense:String = GanttServiceReference.getParamString(ParamtersConstants.DISP_ID_OR_LICEN);
					if (idOrLicense == "LI"){
						etiqueta = data["licenseNum"]+ "("+totalChildrenCount+")";
						//
					}
					else {
						//etiqueta = data["equipNumber"];
						etiqueta = data["equipNumber"]+ "("+totalChildrenCount+")";
						//etiqueta = getLabelFromField("equipNumber");
					}
					//etiqueta = getLabelFromField("licenseNum");
					etiqueta = data["licenseNum"]+ "("+totalChildrenCount+")";
				}	
				else {
					//etiqueta = getLabelFromField("equipLabel");
					
					// No se hace nada porque se mandó bien de 
				}*/
				etiqueta = data["equipLabel"]+ "("+totalChildrenCount+")";
				if(GanttServiceReference.getParamString("DS_DISP_SO_CUST",this.data.plant)=="NUMCUS" && totalChildrenCount>0){
					additionalExtra="\n"+super.data["soldToName"];
				}
			} 
			return etiqueta+additionalExtra;
		}
		
	
	}
}