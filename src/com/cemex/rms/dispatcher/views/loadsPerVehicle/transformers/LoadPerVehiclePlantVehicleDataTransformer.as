package com.cemex.rms.dispatcher.views.loadsPerVehicle.transformers
{
	
	import com.cemex.rms.common.flashislands.vo.TVARVC;
	import com.cemex.rms.common.gantt.GanttResource;
	import com.cemex.rms.common.transformer.FieldColumn;
	import com.cemex.rms.common.transformer.GenericDataTransformer;
	import com.cemex.rms.dispatcher.constants.ExtrasConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.LoadsPerVehicleResourceRenderer;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.LoadsPerVehicleTaskRenderer;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.headers.LoadsPerVehicleHeader;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.tree.LoadsPerVehicleResource;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.tree.LoadsPerVehicleTree;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.advancedDataGridClasses.AdvancedDataGridColumn;
	import mx.core.ClassFactory;
	import mx.core.IFactory;
	
	import ilog.gantt.GanttDataGrid;

	
	/**
	 * Esta clase transforma los datos para que en el arbol se vean Primero las plantas y despues,como hijos las ordenes 
	 */
	public class LoadPerVehiclePlantVehicleDataTransformer extends GenericDataTransformer
	{
		public function LoadPerVehiclePlantVehicleDataTransformer() {
			super();
			
			setFields(new ArrayCollection(["plant","equipStatus","equipNumber|orderNumber"])
				);//,LoadPerVehicleTooltipHelper.getResourceData());
			
			setFieldColumns(new ArrayCollection([
				new FieldColumn("label",null,"Tree",175,null,LoadsPerVehicleHeader),
				//new FieldColumn("plant","Plant",70),
				//new FieldColumn("equipNumber","Carro",70),
				new FieldColumn("startTime",null,"",25,LoadsPerVehicleResourceRenderer)
			]));
		}
		
		public override function getTaskRenderer():IFactory{
			return new ClassFactory(LoadsPerVehicleTaskRenderer);
		}
		
		public override function getResourceRenderer():IFactory{
			return null;
		}
		
		public override function getViewID():String{
			return TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID;
		}
		
		
		public override function setFields(fields:ArrayCollection):void{
			this.fields =  fields;
			tree = new LoadsPerVehicleTree(fields);
		}
		
		
		public override function setColumnsDataGridCustomization(dataGrid:GanttDataGrid):void{
			//super.setColumnsDataGridCustomization(dataGrid);
			dataGrid.styleFunction = styleFunction;
			dataGrid.iconFunction = iconFunction;
			tree.currentDataGrid=dataGrid;
			
			
		}
		public function styleFunction(gr:GanttResource,e:AdvancedDataGridColumn):void{
			
			if (gr is LoadsPerVehicleResource && e != null){
				var a:LoadsPerVehicleResource = gr as LoadsPerVehicleResource;
				if (a != null   && 
					 a.getLabelField() == "orderNumber"){
					
					e.setStyle("color",0xFF0000);
				}
				else {
					e.setStyle("color",0x000000);
					///if (){
					//}else {
					//	e.setStyle("color",0x000000);}
					
				}
			}
		}
		
		
		[Embed(source="/assets/s_s_ledg.gif")]            
		[Bindable]             
		private var green:Class;
		
		[Embed(source="/assets/s_s_ledr.gif")]            
		[Bindable]             
		private var red:Class;
		
		[Embed(source="/assets/s_s_ledy.gif")]            
		[Bindable]             
		private var yellow:Class;
		
		
		
		// Vehiculos
		[Embed(source="/assets/s_b_trns_red_gps.gif")]            
		[Bindable]             
		private var vehicle_red_gps:Class;
		
		[Embed(source="/assets/s_b_trns_green_gps.gif")]            
		[Bindable]             
		private var vehicle_green_gps:Class;
		
		[Embed(source="/assets/s_b_trns_red.gif")]            
		[Bindable]             
		private var vehicle_red:Class;
		
		[Embed(source="/assets/s_b_trns_green.gif")]            
		[Bindable]             
		private var vehicle_green:Class;
		
		
		[Embed(source="/assets/s_b_trns_white.gif")]            
		[Bindable]             
		private var vehicle_white:Class;
		
		[Embed(source="/assets/truck_drop.png")]            
		[Bindable]             
		private var vehicle_water:Class;
		
		
		
		// Collect
		[Embed(source="/assets/s_b_trns_c_red_gps.gif")]            
		[Bindable]             
		private var vehicle_c_red_gps:Class;
		
		[Embed(source="/assets/s_b_trns_c_green_gps.gif")]            
		[Bindable]             
		private var vehicle_c_green_gps:Class;
		
		[Embed(source="/assets/s_b_trns_c_red.gif")]            
		[Bindable]             
		private var vehicle_c_red:Class;
		
		[Embed(source="/assets/s_b_trns_c_green.gif")]            
		[Bindable]             
		private var vehicle_c_green:Class;
		
		
		
		
		[Embed(source="/assets/s_b_trns_empty.gif")]            
		[Bindable]             
		private var vehicle_unassigned:Class;
		
		/*[Embed(source="/assets/s_s_wspo_rbom.png")]            
		[Bindable]             
		private var order:Class;*/
		
		[Embed(source="/assets/s_s_wspo.gif")]
		[Bindable]
		private var normal_order:Class;
		
		[Embed(source="/assets/s_s_wspo_orange.png")]            
		[Bindable]             
		private var order_bom_orange:Class;
		
		[Embed(source="/assets/s_s_wspo_gray.png")]            
		[Bindable]             
		private var order_bom_gray:Class;
		
		[Embed(source="/assets/s_s_wspo_red.png")]            
		[Bindable]             
		private var order_bom_red:Class;
		
		[Embed(source="/assets/s_s_wspo_3.png")]            
		[Bindable]             
		private var order:Class;
		
		[Embed(source="/assets/s_s_wspo_green.png")]            
		[Bindable]             
		private var order_green:Class;

		
		
		
		
		[Embed(source="/assets/s_b_plnt_distr.gif")]            
		[Bindable]             
		private var plant_distribuida:Class;
		
		[Embed(source="/assets/s_b_plnt_centr.gif")]            
		[Bindable]             
		private var plant_centralizada:Class;
		
		[Embed(source="/assets/s_b_plnt_auton.gif")]            
		[Bindable]             
		private var plant_autonoma:Class;
		
		[Embed(source="/assets/s_b_plnt_centr_stop.png")]            
		[Bindable]             
		private var plant_centralizada_stop:Class;
		
		[Embed(source="/assets/s_b_plnt_distr_closed2.png")]            
		[Bindable]             
		private var plant_distribuida_stop:Class;
		
		
		private function iconFunction(resource:GanttResource):Class{
			var equipStatus:String ;
			if (resource.getLabelField() == "equipNumber"){
				
				var fuelSec:String = (""+resource.data["statusLight"]);
				var equicatgry:String = (""+resource.data["equicatgry"]);
				equipStatus = (""+resource.data["equipStatus"]);
				if( equicatgry == null) {
					return vehicle_white;
				} else if(resource.data["swFlag"]!="" && resource.data["swFlag"]!=null){
					//es un camion con stonewash
					return vehicle_water;
				}
				else if (equicatgry == "V"){
					if (equipStatus == ExtrasConstants.VEHICLE_DISPONIBLE){
						if (fuelSec == "Y"){
							return vehicle_green_gps;
						}
						else {
							return vehicle_green;
						}
					}
					else if (equipStatus == ExtrasConstants.VEHICLE_ASSIGNED){	
						if (fuelSec == "Y"){
							return vehicle_red_gps;
						}
						else {
							return vehicle_red;
						}
					}
				}
				else {
					if (equipStatus == ExtrasConstants.VEHICLE_DISPONIBLE){
						
						if (fuelSec == "Y"){
							return vehicle_c_green_gps;
						}
						else {
							return vehicle_c_green;
						}
					}
					else if (equipStatus == ExtrasConstants.VEHICLE_ASSIGNED){	
						if (fuelSec == "Y"){
							return vehicle_c_red_gps;
						}
						else {
							return vehicle_c_red;
						}
					}
				}
			}
			else if (resource.getLabelField() == "equipStatus"){
				equipStatus = (""+resource.data["equipStatus"]);
				
				if (equipStatus == ExtrasConstants.LOAD_UNASSIGNED){
					return vehicle_unassigned;
				}
				else if (equipStatus == ExtrasConstants.VEHICLE_ASSIGNED){
					return vehicle_red;
				}
				else if (equipStatus == ExtrasConstants.VEHICLE_DISPONIBLE){
					return vehicle_green;
				}
			}
			else if (resource.getLabelField() == "orderNumber"){
				if(resource.data.bomValid=="1"){
					//rojo
					return order_bom_red;
				} else if(resource.data.bomValid=="2"){
					//color naranja
					return order_bom_orange;
				} else if(resource.data.bomValid=="3"){
					//color gris
					return order_bom_gray;
				}
				if(resource.data.flagRecipe!=""){
					//return order_rbom;
				}
				return order_green;
			}
			else if (resource.getLabelField() == "plant"){
				
				var plantId:String = (""+resource.data["plantId"]);
				var plantType:String = GanttServiceReference.getPlantType(plantId);
				var plantFull:ArrayCollection=GanttServiceReference.getPlants();
				var currPlant:Object;
				for(var vi:int=0;vi<plantFull.length;vi++){
					if(plantId==plantFull.getItemAt(vi).plantId){
						currPlant=plantFull.getItemAt(vi);
					}
				}
				//Alert.show(plantId +"-" + plantType);
				/*if (plantType == GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_D).low){
					return plant_distribuida;	
				}*/
				/*if (plantType == "02"/* && currPlant.status=="E0001"){
					return plant_distribuida;
					
				}*/ /*else if(plantType == "02" && currPlant.status=="Stop"){
					return plant_distribuida_stop;
				}*/
				/*else if (plantType == GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_C).low){
					return plant_centralizada;
				}*/
				if (plantType == "01" && currPlant.status=="E0001"){
					return plant_centralizada;
				} else if(plantType == "01" && currPlant.status=="E0002"){
					return plant_distribuida_stop;
				} else if(plantType=="01" && currPlant.status=="E0003"){
					return plant_centralizada_stop
				} else if (Number(plantType) == Number(GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_D).low)){
					return plant_distribuida;	
				} else if (Number(plantType) == Number(GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_C).low)){
					return plant_centralizada;
				} else if (Number(plantType) == Number(GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_A).low)){
					return plant_autonoma;
				}
				/*else if (plantType == GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_A).low){
					return plant_autonoma;
				}*/
				
			}
			return null;
		}
		
		
		public override function setTreePlusMinus(dataGrid:GanttDataGrid):void{
			
			dataGrid.setStyle("disclosureOpenIcon", minus);
			dataGrid.setStyle("disclosureClosedIcon", plus);
			
		}
		
		[Embed(source="/assets/plus.gif")]
		public var plus:Class;
		
		[Embed(source="/assets/minus.gif")]
		public var minus:Class; 
		
		
	}
}