package com.cemex.rms.dispatcher.views.loadsPerVehicle.mediators
{
	import com.cemex.rms.common.helpers.PopupHelper;
	import com.cemex.rms.dispatcher.constants.ExtrasConstants;
	import com.cemex.rms.dispatcher.constants.OTRConstants;
	import com.cemex.rms.dispatcher.constants.SecurityConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.helpers.TooltipHelper;
	import com.cemex.rms.dispatcher.services.IGanttServiceFactory;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandImpl;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.ChangeDriver;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.OpenLPH;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.OpenOTS;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.OpenOrderQuantity;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.StoneWash;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.UnassignPlant;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.UnassignVehicle;
	import com.cemex.rms.dispatcher.services.flashislands.requests.vehicle.ReleaseVehicle;
	import com.cemex.rms.dispatcher.services.flashislands.requests.vehicle.SupportPlant;
	import com.cemex.rms.dispatcher.services.flashislands.requests.vehicle.VehicleInPlant;
	import com.cemex.rms.dispatcher.views.DispatcherView;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.LoadsPerVehicleResourceRenderer;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.tree.LoadsPerVehicleResource;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	import com.cemex.rms.dispatcher.views.mediators.GenericResourceRendererMediator;
	import com.cemex.rms.dispatcher.views.reports.PlantReport;
	import com.cemex.rms.dispatcher.views.reports.VehicleReport;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import flash.events.MouseEvent;
	
	import mx.binding.utils.BindingUtils;
	import mx.collections.ArrayCollection;
	import mx.controls.Image;
	import mx.controls.Menu;
	import mx.core.UIComponent;
	import mx.events.MenuEvent;
	
	public class LoadPerVehicleResourceRendererMediator extends GenericResourceRendererMediator
	{
		public function LoadPerVehicleResourceRendererMediator()
		{
			super();
		}
		
		[Inject]
		[Bindable]
		public var view:LoadsPerVehicleResourceRenderer;
		
		[Inject] public var service:IGanttServiceFactory;
		
		public override function getView():UIComponent{
			return view;
		}
		public override function processClickAction():void{
			//Alert.show("Actions:view.toolTip"+view.toolTip + "\n"+view["data"]);
		}
		
		
		public function popup(event:MouseEvent):void {
			/*
			Esta linea verifica que los menus no esten bloqueados debido a la fecha
			actual
			*/
			if(!DispatcherIslandImpl.areMenusAvailable){
				return;
			}
			if (getLabeField() == "equipNumber" && getGanttResource()["isHeader"]){
				var menuObject:XML=getResourceActions();
				if(menuObject.length()>0){
					var actions:Menu = PopupHelper.popupFromXML(event,"@label",menuObject);
					actions.addEventListener(MenuEvent.ITEM_CLICK,menuClick);
					
				}
			} else if(getLabeField() == "orderNumber" && openOTPermission()){
				var actions:Menu = PopupHelper.popupFromXML(event,"@label",getResourceActions());
				actions.addEventListener(MenuEvent.ITEM_CLICK,menuClick);
			} else if(getLabeField()=="plant"){
				var actions:Menu = PopupHelper.popupFromXML(event,"@label",getResourceActions());
				actions.addEventListener(MenuEvent.ITEM_CLICK,menuClick);
			}
		}
		public function openOTPermission():Boolean{
			var plantas:ArrayCollection=GanttServiceReference.getPlants();
			var currPlant:Object;
			for(var vi:int=0;vi<plantas.length;vi++){
				if(plantas.getItemAt(vi).plantId==this.view.data.payload.plant){
					currPlant=plantas.getItemAt(vi);
				}
			}
			/*if(((currPlant.zoperativeMode=="01" || currPlant.zoperativeMode=="02") && DispatcherIslandImpl.isBatcher==false) || (currPlant.zoperativeMode=="03" && DispatcherIslandImpl.isBatcher==true)){
				return true;
			}*/
			//return false;
			return GanttServiceReference.isVisibleField(TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID,SecurityConstants.DISP_OPEN_OTS,GanttServiceReference.getPlantType(currPlant.plantId));
		}
		public function menuClick(event:MenuEvent):void {
			
			var data:String = event.item.@data;
			if ( data!= null && data != ""  ){
				var dato:Object=getGanttResource()["data"];
				var plant:String = getGanttResource()["data"]["plant"];
				var vehicle:String = getGanttResource()["data"]["equipNumber"];
				var driver:String = getGanttResource()["data"]["driver"];
				var operativeMode:String = GanttServiceReference.getPlantType(plant);
				if(getLabeField() == "orderNumber"){
					if(event.item.@label==getLabel(OTRConstants.ORDER_QUANTITY)){
						var supporti:OpenOrderQuantity = new OpenOrderQuantity();
						supporti.orderNumber=dato['orderNumber'];
						GanttServiceReference.dispatchIslandEvent(supporti);
						return;
					} else {
						var supportii:OpenOTS = new OpenOTS();
						supportii.orderNumber=dato['orderNumber'];
						GanttServiceReference.dispatchIslandEvent(supportii);
						return;
					}
				}
				if(getLabeField() == "plant"){
					var openlph:OpenLPH = new OpenLPH();
					openlph.plant=plant;
					GanttServiceReference.dispatchIslandEvent(openlph);
					return;
				}
				if (data == SecurityConstants.MENU_SUPPORT_PLANT){
					var support:SupportPlant = new SupportPlant();
					support.EQUNR = vehicle;
					support.WERKS = plant;
					if (GanttServiceReference.isEnabledField(getViewName(),SecurityConstants.MENU_SUPPORT_PLANT,operativeMode)){
						GanttServiceReference.dispatchIslandEvent(support);
					}
				} else if(data == SecurityConstants.DISP_STONE_WASH){
					//mandamos el eventico de esa madre para que se abra
					var stwash:StoneWash = new StoneWash();
					stwash.EQUNR=vehicle;
					
						GanttServiceReference.dispatchIslandEvent(stwash);
					
				}
				else if (data == SecurityConstants.MENU_RELEASE_VEHICLE){
					
					var release:ReleaseVehicle = new ReleaseVehicle();
					release.EQUNR = vehicle;
					release.WERKS = plant;
					/*if (GanttServiceReference.isEnabledField(getViewName(),SecurityConstants.MENU_RELEASE_VEHICLE,operativeMode)){
					GanttServiceReference.dispatchIslandEvent(release);
					}*/
				}
				else if (data == SecurityConstants.MENU_VEHICLE_IN_PLANT){
					
					var vehicleInPlant:VehicleInPlant = new VehicleInPlant();
					vehicleInPlant.EQUNR = vehicle;
					vehicleInPlant.WERKS = plant;
					
					if (GanttServiceReference.isEnabledField(getViewName(),SecurityConstants.MENU_VEHICLE_IN_PLANT,operativeMode)){
						GanttServiceReference.dispatchIslandEvent(vehicleInPlant);
					}
				} else if (data == SecurityConstants.MENU_CHANGE_DRIVER){
					var changeDriver:ChangeDriver= new ChangeDriver();
					changeDriver.driver= driver;
					changeDriver.vehicle = vehicle;
					changeDriver.plant = plant;
					if (GanttServiceReference.isEnabledField(getViewName(),SecurityConstants.MENU_CHANGE_DRIVER,operativeMode)){
						GanttServiceReference.dispatchIslandEvent(changeDriver);
					}
				} else if(data==SecurityConstants.MENU_UNASSIGN){
					var carga:OrderLoad=searchLoadInNOST(dato.equipment)
					var desa:UnassignVehicle=new UnassignVehicle();
					if( carga!=null){
						desa.itemNumber=carga.itemNumber;
						desa.loadStatus=carga.loadStatus;
						desa.orderNumber=carga.orderNumber;
						desa.piEquipment=carga.equipNumber;
						desa.toPlant=carga.plant;
						desa.toPlantType=GanttServiceReference.getPlantType(carga.plant);
						GanttServiceReference.dispatchIslandEvent(desa);
					}
				}
			}
		}
		
		private function searchLoadInNOST(vehicleID:String):OrderLoad{
			var tareas:ArrayCollection=DispatcherViewMediator.currentInstance.currentTransformer.getTasksTree();
			for (var ci:int=0;ci<tareas.length;ci++){
				if(tareas.getItemAt(ci).payload.loadStatus=="NOST" && tareas.getItemAt(ci).payload.equipNumber==vehicleID){
					return tareas.getItemAt(ci).payload; 
				}
			}
			return null;
		}
		
		public function getViewName():String{
			return TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID;
		}
		
		
		
		public function getLabel(label:String):String{
			return GanttServiceReference.getLabel(label);
		}
		public function getResourceActions():XML {
			var result:XML = PopupHelper.getXML("root");
			//result.appendChild(PopupHelper.getMenuItemSepataror());
			var menu:XML;
			if(getLabeField() == "orderNumber"){
				if(openOTPermission()){
					
					menu = PopupHelper.getMenuItem(getLabel(OTRConstants.DISP_OPEN_OTS),SecurityConstants.DISP_OPEN_OTS,"extra","EVENTO/Funcion/REF",4);
					result.appendChild(menu);
					menu = PopupHelper.getMenuItem(getLabel(OTRConstants.ORDER_QUANTITY),SecurityConstants.DISP_OPEN_OTS,"extra","EVENTO/Funcion/REF",4);
					result.appendChild(menu);
					return result;
				}
				/*menu = PopupHelper.getMenuItem(getLabel(OTRConstants.ORDER_QUANTITY),SecurityConstants.DISP_OPEN_OTS,"extra","EVENTO/Funcion/REF",4);
				result.appendChild(menu);
				return result;*/
			}
			if(getLabeField() == "plant"){
				
				menu = PopupHelper.getMenuItem("Loads per hour",SecurityConstants.MENU_ASSIGN_PLANT,"extra","EVENTO/Funcion/REF",4);
				result.appendChild(menu);
				return result;
			}
			if(this.view.data.equipStatus==ExtrasConstants.VEHICLE_DISPONIBLE){
				menu = PopupHelper.getMenuItem(getLabel(OTRConstants.SUPPORT_PLANT),SecurityConstants.MENU_SUPPORT_PLANT,"extra","EVENTO/Funcion/REF",4);
				result.appendChild(menu);
				var plant:String = getGanttResource()["data"]["plant"];
				var operativeMode:String = GanttServiceReference.getPlantType(plant);
				if (GanttServiceReference.isEnabledField(getViewName(),SecurityConstants.MENU_CHANGE_DRIVER,operativeMode)){
					menu = PopupHelper.getMenuItem(getLabel(OTRConstants.CHANGE_DRIVER),SecurityConstants.MENU_CHANGE_DRIVER,"extra","EVENTO/Funcion/REF",4);
					result.appendChild(menu);
				}
			} else {
				var tareas:ArrayCollection=DispatcherViewMediator.currentInstance.currentTransformer.getTasksTree();
				for (var ci:int=0;ci<tareas.length;ci++){
					if((tareas.getItemAt(ci).payload.loadStatus!="LDNG" || tareas.getItemAt(ci).payload.codStatusProdOrder=="80") && tareas.getItemAt(ci).payload.equipNumber==this.view.data.equipNumber){
						menu = PopupHelper.getMenuItem(getLabel(OTRConstants.VEHICLE_IN_PLANT),SecurityConstants.MENU_VEHICLE_IN_PLANT,"extra","EVENTO/Funcion/REF",4);
						result.appendChild(menu);
						break;
					}
				}
				
				/*Heavy pidió que se quitara en un mail por que causaba confusion
				menu = PopupHelper.getMenuItem(getLabel(OTRConstants.UNASSIGN),SecurityConstants.MENU_UNASSIGN,"extra","EVENTO/Funcion/REF",4);
				result.appendChild(menu);*/
				
			}
			if(this.view.data.equipStatus!=ExtrasConstants.VEHICLE_ASSIGNED){
				if (GanttServiceReference.getParamNumber("RMS_DIS_STWASH")==1){
					menu = PopupHelper.getMenuItem(getLabel(OTRConstants.DISP_STONEWASH),SecurityConstants.DISP_STONE_WASH,"extra","EVENTO/Funcion/REF",4);
					result.appendChild(menu);
				}
			}
			//si llego a esta parte entonces son vehiculos los que se les dio click, hay que verificar con el stonewash
			
			/*menu = PopupHelper.getMenuItem(getLabel(OTRConstants.RELEASE_VEHICLE),SecurityConstants.MENU_RELEASE_VEHICLE,"extra","EVENTO/Funcion/REF",4);
			result.appendChild(menu);*/
			//add2(result,getMenuItemOverLap(overlaps,null,"Show Data",OrderOperationRequestEvent.SHOW_DATA_REQUEST,null,PopupHelper.MENU_ACTION_FUNCTION));
			
			
			//add2(result,getMenuItemOverLap(overlaps,null,"Show Data",OrderOperationRequestEvent.SHOW_DATA_REQUEST,null,PopupHelper.MENU_ACTION_FUNCTION));
			return result;
		}
		override public function onRegister():void {
			super.onRegister();
			//view.toolTip = TooltipHelper.getOrderTooltip(getGanttResource());
			if(getLabeField() == "orderNumber"){
				view.toolTip=getLabel(OTRConstants.SUPPORT_PLANT);
			}
			BindingUtils.bindSetter(dataSetter,view,"data");
			getView().addEventListener(MouseEvent.CLICK, popup);
			
			updateIcon();
		}
		
		public function dataSetter(data:*):void{
			
			updateIcon();
		}
		
		public function updateIcon():void{
			if (getView() != null){
				var info:Image = getView()["info"] as Image;
				
				if (getLabeField() == "equipNumber"  ){
					
					info.source = tooltipOpcion;
				}
				else if (getLabeField() == "equipStatus"){
					info.source = null;
				}
				else {
					info.source = tootipNormal;
				}
			}
		}
		
		[Bindable] [Embed(source="/assets/Request.gif")] private var tootipNormal:Class;
		[Bindable] [Embed(source="/assets/s_wdvtry.gif")] private var tooltipOpcion:Class;
		
		
		
		public override function showTooltip(field:String):String{
			var resource:LoadsPerVehicleResource = getGanttResource() as LoadsPerVehicleResource;
			if (field == "plant"){
				
				if(resource != null){
					var plant:PlantReport = resource.getReportsObject() as PlantReport;
					if (plant != null){
						var retstring:String
						try{
							retstring=TooltipHelper.getPlantsTooltip(getGanttResource(),plant);
						} catch(e:Error){
							retstring="";
						}
						return retstring;
					}
				}
				return "Cannot process Tooltip for Plant["+resource.label+"]";
			}
			else if (field == "equipNumber"){
				if(resource != null){
					var vehicle:VehicleReport = resource.getReportsObject() as VehicleReport;
					if (vehicle != null){
						return TooltipHelper.getVehicleTooltip(getGanttResource(),vehicle);
					}
				}
				return "Cannot process Tooltip for Equipment["+resource.label+"]";
			}
			else if (field == "orderNumber"){
				return  TooltipHelper.getOrderTooltip(getGanttResource());	
			}/*
				else if(field == "equipStatus"){
				return "";
				}*/
			else{ 
				return super.showTooltip(field);
			}
			
		}
	}
}