package com.cemex.rms.dispatcher.views.loadsPerVehicle.mediators
{
	import com.cemex.rms.common.gantt.GanttTask;
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.events.SearchBtnClickViewEvent;
	import com.cemex.rms.dispatcher.events.SearchBtnVehicleEvent;
	import com.cemex.rms.dispatcher.helpers.StatusHelper;
	import com.cemex.rms.dispatcher.services.IGanttServiceFactory;
	import com.cemex.rms.dispatcher.services.flashislands.helper.WDRequestHelper;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.LoadsPerVehicleTaskRenderer;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.tree.LoadsPerVehicleTask;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	import com.cemex.rms.dispatcher.views.mediators.GenericTaskRendererMediator;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import flash.filters.BitmapFilterQuality;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	
	import mx.controls.Alert;
	import mx.core.UIComponent;
	
	public class LoadPerVehicleTaskRendererMediator extends GenericTaskRendererMediator
	{
		public function LoadPerVehicleTaskRendererMediator()
		{
			super();
			
		}
		
		[Inject]
		public var view: LoadsPerVehicleTaskRenderer;
		
		[Inject]
		public var services: IGanttServiceFactory;
		
		protected override function getView():UIComponent{
			return view;
		}
		override public function onRegister():void {
			super.onRegister();
			
			var task:LoadsPerVehicleTask = getGanttTask() as LoadsPerVehicleTask;
			if (task != null){
				task.setView(view);
			}
			eventMap.mapListener(eventDispatcher,SearchBtnVehicleEvent.SEARCH_BTN_VEHICLE_CLICK_EVENT,seekInTreeView);
			/*eventMap.mapListener(eventDispatcher,SearchBtnClickViewEvent.SEARCH_BTN_CLICK_EVENT,seekInTreeView);*/
		}
		private function seekInTreeView(evt:SearchBtnVehicleEvent):void{
			if (DispatcherViewMediator.textToSearch==null) return;
			try{
				var textToSearch:String = DispatcherViewMediator.textToSearch.toLowerCase();
				var currentOrder:String = (getGanttTask().data as OrderLoad).orderLoad.toLowerCase();
				
				//efecto borde azul para búsquedas
				var _searchHiglightFilter:GlowFilter = new GlowFilter(0x23A1FF, 4, 6, 6, 6, BitmapFilterQuality.HIGH, false, false);
				//efecto sombra
				var _shadow:DropShadowFilter = new DropShadowFilter(4,72,0,0.3,4,4);
				
				if(currentOrder.indexOf(textToSearch)!=-1 && textToSearch!=""){
					(getGanttTask().data as OrderLoad).mustGlowBySearch = true;
					view.filters=[_shadow, _searchHiglightFilter];
				} else {
					(getGanttTask().data as OrderLoad).mustGlowBySearch = false;
					view.filters=[_shadow];
				}
			}catch(e:Error){
				trace("ERROR en búsqueda (LoadPerVehicle)");
			}
			
		}
		
		public override function getViewName():String{
			return TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID;
		}
		
		
		private var logger:ILogger = LoggerFactory.getLogger("LoadPerVehicle");
		
		public override function showChecked(action:String, task:GanttTask):Boolean{
			if(action == WDRequestHelper.ORDER_ON_HOLD_REQUEST){
				return StatusHelper.isOrderOnHold(task.data as OrderLoad);
			}
			return false;
		}
		
		public override function getExtraInfoIfOverlap(task:GanttTask):String {
			var payload:OrderLoad = task.data as OrderLoad;
			return payload.orderNumber+":"+payload.itemNumber+":"+payload.loadNumber + "("+payload.loadingTime+")";
		}		
	}
}