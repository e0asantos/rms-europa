package com.cemex.rms.dispatcher.views.loadsPerOrder.mediators
{
	import com.cemex.rms.common.helpers.PopupHelper;
	import com.cemex.rms.dispatcher.constants.OTRConstants;
	import com.cemex.rms.dispatcher.constants.SecurityConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.helpers.TooltipHelper;
	import com.cemex.rms.dispatcher.services.IGanttServiceFactory;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandImpl;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.OpenLPH;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.OpenOTS;
	import com.cemex.rms.dispatcher.services.flashislands.vo.Plant;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.LoadPerOrderPlantOrderResourceRenderer;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.tree.LoadsPerOrderResource;
	import com.cemex.rms.dispatcher.views.mediators.GenericResourceRendererMediator;
	import com.cemex.rms.dispatcher.views.reports.PlantReport;
	
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.Image;
	import mx.controls.Menu;
	import mx.core.UIComponent;
	import mx.events.MenuEvent;
	
	import sap.island_internal;
	
	
	
	public class LoadPerOrderPlantOrderResourceRendererMediator extends GenericResourceRendererMediator
	{
		public function LoadPerOrderPlantOrderResourceRendererMediator()
		{
			super();
		}
		
		[Inject]
		public var view:LoadPerOrderPlantOrderResourceRenderer;
		
		[Inject]
		public var services:IGanttServiceFactory;
		
		
		
		public override function getView():UIComponent{
			return view;
		}
		
		
		override public function onRegister():void {
			super.onRegister();
			view.addEventListener(MouseEvent.CLICK,clickDetection);
			//view.toolTip = TooltipHelper.getOrderTooltip(getGanttResource());
			
			var info:Image = getView()["info"] as Image;
			//if (getLabeField() == "equipLabel" && getGanttResource()["isHeader"]){
			info.source = tootipNormal;
		}
		public function getLabel(label:String):String{
			return GanttServiceReference.getLabel(label);
		}
		public function clickDetection(evt:MouseEvent):void{
			var inEvent:*=evt;
			
			var result:XML = PopupHelper.getXML("root");
			var menu:XML;
			var premenu:XML;
			if(getLabeField() == "plant"){
				premenu = PopupHelper.getMenuItem("Loads per hour",SecurityConstants.MENU_ASSIGN_PLANT,"extra","EVENTO/Funcion/REF",4);
				result.appendChild(premenu);
				var actions:Menu = PopupHelper.popupFromXML(evt,"@label",result);
				actions.addEventListener(MenuEvent.ITEM_CLICK,menuClick);
			} else if(openOTPermission()){
				premenu = PopupHelper.getMenuItem(getLabel(OTRConstants.DISP_OPEN_OTS),SecurityConstants.DISP_OPEN_OTS,"extra","EVENTO/Funcion/REF",4);
				result.appendChild(premenu);
				var actions:Menu = PopupHelper.popupFromXML(evt,"@label",result);
				actions.addEventListener(MenuEvent.ITEM_CLICK,menuClick);
			}
		}
		public function openOTPermission():Boolean{
			var plantas:ArrayCollection=GanttServiceReference.getPlants();
			var currPlant:Object;
			for(var vi:int=0;vi<plantas.length;vi++){
				if(plantas.getItemAt(vi).plantId==this.view.data.payload.plant){
					currPlant=plantas.getItemAt(vi);
				}
			}
			/*if(((currPlant.zoperativeMode=="01" || currPlant.zoperativeMode=="02") && DispatcherIslandImpl.isBatcher==false) || (currPlant.zoperativeMode=="03" && DispatcherIslandImpl.isBatcher==true)){
				return true
			}
			return false;*/
			return GanttServiceReference.isVisibleField(TVARVCPreffixes.LOADS_PER_ORDER_VIEW_ID,SecurityConstants.DISP_OPEN_OTS,GanttServiceReference.getPlantType(currPlant.plantId));
		}
		public function menuClick(event:MenuEvent):void {
			var plant:String = getGanttResource()["data"]["plant"];
			if(getLabeField() == "plant"){
				var openlph:OpenLPH = new OpenLPH();
				openlph.plant=plant;
				GanttServiceReference.dispatchIslandEvent(openlph);
				return;
			}
			var supporti:OpenOTS = new OpenOTS();
			var dato:Object=getGanttResource()["data"];
			supporti.orderNumber=dato['orderNumber'];
			GanttServiceReference.dispatchIslandEvent(supporti);
		}
		[Bindable] [Embed(source="/assets/Request.gif")] private var tootipNormal:Class;
		[Bindable] [Embed(source="/assets/Request_2.gif")] private var tooltipOpcion:Class;
		
		
		public override function showTooltip(field:String):String{
			
			if (field == "plant"){
				var resource:LoadsPerOrderResource = getGanttResource() as LoadsPerOrderResource;
				if(resource != null){
					var plant:PlantReport = resource.getReportsObject() as PlantReport;
					if (plant != null){
						var algo:*=getGanttResource();
						var planta:*=plant;
						
						var retstring:String
						try{
							retstring=TooltipHelper.getPlantsTooltip(getGanttResource(),plant);
						} catch(e:Error){
							retstring="";
						}
						return retstring;
					}
				}
				return "Cannot process Tooltip for Plant["+resource.label+"]";
			}
			else if (field == "orderNumber"){
				return  TooltipHelper.getOrderTooltip(getGanttResource());	
			}
			else{ 
				return super.showTooltip(field);
			}
			
		}
	}
}