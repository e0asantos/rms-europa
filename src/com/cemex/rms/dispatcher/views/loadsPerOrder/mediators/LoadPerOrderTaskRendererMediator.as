package com.cemex.rms.dispatcher.views.loadsPerOrder.mediators
{
	import com.cemex.rms.common.filters.FilterCondition;
	import com.cemex.rms.common.filters.FilterHelper;
	import com.cemex.rms.common.gantt.GanttTask;
	import com.cemex.rms.common.helpers.PopupHelper;
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.common.utils.DictionaryMap;
	import com.cemex.rms.dispatcher.constants.OTRConstants;
	import com.cemex.rms.dispatcher.constants.ParamtersConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.events.DispatcherConstants;
	import com.cemex.rms.dispatcher.events.SearchBtnClickViewEvent;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.helpers.StatusHelper;
	import com.cemex.rms.dispatcher.services.IGanttServiceFactory;
	import com.cemex.rms.dispatcher.services.flashislands.helper.WDRequestHelper;
	import com.cemex.rms.dispatcher.services.flashislands.vo.Equipment;
	import com.cemex.rms.dispatcher.services.flashislands.vo.Plant;
	import com.cemex.rms.dispatcher.services.flashislands.vo.Workcenter;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.LoadPerOrderTaskRenderer;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.LoadsPerVehicleTaskRenderer;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.mediators.LoadPerVehicleTaskRendererMediator;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.tree.LoadsPerVehicleTask;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	import com.cemex.rms.dispatcher.views.mediators.GenericTaskRendererMediator;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import flash.filters.BitmapFilterQuality;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.controls.Alert;
	import mx.core.UIComponent;
	import mx.utils.ObjectUtil;
	
	public class LoadPerOrderTaskRendererMediator extends GenericTaskRendererMediator
	{
		public function LoadPerOrderTaskRendererMediator()
		{
			super();
			
		}
		
		[Inject]
		public var view: LoadPerOrderTaskRenderer;
		[Inject]
		public var services: IGanttServiceFactory;
		
		protected override function getView():UIComponent{
			/*view.invalidateDisplayList();
			view.invalidateProperties();
			view.validateNow();*/
			return view;
		}
		override public function onRegister():void {
			super.onRegister();
			
			//eventMap.mapListener(eventDispatcher,UpdateDelayedTimeEvent.UPDATE_DELAYED_TIME, processTimeChanged);
			eventMap.mapListener(eventDispatcher,SearchBtnClickViewEvent.SEARCH_BTN_CLICK_EVENT,seekInTreeView);
		}
		private function seekInTreeView(evt:SearchBtnClickViewEvent):void{
			if (DispatcherViewMediator.textToSearch==null) return;
			try{
				var textToSearch:String = DispatcherViewMediator.textToSearch.toLowerCase();
				var currentOrder:String = (getGanttTask().data as OrderLoad).orderLoad.toLowerCase();
				
				//efecto borde azul para búsquedas
				var _searchHiglightFilter:GlowFilter = new GlowFilter(0x23A1FF, 4, 6, 6, 6, BitmapFilterQuality.HIGH, false, false);
				//efecto sombra
				var _shadow:DropShadowFilter = new DropShadowFilter(4,72,0,0.3,4,4);
				
				if(currentOrder.indexOf(textToSearch)!=-1 && textToSearch!=""){
					(getGanttTask().data as OrderLoad).mustGlowBySearch = true;
					view.filters=[_shadow, _searchHiglightFilter];
				} else {
					(getGanttTask().data as OrderLoad).mustGlowBySearch = false;
					view.filters=[_shadow];
				}
			}catch(e:Error){
				trace("Error en búsqueda (LoadPerOrder)");
			}
			
		}
		
		/*
		public function processTimeChanged(e:UpdateDelayedTimeEvent):void{
		var delayedTime:Date =  new Date();
		delayedTime.setSeconds(0);
		checkDelayedTime(delayedTime);
		}
		*/
		public var lastRendered:String;
		
		
		
		public override function getViewName():String{
			view.invalidateDisplayList();
			view.invalidateProperties();
			view.validateNow();
			return TVARVCPreffixes.LOADS_PER_ORDER_VIEW_ID;
		}
		
		private var logger:ILogger = LoggerFactory.getLogger("LoadPerOrder");
		public override function showChecked(action:String, task:GanttTask):Boolean{
			if(action == WDRequestHelper.ORDER_ON_HOLD_REQUEST){
				var payload:OrderLoad = task.data as OrderLoad;
				return StatusHelper.isOrderOnHold(payload);
			}
			return false;
		}
		
		
		public override function getExtraInfoIfOverlap(task:GanttTask):String {
			/*view.invalidateDisplayList();
			view.invalidateProperties();
			view.validateNow();*/
			var payload:OrderLoad = task.data as OrderLoad;
			return payload.orderNumber+":"+payload.itemNumber+":"+payload.loadNumber + "("+payload.loadingTime+")";
		}		
		
		
		/*
		public function getTVARV(name:String):TVARVC{
		return services.getFlashIslandService().getTVARVCValue(name);
		}
		public function getLabel(label:String):String{
		return services.getFlashIslandService().getOTRValue(label);
		}
		*/
		
		
		
		/*
		
		public override function getTaskActions():XML {
		
		var result:XML = PopupHelper.getXML("root");
		result.appendChild(PopupHelper.getMenuItem(getExtraInfoIfOverlap(getGanttTask()),null,null,null,0,false));
		
		
		var overlaps:ArrayCollection = getOverlap();
		
		//Cuidar el orden de este metodo y de showMenu
		addDispatcherMenu(result,overlaps);
		/ *
		if (services.getFlashIslandService().isBatcher()){
		addBatcherMenu(result,overlaps);
		}
		else if (services.getFlashIslandService().isDispatcher()){
		addDispatcherMenu(result,overlaps);
		}	
		else {
		Alert.show("No se puede desplegar El Menu, porque no tiene Asignado ningun Rol Valido el usuario ["+services.getFlashIslandService().getUser()+"] (BATCHER O DISPATCHER)");
		}
		* /
		add2(result,getMenuItemOverLap(overlaps,null,"Show Data",WDRequestHelper.SHOW_DATA_REQUEST,null,PopupHelper.MENU_ACTION_FUNCTION));
		return result;
		}
		public override function showMenu(action:String, task:GanttTask,extra:String):Boolean{
		/ *
		if(services.getFlashIslandService().isBatcher()){
		return showBatcherMenu(action,task,extra);
		}
		else if (services.getFlashIslandService().isDispatcher()){
		return showDispatcherMenu(action,task,extra);
		}
		* /
		return showDispatcherMenu(action,task,extra);
		return false;
		}
		
		*/
		
		/*
		public function addDispatcherMenu(result:XML,overlaps:ArrayCollection) :void {
		
		add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.ASSIGN_TO_PLANT), WDRequestHelper.ASSIGN_PLANT_REQUEST));
		
		
		var changePlant:XML = PopupHelper.getMenuItem(getLabel(OTRConstants.CHANGE_PLANT),null,null,null,-1,true,null,false);
		
		add2(changePlant,getMenuItemOverLap(overlaps,DispatcherConstants.ORDER_SCOPE,getLabel(OTRConstants.ORDER_LABEL), WDRequestHelper.CHANGE_PLANT_REQUEST_MENU));
		add2(changePlant,getMenuItemOverLap(overlaps,DispatcherConstants.LOAD_SCOPE,getLabel(OTRConstants.LOAD_LABEL), WDRequestHelper.CHANGE_PLANT_REQUEST_MENU));
		add2(result,changePlant);
		
		
		add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.CHANGE_VOLUME), WDRequestHelper.CHANGE_VOLUME_REQUEST));
		//add2(result,getMenuItemOverLap(overlaps,null,getLabel("REUSE"), WDRequestHelper.REUSE_CONCRETE_REQUEST));
		add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.ORDER_ON_HOLD), WDRequestHelper.ORDER_ON_HOLD_REQUEST,"check"));
		add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.CHANGE_DELIVERY_TIME), WDRequestHelper.CHANGE_DELIVERY_TIME_REQUEST));
		add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.CHANGE_FREQUENCY), WDRequestHelper.CHANGE_FREQUENCY_REQUEST));
		
		add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.ASSIGN_VEHICLE), WDRequestHelper.ASSIGN_VEHICLE_REQUEST));
		addAssignVehicles(result,overlaps);
		
		}
		
		/*
		public function filterPlants(plants:ArrayCollection , overlaps:ArrayCollection) :DictionaryMap {
		var result:DictionaryMap =  new DictionaryMap();
		for(var i:int  = 0 ; i < overlaps.length ; i ++){
		var task:GanttTask = overlaps.getItemAt(i) as GanttTask;
		for (var j:int = 0 ; j < plants.length ; j ++){
		var plant:Plant = plants.getItemAt(j) as Plant;
		var payload:OrderLoad = task.data as OrderLoad;
		if (plant.plantId == payload.plant && result.get(plant.plantId) == null){
		result.put(plant.plantId , plant);
		}
		}
		}
		return result;
		}
		
		/*
		public function addAssignVehicles(result:XML,overlaps:ArrayCollection):void{
		var plants:DictionaryMap = filterPlants(services.getFlashIslandService().getPlants(), overlaps);
		var keys:ArrayCollection = plants.getAvailableKeys();
		
		for (var i:int = 0 ; i < keys.length ; i ++ ) {
		var key:String = keys.getItemAt(i) as String;
		var plant:Plant = plants.get(key);
		
		var eq:ArrayCollection = plant.equipments;
		var sort:Sort =  new Sort();
		sort.compareFunction = LoadPerVehicleTaskRendererMediator.compareEquipments;
		eq.sort = sort;
		
		eq.refresh();
		
		// sort
		var availableVehicles:ArrayCollection = new ArrayCollection();
		
		availableVehicles.addItem(FilterHelper.getFilterConditionTVARVC("status",new ArrayCollection([GanttServiceReference.getTVARVCParam(TVARVCConstants.VEHICLES_STATUS_AVAILABLE)]),FilterCondition.EQUALS));
		if(!GanttServiceReference.getCollectFilterStatus()){
		availableVehicles.addItem(FilterHelper.getFilterCondition("equicatgry",new ArrayCollection(["V"]),FilterCondition.EQUALS));
		}
		var maxVehicles:Number = GanttServiceReference.getParamNumber(ParamtersConstants.NUM_VEHI_DISP);
		var count:int = 0 ;
		for (var k:int  = 0; k < eq.length && count < maxVehicles; k++){
		var equipment:Equipment = eq.getItemAt(k) as Equipment;
		if (FilterHelper.matchFilters(equipment,availableVehicles)){
		add2(result,getMenuItemOverLap(overlaps,WDRequestHelper.getAssignOneVehicleExtraID(plant,equipment),getLabel(OTRConstants.ASSIGN_VEHICLE)+":"+plant.plantId + ":" + equipment.equipment, WDRequestHelper.ASSIGN_ONE_VEHICLE_REQUEST));
		count++;
		}	
		}
		}
		}
		
		/*
		public function addBatcherMenu(result:XML,overlaps:ArrayCollection) :void {
		
		addDispatcherMenu(result,overlaps);
		
		
		add2(result,PopupHelper.getMenuItemSepataror());
		var plants:DictionaryMap = filterPlants(services.getFlashIslandService().getPlants(), overlaps);
		var keys:ArrayCollection = plants.getAvailableKeys();
		for (var i:int = 0 ; i < keys.length ; i ++ ) {
		var key:String = keys.getItemAt(i) as String;
		var plant:Plant = plants.get(key);
		var wc:ArrayCollection = plant.workcenter;
		for (var j:int  = 0; j < wc.length; j++ ){
		var workcenter:Workcenter = wc.getItemAt(j) as Workcenter;
		add2(result,getMenuItemOverLap(overlaps,workcenter.arbpl,getLabel(OTRConstants.BATCH) + ":" + workcenter.arbpl, WDRequestHelper.BATCH_REQUEST));
		}
		}
		add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.MANUAL_BATCH), WDRequestHelper.MANUAL_BATCH_REQUEST));
		
		add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.RE_USE), WDRequestHelper.RE_USE_REQUEST));
		add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.CONTINUE_PRODUCTION), WDRequestHelper.CONTINUE_PRODUCTION_REQUEST));
		add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.RESTART_LOAD), WDRequestHelper.RESTART_LOAD_REQUEST));
		add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.QUALITY_ADJUSTMENT), WDRequestHelper.QUALITY_ADJUSTMENT_REQUEST));
		
		
		add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.RE_PRINT), WDRequestHelper.RE_PRINT_REQUEST));
		
		add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.DISPATCH), WDRequestHelper.DISPATCH_REQUEST));
		
		// Este siempre se despliega
		add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.DISPLAY_PO), WDRequestHelper.DISPLAY_PO_REQUEST));
		}
		*/
		
		
		
		/*
		public function showDispatcherMenu(action:String, task:GanttTask, extra:String):Boolean{
		var payload:OrderLoad = task.data as OrderLoad;
		var plant:String = payload.plant;
		var operationType:String = services.getFlashIslandService().getPlantType(plant);
		
		var extraSplit:Array;
		switch (action){
		
		case WDRequestHelper.ASSIGN_VEHICLE_REQUEST:
		return checkAssignVehicle(operationType);
		case WDRequestHelper.ASSIGN_ONE_VEHICLE_REQUEST:
		
		extraSplit = extra.split(":");
		//if ((payload.shipConditions == GanttServiceReference.getTVARVCParam(TVARVCConstants.COLLECT_ORDER).low && extraSplit[4] == "V")
		//	|| (payload.shipConditions != GanttServiceReference.getTVARVCParam(TVARVCConstants.COLLECT_ORDER).low && extraSplit[4] == "W") ){
		if ((payload.shipConditions == GanttServiceReference.getTVARVCParam(TVARVCConstants.SHIPCOND).low && extraSplit[5] == "W")
		|| (payload.shipConditions != GanttServiceReference.getTVARVCParam(TVARVCConstants.SHIPCOND).low && extraSplit[5] == "V") ){
		//se deben mostra
		if (extra.indexOf(plant) == 0){
		logger.debug("ASSIGN_ONE_VEHICLE_REQUEST ("+checkAssignVehicle(operationType)+":"+plant+":"+operationType+") extra:("+extra + "):payload.shipConditions:(" + payload.shipConditions+")"+ ":SHIPCOND("+GanttServiceReference.getTVARVCParam(TVARVCConstants.SHIPCOND).low+")");
		return checkAssignVehicle(operationType);
		}
		else {
		logger.debug("ASSIGN_ONE_VEHICLE_REQUEST (false:"+plant+") extra:("+extra + "):payload.shipConditions:(" + payload.shipConditions+")"+ ":SHIPCOND("+GanttServiceReference.getTVARVCParam(TVARVCConstants.SHIPCOND).low+")");
		return false;
		}
		}
		else {
		
		logger.debug("ASSIGN_ONE_VEHICLE_REQUEST (false) extra:("+extra + "):payload.shipConditions:(" + payload.shipConditions+")"+ ":SHIPCOND("+GanttServiceReference.getTVARVCParam(TVARVCConstants.SHIPCOND).low+")");
		return false;
		}
		
		case WDRequestHelper.ASSIGN_PLANT_REQUEST:
		
		}
		//Alert.show("Show:" +action);
		return true;
		}
		public function checkAssignVehicle(operationType:String):Boolean {
		
		
		if (operationType == GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_C).low){
		// CENTRALIZADO				
		return true;	
		} 
		else if (operationType == GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_D).low
		|| operationType == GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_A).low){
		// DISTRIBUIDO// AUTONOMOUS
		return false;
		}
		else {
		return false;
		}
		
		}
		*/
		
		
		
		
	}
}