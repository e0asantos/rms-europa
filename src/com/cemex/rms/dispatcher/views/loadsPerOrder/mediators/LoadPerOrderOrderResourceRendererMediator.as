package com.cemex.rms.dispatcher.views.loadsPerOrder.mediators
{
	import com.cemex.rms.dispatcher.helpers.TooltipHelper;
	import com.cemex.rms.dispatcher.services.IGanttServiceFactory;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.LoadPerOrderOrderResourceRenderer;
	import com.cemex.rms.dispatcher.views.mediators.GenericResourceRendererMediator;
	
	import flash.events.MouseEvent;
	
	import mx.controls.Image;
	import mx.core.UIComponent;
	
	
	
	public class LoadPerOrderOrderResourceRendererMediator extends GenericResourceRendererMediator
	{
		public function LoadPerOrderOrderResourceRendererMediator()
		{
			super();
		}
		
		[Inject]
		public var view:LoadPerOrderOrderResourceRenderer;
		
		[Inject]
		public var services:IGanttServiceFactory;
		
		
		
		public override function getView():UIComponent{
			return view;
		}
		
		override public function onRegister():void {
			super.onRegister();
			view.addEventListener(MouseEvent.CLICK,clickDetection);
			var info:Image = view.info;
			var order:Image = view.order;
			//if (getLabeField() == "equipLabel" && getGanttResource()["isHeader"]){
			info.source = tootipNormal;
			order.source = orderIcon;
			
			
		}
		
		public function clickDetection(evt:MouseEvent):void{
			var inEvent:*=evt;
		}
		
		/*[Embed(source="/assets/s_s_wspo.gif")]            
		[Bindable]             
		private var orderIcon:Class;
*/		
		[Embed(source="/assets/s_s_wspo_bom_2.png")]            
		[Bindable]             
		private var order_bom:Class;
		
		[Embed(source="/assets/s_s_wspo_rbom.png")]            
		[Bindable]             
		private var order_rbom:Class;
		
		[Embed(source="/assets/s_s_wspo_3.png")]            
		[Bindable]             
		private var orderIcon:Class;
		
		
		[Bindable] [Embed(source="/assets/Request.gif")] private var tootipNormal:Class;
		[Bindable] [Embed(source="/assets/Request_2.gif")] private var tooltipOpcion:Class;
		
		
		public override function showTooltip(field:String):String{
			
			if (field == "orderNumber"){
				return  TooltipHelper.getOrderTooltip(getGanttResource());	
			}
			else{ 
				return super.showTooltip(field);
			}
			
		}
	}
}