package com.cemex.rms.dispatcher.views.loadsPerOrder.tree
{
	import com.cemex.rms.common.gantt.GanttResource;
	import com.cemex.rms.dispatcher.constants.OTRConstants;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.helpers.StatusHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import flash.events.Event;
	
	import mx.controls.Alert;
	import mx.controls.dataGridClasses.DataGridListData;

	public dynamic class LoadsPerOrderResource extends GanttResource
	{
		public var indexRow:int=-1;
		public var indexColumn:int=-1;
		public function LoadsPerOrderResource() {
			super();
		}
		
		public function handleDataChanged(event:Event):void {       
			// Cast listData to DataGridListData. 
			/*var myListData:DataGridListData = 
				DataGridListData(listData);*/
			
			// Access information about the data passed 
			// to the cell renderer.
			/*text="row index: " + String(myListData.rowIndex) + 
				" column index: " + String(myListData.columnIndex);*/
		}  
		
		public override function get label():String {
			var etiqueta:String = super.label;
			if(super.labelField=="plant" && super.data.hasOwnProperty("name")){
				etiqueta=etiqueta+" "+super.data["name"];
			}
			var extra:String = "";
			var additionalExtra:String="";
			if (getLabelField() == "orderNumber"){
				var algo1:*=data;
				if(data.hasOwnProperty("renegFlag")){
					if(data["renegFlag"]==true){
						extra = "\n"+GanttServiceReference.getLabel(OTRConstants.RENEGOTIATION_LABEL);
					}
				}
				if (StatusHelper.isOrderRenegotiated(data)){
					extra = "\n"+GanttServiceReference.getLabel(OTRConstants.RENEGOTIATION_LABEL);
				}
				 
				/*if((data as OrderLoad).renegFlag){
					extra = "\n"+GanttServiceReference.getLabel(OTRConstants.RENEGOTIATION_LABEL);
				}*/
				if (this.data[ "paytermLabel"] != null && this.data[ "paytermLabel"] != ""){
					if (extra == ""){
						extra +="\n";
					}
					else {
						extra +=" - ";
					}
					extra += this.data["paytermLabel"]
				}
				if(String(this.data["prodOrder"]).toLowerCase().indexOf("res")!=-1){
					if (extra == ""){
						extra +="\n";
					}
					else {
						extra +=" - ";
					}
					extra += this.data["prodOrder"];
				}
				if(GanttServiceReference.getParamString("DS_DISP_SO_CUST",this.data.plant)=="NUMCUS"){
					additionalExtra="\n"+super.data["soldToName"];
				}
					
			}
			return etiqueta+extra+additionalExtra;
		}
			
	
		
			
		
		
	}
}