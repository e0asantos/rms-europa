package com.cemex.rms.dispatcher.views.loadsPerOrder.tree
{
	import com.cemex.rms.common.date.FlexDateHelper;
	import com.cemex.rms.dispatcher.constants.OTRConstants;
	import com.cemex.rms.dispatcher.constants.ParamtersConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.helpers.ColorHelper;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.helpers.StatusHelper;
	import com.cemex.rms.dispatcher.services.flashislands.vo.ColorSetting;
	import com.cemex.rms.dispatcher.views.tree.OrderLoadGanttTask;

	public class LoadsPerOrderTask extends OrderLoadGanttTask
	{
		public function LoadsPerOrderTask()
		{
			super();
			
		}
		
		public override function get startTime():Date{
			if (payload == null){
				return null;
			}
			/*
			if (payload.startTime == null ){
				payload.startTime = getRawStartTime();
			}*/
			return payload.startTime;
		}
		
		public override function set startTime(_startTime:Date):void{
			if (payload != null){
				payload.startTime = _startTime;
			}
		}
		public override function get endTime():Date {
			if (payload == null){
				return null;
			}
			return getRawEndTime();
		}
		public override function set endTime(_endTime:Date):void{
			if (payload != null){
				payload.endTime = _endTime;
			}
		}
		
		public function get vehicleNumber():String {
			if (payload == null){
				return null;
			}
			return "-";
		}
		
		/*
		public override function getRawStartTime():Date{
			return getStartTime();
		}
		public override function getRawEndTime():Date{
			return getEndTime();
		}
		*/
		
		
		public override function getRawStartTime():Date{
			//var payload:OrderLoad = data as OrderLoad;
			
			//return FlexDateHelper.parseStringTime(payload.loadingDate,payload.loadingTime);
			
			return FlexDateHelper.parseTime(payload.loadingDate,payload.loadingTime);
			
			//return FlexDateHelper.parseTime(FlexDateHelper.getDateObject(payload.schedLineDate),payload.loadingTime);
		}
		public override function getRawEndTime():Date{
			//var payload:OrderLoad = data as OrderLoad;
			//antes teníamos unos minutos definidos en 3
			//return FlexDateHelper.addMinutes(startTime,GanttServiceReference.getLoadMinutes());
			//ahora por parametro estamos obteniendo los minutos
			if(GanttServiceReference.getParamString(ParamtersConstants.RMS_LOAD_SIZE)==null){
				return 	FlexDateHelper.addMinutes(startTime,GanttServiceReference.getLoadMinutes());
			} else {
				return FlexDateHelper.addMinutes(startTime,Number(GanttServiceReference.getParamNumber(ParamtersConstants.RMS_LOAD_SIZE)));
			}
		}
		

		public function get valueAddedCP():String {
			var result:String = "";
			if (payload.valueAdded != null){
				result = payload.valueAdded;	
			}
			if(payload.constructionProduct != null){
				if (result != ""){
					result += "-";
				}
				result += payload.constructionProduct;
			}
			return result;
		}
		
		
		public override function initExtraValues():void {
			super.initExtraValues();
		
			
			
			if (payload == null){
				return ;
			}
			payload.startTime = getRawStartTime();
			
				
			if (StatusHelper.isRenegotiated(payload)){
				renegotiationLabel = GanttServiceReference.getLabel(OTRConstants.RENEGOTIATION_LABEL);
			}
			//var currColor:ColorSetting = DispatcherColorHelper.getColor(payload);
			var currColor:ColorSetting = ColorHelper.getColor(payload,TVARVCPreffixes.LOAD_SCOPE);
			//Alert.show(currColor.status);
			/*color = currColor.fillColor;
			border = currColor.borderColor;
			thickness = currColor.borderThickness;*/
			
			
			//Alert.show(ReflectionHelper.object2XML(currColor,"currColor"));
			if (StatusHelper.isConstructionProduct(payload)){
				cpWidth = "100%";
				pumpWidth = "0";
				loadWidth = "0";
				
			}
			else if (StatusHelper.isPumpingService(payload)){
				cpWidth = "0";
				pumpWidth = "100%";
				loadWidth = "0";			
			}
			else  {
				cpWidth = "0";
				pumpWidth =  "0";
				loadWidth = "100%";
			}
			
			//resourcePosition()
		}
		
		
		public var cpLabel:String;
		
		[Bindable]
		public var pumpWidth:String = "0";
		
		[Bindable]
		public var loadWidth:String = "0";
		
		[Bindable]
		public var cpWidth:String = "0";
		
	}
}