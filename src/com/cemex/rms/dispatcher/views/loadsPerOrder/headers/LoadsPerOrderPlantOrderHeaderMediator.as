package com.cemex.rms.dispatcher.views.loadsPerOrder.headers
{
	
	import com.cemex.rms.dispatcher.constants.OTRConstants;
	import com.cemex.rms.dispatcher.events.FocusInSearchEvent;
	import com.cemex.rms.dispatcher.events.SearchBtnClickViewEvent;
	import com.cemex.rms.dispatcher.events.TogglePlaintViewEvent;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	
	import org.robotlegs.mvcs.Mediator;
	
	
	
	public class LoadsPerOrderPlantOrderHeaderMediator extends Mediator
	{
		public function LoadsPerOrderPlantOrderHeaderMediator()
		{
			super();
		}
		
		[Inject]
		public var view:LoadsPerOrderPlantOrderHeader;
		
		
		public function getLabel(label:String):String{
			return GanttServiceReference.getLabel(label);
		}
		override public function onRegister():void {
			
			view.allPlants.text = getLabel(OTRConstants.DISP_SEARCH);
			view.allPlants.toolTip = getLabel(OTRConstants.ALL_LINK_TOOLTIP);
			view.allPlants.addEventListener(MouseEvent.CLICK,mouseeventHandler);
			//view.searchBtn.addEventListener(MouseEvent.CLICK,searchBtnClick)
			view.searchText.addEventListener(Event.CHANGE,searchBtnClick);
			/*view.searchText.addEventListener(KeyboardEvent.KEY_UP,searchBtnClick);
			eventMap.mapListener(eventDispatcher,KeyboardEvent.KEY_DOWN,setTextGlobalSearch);*/
			view.searchText.text="";
		}
		public function setTextGlobalSearch(evt:KeyboardEvent):void{
			if(evt.charCode==8){
				view.searchText.text=view.searchText.text.substr(0,-1);
			} else {
				if(evt.charCode!=Keyboard.ENTER){
					view.searchText.text+=String.fromCharCode(evt.charCode);
				}
			}
			
			view.searchText.setSelection(view.searchText.text.length,view.searchText.text.length);
		}
		public function searchBtnClick(evt:Event):void{
			DispatcherViewMediator.textToSearch=view.searchText.text;
			var busqueda:SearchBtnClickViewEvent=new SearchBtnClickViewEvent();
			busqueda.searchString=view.searchText.text;
			busqueda.refTextInput=view.searchText;
			eventDispatcher.dispatchEvent(busqueda);
		}
		public function  mouseeventHandler(e:MouseEvent):void{
			eventDispatcher.dispatchEvent(new TogglePlaintViewEvent());
			
		}
		public function elementFocusChange(evt:FocusEvent):void{	
			
			var focusIn:FocusInSearchEvent= new FocusInSearchEvent(FocusInSearchEvent.FOCUS_IN_SEARCH);
			focusIn.refTextInput=view.searchText;
			eventDispatcher.dispatchEvent(focusIn);
		}
	}
}