package com.cemex.rms.dispatcher.views.loadsPerOrder.headers
{
	
	import com.cemex.rms.dispatcher.constants.OTRConstants;
	import com.cemex.rms.dispatcher.events.TogglePlaintViewEvent;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	
	import flash.events.MouseEvent;
	
	import org.robotlegs.mvcs.Mediator;
	
	
	
	public class LoadsPerOrderOrderHeaderMediator extends Mediator
	{
		public function LoadsPerOrderOrderHeaderMediator()
		{
			super();
		}
		
		[Inject]
		public var view:LoadsPerOrderOrderHeader;
		
		override public function onRegister():void {
			
			view.groupPerPlant.text = getLabel(OTRConstants.LOADS_PER_ORDER_HEADER);
			view.groupPerPlant.toolTip = getLabel(OTRConstants.LOADS_PER_ORDER_HEADER_TOOLTIP);
			
			
			view.groupPerPlant.addEventListener(MouseEvent.CLICK,mouseeventHandler);
		}
		
		public function  mouseeventHandler(e:MouseEvent):void{
			eventDispatcher.dispatchEvent(new TogglePlaintViewEvent());
			
		}
		
		public function getLabel(label:String):String{
			return GanttServiceReference.getLabel(label);
		}
	}
}