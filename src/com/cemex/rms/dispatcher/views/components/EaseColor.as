package com.cemex.rms.dispatcher.views.components
{
	import fl.motion.Color;
	import flash.geom. *;
	import flash.events.EventDispatcher;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import flash.display.*;
	
	public class EaseColor extends flash.events.EventDispatcher
	{
		////////////////////////////////////////////////////////////
		//
		// Variables
		//
		////////////////////////////////////////////////////////////
		// init vars
		protected var timer : Number;
		protected var speed : Number;
		protected var obj : Object;
		protected var origRgb : uint;
		protected var newRgb : uint;
		protected var easeEquation : Function;
		// Private
		protected var time : Number;
		protected var type : Function;
		protected var timerObject : Timer;
		// Public
		public var isTweening : Boolean = false;
		// Color objects
		protected var colorTrans : ColorTransform;
		protected var trans : Transform;
		public function EaseColor (oc : Number, nc : Number, o : DisplayObject , completionTime : Number, s : Number, eeq : Function=null)
		{
			timer = 0;
			speed = s;
			obj = o;
			origRgb = oc;
			newRgb = nc;
			time = completionTime;
			type = eeq;
			colorTrans = new ColorTransform ();
			buildInterval();
		}
		/*
		*easing engine
		* usage: easeEquation (timeCount, start, finish - start, easeTime);
		*/
		protected function doEase (event : TimerEvent) : void
		{
			var e : Number = type (timer, 0, 1 - 0, time);
			//var c : uint = Color.interpolateColor (origRgb, newRgb, e);
			if (timer == Math.round (time / 3))
			{
				dispatchEvent (new Event ("oneThird"));
			}
			if (timer == Math.round (time / 2))
			{
				dispatchEvent (new Event ("halfWay"));
			}
			if (timer == Math.round ((time / 3) * 2))
			{
				dispatchEvent (new Event ("twoThirds"));
			}
			if (timer >= time)
			{
				timerObject.stop ();
				timerObject.reset ();
				isTweening = false;
				dispatchEvent (new Event ("finish"));
			}
			colorTrans.color = c;
			//this.obj.transform.colorTransform = colorTrans;
			event.updateAfterEvent ();
			timer ++;
		}
		protected function buildInterval () : void
		{
			timerObject = new Timer (speed, 0);
			timerObject.addEventListener ("timer", doEase);
			timerObject.start ();
			isTweening = true;
		}
		public function continueTo (origColor : uint, newColor : uint, s : Number, t : Number) : void
		{
			timer = 0;
			origRgb = origColor;
			newRgb = newColor;
			speed = s;
			time = t;
			buildInterval();
		}
		public function kill () : void
		{
			timerObject.stop ();
			timerObject.reset ();
		}
	}

}