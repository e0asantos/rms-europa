package com.cemex.rms.dispatcher.views.reports
{
	public class VehicleReport extends ReportObject
	{
		public function VehicleReport()
		{
		}
		
		
		[Bindable]
		public var UOM:String ="" ;
		[Bindable]
		public var DeliveredVolume:Number = 0 ;
		[Bindable]
		public var DeliveredLoadsCount:Number = 0 ;
		
		[Bindable]
		public var PendingConfirmedVolume:Number = 0;
		[Bindable]
		public var PendingConfirmedLoadsCount:Number = 0 ;
		
		//Includes on hold and make up loads
		[Bindable]
		public var PendingToBeConfirmedVolume:Number = 0;
		[Bindable]
		public var PendingToBeConfirmedLoadsCount:Number = 0 ;
	}
}