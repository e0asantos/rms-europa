package com.cemex.rms.dispatcher.views.reports
{
	import mx.controls.Alert;

	public class ViewReportObject extends ReportObject
	{
		public function ViewReportObject()
		{
		}
		[Bindable]
		public var optimalCost:Number = 0 ;
		[Bindable]
		public var currentCost:Number = 0 ;
	}
}