package com.cemex.rms.dispatcher.views.reports
{
	import com.cemex.rms.common.filters.FilterHelper;
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	
	import mx.collections.ArrayCollection;

	public class ReportConfig
	{
		public function ReportConfig() {
		}
		
		
		public static const COUNT:String = "COUNT_REPORT";
		public static const ACCUMULATE:String = "ACCUMULATE_REPORT";
		public static const KEEP_LAST_VALUE:String = "KEEP_LAST_VALUE_REPORT";
		
		
		public var name:String;

		public var type:String;
		
		//public var resourceField:String;
		public var reportField:String;
		public var dataField:String;
		public var filters:ArrayCollection;
		
		private static var logger:ILogger = LoggerFactory.getLogger("ReportConfig");
		
		
		public function valid(data:Object):Boolean{
			if (data == null){
				return false;
			}
			if (filters != null){
				if (!FilterHelper.matchFilters(data,filters)){
					return false;
				}
			}
			return true;
		}
		public function addData(data:Object,reportObject:Object):void {
			
			if (valid(data) && data.hasOwnProperty(dataField)) {
				
				if (data[dataField] != null){
					if (type == COUNT){
						setData(reportObject,reportField, reportObject[reportField] + 1); 
					}
					else if (type == KEEP_LAST_VALUE){
						setData(reportObject,reportField, data[dataField]);
					}
					else if (type == ACCUMULATE) {
						
						if (reportObject[reportField] == null){
							setData(reportObject,reportField, data[dataField]);
						}
						else {
							setData(reportObject,reportField, reportObject[reportField] + data[dataField]);
						}
						logger.debug("["+reportField+"]new value["+reportObject[reportField]+"] added ["+data[dataField]+"]");
					}
				}
			}			
		}
		
		public function setData(reportObject:Object , reportField:String,data:Object):void{
			if (data != null){
				reportObject[reportField] = data;
			}
		}
		
		public function delData(data:Object,reportObject:Object):void {
			
			if (valid(data) && data.hasOwnProperty(dataField)) {
				if (data[dataField] != null){
					if (type == COUNT){
						setData(reportObject,reportField, reportObject[reportField] - 1);
						
					}
					else if (type == KEEP_LAST_VALUE){
						setData(reportObject,reportField, data[dataField]);
						
					}
					else if (type == ACCUMULATE) {
						if (reportObject[reportField] == null){
							setData(reportObject,reportField, data[dataField]);
						}
						else {
							setData(reportObject,reportField, reportObject[reportField] - data[dataField]);
						}
						logger.debug("["+reportField+"]new value["+reportObject[reportField]+"] delete ["+data[dataField]+"]");
					}
				}
			}
			
		}
	}
}