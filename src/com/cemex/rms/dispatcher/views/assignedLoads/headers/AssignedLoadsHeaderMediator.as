package com.cemex.rms.dispatcher.views.assignedLoads.headers
{
	
	import com.cemex.rms.dispatcher.constants.OTRConstants;
	import com.cemex.rms.dispatcher.events.FocusInSearchEvent;
	import com.cemex.rms.dispatcher.events.SearchBtnClickViewEvent;
	import com.cemex.rms.dispatcher.events.SearchBtnVehicleEvent;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	
	import org.robotlegs.mvcs.Mediator;
	
	
	
	public class AssignedLoadsHeaderMediator extends Mediator
	{
		public function AssignedLoadsHeaderMediator()
		{
			super();
		}
		
		[Inject]
		public var view:AssignedLoadsHeader;
		
		
		public function getLabel(label:String):String{
			return GanttServiceReference.getLabel(label);
		}
		
		override public function onRegister():void {
			
			//view.addEventListener(MouseEvent.CLICK,mouseeventHandler);
			view.assignedLoads.text = "Search";//getLabel(OTRConstants.ASSIGNED_LOADS_HEADER);
			view.searchText.text="";
			view.assignedLoads.toolTip = getLabel(OTRConstants.ASSIGNED_LOADS_HEADER_TOOLTIP);
			view.searchText.addEventListener(Event.CHANGE,searchBtnClick);
			/*view.searchText.addEventListener(KeyboardEvent.KEY_UP,searchBtnClick);
			eventMap.mapListener(eventDispatcher,KeyboardEvent.KEY_DOWN,setTextGlobalSearch);*/
		}
		public function setTextGlobalSearch(evt:KeyboardEvent):void{
			if(evt.charCode==8){
				view.searchText.text=view.searchText.text.substr(0,-1);
			} else {
				view.searchText.text+=String.fromCharCode(evt.charCode);
			}
			view.searchText.setSelection(view.searchText.text.length,view.searchText.text.length);
		}
		public function searchBtnClick(evt:Event):void{
			
			DispatcherViewMediator.textToSearch=view.searchText.text;
			var busqueda:SearchBtnVehicleEvent=new SearchBtnVehicleEvent(SearchBtnVehicleEvent.SEARCH_BTN_VEHICLE_CLICK_EVENT);
			busqueda.searchString=view.searchText.text;
			busqueda.refTextInput=view.searchText;
			eventDispatcher.dispatchEvent(busqueda);
		}
		public function  mouseeventHandler(e:MouseEvent):void{
			if (e.ctrlKey){
				
				
				e.stopPropagation();
				
			}
		}
		public function elementFocusChange(evt:FocusEvent):void{			
			var focusIn:FocusInSearchEvent= new FocusInSearchEvent(FocusInSearchEvent.FOCUS_IN_SEARCH);
			focusIn.refTextInput=view.searchText;
			eventDispatcher.dispatchEvent(focusIn);
		}

	}
}