package com.cemex.rms.dispatcher.views.assignedLoads.mediators
{
	import com.cemex.rms.common.helpers.PopupHelper;
	import com.cemex.rms.dispatcher.constants.OTRConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.events.SearchBtnVehicleEvent;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.helpers.StatusHelper;
	import com.cemex.rms.dispatcher.services.flashislands.helper.WDRequestHelper;
	import com.cemex.rms.dispatcher.views.assignedLoads.AssignedLoadsTaskRenderer;
	import com.cemex.rms.dispatcher.views.assignedLoads.tree.AssignedLoadsTask;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	import com.cemex.rms.dispatcher.views.mediators.GenericTaskRendererMediator;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import flash.filters.BitmapFilterQuality;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	
	import mx.collections.ArrayCollection;
	import mx.core.UIComponent;
	import mx.events.ResizeEvent;
	
	public class AssignedLoadsTaskRendererMediator extends GenericTaskRendererMediator
	{
		public function AssignedLoadsTaskRendererMediator()
		{
			super();
			
		}
		
		[Inject]
		public var view: AssignedLoadsTaskRenderer;
		
		protected override function getView():UIComponent{
			return view;
		}
		override public function onRegister():void {
			super.onRegister();
			
			var task:AssignedLoadsTask = getGanttTask() as AssignedLoadsTask;
			
			view.addEventListener(ResizeEvent.RESIZE,resize);
			if (task != null){
				task.setView(view);
			} 
			eventMap.mapListener(eventDispatcher,SearchBtnVehicleEvent.SEARCH_BTN_VEHICLE_CLICK_EVENT,seekInTreeView);
			//BindingUtils.bindSetter(invalidateAlgo,view,["width"]);
		}
		private function seekInTreeView(evt:SearchBtnVehicleEvent):void{
			if (DispatcherViewMediator.textToSearch==null) return;
			try{
				var textToSearch:String = DispatcherViewMediator.textToSearch.toLowerCase();
				var currentOrder:String = (getGanttTask().data as OrderLoad).orderLoad.toLowerCase();
				
				//efecto borde azul para búsquedas
				var _searchHiglightFilter:GlowFilter = new GlowFilter(0x23A1FF, 4, 6, 6, 6, BitmapFilterQuality.HIGH, false, false);
				//efecto sombra
				var _shadow:DropShadowFilter = new DropShadowFilter(4,72,0,0.3,4,4);
				
				if(currentOrder.indexOf(textToSearch)!=-1 && textToSearch!=""){
					(getGanttTask().data as OrderLoad).mustGlowBySearch = true;
					view.filters=[_shadow, _searchHiglightFilter];
				} else {
					(getGanttTask().data as OrderLoad).mustGlowBySearch = false;
					view.filters=[_shadow];
				}
			}catch(e:Error){
				trace("Error en búsqueda (AssignedLoads)");
			}
			
		}
		public override function getViewName():String{
			return TVARVCPreffixes.ASSIGNED_LOADS_VIEW_ID;
		}
		public function resize(e:ResizeEvent):void{
			var task:AssignedLoadsTask = getGanttTask() as AssignedLoadsTask;
			if(task!=null){
				task.dynamicColors();
			}
			//dynamicColors();
		}
		
		public var lastRendered:String;
		/*
		public override function processTimeChangedEveryMinute():void{
		updateColorsTask();
		}
		*/
		public function isOrderOnHold():Boolean {
			return StatusHelper.isOrderOnHold(getGanttTask().data as OrderLoad);
		}
		
		/*
		public function getLabel(label:String):String{
		return GanttServiceReference.getLabel(label);
		}
		*/
		/*
		public override function getTaskActions():XML {
		var result:XML = PopupHelper.getXML("root");
		
		
		var overlaps:ArrayCollection = getOverlap();
		
		
		
		add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.START_BATCHING), WDRequestHelper.START_BATCHING_REQUEST));
		add2(result,PopupHelper.getMenuItemSepataror());
		add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.CHANGE_PLANT), WDRequestHelper.CHANGE_PLANT_REQUEST_MENU));
		add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.RENEGOTIATE), WDRequestHelper.RENEGOTIATE_LOAD_REQUEST));
		add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.UNASSIGN_TO_PLANT), WDRequestHelper.UNASSIGN_PLANT_REQUEST));
		
		add2(result,PopupHelper.getMenuItemSepataror());
		add2(result,getMenuItemOverLap(overlaps,null,"Show Data",WDRequestHelper.SHOW_DATA_REQUEST,null,PopupHelper.MENU_ACTION_FUNCTION));
		
		return result;
		}
		*/
		
		
		
		
		/* 
		
		private function getGanttTask():GanttTask {
		if (view["data"] != null){
		return view["data"]["data"] as GanttTask;
		}
		return null;
		}
		*/
		
	}
}