package com.cemex.rms.dispatcher.views.assignedLoads.mediators
{
	import com.cemex.rms.common.helpers.PopupHelper;
	import com.cemex.rms.dispatcher.constants.OTRConstants;
	import com.cemex.rms.dispatcher.constants.SecurityConstants;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.helpers.TooltipHelper;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandImpl;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.OpenLPH;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.OpenOTS;
	import com.cemex.rms.dispatcher.views.assignedLoads.AssignedLoadsResourceRenderer;
	import com.cemex.rms.dispatcher.views.assignedLoads.tree.AssignedLoadsResource;
	import com.cemex.rms.dispatcher.views.mediators.GenericResourceRendererMediator;
	import com.cemex.rms.dispatcher.views.reports.PlantReport;
	import com.cemex.rms.dispatcher.views.reports.VehicleReport;
	
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Image;
	import mx.controls.Menu;
	import mx.core.UIComponent;
	import mx.events.MenuEvent;
	
	
	
	public class AssignedLoadsResourceRendererMediator extends GenericResourceRendererMediator
	{
		public function AssignedLoadsResourceRendererMediator()
		{
			super();
		}
		
		[Inject]
		public var view:AssignedLoadsResourceRenderer;
		
		public override function getView():UIComponent{
			return view;
		}
		public override function processClickAction():void{
			//Alert.show("Actions:view.toolTip"+view.toolTip + "\n"+view["data"]);
		}
		
		public override function processClickDato():void{
			//Alert.show("DATOS:view.toolTip"+view.toolTip + "\n"+view["data"]);
		}
		
		
		override public function onRegister():void {
			super.onRegister();
			view.toolTip = TooltipHelper.getOrderTooltip(getGanttResource());
			view.addEventListener(MouseEvent.CLICK,clickDetection);
			var info:Image = getView()["info"] as Image;
			
			info.source = tootipNormal;
		}
		public function getLabel(label:String):String{
			return GanttServiceReference.getLabel(label);
		}
		public function clickDetection(evt:MouseEvent):void{
			var inEvent:*=evt;
			
			var result:XML = PopupHelper.getXML("root");
			var menu:XML;
			var premenu:XML;
			if(getLabeField() == "plant"){
				premenu = PopupHelper.getMenuItem("Loads per hour",SecurityConstants.MENU_ASSIGN_PLANT,"extra","EVENTO/Funcion/REF",4);
				result.appendChild(premenu);
				var actionsForLPH:Menu = PopupHelper.popupFromXML(evt,"@label",result);
				actionsForLPH.addEventListener(MenuEvent.ITEM_CLICK,menuClick);
				return;
			}
			
			if(openOTPermission()){
				
				premenu = PopupHelper.getMenuItem(getLabel(OTRConstants.DISP_OPEN_OTS),SecurityConstants.MENU_ASSIGN_PLANT,"extra","EVENTO/Funcion/REF",4);
				result.appendChild(premenu);
				var actions:Menu = PopupHelper.popupFromXML(evt,"@label",result);
				actions.addEventListener(MenuEvent.ITEM_CLICK,menuClick);
			}
		}
		public function openOTPermission():Boolean{
			var plantas:ArrayCollection=GanttServiceReference.getPlants();
			var currPlant:Object;
			for(var vi:int=0;vi<plantas.length;vi++){
				if(plantas.getItemAt(vi).plantId==this.view.data.payload.plant){
					currPlant=plantas.getItemAt(vi);
				}
			}
			if(((currPlant.zoperativeMode=="01" || currPlant.zoperativeMode=="02") && DispatcherIslandImpl.isBatcher==false) || (currPlant.zoperativeMode=="03" && DispatcherIslandImpl.isBatcher==true)){
				return true
			}
			return false;
		}
		public function menuClick(event:MenuEvent):void {
			var plant:String = getGanttResource()["data"]["plant"];
			if(getLabeField() == "plant"){
				var openlph:OpenLPH = new OpenLPH();
				openlph.plant=plant;
				GanttServiceReference.dispatchIslandEvent(openlph);
				return;
			}
			var supporti:OpenOTS = new OpenOTS();
			var dato:Object=getGanttResource()["data"];
			supporti.orderNumber=dato['orderNumber'];
			GanttServiceReference.dispatchIslandEvent(supporti);
		}
		[Bindable] [Embed(source="/assets/Request.gif")] private var tootipNormal:Class;
		[Bindable] [Embed(source="/assets/Request_2.gif")] private var tooltipOpcion:Class;
		
		
		
		public override function showTooltip(field:String):String{
			var resource:AssignedLoadsResource = getGanttResource() as AssignedLoadsResource;
			if (field == "plant"){
				
				if(resource != null){
					var plant:PlantReport = resource.getReportsObject() as PlantReport;
					if (plant != null){
						var retstring:String;
						try{
							retstring=TooltipHelper.getPlantsTooltip(getGanttResource(),plant);
						} catch(e:Error){
							retstring="";
						}
						return retstring;
					}
				}
				return "Cannot process Tooltip for Plant["+resource.label+"]";
			}
			else if (field == "equipLabel"){
				if(resource != null){
					var vehicle:VehicleReport = resource.getReportsObject() as VehicleReport;
					if (vehicle != null){
						return TooltipHelper.getVehicleTooltip(getGanttResource(),vehicle);
					}
				}
				return "Cannot process Tooltip for Plant["+resource.label+"]";
			}
			else if (field == "orderNumber" || field=="orderLoad"){
				return  TooltipHelper.getOrderTooltip(getGanttResource());	
			}
			else{ 
				return super.showTooltip(field);
			}
			
		}
	}
	
}