package com.cemex.rms.dispatcher.views.serviceAgentScreen.tree
{
	import com.cemex.rms.common.gantt.GanttResource;
	import com.cemex.rms.common.gantt.GanttTree;
	import com.cemex.rms.common.tree.NodeResource;
	import com.cemex.rms.common.tree.NodeTask;
	import com.cemex.rms.dispatcher.helpers.TimeZoneHelper;
	
	import mx.collections.ArrayCollection;
	
	public class ServiceAgentScreenTree extends GanttTree
	{
		public function ServiceAgentScreenTree(fields:ArrayCollection)
		{
			super(fields);
		}
		
		protected override function hasSameValues(target:Object,temp:Object):Boolean {
			var targetTask:ServiceAgentScreenTask = target as ServiceAgentScreenTask;
			var tempTask:ServiceAgentScreenTask = temp as ServiceAgentScreenTask;
			return targetTask != null && tempTask != null &&
				(""+targetTask.payload.itemNumber) == (""+tempTask.payload.itemNumber) &&
				(""+targetTask.payload.orderNumber) == (""+tempTask.payload.orderNumber);
		}
		
		protected override function newNode():NodeResource{
			return new ServiceAgentScreenResource();
		}
		
		protected override function newTask():NodeTask {
			return new ServiceAgentScreenTask();
		}
		
		public override function timeChangedTask(time:Date,nodeTask:NodeTask):Boolean{
			
			var nowTime:Number=TimeZoneHelper.getServer().getTime();
			var task:ServiceAgentScreenTask = nodeTask as ServiceAgentScreenTask;
			var startingTime:Number = task.startTime.getTime();
			var endTime:Number = task.endTime.getTime();
			var loadingTime:Number = task.getRawStartTime().getTime();
			var timeTemp:Number = (nowTime - loadingTime) / (60*1000);
			task.initExtraValues();
			
			getTasksTree().itemUpdated(task);
			return true;
		}
		override public function deleteOldNode(oldNode:NodeResource,node:NodeResource,field:String):Boolean{
			return false;
		}
		
	}
}