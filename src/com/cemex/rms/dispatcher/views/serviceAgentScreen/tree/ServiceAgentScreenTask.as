package com.cemex.rms.dispatcher.views.serviceAgentScreen.tree
{
	import com.cemex.rms.common.date.FlexDateHelper;
	import com.cemex.rms.common.tree.NodeTask;
	import com.cemex.rms.common.utils.DictionaryMap;
	import com.cemex.rms.dispatcher.constants.OTRConstants;
	import com.cemex.rms.dispatcher.constants.ParamtersConstants;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.helpers.StatusHelper;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandImpl;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.LoadsPerVehicleTaskRenderer;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	import com.cemex.rms.dispatcher.views.serviceAgentScreen.renderers.ServiceAgentScreenResourceRenderer;
	import com.cemex.rms.dispatcher.views.serviceAgentScreen.renderers.ServiceAgentScreenTaskRenderer;
	import com.cemex.rms.dispatcher.views.tree.OrderLoadGanttTask;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	public class ServiceAgentScreenTask extends OrderLoadGanttTask
	{
		public static var franceTaskAcomodationRows:DictionaryMap=new DictionaryMap();
		public static var franceTaskProcessed:Array=[];
		
		public function ServiceAgentScreenTask()
		{
			super();
			//create the special france effect
		}
		public override function get startTime():Date{
			if (payload == null){
				return null;
			}
			/*
			if (payload.startTime == null ){
			payload.startTime = getRawStartTime();
			}*/
			return payload.startTime;
		}
		public override function set startTime(_startTime:Date):void{
			if (payload != null){
				payload.startTime = _startTime;
			}
		}
		private var _view:ServiceAgentScreenTaskRenderer;
		public function setView(_view:ServiceAgentScreenTaskRenderer):void{
			this._view = _view;
			initExtraValues();
		}
		public override function get endTime():Date {
			if (payload == null){
				return null;
			}
			return getRawEndTime();
		}
		public override function set endTime(_endTime:Date):void{
			if (payload != null){
				payload.endTime = _endTime;
			}
		}
		
		public function get vehicleNumber():String {
			if (payload == null){
				return null;
			}
			return "-";
		}
		
		/*
		public override function getRawStartTime():Date{
		return getStartTime();
		}
		public override function getRawEndTime():Date{
		return getEndTime();
		}
		*/
		
		
		public override function getRawStartTime():Date{
			//var payload:OrderLoad = data as OrderLoad;
			
			//return FlexDateHelper.parseStringTime(payload.loadingDate,payload.loadingTime);
			
			return FlexDateHelper.parseTime(payload.loadingDate,payload.loadingTime);
			
			//return FlexDateHelper.parseTime(FlexDateHelper.getDateObject(payload.schedLineDate),payload.loadingTime);
		}
		public override function getRawEndTime():Date{
			//var payload:OrderLoad = data as OrderLoad;
			/*return FlexDateHelper.addMinutes(startTime,GanttServiceReference.getLoadMinutes());*/
			var payload:OrderLoad = data as OrderLoad;
			var fecha:Date =new Date();
			if(payload.constructionProduct=="EP"){
				if(GanttServiceReference.getParamNumber(ParamtersConstants.RMS_LOAD_SIZE)!=0){
					fecha=FlexDateHelper.addMinutes(startTime,Number(GanttServiceReference.getParamNumber(ParamtersConstants.RMS_LOAD_SIZE)));
				} else {
					fecha=FlexDateHelper.addSeconds(startTime,payload.cycleTimeMillis);
				}
				
			} else {
				fecha=FlexDateHelper.addSeconds(startTime,payload.cycleTimeMillis);
			}
			return fecha;
		}
		
		
		public function get valueAddedCP():String {
			var result:String = "";
			if (payload.valueAdded != null){
				result = payload.valueAdded;	
			}
			if(payload.constructionProduct != null){
				if (result != ""){
					result += "-";
				}
				result += payload.constructionProduct;
			}
			return result;
		}
		
		
		public override function initExtraValues():void {
			super.initExtraValues();
			
			
			
			if (payload == null){
				return ;
			}
			payload.startTime = getRawStartTime();
			
			
			if (StatusHelper.isRenegotiated(payload)){
				renegotiationLabel = GanttServiceReference.getLabel(OTRConstants.RENEGOTIATION_LABEL);
			}
			
			if (StatusHelper.isConstructionProduct(payload)){
				cpWidth = "100%";
				pumpWidth = "0";
				loadWidth = "0";
				
			}
			else if (StatusHelper.isPumpingService(payload)){
				cpWidth = "0";
				pumpWidth = "100%";
				loadWidth = "0";			
			}
			else  {
				cpWidth = "0";
				pumpWidth =  "0";
				loadWidth = "100%";
			}
			
			if(franceTaskProcessed.indexOf(payload.orderNumber+payload.itemNumber)==-1){
				var numOrder:String=payload.orderNumber;
				var itemNumber:String=payload.itemNumber;
				var currentOrderArray:Array=franceTaskAcomodationRows.get(payload.orderNumber);
				if(currentOrderArray==null){
					franceTaskAcomodationRows.put(payload.orderNumber,new Array());
					currentOrderArray=franceTaskAcomodationRows.get(payload.orderNumber);
				}
				var isDisplayed:Boolean=false;
				for(var l:int=0;l<currentOrderArray.length;l++){
					if(getRawStartTime().time>currentOrderArray[l]){
						//create new row
						payload.indexPosition=l;
						currentOrderArray[l]=getRawEndTime().time;
						isDisplayed=true;
						break;
					} /*else if(getRawStartTime().time<currentOrderArray[l]){
						
						break;
					}*/
				}
				if(!isDisplayed){
					currentOrderArray.push(getRawEndTime().time);
					payload.indexPosition=currentOrderArray.length-1;
				}
				if(currentOrderArray.length==0){
					currentOrderArray.push(getRawEndTime().time);
				}
				franceTaskProcessed.push(payload.orderNumber+payload.itemNumber);
				ServiceAgentScreenResourceRenderer.expandableItems.put(payload.plant+":"+payload.orderNumber,currentOrderArray.length);
			}
		}
		
		
		public var cpLabel:String;
		
		[Bindable]
		public var pumpWidth:String = "0";
		
		[Bindable]
		public var loadWidth:String = "0";
		
		[Bindable]
		public var cpWidth:String = "0";
	}
}