package com.cemex.rms.dispatcher.views.serviceAgentScreen.transformers
{
	import com.cemex.rms.common.gantt.GanttResource;
	import com.cemex.rms.common.gantt.GanttTask;
	import com.cemex.rms.common.gantt.GanttTree;
	import com.cemex.rms.common.transformer.FieldColumn;
	import com.cemex.rms.common.transformer.GenericDataTransformer;
	import com.cemex.rms.common.transformer.IDataTransformer;
	import com.cemex.rms.common.utils.DictionaryMap;
	import com.cemex.rms.dispatcher.constants.TVARVCConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.views.serviceAgentScreen.headers.ServiceAgentScreenHeaderView;
	import com.cemex.rms.dispatcher.views.serviceAgentScreen.renderers.ServiceAgentScreenResourceRenderer;
	import com.cemex.rms.dispatcher.views.serviceAgentScreen.renderers.ServiceAgentScreenTaskRenderer;
	import com.cemex.rms.dispatcher.views.serviceAgentScreen.renderers.ServiceAgentScreenTreeResourceRenderer;
	import com.cemex.rms.dispatcher.views.serviceAgentScreen.tree.ServiceAgentScreenTask;
	import com.cemex.rms.dispatcher.views.serviceAgentScreen.tree.ServiceAgentScreenTree;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.core.ClassFactory;
	import mx.core.IFactory;
	
	import ilog.gantt.GanttDataGrid;
	import ilog.gantt.GanttSheet;
	
	public class ServiceAgentScreenDataTransformer extends com.cemex.rms.common.transformer.GenericDataTransformer
	{
		
		[Embed(source="/assets/plus.gif")]
		public var plus:Class;
		
		[Embed(source="/assets/minus.gif")]
		public var minus:Class; 
		
		[Embed(source="/assets/s_b_plnt.gif")]            
		[Bindable]             
		private var plant:Class;
		
		public function ServiceAgentScreenDataTransformer()
		{
			//TODO: implement function
			super();
			ServiceAgentScreenTask.franceTaskAcomodationRows=new DictionaryMap();
			ServiceAgentScreenTask.franceTaskProcessed=[];
			setFields(new ArrayCollection(["plant","orderNumber"])
			);//,AssignedLoadsTooltipHelper.getResourceData());
			
			setFieldColumns(new ArrayCollection([
				new FieldColumn("label",null,"Tree",200,ServiceAgentScreenTreeResourceRenderer,ServiceAgentScreenHeaderView),
				//				new FieldColumn("orderNumber","Order",150),
				new FieldColumn("label",null,"",0,ServiceAgentScreenResourceRenderer)
			]));
		}
		public override function getTaskRenderer():IFactory{
			return new ClassFactory(ServiceAgentScreenTaskRenderer);
		}
		public override function getResourceRenderer():IFactory{
			return null;
		}
		public override function getViewID():String{
			return TVARVCPreffixes.SERVICE_AGENT_SCREEN_VIEW_ID;
		}
		public override function addData(data:Object, step:String, date:Date):void{
			super.addData(data,step,date);
		}
		public override function addHeader(data:Object):void{
			if(!data.hasOwnProperty("arrivalDate")){
				super.addHeader(data);
			}
		}
		public override function setTreePlusMinus(dataGrid:GanttDataGrid):void{
			
			dataGrid.setStyle("disclosureOpenIcon", minus);
			dataGrid.setStyle("disclosureClosedIcon", plus);
			
		}
		
		public override function setFields(fields:ArrayCollection):void{
			this.fields =  fields;
			tree = new ServiceAgentScreenTree(fields);
		}
		
		public override function setColumnsDataGridCustomization(dataGrid:GanttDataGrid):void{
			//super.setColumnsDataGridCustomization(dataGrid);
			//dataGrid.styleFunction = styleFunction;
			dataGrid.iconFunction = iconFunction;
			tree.currentDataGrid=dataGrid;
			
			
		}
		
		override public function getSorter():Sort
		{
			//TODO: implement function
			return null;
		}
		
		override public function getSorterFunction(field:String):Function
		{
			//TODO: implement function
			return null;
		}
		[Embed(source="/assets/s_s_wspo_orange.png")]            
		[Bindable]             
		private var order_bom_orange:Class;
		
		[Embed(source="/assets/s_s_wspo_gray.png")]            
		[Bindable]             
		private var order_bom_gray:Class;
		
		[Embed(source="/assets/s_s_wspo_red.png")]            
		[Bindable]             
		private var order_bom_red:Class;
		
		[Embed(source="/assets/s_s_wspo_3.png")]            
		[Bindable]             
		private var order:Class;
		
		[Embed(source="/assets/s_s_wspo_rbom.png")]            
		[Bindable]             
		private var order_rbom:Class;
		
		[Embed(source="/assets/s_s_wspo.gif")]
		[Bindable]
		private var normal_order:Class;
		
		
		[Embed(source="/assets/s_b_plnt_distr.gif")]            
		[Bindable]             
		private var plant_distribuida:Class;
		
		[Embed(source="/assets/s_b_plnt_centr.gif")]            
		[Bindable]             
		private var plant_centralizada:Class;
		
		[Embed(source="/assets/s_b_plnt_auton.gif")]            
		[Bindable]             
		private var plant_autonoma:Class;
		
		[Embed(source="/assets/s_s_wspo_green.png")]            
		[Bindable]             
		private var order_green:Class;
		private function iconFunction(resource:GanttResource):Class{
			/*(order_bom as Image).toolTip="Hello World";
			(order as Image).toolTip="Hello World";*/
			if (resource.getLabelField() == "orderNumber"){
				/*if(String(resource.data.bomValid).toLowerCase()=="x"){
				return order_bom;
				} else {
				return order;	
				}
				*/					
				if(resource.data.bomValid=="1"){
					//rojo
					return order_bom_red;
				} else if(resource.data.bomValid=="2"){
					//color naranja
					return order_bom_orange;
				} else if(resource.data.bomValid=="3"){
					//color gris
					return order_bom_gray;
				}
				if(resource.data.flagRecipe!=""){
					//return order_rbom;
				}
				return order_green;
				
			}
			else if (resource.getLabelField() == "plant"){
				var plantId:String = (""+resource.data["plantId"]);
				var plantType:String = GanttServiceReference.getPlantType(plantId);
				var allothem:String=GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_D).low+"_"+GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_C).low+"_"+GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_A).low;
				if (Number(plantType) == Number(GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_D).low)){
					return plant_distribuida;	
				}
				else if (Number(plantType) == Number(GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_C).low)){
					return plant_centralizada;
				}
				else if (Number(plantType) == Number(GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_A).low)){
					return plant_autonoma;
				}
			}
			return null;
		}
	}
}