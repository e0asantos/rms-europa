package com.cemex.rms.dispatcher.vo
{
	import com.cemex.rms.common.date.FlexDateHelper;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.dispatcher.constants.ExtrasConstants;
	import com.cemex.rms.dispatcher.helpers.TimeZoneHelper;
	import com.cemex.rms.dispatcher.services.IGanttServiceFactory;
	
	import flash.display.DisplayObject;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;

	public class OrderLoad extends Object
	{	
		public function OrderLoad() {
		}
		
		// Load Data
		public var graphicReference:Canvas;
		public var itemNumber:String="";
		public var loadNumber:String="";
		public var plant:String="";
		public var equipLabel:String="";
		public var strSuppl3:String="";
		public var location:String="";
		
		
		private var _loadingDate:Date;
		private var _loadingTime:String="";

		public var resId:String="";
		public var materialId:String="";
		public var materialDes:String="";
		public var comingFromPush:Boolean=false;
		public var vehicleLabel:String="";
		public var loadVolume:Number;
		public var loadUom:String="";
		public var itemCurrency:String="";
		public var itemCategory:String="";
		public var confirmQty:Number;
		
		public var hlevelItem:Number;
		public var mdLabel:String;
		public var deliveryGroup:Number;
		public var SAPStamp:String;
		public var codStatusProdOrder:String;
		public var _prodOrder:String;
		public var numProdOrder:String;
		public var txtStatusProdOrder:String;
		public var _indexPosition:Number;
		public var _indexPositionAux:Number;
		public var workAroundDestroy:String;
		public var visible:String;
		public var slump:String;
		public var taskNumber:String;
		public var timeStamp:String;
		public var totalLoads:String;
		public var bigOrder:String;
		public var mdGroup:String;
		public var mdGroupQty:String;
		public var loadCount:String;
		//public var numberTimeStamp:Number;
		public var _flagRecipe:String="";
		public var mustGlowBySearch:Boolean;
		public var overlapedTask:Boolean;
		public var otcFlag:String;
		public var deliveryDate:String;
		public var podCity:String;
		public var podHouseNoStreet:String;
		public var podPostalCode:String;
		public var podSiteName:String
		
		public function set podcity(value:String):void{
			if(value!=null){
				podCity=value;
			}
		}
		public function set podhousenostreet(value:String):void{
			if(value!=null){
				podHouseNoStreet=value;
			}
		}
		public function set podpostalcode(value:String):void{
			if(value!=null){
				podPostalCode=value;
			}
		}
		public function set podsitename(value:String):void{
			if(value!=null){
				podSiteName=value;
			}
		}
		public function get podcity():String{
			return podCity;
		}
		public function get podhousenostreet():String{
			return podHouseNoStreet;
		}
		public function get podpostalcode():String{
			return podPostalCode;
		}
		public function get podsitename():String{
			return podSiteName;
		}
		
		public function set flagRecipe(value:String):void{
			if(value!=null){
				_flagRecipe=value;
			} else {
				_flagRecipe="";
			}
		}
		public function set numberTimeStamp(value:Number):void{
			//read only
		}
		public function get numberTimeStamp():Number{
			if(timeStamp==null || timeStamp==""){
				return 0;
			}
			return Number(timeStamp.substr(0,-4));
		}
		public function get flagRecipe():String{
			return _flagRecipe;
		}
		public function set indexPosition(value:Number):void{
			if(!isNaN(value)){
				_indexPosition=value;
			} else {
				_indexPosition=1;
			}
		}
		
		public function get indexPosition():Number{
			if(isNaN(_indexPosition)){
				return 0;
			} else {
				return _indexPosition;
			}
		}
		
		
		public function set indexPositionAux(value:Number):void{
			if(!isNaN(value)){
				_indexPositionAux=value;
			} else {
				_indexPositionAux=1;
			}
		}
		
		public function get indexPositionAux():Number{
			if(isNaN(_indexPositionAux)){
				return 0;
			} else {
				return _indexPositionAux;
			}
		}		
		/*
		Se borran los datos de  vehiculos
		//public var license:String="";
		//public var vehicleId:String="";
		//public var driver:String="";
		//public var truckVolume:String="";
		//public var equipLabel:String="";
		//public var vehicleType:String="";
		*/
		
		public var equipNumber:String="";
		public function get equipStatus():String{
			
			if (equipNumber != null && equipNumber != ""){
				return ExtrasConstants.VEHICLE_ASSIGNED;
			}
			else {
				return ExtrasConstants.LOAD_UNASSIGNED;
			}	
		}
		public function set prodOrder(valor:String):void{
			_prodOrder=valor;
		}
		
		public function get prodOrder():String{
			if(_prodOrder!=null){
				return _prodOrder;
			} else {
				return numProdOrder;
			}
		}
		
		public function set equipStatus(_equipStatus:String):void{}
		
		
		
		public var idHlevelItem:String="";
		private var _timePumpJobs:String="";
		
		public var remainFlag:Boolean;
		public var additionalFlag:Boolean;
		private var _cycleTime:String="";
		
		private var _unloadingTime:String="";
		private var _deliveryTime:String="";
		
		public var valueAdded:String="";
		public var constructionProduct:String="";
		public var mepTotcost:Number;
		public var totCost:Number;
		public var loadStatus:String="";

		public var timeFixIndicator:String="";
		public var loadFrequency:String="";
		public var deliveryAmount:String="";
		public var shipmentStatus:String="";
		public var completedDelayed:Boolean;
		
		
		
		
		
		
		//Jobsite Info
		public var jobSiteId:String="";
		public var jobSiteName:String="";
		public var houseNoStreet:String="";
		public var postalCode:String="";
		public var city:String="";
		public var city2:String="";
		public var comentDisp:String="";
		
		
		
		// Order Info
		public var orderNumber:String="";
		public var soldToNumber:String="";
		public var soldToName:String="";
		public var contactName1:String="";
		public var contactName2:String="";
		public var contactTel:String="";
		public var orderStatus:String="";
		public var deliveryBlock:String="";
		public var shipConditions:String="";
		public var renegFlag:Boolean;
		
		public var orderUom:String="";
		public var orderVolume:Number;
		
		public var warning:String="";
		public var paytermLabel:String="";
		public var orderReason:String="";
		public var vehicleMaxVol:Number;
		public var vehicleType:String;
		
		public var fillColor:String="";
		public var borderColor:String="";
		
		public var pfillColor:String="";
		public var pborderColor:String="";
		
		public var _bomValid:String="";
		public var useGlow:Boolean=false;
		
		
		//EXtras
		public var addProd:ArrayCollection;
		public var _texts:ArrayCollection;
		
		public function set bomValid(value:String):void{
			if(value != null){
				_bomValid=value;
			}
		}
		
		public function get bomValid():String{
			return _bomValid;
		}
		
		public function set texts(value:ArrayCollection):void{
			if(value!=null){
				if(value.length>0){
					_texts=value;
				}
			}
		}
		public function get texts():ArrayCollection{
			return _texts;
		}
		
		public function set text(value:Array):void{
			_texts=new ArrayCollection(value);
		}
		
		public function get text():Array{
			if(_texts==null){
				return [];
			} else {
				return _texts.source;
			}
		}
		
		
		// FirstConcrete
		public var firstOrderNumber:String="";
		public var firstDescription:String="";
		private var _firstDeliveryTime:String="";
		
		
		
		// ConstructionProduct
		public var cpOrderNumber:String="";
		public var cpDescription:String="";
		public var cpQuantity:Number;
		public var cpUom:String="";
		
		
		
		// customInformation	
		//public var totalVolumeService:String="";
		public var _startTime:Date;
		public var _numericDate:int;
		public var endTime:Date; 
		public var orderLoad:String="";
		private var _loadingTimestamp:Date;
		public var cycleTimeMillis:Number;
		
		
		public function set startTime(value:Date):void{
			_startTime=value;
		}
		
		public function get startTime():Date{
			return _startTime;
		}
		
		private var _isDelayed:Boolean=false;
		public function set isDelayed(value:Boolean):void{
			//_isDelayed=value;
		}
		
		public function get isDelayed():Boolean{
			return _isDelayed;
		}
		public function loadingTimeStampFix():Date{
			var __loading:Date=new Date();
			if(this.loadingDate!=null){
				__loading=this.loadingDate;
				//pegar hora
				var splitTime:Array=this.loadingTime.split(":");
				__loading.hours=parseInt(splitTime[0]);
				__loading.minutes=parseInt(splitTime[0]);
			}
			return __loading;
		}
		public function get loadingTimestamp():Date{
			if (_loadingTimestamp == null){
				if (_loadingTime != null && _loadingDate != null){
					_loadingTimestamp = FlexDateHelper.parseTime(_loadingDate,_loadingTime);
				}
				if(_loadingDate==null && this.loadStatus=="DELE"){
					_loadingTimestamp=new Date();
				}
				_numericDate=this._loadingTimestamp.getTime();
				_numericDate=this._loadingTimestamp.time;
				if(this._loadingTimestamp.time<TimeZoneHelper.getServer().time){
					_isDelayed=true;
				}
			return _loadingTimestamp;
			}
			return TimeZoneHelper.getServer();
		}
		public function set loadingTimestamp(_loadingTimestamp:Date):void{
			this._loadingTimestamp = _loadingTimestamp;
			_numericDate=this._loadingTimestamp.getTime();
			_numericDate=this._loadingTimestamp.time;
			if(this._loadingTimestamp.time<TimeZoneHelper.getServer().time){
				_isDelayed=true;
			}
		}
		
		public function get loadingTime():String{
			return _loadingTime;
		}
		public function get loadingDate():Date{
			return _loadingDate;
		}
		public function set loadingTime(_loadingTime:String):void{
			
			this._loadingTime = FlexDateHelper.arrangeHour(_loadingTime);
			if (_loadingDate != null){
				_loadingTimestamp = FlexDateHelper.parseTime(_loadingDate,_loadingTime);
			}
			if (this._loadingTimestamp!=null){
				_isDelayed=false;
				if(this._loadingTimestamp.time<TimeZoneHelper.getServer().time){
					_isDelayed=true;
				}
			}
		}
		public function set loadingDate(_loadingDate:Date):void{
			this._loadingDate = _loadingDate;
			if (_loadingTime != null){
				_loadingTimestamp = FlexDateHelper.parseTime(_loadingDate,_loadingTime);
			}
			if(this._loadingTimestamp.time<TimeZoneHelper.getServer().time){
				_isDelayed=true;
			}
		}
		public function set cycleTime(_cycleTime:String):void {
			if (_cycleTime != null){
				this._cycleTime = FlexDateHelper.arrangeHour(_cycleTime);
				var split:Array = this._cycleTime.split(":");
				cycleTimeMillis = Number(split[0])*3600 + Number(split[1])*60 + Number(split[2]);
			}	
		}
		public function set mepTotCost(value:Number):void{
			this.mepTotcost=value;
		}
		public function get mepTotCost():Number{
			return this.mepTotcost;
		}
		public function get cycleTime():String{
			return _cycleTime;
		}
		
		
		public function set unloadingTime(_unloadingTime:String):void {
			if (_unloadingTime != null){
				this._unloadingTime = FlexDateHelper.arrangeHour(_unloadingTime);
			}	
		}
		public function get unloadingTime():String{
			return _unloadingTime;
		}
		public function set deliveryTime(_deliveryTime:String):void {
			if (_deliveryTime != null){
				this._deliveryTime = FlexDateHelper.arrangeHour(_deliveryTime);
			}	
		}
		public function get deliveryTime():String{
			return _deliveryTime;
		}
		
		
		public function set firstDeliveryTime(_firstDeliveryTime:String):void {
			if (_firstDeliveryTime != null){
				this._firstDeliveryTime = FlexDateHelper.arrangeHour(_firstDeliveryTime);
			}	
		}
		public function get firstDeliveryTime():String{
			return _firstDeliveryTime;
		}
		public function set timePumpJobs(_timePumpJobs:String):void {
			if (_timePumpJobs != null){
				this._timePumpJobs = FlexDateHelper.arrangeHour(_timePumpJobs);
			}	
		}
		public function get timePumpJobs():String{
			return _timePumpJobs;
		}
		
		
		
		
		public function clone():OrderLoad{
			return ReflectionHelper.cloneObject(this) as OrderLoad;
		}
		
		public function equals(load:OrderLoad):Boolean {
			return ReflectionHelper.compareObject(this,load);
		}
		
	}
}