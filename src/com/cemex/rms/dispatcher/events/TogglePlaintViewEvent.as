package com.cemex.rms.dispatcher.events
{
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	import flash.events.Event;
	/**
	 * Este evento le avisa a todos los mediadores que lo requieran que el tiempo cambio
	 * para que cambien su vista
	 * 
	 * @author japerezh
	 * 
	 */	
	public class TogglePlaintViewEvent extends Event
	{
		public static const TOGGLE_PLAIN_VIEW:String ="togglePlainView";
		
		
		public function TogglePlaintViewEvent(bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(TOGGLE_PLAIN_VIEW, bubbles, cancelable);
		}
	}
}