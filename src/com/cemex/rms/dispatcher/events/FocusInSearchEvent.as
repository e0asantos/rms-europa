package com.cemex.rms.dispatcher.events
{
	import flash.events.Event;
	
	import mx.controls.TextInput;
	
	public class FocusInSearchEvent extends Event
	{
		public static const FOCUS_IN_SEARCH:String="foscusSearchEvent;"
		public var refTextInput:TextInput;
		
		public function FocusInSearchEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}