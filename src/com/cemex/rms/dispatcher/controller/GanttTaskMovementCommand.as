package com.cemex.rms.dispatcher.controller
{
	import com.cemex.rms.common.date.FlexDateHelper;
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.dispatcher.constants.ParamtersConstants;
	import com.cemex.rms.dispatcher.constants.SecurityConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.events.DispatcherConstants;
	import com.cemex.rms.dispatcher.events.GanttTaskMovementEvent;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.services.flashislands.helper.WDRequestHelper;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandImpl;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.UnassignQuick;
	import com.cemex.rms.dispatcher.services.flashislands.vo.ProductionData;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	
	import org.robotlegs.mvcs.Command;

	public class GanttTaskMovementCommand extends Command
	{
		public function GanttTaskMovementCommand() {
			
			super();
		}
		private var logger:ILogger = LoggerFactory.getLogger("GanttTaskMovementCommand");
		
		[Inject] public var event:GanttTaskMovementEvent;
		
		/**
		 * Este metodo lanza los eventos respectivos utilizando los datos necesarios respecto al cambio que se hizo
		 * durante el drag and drop
		 * 
		 */
		
		
		public override function  execute():void {
			var vista:String=DispatcherViewMediator.currentView;
			var from:OrderLoad = event.origin;
			var to:OrderLoad = event.destiny;
			
			var request:OrderLoad = from.clone();
			var prodData:ProductionData = GanttServiceReference.getProductionData(request);
			var toPlantType:String = GanttServiceReference.getPlantType(request.plant);
			if(from.orderStatus=="BLCK" || from.loadStatus=="BLCK" || vista==TVARVCPreffixes.PLANNING_SCREEN_VIEW_ID){
				return;
			}
			//logger.info("from("+ReflectionHelper.object2XML(from)+")\n"+
			//	"to("+ReflectionHelper.object2XML(to)+")\n"+
			//	"request("+ReflectionHelper.object2XML(request)+")\n");
			if (from.plant != to.plant) {
				logger.info("plant");
				request.plant = to.plant;
				
				if (GanttServiceReference.isEnabledField(DispatcherViewMediator.currentView,SecurityConstants.DRAG_CHANGE_PLANT,toPlantType) && GanttServiceReference.isVisibleField(DispatcherViewMediator.currentView,SecurityConstants.DRAG_CHANGE_PLANT,toPlantType)){	
					GanttServiceReference.dispatchIslandEvent(
						WDRequestHelper.getChangePlantRequestDrag(request,prodData,toPlantType,
							DispatcherConstants.LOAD_SCOPE						
						)	
					);
				}
			}
			else if (from.equipNumber != to.equipNumber) {
				logger.info("equipNumber");
				var smt:Object=GanttServiceReference.getParamNumber("RMS_CHG_VEH_PROG");
				if(to.equipNumber == ""){
					request.equipNumber = to.equipNumber;
					//request.equipLabel = to.equipLabel;
					if (GanttServiceReference.isEnabledField(DispatcherViewMediator.currentView,SecurityConstants.DRAG_UNASSIGN_VEHICLE,toPlantType) && smt==1){	
						GanttServiceReference.dispatchIslandEvent(
							WDRequestHelper.getUnassignVehicleRequest(request,prodData,toPlantType,null)
						);
					}
				} else if(from.equipNumber==""){
					//probar que la carga pueda entrar en el mismo tipo de vehiculo
					if(isConveyor(from,to.equipNumber)){
						request.equipNumber = to.equipNumber;
						//request.equipLabel = to.equipLabel;
						if (GanttServiceReference.isEnabledField(DispatcherViewMediator.currentView,SecurityConstants.DRAG_ASSIGN_VEHICLE,toPlantType) ){
							//si no es verde ni gris
							if(/*toPlantType!="03" &&*/ toPlantType!="02"){
								GanttServiceReference.dispatchIslandEvent(
									WDRequestHelper.getAssignVehicleRequest(request,prodData,toPlantType,null,"X")
								);
							}
						}
					} else {
						Alert.show("The load needs to be assigned to a conveyor vehicle type","Error");
					}
				} else {
					if(isConveyor(from,to.equipNumber)){
						request.equipNumber = to.equipNumber;
						//request.equipLabel = to.equipLabel;
						if (GanttServiceReference.isEnabledField(DispatcherViewMediator.currentView,SecurityConstants.DRAG_ASSIGN_VEHICLE,toPlantType)){
							//si no es verde ni gris
							if((toPlantType!="03" && toPlantType!="02" && DispatcherIslandImpl.isBatcher==false) || ((toPlantType=="03" || toPlantType=="02") && DispatcherIslandImpl.isBatcher==true)){
								GanttServiceReference.dispatchIslandEvent(
									WDRequestHelper.getExchangeVehicleRequest(request,prodData,toPlantType,null,"X")
								);
							}
						}
					} else {
						Alert.show("The load needs to be assigned to a conveyor vehicle type","Error")
					}
				}
			}
			else if (FlexDateHelper.getTimestampString(from.startTime) != FlexDateHelper.getTimestampString(to.startTime) && ((to.resId.split(":").length==3 && DispatcherViewMediator.currentView==TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID) || (to.resId.split(":").length==2 && DispatcherViewMediator.currentView==TVARVCPreffixes.LOADS_PER_ORDER_VIEW_ID)) ){
				
				logger.info("time");
				request.startTime = FlexDateHelper.copyDate(to.startTime);
				//request.endTime = FlexDateHelper.addMinutes(request.startTime,3);
				
				if (GanttServiceReference.isEnabledField(DispatcherViewMediator.currentView,SecurityConstants.DRAG_CHANGE_DELIVERY_TIME,toPlantType)){
					GanttServiceReference.dispatchIslandEvent(
						WDRequestHelper.getChangeDeliveryTimeRequest(request,prodData,toPlantType,null)
					);				
				}
			}
			else {
				var unasp:UnassignQuick =  new UnassignQuick();
				unasp.orderNumber =  from.orderNumber;
				unasp.itemNumber  = from.itemNumber;
				unasp.toPlant = from.plant;
				unasp.toPlantType = toPlantType;
				unasp.pi_equipment= " ";
				GanttServiceReference.dispatchIslandEvent(unasp);
				// No se tiene otra funcionalidad con el Drag & Drop
			}
			/*
			
			service.getFlashIslandService().orderOnHold(
				
				GanttTaskRendererHelper.getOrderOnHoldRequest(
					event.task.payload
				)
			);*/
			
		}
		
		public function isConveyor(sourceLoad:OrderLoad,vehicleDestination:String):Boolean{
			var ve:ArrayCollection=DispatcherViewMediator.vehiculosCorrectos;
			var isConveyor:Boolean=false;
			for(var q:int=ve.length-1;q>-1;q--){
				if(ve.getItemAt(q).vehicleType=="995" && vehicleDestination==ve.getItemAt(q).equipment){
					isConveyor=true;
				}
			}
			var isLoadConveyor:Boolean=false;
			for(var qq:int=sourceLoad.addProd.length-1;qq>-1;qq--){
				if(sourceLoad.addProd.getItemAt(qq).conveyor=="X" && sourceLoad.addProd.getItemAt(qq).itemNumber==sourceLoad.itemNumber && isConveyor){
					isLoadConveyor=true;
				}
			}
			//si la carga no es conveyor pero el vehiculo si, tmb se apica
			if(isLoadConveyor && isConveyor){
				return true;
			} else if(!isLoadConveyor && isConveyor){
				return true;
			} else if(!isLoadConveyor && !isConveyor){
				return true;
			} else {
				return false;
			}
		}
	}
}