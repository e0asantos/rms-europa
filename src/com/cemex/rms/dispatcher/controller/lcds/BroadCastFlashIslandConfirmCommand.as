package com.cemex.rms.dispatcher.controller.lcds
{
	import com.cemex.rms.dispatcher.services.IGanttServiceFactory;
	import com.cemex.rms.dispatcher.services.flashislands.events.FlashIslandsDataEvent;
	
	import mx.controls.Alert;
	
	import org.robotlegs.mvcs.Command;
	
	/**
	 * ESta clase hace el broadcast a Todos los clientes de 
	 * 
	 */
	public class BroadCastFlashIslandConfirmCommand extends Command
	{
		public function BroadCastFlashIslandConfirmCommand()
		{
			super();
		}
		
		[Inject] public var service:IGanttServiceFactory;
		
		[Inject] public var event:FlashIslandsDataEvent;
		
		public override function  execute():void {
			/*Alert.show(
				"\n"+"event:"+event+
				"\n"+"event.data:"+event.data+
				"\n"+"event.data.datas:"+event.data.datas+
				"\n"+"service:"+service+
				"\n"+"service.getDispatchInternalProducer():"+service.getDispatchInternalProducer()
			);
			*/
			//service.getDispatchInternalProducer().sendObject(event.data.loads);
		}
	}
}