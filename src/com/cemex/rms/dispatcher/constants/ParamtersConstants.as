package com.cemex.rms.dispatcher.constants
{
	public class ParamtersConstants {
		
		public function ParamtersConstants() {
		}
		/** 
		 * Plant Visibility Area Limit (Mins)	
		 * It will define the number of minutes ahead of the current time that the schedule will be visible for the plants. 
		 */
		public static const PLANT_VIS_AREA_L:String = "PLANT_VIS_AREA_L"
		
		/**
		 * Maximum Display Delivered Orders (Min)	
		 * It will define the number of minutes a load is delivered, that will be visible 
		 * in the Loads per Vehicle view. After this time, it will disappear from the screen.

		 */	
		public static const MAX_DISP_DELIVER:String = "MAX_DISP_DELIVER"
		
		/**
		 * 
		 */
		public static const MAX_DISP_FUTURE:String = "MAX_DISP_FUTURE"
		
		
		/**
		 * This parameter sets the Collect ORder Checkbox  at the initialization,
		 * which controls the collect order filters
		 */
		public static const COLLECT_LOAD_DIS:String = "COLLECT_LOAD_DIS"
		
		/**
		 * This parameter is used to filter or not the Pump services 
		 */
		public static const DISP_PUMP_DFE:String = "DISP_PUMP_DFE"
		
		/**
		 * This parameter is Uses to display un the popup menu  
		 */
		public static const NUM_VEHI_DISP:String = "NUM_VEHI_DISP"
			
			
			
		/**
		 * Este parametro nos ayuda a ver que etiqueta se le va a poner al
		 * carro
		 * 
		 */
		public static const DISP_ID_OR_LICEN:String = "DISP_ID_OR_LICEN"
			
		public static const LATE_SERVICES:String="LATE_SERVICES";
		
		/**
		 * Este parametro controla el ancho de las cajitas
		 */
		public static const RMS_LOAD_SIZE:String="RMS_LOAD_SIZE";
		
		//public static const ASSI_VEHI_NOT_PL:String = "ASSI_VEHI_NOT_PL"
		//public static const CHANGE_ASSI_VEHI:String = "CHANGE_ASSI_VEHI"
		//public static const CHANGE_VOL_CASH:String = "CHANGE_VOL_CASH"
		
		//public static const CONSTRUC_PRO_DIS:String = "CONSTRUC_PRO_DIS"
		//public static const COST_REFRESH_TIM:String = "COST_REFRESH_TIM"
		//
		//public static const LIFE_SPAN:String = "LIFE_SPAN"
		//public static const MAX_TIME_ANTICIP:String = "MAX_TIME_ANTICIP"
		//public static const MAX_TIME_ASSI_VE:String = "MAX_TIME_ASSI_VE"
		//public static const NUM_DAY_NON_AP_T:String = "NUM_DAY_NON_AP_T"
		//public static const NUM_OPC_DISP_MEP:String = "NUM_OPC_DISP_MEP"
		//public static const NUM_VEHI_DISP:String = "NUM_VEHI_DISP"
		
		//public static const PUNCTUAL_MEASURE:String = "PUNCTUAL_MEASURE"
		//public static const REFERENCE_PUNTU:String = "REFERENCE_PUNTU"
		//public static const TIME_LOAD_DELAY:String = "TIME_LOAD_DELAY"
		//public static const TIME_REDIRECT:String = "TIME_REDIRECT"
		//public static const TIME_REFRESH_DFE:String = "TIME_REFRESH_DFE"
		//public static const TIME_SLOTS_DFE:String = "TIME_SLOTS_DFE"
		//public static const VEHIC_MAX_CAPACI:String = "VEHIC_MAX_CAPACI"
	}
}