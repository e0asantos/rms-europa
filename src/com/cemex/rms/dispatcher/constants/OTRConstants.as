package com.cemex.rms.dispatcher.constants
{
	public class OTRConstants {
		
		public function OTRConstants(){
		}
		
		public static const ORDER_LABEL:String="DISP_ORDER_LABEL";
		public static const ORDER_DETAIL:String="DISP_ORDER_DETAIL";
		public static const CHANGE_DRIVER:String = "DISP_CHG_VEH_DRIVER";
		public static const LOAD_LABEL:String="DISP_LOAD_LABEL2";
		public static const COLLECT_ORDERS:String="DISP_COLLECT_ORDERS";
		public static const ORDER_QUANTITY:String="DISP_ORDER_QUANTITY";
		public static const DISP_LOADS_WITHOUT_RECIPE:String="DISP_LOADS_WITHOUT_RECIPE";
		public static const SHOW_NO_RECIPE:String="DISP_SHOW_NO_RECIPE";
		public static const SHOW_INTERCHANGE_VEHICLE:String="DISP_INTERCHANGE_VEHICLE2";
		public static const PLANNING_SCREEN:String="DISP_PLANNING_SCREEN_HEADER";
		public static const SERVICE_AGENT_SCREEN:String="DISP_SERVICE_AGENT_SCREEN";
		public static const LOADS_PER_VEHICLE_HEADER:String="DISP_LOADS_PER_VEHICLE_HEADER2";
		public static const LOADS_PER_VEHICLE_HEADER_TOOLTIP:String="DISP_LOADS_PER_VEHICLE_HEADER_TOOLTIP2";
		public static const LOADS_PER_ORDER_HEADER:String="DISP_LOADS_PER_ORDER_HEADER2";
		public static const LOADS_PER_ORDER_HEADER_TOOLTIP:String="DISP_LOADS_PER_ORDER_HEADER_TOOLTIP2";
		public static const MULTI_DELIVERY:String="MULTI_DELIVERY";
		
		public static const ASSIGNED_LOADS_HEADER:String="DISP_ASSIGNED_LOADS_HEADER";
		public static const ASSIGNED_LOADS_HEADER_TOOLTIP:String="DISP_ASSIGNED_LOADS_HEADER_TOOLTIP";
		
		public static const ALL_LINK_TOOLTIP:String="DISP_ALL_LINK_TOOLTIP";
		public static const ASSIGN_CHANGE_VEHICLE:String="DISP_ASSIGN_CHANGE_VEHICLE";
		public static const ASSIGN_TO_PLANT:String="DISP_ASSIGN_TO_PLANT";
		public static const START_BATCHING:String="DISP_START_BATCHING";
		public static const RENEGOTIATE:String="DISP_RENEGOTIATE";
		public static const UNASSIGN_TO_PLANT:String="DISP_UNASSIGN_TO_PLANT2";
		public static const CHANGE_PLANT:String="DISP_CHANGE_PLANT";
		public static const UNASSIGN:String="DISP_UNASSIGN";
		public static const DISP_ERROR_DELIVERY_DATE:String="DISP_ERROR_DELIVERY_DATE";
		
		public static const BOM_REQUEST:String="DISP_BOM_REQUEST";
		public static const TELEPHONE_SHORT:String="DISP_TELEPHONE_SHORT";
		public static const CONTACT:String="DISP_CONTACT";
		public static const DRIVER_NAME_TOOLTIP:String ="DISP_DRIVER";
		
		public static const LOAD:String="DISP_LOAD2";
		public static const ORDER_NUMBER:String="DISP_ORDER_NUMBER2";
		
		public static const PUMPING_SERVICE:String="DISP_PUMPING_SERVICE";
		
		public static const ANTICIPATED_LOADS:String="DISP_ASSIGN_ANTICIPATED_LOAD";
		
		
		
		
		public static const ORDER_ON_HOLD:String="DISP_ORDER_ON_HOLD";
		public static const CHANGE_DELIVERY_TIME:String="DISP_CHANGE_DELIVERY_TIME";
		public static const CHANGE_FREQUENCY:String="DISP_CHANGE_FREQUENCY";
		
		public static const ASSIGN_VEHICLE:String="DISP_ASSIGN_VEHICLE";
		public static const CHANGE_VOLUME:String="DISP_CHANGE_VOLUME";
		
		public static const PRODUCTION_ORDER_TOOLTIP:String="DISP_PRODUCTION_ORDER_TOOLTIP";
		
		// Nuevos
		public static const DISPLAY_PO:String="DISP_DISPLAY_PO";
		public static const BATCH:String="DISP_BATCH";
		public static const MANUAL_BATCH:String="DISP_MANUAL_BATCH";
		public static const RE_PRINT:String="DISP_RE_PRINT";
		public static const DISPATCH:String="DISP_DISPATCH";
		
		public static const RE_USE:String="DISP_RE_USE";
		public static const CONTINUE_PRODUCTION:String="DISP_CONTINUE_PRODUCTION";
		public static const RESTART_LOAD:String="DISP_RESTART_LOAD";
		public static const QUALITY_ADJUSTMENT:String="DISP_QUALITY_ADJUSTMENT";
		public static const LAUNCH_PUMP_OPERATION:String="DISP_LAUNCH_PUMP";
		
		public static const ZOOM_IN_TOOLTIP:String = "DISP_ZOOM_IN_TOOLTIP";
		public static const ZOOM_OUT_TOOLTIP:String = "DISP_ZOOM_OUT_TOOLTIP";
		public static const SHOW_ALL_TOOLTIP:String = "DISP_SHOW_ALL_TOOLTIP";
		public static const CURRENT_TIME_TOOLTIP:String = "DISP_CURRENT_TIME_TOOLTIP";
		public static const REFRESH_TOOLTIP:String = "DISP_REFRESH_TOOLTIP";
		public static const DISP_CLEAN_LOG:String="DISP_CLEAN_BTN";
		public static const ASSIGN_LOAD:String="DISP_ASSIGN_LOAD";
		public static const NOT_ASSIGN_LOAD:String="DISP_NOT_ASSIGN_LOAD2";
		public static const AVAILABLE_LOAD:String="DISP_AVAILABLE_LOAD2";
		
	
		public static const CURRENT_COST_LABEL:String = "DISP_CURRENT_COST_LABEL";
		public static const CURRENT_COST_CURRENCY:String = "DISP_CURRENT_COST_CURRENCY";
		
		public static const OPTIMAL_COST_LABEL:String = "DISP_OPTIMAL_COST_LABEL";
		public static const OPTIMAL_COST_CURRENCY:String = "DISP_OPTIMAL_COST_CURRENCY";
		
		public static const WARNING:String = "DISP_WARNING";
		
		
		
		public static const SUPPORT_PLANT:String = "DISP_SUPPORT_PLANT";
		public static const RELEASE_VEHICLE:String = "DISP_RELEASE_VEHICLE";
		public static const VEHICLE_IN_PLANT:String = "DISP_VEHICLE_IN_PLANT";
		
		
		public static const RENEGOTIATION_LABEL:String = "DISP_RENEGOTIATION_LABEL";
		
		
		
		// Nuevas nuevas nuevas
		
		public static const PLANT_ASSIGNED_TOOLTIP:String 			= "DISP_PLANT_ASSIGNED_TOOLTIP";
		public static const PLANT_SCHEDULED_TOOLTIP:String 			= "DISP_PLANT_SCHEDULED_TOOLTIP";
		public static const PLANT_DELIVERED_TOOLTIP:String 			= "DISP_PLANT_DELIVERED_TOOLTIP";
		public static const PLANT_CONFIRMED_TOOLTIP:String 			= "DISP_PLANT_CONFIRMED_TOOLTIP";
		public static const PLANT_TO_BE_CONFIRMED_TOOLTIP:String 	= "DISP_TO_BE_CONFIRMED_TOOLTIP";
		public static const PLANT_CANCELED_TOOLTIP:String 			= "DISP_CANCELED_TOOLTIP";
		public static const PLANT_CENTRALIZED:String 				= "DISP_PLANT_CENTRALIZED";
		public static const PLANT_AUTONOMOUS:String 				= "DISP_PLANT_AUTONOMOUS";
		public static const PLANT_DISTRIBUTED:String 				= "DISP_PLANT_DISTRIBUTED";
		
		
		
		
		public static const VEHICLE_DELIVERED_TOOLTIP:String 		= "DISP_VEHICLE_DELIVERED_TOOLTIP";
		public static const VEHICLE_CONFIRMED_TOOLTIP:String 		= "DISP_VEHICLE_CONFIRMED_TOOLTIP";
		public static const VEHICLE_TO_BE_CONFIRMED_TOOLTIP:String 	= "DISP_VEHICLE_TO_BE_CONFIRMED_TOOLTIP";
		
		// order otr labels
		public static const DISP_CHANGE_GLOBAL_VOLUME:String 		= "DISP_CHANGE_GLOBAL_VOLUME";
		public static const DISP_CHANGE_GLOBAL_DELIVERY:String 		= "DISP_CHANGE_GLOBAL_DELIVERY";
		public static const DISP_CHANGE_GLOBAL_FREQUENCY:String 	= "DISP_CHANGE_GLOBAL_FREQUENCY";
		public static const DISP_CHANGE_UNLOADING_TIME:String 		= "DISP_CHANGE_UNLOADING_TIME";
		public static const DISP_CHANGE_DELIVERY_PLANT:String 		= "DISP_CHANGE_DELIVERY_PLANT";
		public static const DISP_CHANGE_TRAVEL_TIME:String 			= "DISP_CHANGE_TRAVEL_TIME";
		public static const DISP_CHANGE_STATUS_ORDER:String 		= "DISP_CHANGE_STATUS_ORDER";
		public static const DISP_CHANGE_ADDITIONAL_SERVICE:String 	= "DISP_CHANGE_ADDITIONAL_SERVICE";
		public static const DISP_OPEN_OTS:String 					= "DISP_OPEN_OTS";
		public static const DISP_COPY_ORDER:String 					= "DISP_COPY_ORDER";
		
		//single load otr labels france
		public static const DISP_LOAD_MODIFY_LOAD:String			="DISP_LOAD_MODIFY_LOAD";
		public static const DISP_LOAD_CHANGE_VOLUME:String			="DISP_LOAD_CHANGE_VOLUME";
		public static const DISP_LOAD_DELIVERY_HOUR:String			="DISP_LOAD_DELIVERY_HOUR";
		public static const DISP_LOAD_DELIVERY_PLANT:String			="DISP_LOAD_DELIVERY_PLANT";
		public static const DISP_LOAD_CHANGE_TRAVEL_TIME:String		="DISP_LOAD_CHANGE_TRAVEL_TIME";
		public static const DISP_LOAD_CHANGE_LOAD_STATUS:String		="DISP_LOAD_CHANGE_LOAD_STATUS";
		public static const DISP_LOAD_LAUNCH_PRODUCTION:String		="DISP_LOAD_LAUNCH_PRODUCTION";
		public static const DISP_LOAD_CHANGE_FREQUENCY:String		="DISP_LOAD_CHANGE_FREQUENCY";
		public static const DISP_DISPLAY_ORDER_OT:String			="DISP_DISPLAY_ORDER_OT";
		public static const DISP_CANCEL_ORDER:String				="DISP_CANCEL_ORDER";
		public static const DISP_CANCEL_LOAD:String				="DISP_CANCEL_LOAD";
		
		public static const DISP_CHANGE_ITEM:String					="DISP_CHANGE_ITEM";
		
		public static const DISP_TRUCK_DATABASE:String				="DISP_TRUCK_DATABASE";
		public static const DISP_TRUCK_REPORT:String				="DISP_TRUCK_REPORT";
		//
		public static const LOADS_WITHOUT_RECIPE:String = "DISP_LOADS_WITHOUT_RECIPE";
		public static const LOADS_PER_HOUR:String= "DISP_LOADS_PER_HOUR";
		public static const COMMENT_DISP:String = "DISP_COMMENT_DISP";
		
		public static const PLANT_READY:String = "DISP_READY";
		public static const PLANT_CLOSE:String = "DISP_CLOSE";
		//public static const ORDER_DETAIL:String="DISP_ORDER_DETAIL";
		public static const PLANT_STOPPAGE:String = "DISP_STOPPAGE";
		public static const DISP_PS_VOL_EXC:String="DISP_PS_VOL_EXC";
		public static const DISP_PS_LOAD_CNV:String="DISP_PS_LOAD_CNV";
		public static const DISP_PS_TRUCK_NO_AV:String="DISP_PS_TRUCK_NO_AV";
		public static const DISP_PS_VEHI_ERR:String="DISP_PS_VEHI_ERR";
		public static const FICTIVE_MIXER:String="DISP_FICT_MIX";
		public static const FICTIVE_CONVEYOR:String="DISP_FICT_CONV";
		
		
		public static const DISP_PS_DRAG_VEH:String="DISP_PS_DRAG_VEH";
		public static const DISP_PS_LOAD_ASS_ERR:String="DISP_PS_LOAD_ASS_ERR";
		public static const DISP_PS_CLEAR_PLAN:String="DISP_PS_CLEAR_PLAN";
		public static const DISP_PS_SAVE_PLAN:String="DISP_PS_SAVE_PLAN";
		public static const DISP_PS_SEL_PLANT:String="DISP_PS_SEL_PLANT";
		public static const DISP_PS_SEL_CRIT:String="DISP_PS_SEL_CRIT";
		public static const DISP_PS_SELECT:String="DISP_PS_SELECT";
		public static const DISP_CLEAN:String="DISP_CLEAN";
		public static const DISP_SEARCH:String="DISP_SEARCH";
		public static const DISP_PS_PLANNING_PLANT:String="DISP_PS_PLANNING_PLANT";
		public static const DISP_TT_AUT_DISP:String="DISP_TT_AUT_DISP";
		public static const DISP_TT_SAVE_CHANGES:String="DISP_TT_SAVE_CHANGES";
		public static const DISP_LAUNCHTICKET:String= "DISP_LAUNCHTICKET";
		
		public static const DISP_STONEWASH:String= "DISP_BTN_STONEWASH";
		public static const MENU_CHANGE_DRIVER:String	="VEHICLE_CHANGE_DRIVER";
		
		
	}
}