package com.cemex.rms.dispatcher.constants
{
	public class SecurityConstants
	{
		public function SecurityConstants()
		{
		} 
		
		//<DM_BATC>_<FXD_LPO>_ASSIGN_TO_PLANT_*01*
		public static const LOAD_TOOLTIP:String 				="TIP_LOAD";
		public static const BOM_REQUEST:String 				="BTN_BOM_REQUEST";
		public static const LOAD_TOOLTIP_PRODUCTION:String 		="TIP_LOAD_PRODUCTION";
		
		public static const DRAG_UNASSIGN_VEHICLE:String		="DRG_UNASSIGN_VEHICLE";
		public static const DRAG_ASSIGN_VEHICLE:String			="DRG_ASSIGN_VEHICLE";
		public static const DRAG_CHANGE_DELIVERY_TIME:String	="DRG_CHANGE_DELIVERY_TIME";
		public static const DRAG_CHANGE_PLANT:String	="DRG_CHANGE_PLANT";
		
		
		public static const MENU_ASSIGN_VEHICLE:String		="BTN_ASSIGN_VEHICLE";
		public static const MENU_ASSIGN_ONE_VEHICLE:String	="BTN_ASSIGN_ONE_VEHICLE";
		public static const MENU_ASSIGN_PLANT:String			="BTN_ASSIGN_PLANT";
		public static const BTN_LAUNCH_PUMP:String			="BTN_LAUNCH_PUMP";
		public static const MENU_CHANGE_PLANT:String	="BTN_CHANGE_PLANT";
		public static const MENU_CHANGE_VOLUME:String		="BTN_CHANGE_VOLUME";
		public static const MENU_ORDER_ON_HOLD:String		="BTN_ORDER_ON_HOLD";
		public static const MENU_CHANGE_DELIVERY_TIME:String	="BTN_CHANGE_DELIVERY_TIME";
		public static const MENU_DISPLAY_PO:String			="BTN_DISPLAY_PO";
		public static const MENU_DISPATCH:String				="BTN_DISPATCH";
		public static const MENU_BATCH:String				="BTN_BATCH";
		public static const MENU_MANUAL_BATCH:String			="BTN_MANUAL_BATCH";
		public static const MENU_RE_PRINT:String				="BTN_RE_PRINT";
		public static const MENU_RE_USE:String				="BTN_RE_USE";
		public static const MENU_CONTINUE_PRODUCTION:String	="BTN_CONTINUE_PRODUCTION";
		public static const MENU_RESTART_LOAD:String			="BTN_RESTART_LOAD";
		public static const MENU_QUALITY_ADJUSTMENT:String	="BTN_QUALITY_ADJUSTMENT";
		public static const MENU_SHOW_DATA:String			="BTN_SHOW_DATA";
		public static const MENU_BATCH_PER_WORK_CENTER:String="MENU_BATCH_PER_WORK_CENTER";
		public static const MENU_ANTICIPATED_LOADS:String="BTN_ANTICIPATED_LOADS";
		public static const MENU_UNASSIGN:String="BTN_UNASSIGN";
		public static const MENU_INTERCHANGE_VEHICLE:String="BTN_INTERCHANGE_VEHICLE";
		public static const MENU_MULTI_DELIVERY:String		="BTN_MULTI_DELIVERY";
		
		
		//public static const REUSE_CONCRETE_REQUEST:String	="REUSE_CONCRETE_REQUEST";
		public static const MENU_CHANGE_FREQUENCY:String	="BTN_CHANGE_FREQUENCY";
		
		// ASL
		public static const MENU_START_BATCHING:String	="BTN_START_BATCHING";
		public static const MENU_RENEGOTIATE_LOAD:String	="BTN_RENEGOTIATE_LOAD";
		public static const MENU_UNASSIGN_PLANT:String	="BTN_UNASSIGN_PLANT";
		
		
		//Vehicle
		public static const MENU_SUPPORT_PLANT:String	="VEHICLE_SUPPORT_PLANT";
		public static const MENU_RELEASE_VEHICLE:String	="VEHICLE_RELEASE_VEHICLE";
		public static const MENU_VEHICLE_IN_PLANT:String="VEHICLE_VEHICLE_IN_PLANT";
		
		//france menus
		public static const DISP_LOAD_MODIFY_LOAD:String="BTN_DISP_LOAD_MODIFY_LOAD";
		public static const DISP_LOAD_CHANGE_VOLUME:String="BTN_DISP_LOAD_CHANGE_VOLUME";
		public static const DISP_LOAD_DELIVERY_HOUR:String="BTN_DISP_LOAD_DELIVERY_HOUR";
		public static const DISP_LOAD_DELIVERY_PLANT:String="BTN_DISP_LOAD_DELIVERY_PLANT";
		public static const DISP_LOAD_CHANGE_TRAVEL_TIME:String="BTN_DISP_LOAD_CHANGE_TRAVEL_TIME";
		public static const DISP_LOAD_CHANGE_LOAD_STATUS:String="BTN_DISP_LOAD_CHANGE_LOAD_STATUS";
		public static const DISP_LOAD_LAUNCH_PRODUCTION:String="BTN_DISP_LOAD_LAUNCH_PRODUCTION";
		public static const DISP_LOAD_CHANGE_FREQUENCY:String="BTN_DISP_LOAD_CHANGE_FREQUENCY";
		public static const MENU_COMMENT_DISP:String="BTN_COMMENT_DISP";
		public static const DRAG_PLANNING_ASSIGNATION:String="DRG_PLANNING_ASSIGNATION"; 
		public static const SHOW_CHANGE_ITEM:String="BTN_CHANGE_ITEM"; 
		
		//france menus for order
		public static const DISP_CHANGE_GLOBAL_VOLUME:String="BTN_DISP_CHANGE_GLOBAL_VOLUME";
		public static const DISP_CHANGE_GLOBAL_DELIVERY:String="BTN_DISP_CHANGE_GLOBAL_DELIVERY";
		public static const DISP_CHANGE_GLOBAL_FREQUENCY:String="BTN_DISP_CHANGE_GLOBAL_FREQUENCY";
		public static const DISP_CHANGE_UNLOADING_TIME:String="BTN_DISP_CHANGE_UNLOADING_TIME";
		public static const DISP_CHANGE_DELIVERY_PLANT:String="BTN_DISP_CHANGE_DELIVERY_PLANT"
		public static const DISP_CHANGE_TRAVEL_TIME:String="BTN_DISP_CHANGE_TRAVEL_TIME";
		public static const DISP_CHANGE_STATUS_ORDER:String="BTN_DISP_CHANGE_STATUS_ORDER";
		public static const DISP_CHANGE_ADDITIONAL_SERVICE:String="DISP_CHANGE_ADDITIONAL_SERVICE";
		public static const DISP_OPEN_OTS:String="BTN_DISP_OPEN_OTS";
		public static const DISP_COPY_ORDER:String="BTN_DISP_COPY_ORDER";
		public static const DISP_LAUNCH_TICKET:String="BTN_LAUNCH_TICKET";
		public static const DISP_STONE_WASH:String="BTN_STONEWASH";
		public static const MENU_CHANGE_DRIVER:String	="VEHICLE_CHANGE_DRIVER";
		
	}
}