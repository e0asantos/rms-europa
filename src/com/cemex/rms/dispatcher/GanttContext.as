package com.cemex.rms.dispatcher
{
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.logging.impl.EventLogger;
	import com.cemex.rms.common.services.IServiceFactory;
	import com.cemex.rms.common.services.ServiceFactory;
	import com.cemex.rms.common.services.events.ServiceManagerEvent;
	import com.cemex.rms.dispatcher.controller.GanttTaskMovementCommand;
	import com.cemex.rms.dispatcher.controller.ServicesReadyCommand;
	import com.cemex.rms.dispatcher.controller.StartupCommand;
	import com.cemex.rms.dispatcher.controller.lcds.BroadCastFlashIslandConfirmCommand;
	import com.cemex.rms.dispatcher.controller.lcds.LCDSReconnectCommand;
	import com.cemex.rms.dispatcher.events.GanttTaskMovementEvent;
	import com.cemex.rms.dispatcher.events.lcds.LCDSReconnectEvent;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.services.GanttServiceFactory;
	import com.cemex.rms.dispatcher.services.GanttServicesModel;
	import com.cemex.rms.dispatcher.services.IGanttServiceFactory;
	import com.cemex.rms.dispatcher.services.flashislands.events.FlashIslandsDataEvent;
	import com.cemex.rms.dispatcher.views.DispatcherView;
	import com.cemex.rms.dispatcher.views.GanttDispatcherPlanningView;
	import com.cemex.rms.dispatcher.views.GanttDispatcherView;
	import com.cemex.rms.dispatcher.views.assignedLoads.AssignedLoadsResourceRenderer;
	import com.cemex.rms.dispatcher.views.assignedLoads.AssignedLoadsTaskRenderer;
	import com.cemex.rms.dispatcher.views.assignedLoads.headers.AssignedLoadsHeader;
	import com.cemex.rms.dispatcher.views.assignedLoads.headers.AssignedLoadsHeaderMediator;
	import com.cemex.rms.dispatcher.views.assignedLoads.mediators.AssignedLoadsResourceRendererMediator;
	import com.cemex.rms.dispatcher.views.assignedLoads.mediators.AssignedLoadsTaskRendererMediator;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.LoadPerOrderOrderResourceRenderer;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.LoadPerOrderPlantOrderResourceRenderer;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.LoadPerOrderTaskRenderer;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.headers.LoadsPerOrderOrderHeader;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.headers.LoadsPerOrderOrderHeaderMediator;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.headers.LoadsPerOrderPlantOrderHeader;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.headers.LoadsPerOrderPlantOrderHeaderMediator;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.mediators.LoadPerOrderOrderResourceRendererMediator;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.mediators.LoadPerOrderPlantOrderResourceRendererMediator;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.mediators.LoadPerOrderTaskRendererMediator;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.LoadsPerVehicleResourceRenderer;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.LoadsPerVehicleTaskRenderer;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.headers.LoadsPerVehicleHeader;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.headers.LoadsPerVehicleHeaderMediator;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.mediators.LoadPerVehicleResourceRendererMediator;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.mediators.LoadPerVehicleTaskRendererMediator;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	import com.cemex.rms.dispatcher.views.mediators.GanttDispatcherViewMediator;
	import com.cemex.rms.dispatcher.views.mediators.GanttDispatcherViewPlanningMediator;
	import com.cemex.rms.dispatcher.views.planningScreen.headers.PlanningScreenHeaderView;
	import com.cemex.rms.dispatcher.views.planningScreen.headers.PlanningScreenHeaderViewMediator;
	import com.cemex.rms.dispatcher.views.planningScreen.mediators.PlanningScreenTaskRendererMediator;
	import com.cemex.rms.dispatcher.views.planningScreen.mediators.PlanningScreenVehicleTaskRendererMediator;
	import com.cemex.rms.dispatcher.views.planningScreen.renderers.PlanningScreenTaskRenderer;
	import com.cemex.rms.dispatcher.views.planningScreen.renderers.PlanningScreenVehicleTaskRenderer;
	import com.cemex.rms.dispatcher.views.serviceAgentScreen.headers.ServiceAgentScreenHeaderView;
	import com.cemex.rms.dispatcher.views.serviceAgentScreen.headers.ServiceAgentScreenHeaderViewMediator;
	import com.cemex.rms.dispatcher.views.serviceAgentScreen.mediators.ServiceAgentScreenTreeResourceRendererMediator;
	import com.cemex.rms.dispatcher.views.serviceAgentScreen.mediators.ServiceAgentTaskRendererMediator;
	import com.cemex.rms.dispatcher.views.serviceAgentScreen.renderers.ServiceAgentScreenTaskRenderer;
	import com.cemex.rms.dispatcher.views.serviceAgentScreen.renderers.ServiceAgentScreenTreeResourceRenderer;
	
	import org.robotlegs.base.ContextEvent;
	import org.robotlegs.mvcs.Context;
	
	public class GanttContext extends Context
	{
		
	import flash.display.DisplayObjectContainer;
		
		public function GanttContext(contextView:DisplayObjectContainer=null, autoStartup:Boolean=true)
		{
			super(contextView, autoStartup);			
		}
		
		
		
		override public function startup():void
		{
			
			LoggerFactory.defaultLogger = LoggerFactory.LOGGER_TYPE_EVENT;
			EventLogger.staticDispatcher = eventDispatcher;
			// SE inicializa el factory de servicios
			var factory:ServiceFactory = new ServiceFactory();
			
			
			// Se le settea el dispatcher del contexto a los servicios
			// para poder utilizar los listeners de los servicios con la arquitectura de 
			// Robotlegs
			factory.dispatcher = eventDispatcher;
			factory.model = new GanttServicesModel();
			// Se settea el Factory de Servicios
			injector.mapValue(IServiceFactory, factory);
			
			
			var ganttService:GanttServiceFactory = new GanttServiceFactory();
			//injector.injectInto(ganttService);
			ganttService.serviceFactory = factory;
			injector.mapValue(IGanttServiceFactory,ganttService);
			
			GanttServiceReference.services = ganttService;

			
			// Se settea el mapa de Vistas mediador
			mediatorMap.mapView(LoadPerOrderOrderResourceRenderer, LoadPerOrderOrderResourceRendererMediator);
			mediatorMap.mapView(LoadPerOrderPlantOrderResourceRenderer, LoadPerOrderPlantOrderResourceRendererMediator);
			mediatorMap.mapView(LoadPerOrderTaskRenderer, LoadPerOrderTaskRendererMediator);
			
			
			
			mediatorMap.mapView(LoadsPerVehicleResourceRenderer, LoadPerVehicleResourceRendererMediator);
			mediatorMap.mapView(LoadsPerVehicleTaskRenderer, LoadPerVehicleTaskRendererMediator);
			
			mediatorMap.mapView(AssignedLoadsResourceRenderer, AssignedLoadsResourceRendererMediator);
			mediatorMap.mapView(AssignedLoadsTaskRenderer, AssignedLoadsTaskRendererMediator);
			
			
			mediatorMap.mapView(LoadsPerOrderOrderHeader, LoadsPerOrderOrderHeaderMediator);
			mediatorMap.mapView(LoadsPerOrderPlantOrderHeader, LoadsPerOrderPlantOrderHeaderMediator);
			
			mediatorMap.mapView(AssignedLoadsHeader, AssignedLoadsHeaderMediator);
			mediatorMap.mapView(LoadsPerVehicleHeader, LoadsPerVehicleHeaderMediator);
			
			mediatorMap.mapView(ServiceAgentScreenTaskRenderer, ServiceAgentTaskRendererMediator);
			mediatorMap.mapView(ServiceAgentScreenHeaderView, ServiceAgentScreenHeaderViewMediator);
			
			mediatorMap.mapView(PlanningScreenTaskRenderer, PlanningScreenTaskRendererMediator);
			mediatorMap.mapView(PlanningScreenVehicleTaskRenderer, PlanningScreenVehicleTaskRendererMediator);
			
			mediatorMap.mapView(PlanningScreenHeaderView, PlanningScreenHeaderViewMediator);
			
			
			
			
			
			
			//mediatorMap.mapView(TaskBranchRenderer, TaskBranchRendererMediator);
			mediatorMap.mapView(GanttDispatcherView, GanttDispatcherViewMediator);
			/*mediatorMap.mapView(GanttDispatcherPlanningView, GanttDispatcherViewPlanningMediator);*/
			mediatorMap.mapView(DispatcherView, DispatcherViewMediator);
			// se crea el mediador de la vista principal el FlashIsland Settea datos
			// en cuanto se manda a cargar el servicio de FlashIslands
			//mediatorMap.createMediator((contextView as GanttModule).view );
			
			
			// Se mappean los eventos con comandos
			commandMap.mapEvent(ContextEvent.STARTUP, StartupCommand, ContextEvent, true);
			commandMap.mapEvent(ServiceManagerEvent.ALL_SERVICES_READY, ServicesReadyCommand);
			
			/*
			commandMap.mapEvent(DataReloadRequestEvent.DATA_RELOAD_REQUEST, DataReloadCommand);
			commandMap.mapEvent(OrderOperationRequestEvent.ORDER_ON_HOLD_REQUEST, OrderOnHoldCommand);
			commandMap.mapEvent(OrderOperationRequestEvent.CHANGE_PLANT_REQUEST, ChangePlantCommand);
			commandMap.mapEvent(OrderOperationRequestEvent.CHANGE_DELIVERY_TIME_REQUEST, ChangeDeliveryTimeCommand);
			commandMap.mapEvent(OrderOperationRequestEvent.CHANGE_FREQUENCY_REQUEST, ChangeFrequencyCommand);
			
			
			commandMap.mapEvent(OrderOperationRequestEvent.REUSE_CONCRETE_REQUEST, ReuseConcreteCommand);
			commandMap.mapEvent(OrderOperationRequestEvent.REDIRECT_LOAD_REQUEST, RedirectLoadCommand);
			commandMap.mapEvent(OrderOperationRequestEvent.ASSIGN_PLANT_REQUEST, AssignPlantCommand);
			commandMap.mapEvent(OrderOperationRequestEvent.CHANGE_VOLUME_REQUEST, ChangeVolumeCommand);
			commandMap.mapEvent(OrderOperationRequestEvent.ASSIGN_VEHICLE_REQUEST, AssignVehicleCommand);
			
			commandMap.mapEvent(OrderOperationRequestEvent.UNASSIGN_PLANT_REQUEST, UnassignPlantCommand);
			commandMap.mapEvent(OrderOperationRequestEvent.RENEGOTIATE_LOAD_REQUEST, RenegotiateLoadCommand);
			
			
			commandMap.mapEvent(OrderOperationRequestEvent.START_BATCHING_REQUEST, StartBatchingCommand);
			commandMap.mapEvent(OrderOperationRequestEvent.DISPLAY_PO_REQUEST, DisplayPOCommand);
			commandMap.mapEvent(OrderOperationRequestEvent.BATCH_REQUEST, BatchCommand);
			commandMap.mapEvent(OrderOperationRequestEvent.MANUAL_BATCH_REQUEST, ManualBatchCommand);
			commandMap.mapEvent(OrderOperationRequestEvent.RE_PRINT_REQUEST, RePrintCommand);
			commandMap.mapEvent(OrderOperationRequestEvent.RE_USE_REQUEST, ReUseCommand);
			commandMap.mapEvent(OrderOperationRequestEvent.CONTINUE_PRODUCTION_REQUEST, ContinueProductionCommand);
			commandMap.mapEvent(OrderOperationRequestEvent.RESTART_LOAD_REQUEST, RestartLoadCommand);
			commandMap.mapEvent(OrderOperationRequestEvent.QUALITY_ADJUSTMENT_REQUEST, QualityAdjustmentCommand);
			commandMap.mapEvent(OrderOperationRequestEvent.DISPATCH_REQUEST, DispatchCommand);
			
			*/
			//ColorHelper.getColorMap();
			commandMap.mapEvent(GanttTaskMovementEvent.GANTT_TASK_MOVE,GanttTaskMovementCommand);
			commandMap.mapEvent(LCDSReconnectEvent.LCDS_RECONNECT_REQUEST,LCDSReconnectCommand);
			
			commandMap.mapEvent(FlashIslandsDataEvent.FLASHISLAND_DATA_CONFIRM,BroadCastFlashIslandConfirmCommand);
			
			// Se mandan a inicializar las cosas
			dispatchEvent( new ContextEvent( ContextEvent.STARTUP ) );
			
			
			
		}
	}
}