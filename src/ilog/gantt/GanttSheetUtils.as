package ilog.gantt
{
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;

	public class GanttSheetUtils extends GanttSheet
	{
		private var sheet:GanttSheet;
		public function GanttSheetUtils(sheet:GanttSheet)
		{
			this.sheet = sheet;
		}
		public function releaseDragDrop():void{
			var release:KeyboardEvent  =  new KeyboardEvent(KeyboardEvent.KEY_DOWN);
			release.keyCode == Keyboard.ESCAPE;
			//sheet.keyDownHandler(release);
			sheet.enabled = true;
			sheet._editKind ="X";
			sheet.dispatchEvent(release);
			
		}
	}
}